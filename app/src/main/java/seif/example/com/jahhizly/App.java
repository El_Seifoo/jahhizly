package seif.example.com.jahhizly;

import android.app.Application;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;

import net.gotev.uploadservice.UploadService;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        printHash();
        UploadService.NAMESPACE = "seif.example.com.jahhizly";

    }

    public void printHash() {

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "seif.example.com.jahhizly",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash: ", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }

        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

    }
}
