package seif.example.com.jahhizly.BookTable;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.shawnlin.numberpicker.NumberPicker;
import com.tuyenmonkey.mkloader.MKLoader;

import org.json.JSONException;

import java.sql.Time;
import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import me.anwarshahriar.calligrapher.Calligrapher;

import seif.example.com.jahhizly.Branches.BranchesActivity;
import seif.example.com.jahhizly.Branches.MakeOrder.Branches;
import seif.example.com.jahhizly.Other.CircleTransform;
import seif.example.com.jahhizly.R;
import seif.example.com.jahhizly.Utils.MySingleton;
import seif.example.com.jahhizly.Utils.URLs;

public class BookTableActivity extends AppCompatActivity implements BookTableMVP.view {
    private ArrayList<String> guests, date, time;
    private String selectedGuests, selectedDate, selectedTime;
    int orderId = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_table);
        Calligrapher calligrapher = new Calligrapher(this);
        if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            calligrapher.setFont(this, "fonts/Arabic/arabic_font.ttf", true);
        } else {
            calligrapher.setFont(this, "fonts/English/Roboto_Regular.ttf", true);
        }
        getSupportActionBar().setTitle(getString(R.string.book_table_activity));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (getIntent().hasExtra("OrderIDFromCart")) {
            orderId = getIntent().getExtras().getInt("OrderIDFromCart");
        }
        final BookTableMVP.presenter presenter = new BookTablePresenter(this, new BookTableModel());
        ImageView branchLogo;
        TextView branchName;
        branchName = (TextView) findViewById(R.id.book_table_branch_name);
        branchName.setText(MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? convertDigits(((Branches) getIntent().getExtras().getSerializable("BookTableBranch")).getName()) : ((Branches) getIntent().getExtras().getSerializable("BookTableBranch")).getName());
        branchLogo = (ImageView) findViewById(R.id.book_table_brand_logo);
        Glide.with(this).load(URLs.ROOT_URL + ((Branches) getIntent().getExtras().getSerializable("BookTableBranch")).getBrandPhoto())
                .thumbnail(0.5f)
                .error(R.mipmap.restaurant_icon_menu)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .bitmapTransform(new CircleTransform(this))
                .into(branchLogo);


        // manage number of guests numberPicker
        guests = new ArrayList<>();
        Log.e("guest", ((Branches) getIntent().getExtras().getSerializable("BookTableBranch")).getMaxGuests() + "");
        for (int i = 0; i < ((Branches) getIntent().getExtras().getSerializable("BookTableBranch")).getMaxGuests(); i++) {
            if ((i + 1) == 1) {
                guests.add(getString(R.string.guests_one));
            } else if ((i + 1) == 2) {
                guests.add(getString(R.string.guests_two));
            } else if (((i + 1) > 2) && (i + 1) <= 10) {
                guests.add((i + 1) + " " + getString(R.string.guests_three_ten));
            } else {
                guests.add((i + 1) + " " + getString(R.string.guests_11_100));
            }
        }
        createPicker(guests, 0);


        // manage Date numberPicker
        date = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("EEE, d MMM");
        date.add(dateFormat.format(calendar.getTime()));
        for (int i = 1; i < 150; i++) {
            calendar.add(Calendar.DAY_OF_YEAR, 1);
            date.add(dateFormat.format(calendar.getTime()));
        }
        createPicker(date, 1);

        // manage Open-Close time numberPicker
        time = new ArrayList<>();
        Calendar calendarStart = Calendar.getInstance();
        Calendar calendarEnd = Calendar.getInstance();
        String endTime = "";
        String openTime = ((Branches) getIntent().getExtras().getSerializable("BookTableBranch")).getOpenHour();
        String closeTime = ((Branches) getIntent().getExtras().getSerializable("BookTableBranch")).getCloseHour();

        calendarStart.set(1, 1, 1, Integer.valueOf(openTime.split(":")[0]), Integer.valueOf(openTime.split(":")[1]));
        calendarEnd.set(1, 1, 1, Integer.valueOf(closeTime.split(":")[0]), Integer.valueOf(closeTime.split(":")[1]));

        while (!endTime.equals(new SimpleDateFormat("h:mm a").format(calendarEnd.getTime()))) {
            calendarStart.add(Calendar.MINUTE, 15);
            String generatedTime = new SimpleDateFormat("h:mm a").format(calendarStart.getTime());
            time.add(generatedTime);
            endTime = generatedTime;
        }
        createPicker(time, 2);


        ((RadioButton) ((RadioGroup) findViewById(R.id.tables_place_radio_group)).getChildAt(0)).setChecked(true);
        ((RadioButton) ((RadioGroup) findViewById(R.id.family_not_radio_group)).getChildAt(0)).setChecked(true);
        ((RadioGroup) findViewById(R.id.family_not_radio_group)).getChildAt(0).setSelected(true);
        ((Button) findViewById(R.id.btn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedGuests == null) {
                    if (guests.get(0).equals(getString(R.string.guests_one))) {
                        selectedGuests = "1";
                    } else if (guests.get(0).equals(getString(R.string.guests_one))) {
                        selectedGuests = "2";
                    } else {
                        selectedGuests = convertNumbers(guests.get(0).split(" ")[0], 0);
                    }
                }
                if (selectedDate == null) {
                    selectedDate = convertNumbers(returnDate(date.get(0)), 1);
                }
                if (selectedTime == null) {
                    selectedTime = convertNumbers(convertTo24H(time.get(0)), 2);
                }
                Toast.makeText(BookTableActivity.this, selectedGuests + "/" + selectedDate + "/" + selectedTime +
                        "\n" + getSelectedOption(((RadioGroup) findViewById(R.id.tables_place_radio_group))) +
                        "\n" + getSelectedOption(((RadioGroup) findViewById(R.id.family_not_radio_group))), Toast.LENGTH_SHORT).show();
//                Context context, final VolleyCallback callback, int branchId, String tablePlace, String guestType, String note, int chairsCount, String time, int orderId
                try {
                    presenter.onConfirmBtnClicked(BookTableActivity.this, ((Branches) getIntent().getExtras().getSerializable("BookTableBranch")).getId(),
                            getSelectedOption(((RadioGroup) findViewById(R.id.tables_place_radio_group))).equals(getString(R.string.indoor)) ? "0" : "1",
                            getSelectedOption(((RadioGroup) findViewById(R.id.family_not_radio_group))).equals(getString(R.string.no_family)) ? "0" : "1",
                            ((EditText) findViewById(R.id.add_note_edit_text)).getText().toString().trim(), selectedGuests, selectedDate + " " + selectedTime, orderId);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public String getSelectedOption(RadioGroup radioGroup) {
        int selected = radioGroup.getCheckedRadioButtonId();
        RadioButton selectedRadioButton = (RadioButton) findViewById(selected);
        if (selectedRadioButton == null) {
            return null;
        }
        return String.valueOf(selectedRadioButton.getText());
    }

    private void createPicker(final ArrayList<String> data, final int which) {
        NumberPicker numberPicker;
        if (which == 0) {
            //persons
            numberPicker = (NumberPicker) findViewById(R.id.guest_picker);
        } else if (which == 1) {
            //date
            numberPicker = (NumberPicker) findViewById(R.id.date_picker);
        } else {
            //time
            numberPicker = (NumberPicker) findViewById(R.id.time_picker);
        }
        numberPicker.setMaxValue(data.size());
        numberPicker.setMinValue(1);
        numberPicker.setDisplayedValues(data.toArray(new String[data.size()]));
        numberPicker.setWrapSelectorWheel(false);
        // Set divider color
        numberPicker.setDividerColor(ContextCompat.getColor(this, R.color.red_3));
        numberPicker.setDividerColorResource(R.color.red_3);
        // Set selected text color
        numberPicker.setSelectedTextColor(ContextCompat.getColor(this, R.color.red_3));
        numberPicker.setSelectedTextColorResource(R.color.red_3);
        // Set text color
        numberPicker.setTextColor(ContextCompat.getColor(this, R.color.grey_5));
        numberPicker.setTextColorResource(R.color.grey_5);

        // OnValueChangeListener
        numberPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                if (which == 0) {
                    //persons
                    if (guests.get(newVal - 1).equals(getString(R.string.guests_one))) {
                        selectedGuests = "1";
                    } else if (guests.get(newVal - 1).equals(getString(R.string.guests_two))) {
                        selectedGuests = "2";
                    } else {
                        selectedGuests = convertNumbers(guests.get(newVal - 1).split(" ")[0], 0);
                    }
                    Log.e("guests", selectedGuests);
                } else if (which == 1) {
                    // date
                    // Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec
                    selectedDate = convertNumbers(returnDate(data.get(newVal - 1)), 1);
                    Log.e("date", selectedDate);
                } else if (which == 2) {
                    //time
                    selectedTime = convertNumbers(convertTo24H(time.get(newVal - 1)), 2);
                    Log.e("time", selectedTime);

                }
            }
        });
    }

    private String returnDate(String date) {
        String rslt = "";
        Calendar calendarMonth = Calendar.getInstance();
        int currentMonth = calendarMonth.get(Calendar.MONTH) + 1;
        Calendar calendarYear = Calendar.getInstance();
        int year = calendarYear.get(Calendar.YEAR);
        String month = date.split(" ")[2];
        int selectedMonthInt;
        if (month.equals(getResources().getStringArray(R.array.months_of_year)[0])) {
            selectedMonthInt = 1;
            rslt = "-" + 1 + "-" + date.split(" ")[1];
        } else if (month.equals(getResources().getStringArray(R.array.months_of_year)[1])) {
            selectedMonthInt = 2;
            rslt = "-" + 2 + "-" + date.split(" ")[1];
        } else if (month.equals(getResources().getStringArray(R.array.months_of_year)[2])) {
            selectedMonthInt = 3;
            rslt = "-" + 3 + "-" + date.split(" ")[1];
        } else if (month.equals(getResources().getStringArray(R.array.months_of_year)[3])) {
            selectedMonthInt = 4;
            rslt = "-" + 4 + "-" + date.split(" ")[1];
        } else if (month.equals(getResources().getStringArray(R.array.months_of_year)[4])) {
            selectedMonthInt = 5;
            rslt = "-" + 5 + "-" + date.split(" ")[1];
        } else if (month.equals(getResources().getStringArray(R.array.months_of_year)[5])) {
            selectedMonthInt = 6;
            rslt = "-" + 6 + "-" + date.split(" ")[1];
        } else if (month.equals(getResources().getStringArray(R.array.months_of_year)[6])) {
            selectedMonthInt = 7;
            rslt = "-" + 7 + "-" + date.split(" ")[1];
        } else if (month.equals(getResources().getStringArray(R.array.months_of_year)[7])) {
            selectedMonthInt = 8;
            rslt = "-" + 8 + "-" + date.split(" ")[1];
        } else if (month.equals(getResources().getStringArray(R.array.months_of_year)[8])) {
            selectedMonthInt = 9;
            rslt = "-" + 9 + "-" + date.split(" ")[1];
        } else if (month.equals(getResources().getStringArray(R.array.months_of_year)[9])) {
            selectedMonthInt = 10;
            rslt = "-" + 10 + "-" + date.split(" ")[1];
        } else if (month.equals(getResources().getStringArray(R.array.months_of_year)[10])) {
            selectedMonthInt = 11;
            rslt = "-" + 11 + "-" + date.split(" ")[1];
        } else {
            selectedMonthInt = 12;
            rslt = "-" + 12 + "-" + date.split(" ")[1];
        }
        if (currentMonth > selectedMonthInt) {
            rslt = (year + 1) + rslt;
        } else {
            rslt = year + rslt;
        }

        return rslt;
    }

    private String convertTo24H(String time) {

        try {
            SimpleDateFormat displayFormat = new SimpleDateFormat("HH:mm");
            SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm a");
            Date timeDate = parseFormat.parse(time);
            return displayFormat.format(timeDate);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    private String convertNumbers(String number, int type) {
        if (type == 0) {
            //guests
            String guest = "";
            for (int i = 0; i < number.length(); i++) {
                guest += convertDigit(number.charAt(i));
            }
            return guest;
        } else if (type == 1) {
            //date ٢٠١٨-4-٢٠
            String date = "";
            for (int i = 0; i < number.length(); i++) {
                date += convertDigit(number.charAt(i));
            }
            return date;
        } else {
            //time
            String time = "";
            for (int i = 0; i < number.length(); i++) {
                time += convertDigit(number.charAt(i));
            }
            return time;
        }
    }

    private String convertDigit(char digit) {
        switch (digit) {
            case '٠':
                return "0";
            case '١':
                return "1";
            case '٢':
                return "2";
            case '٣':
                return "3";
            case '٤':
                return "4";
            case '٥':
                return "5";
            case '٦':
                return "6";
            case '٧':
                return "7";
            case '٨':
                return "8";
            case '٩':
                return "9";
            default:
                return String.valueOf(digit);

        }
    }


    @Override
    public void showProgress() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        ((MKLoader) findViewById(R.id.loading)).setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        ((MKLoader) findViewById(R.id.loading)).setVisibility(View.GONE);
    }

    @Override
    public void showErrorMessage(String error) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void ReservationDone(String done) {
        final Dialog bookTableDone = new Dialog(this);
        bookTableDone.setContentView(R.layout.book_table_done_dialog);
        TextView txt = (TextView) bookTableDone.findViewById(R.id.message);
        txt.setText(done);
        if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            bookTableDone.getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }
        bookTableDone.setCanceledOnTouchOutside(false);
        bookTableDone.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        bookTableDone.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        bookTableDone.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                Intent intent = new Intent(BookTableActivity.this, BranchesActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                if (MySingleton.getmInstance(BookTableActivity.this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                } else {
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                }
            }
        });
        bookTableDone.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        bookTableDone.show();
        Thread th = new Thread() {
            public void run() {
                try {
                    sleep(1500);
                    bookTableDone.dismiss();
                } catch (Exception e) {

                }
            }
        };
        th.start();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            } else {
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private String convertDigits(String string) {
        if (string == null) return null;
        String rslt = "";
        for (int i = 0; i < string.length(); i++) {
            switch (string.charAt(i)) {
                case '0':
                    rslt += "٠";
                    break;
                case '1':
                    rslt += "١";
                    break;
                case '2':
                    rslt += "٢";
                    break;
                case '3':
                    rslt += "٣";
                    break;
                case '4':
                    rslt += "٤";
                    break;
                case '5':
                    rslt += "٥";
                    break;
                case '6':
                    rslt += "٦";
                    break;
                case '7':
                    rslt += "٧";
                    break;
                case '8':
                    rslt += "٨";
                    break;
                case '9':
                    rslt += "٩";
                    break;
                default:
                    rslt += string.charAt(i);
            }
        }
        return rslt;
    }

}
