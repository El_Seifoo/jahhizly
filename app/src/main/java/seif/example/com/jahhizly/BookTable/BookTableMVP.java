package seif.example.com.jahhizly.BookTable;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by seif on 11/5/2017.
 */
public interface BookTableMVP {
    interface view {
        void showProgress();

        void hideProgress();

        void showErrorMessage(String error);

        void ReservationDone(String done);
    }

    interface presenter {
        void onConfirmBtnClicked(final Context context, int branchId, String tablePlace, String guestType, String note, String chairsCount, String time, int orderId) throws JSONException;
    }
}
