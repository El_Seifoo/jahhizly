package seif.example.com.jahhizly.BookTable;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import seif.example.com.jahhizly.Menus.MenuItem.CartDbHelper;
import seif.example.com.jahhizly.Menus.MenuItem.CartItemDetails;
import seif.example.com.jahhizly.R;
import seif.example.com.jahhizly.Utils.MySingleton;
import seif.example.com.jahhizly.Utils.URLs;

/**
 * Created by seif on 11/5/2017.
 */
public class BookTableModel {

    protected void bookTable(final Context context, final VolleyCallback callback, int branchId, String tablePlace, String guestType, String note, String chairsCount, String time, int orderId) throws JSONException {

        //table_place ( in door => 0 , out door => 1  )
        //guest_type ( singles => 0 , families => 1)

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("branch_id", branchId);
        if (tablePlace.equals(context.getString(R.string.indoor))) {
            jsonObject.put("table_place", "0");
        } else {
            jsonObject.put("table_place", "1");
        }
        if (guestType.equals(context.getString(R.string.no_family))) {
            jsonObject.put("guest_type", "0");
        } else {
            jsonObject.put("guest_type", "1");
        }
        if (!note.equals("")) {
            jsonObject.put("note", note);
        }
        jsonObject.put("reservation_time", time);
        jsonObject.put("chairs_count", chairsCount);
        if (orderId != -1) {
            jsonObject.put("order_id", orderId);
        }

        Log.e("bookTable JSON", jsonObject.toString());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                URLs.RESERVATION,
                jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            callback.onSuccess(context, response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.onFail(context, error);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                String token = "Bearer " + MySingleton.getmInstance(context).UserKey();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Authorization", token);
                return headers;
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getmInstance(context).addToRQ(jsonObjectRequest);

    }


    protected interface VolleyCallback {
        void onSuccess(Context context, JSONObject response) throws JSONException;

        void onFail(Context context, VolleyError error);
    }
}
