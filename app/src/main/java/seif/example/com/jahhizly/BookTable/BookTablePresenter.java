package seif.example.com.jahhizly.BookTable;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

import seif.example.com.jahhizly.Menus.MenuItem.CartDbHelper;
import seif.example.com.jahhizly.R;
import seif.example.com.jahhizly.Utils.MySingleton;

/**
 * Created by seif on 11/5/2017.
 */
public class BookTablePresenter implements BookTableMVP.presenter, BookTableModel.VolleyCallback {
    BookTableMVP.view view;
    BookTableModel model;

    public BookTablePresenter(BookTableMVP.view view, BookTableModel model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void onConfirmBtnClicked(final Context context, int branchId, String tablePlace, String guestType, String note, String chairsCount, String time, int orderId) throws JSONException {
        view.showProgress();
        model.bookTable(context, this, branchId, tablePlace, guestType, note, chairsCount, time, orderId);
    }

    @Override
    public void onSuccess(Context context, JSONObject response) throws JSONException {
        view.hideProgress();
        String status = response.getString("status");
        if (status.equals("success")) {
            view.ReservationDone(context.getString(R.string.reserv_done));
        } else {
            view.showErrorMessage(response.getString("desc"));
        }
    }

    @Override
    public void onFail(Context context, VolleyError error) {
        view.hideProgress();
        if (error instanceof NoConnectionError || error instanceof TimeoutError) {
            Toast.makeText(context, context.getString(R.string.internet_error), Toast.LENGTH_LONG).show();
        } else if (error instanceof ServerError) {
            Toast.makeText(context, context.getString(R.string.server_error), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(context, context.getString(R.string.wrong), Toast.LENGTH_LONG).show();
        }
    }
}
