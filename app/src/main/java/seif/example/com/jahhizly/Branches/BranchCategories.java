package seif.example.com.jahhizly.Branches;

import java.io.Serializable;

/**
 * Created by seif on 11/1/2017.
 */
public class BranchCategories implements Serializable {
    private int id;
    private String name;
    private String photo;
    public BranchCategories() {
    }

    public BranchCategories(int id, String name, String photo) {
        this.id = id;
        this.name = name;
        this.photo = photo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
