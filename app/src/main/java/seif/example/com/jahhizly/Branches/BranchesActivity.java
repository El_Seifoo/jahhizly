package seif.example.com.jahhizly.Branches;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.Build;
import android.os.Handler;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.design.widget.NavigationView;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.LayoutDirection;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.Places;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.util.ArrayList;
import java.util.Locale;

import me.anwarshahriar.calligrapher.Calligrapher;
import seif.example.com.jahhizly.Branches.MakeOrder.Branches;
import seif.example.com.jahhizly.Branches.MakeOrder.MakeOrderFragment;
import seif.example.com.jahhizly.Branches.Orders.OrdersFragment;
import seif.example.com.jahhizly.Branches.Tables.TablesActivity;
import seif.example.com.jahhizly.Login.LoginActivity;
import seif.example.com.jahhizly.Login.UserInfo;
import seif.example.com.jahhizly.Map.MapsActivity;
import seif.example.com.jahhizly.Menus.MenuItem.CartDbHelper;
import seif.example.com.jahhizly.Map.NavigationMap;
import seif.example.com.jahhizly.Other.CircleTransform;
import seif.example.com.jahhizly.Profile.MyAccountActivity;
import seif.example.com.jahhizly.Cart.MyCartActivity;
import seif.example.com.jahhizly.Branches.Favorites.MyFavoritesFragment;
import seif.example.com.jahhizly.R;
import seif.example.com.jahhizly.Utils.MySingleton;
import seif.example.com.jahhizly.Utils.URLs;

public class BranchesActivity extends AppCompatActivity implements BranchesMVP.viewMain, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private NavigationView navigationView;
    private DrawerLayout drawer;
    private View navHeader;
    private Toolbar toolbar;

    // index to know the current nav menu item
    public static int navItemIndex = 1;

    // Tag for attaching the fragments
    private static final String TAG_MY_FAVORITES = "favorites";
    private static final String TAG_MAKE_ORDER = "makeOrder";
    private static final String TAG_BOOK_TABLE = "bookTable";
    private static final String TAG_ORDERS = "orders";
    // Tag for current attaching fragment
    public static String CURRENT_TAG = TAG_MAKE_ORDER;

    // toolbar titles based on selected nav menu item
    private String[] activityTitles;

    private ImageView sideMenuProfPic;
    private TextView sideMenuUsername, sideMenuUserPhone;

    // flag to load makeOrder fragment when user presses back key
    private boolean shouldLoadMakeOrderFragOnBackPress = true;
    private Handler mHandler;
    private Runnable runnable;
    BranchesMVP.presenterMain presenter;
    Bundle saveBundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String localLanguage = MySingleton.getmInstance(this).getAppLang();
        if (localLanguage.equals(getString(R.string.settings_language_arabic_value))) {
            setLocale("ar", this);
        } else if (localLanguage.equals(getString(R.string.settings_language_english_value))) {
            Locale localeEn = new Locale("en");
            setLocale("en", this);
        }
        setContentView(R.layout.activity_branches);
        Calligrapher calligrapher = new Calligrapher(this);
        if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            calligrapher.setFont(this, "fonts/Arabic/arabic_font.ttf", true);
        } else {
            calligrapher.setFont(this, "fonts/English/Roboto_Regular.ttf", true);
        }
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.name_1));
        mHandler = new Handler();
        Log.e("create", "create");
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);

        // Navigation view header
        navHeader = navigationView.getHeaderView(0);
        sideMenuProfPic = (ImageView) navHeader.findViewById(R.id.side_menu_prof_pic);
        sideMenuUsername = (TextView) navHeader.findViewById(R.id.side_menu_user_name);
        sideMenuUserPhone = (TextView) navHeader.findViewById(R.id.side_menu_user_phone);
        img = (ImageView) navHeader.findViewById(R.id.header_bg_image);
        // get toolbar titles from string resources
        activityTitles = getResources().getStringArray(R.array.nav_item_activity_titles);
//
//        if (getIntent().hasExtra("goToTables")) {
//            loadHeader();
//            setUpNavigation();
//            loadMakeOrderFragment();
//            getIntent().removeExtra("goToTables");
//        } else
        if (getIntent().hasExtra("GoToOrders")) {
            Log.e("seifooo", getIntent().getExtras().getString("GoToOrders"));
            navItemIndex = 6;
            CURRENT_TAG = TAG_ORDERS;
            loadHeader();
            // initialize navigation
            setUpNavigation();

            loadMakeOrderFragment();

            time = 0;
            getIntent().removeExtra("GoToOrders");
        } else if (getIntent().hasExtra("loginDialog")) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    showLoginDialog();
                    getIntent().removeExtra("loginDialog");
                }
            }, 1000);
        } else {
            if (!MySingleton.getmInstance(this).isFirstTime()) {
                saveBundle = savedInstanceState;
                checkMapPermission();
            }
        }


        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                presenter.onFilterIconClicked(categories);
            }
        });
        presenter = new BranchesPresenter(this, this, new BranchesModel(this));
        presenter.onMainCreated();

        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey(LATITUDE_KEY) && savedInstanceState.containsKey(LONGITUDE_KEY) &&
                    savedInstanceState.containsKey(SEARCH_KEY) && savedInstanceState.containsKey(TYPE_KEY) &&
                    savedInstanceState.containsKey(CATEGORIES_KEY) && savedInstanceState.containsKey(SORT_KEY)) {
                Log.e("qqqqqqq", "qqqqqqqqq");
                latitude = savedInstanceState.getDouble(LATITUDE_KEY);
                longitude = savedInstanceState.getDouble(LONGITUDE_KEY);
                key = savedInstanceState.getString(SEARCH_KEY);
                type = savedInstanceState.getString(TYPE_KEY);
                sort = savedInstanceState.getString(SORT_KEY);
                selectedCategories = savedInstanceState.getStringArrayList(CATEGORIES_KEY);
            }
        }
    }


    private void setLocale(String lang, Context context) {
        Configuration config = context.getResources().getConfiguration();
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            config.setLocale(locale);
            getApplicationContext().getResources().updateConfiguration(config, null);
        } else {
            config.locale = locale;
            getApplicationContext().getResources().updateConfiguration(config, null);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            config.setLocale(locale);
            config.setLayoutDirection(locale);
            context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
            context.createConfigurationContext(config);
        } else {
            context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
        }
    }


    FloatingActionButton fab;

    @Override
    protected void onResume() {
        super.onResume();
        Log.e("resume", "resume");
        if (saveBundle != null) {
            if (saveBundle.containsKey(CURRENT_STATE_KEY) && saveBundle.containsKey(CURRENT_NUMBER_KEY)) {
                CURRENT_TAG = saveBundle.getString(CURRENT_STATE_KEY);
                navItemIndex = saveBundle.getInt(CURRENT_NUMBER_KEY);
                Log.e("sssssssssss 2", CURRENT_TAG + "//" + navItemIndex);
            }
        }
    }

    @Override
    protected void onPause() {
        key = "";
        super.onPause();
        if (getIntent().hasExtra("loginDialog")) {
            getIntent().removeExtra("loginDialog");
        }
        if (langDialog != null) {
            langDialog.dismiss();
        }
        if (loginDialog != null) {
            loginDialog.dismiss();
        }
        if (dialog != null) {
            dialog.dismiss();
        }


    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.e("restart", "restart");
        time = 1;
        if (!flag) {
            finish();
            startActivity(getIntent());
        }
        loadHeader();

    }

    private String convertDigits(String string) {
        if (string == null) return null;
        String rslt = "";
        for (int i = 0; i < string.length(); i++) {
            switch (string.charAt(i)) {
                case '0':
                    rslt += "٠";
                    break;
                case '1':
                    rslt += "١";
                    break;
                case '2':
                    rslt += "٢";
                    break;
                case '3':
                    rslt += "٣";
                    break;
                case '4':
                    rslt += "٤";
                    break;
                case '5':
                    rslt += "٥";
                    break;
                case '6':
                    rslt += "٦";
                    break;
                case '7':
                    rslt += "٧";
                    break;
                case '8':
                    rslt += "٨";
                    break;
                case '9':
                    rslt += "٩";
                    break;
                default:
                    rslt += string.charAt(i);
            }
        }
        return rslt;
    }

    private String convertDigits1(String string) {
        if (string == null) return null;
        String rslt = "";
        for (int i = 0; i < string.length(); i++) {
            switch (string.charAt(i)) {
                case '٠':
                    rslt += "0";
                    break;
                case '١':
                    rslt += "1";
                    break;
                case '٢':
                    rslt += "2";
                    break;
                case '٣':
                    rslt += "3";
                    break;
                case '٤':
                    rslt += "4";
                    break;
                case '٥':
                    rslt += "5";
                    break;
                case '٦':
                    rslt += "6";
                    break;
                case '٧':
                    rslt += "7";
                    break;
                case '٨':
                    rslt += "8";
                    break;
                case '٩':
                    rslt += "9";
                    break;
                default:
                    rslt += string.charAt(i);
            }
        }
        return rslt;
    }


    /*
     *  load navigation header data
     */
    ImageView img;

    public void loadHeader() {
        ImageView profile = (ImageView) navHeader.findViewById(R.id.profile);
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(BranchesActivity.this, MyAccountActivity.class));
                if (MySingleton.getmInstance(BranchesActivity.this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                } else {
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                }
                drawer.closeDrawers();
            }
        });
        ImageView logout = (ImageView) navHeader.findViewById(R.id.log_out);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MySingleton.getmInstance(BranchesActivity.this).logout();
                CartDbHelper.getmInstance(BranchesActivity.this).deleteAll();
                finish();
                Intent intent = new Intent(BranchesActivity.this, BranchesActivity.class);
                startActivity(intent);
                if (MySingleton.getmInstance(BranchesActivity.this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                } else {
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                }
                drawer.closeDrawers();
            }
        });

        if (MySingleton.getmInstance(this).isLoggedIn()) {
            UserInfo userInfo = MySingleton.getmInstance(BranchesActivity.this).userData();
            Log.e("sdddddddddddddddd", userInfo.getPhone());
            navigationView.getMenu().getItem(7).setVisible(false);
            profile.setVisibility(View.VISIBLE);
            logout.setVisibility(View.VISIBLE);
            sideMenuUsername.setText("  " + userInfo.getName() + "  ");
            sideMenuUserPhone.setText(MySingleton.getmInstance(BranchesActivity.this).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? convertDigits("  " + userInfo.getPhone() + "+  ") : "  +" + userInfo.getPhone() + "  ");
            if (userInfo.getGender().equals("Male")) {
                Glide.with(this).load(URLs.ROOT_URL + userInfo.getPhoto())
                        .error(R.mipmap.sidemenu_pic_profile_man)
                        .crossFade()
                        .thumbnail(0.5f)
                        .bitmapTransform(new CircleTransform(this))
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(sideMenuProfPic);
            } else {
                Glide.with(this).load(URLs.ROOT_URL + userInfo.getPhoto())
                        .error(R.mipmap.sidemenu_pic_profile_woman)
                        .crossFade()
                        .thumbnail(0.5f)
                        .bitmapTransform(new CircleTransform(this))
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(sideMenuProfPic);
            }
            if (CartDbHelper.getmInstance(this).getItems(MySingleton.getmInstance(this).userData().getId()).size() > 0) {
                int count = 0;
                for (int i = 0; i < CartDbHelper.getmInstance(this).getItems(MySingleton.getmInstance(this).userData().getId()).size(); i++) {
                    count += CartDbHelper.getmInstance(this).getItems(MySingleton.getmInstance(this).userData().getId()).get(i).getQuantity();
                }
                navigationView.getMenu().getItem(4).setActionView(R.layout.menu_dot);
                ((TextView) navigationView.getMenu().getItem(4).getActionView().findViewById(R.id.cart_count_text)).setText(count + "");
            }
        } else {
            img.setImageResource(R.mipmap.logo);
            navigationView.getMenu().getItem(0).setVisible(false);
//            navigationView.getMenu().getItem(3).setVisible(false);
            navigationView.getMenu().getItem(5).setVisible(false);
            navigationView.getMenu().getItem(6).setVisible(false);
//            navigationView.getMenu().getItem(8).setVisible(false);
            if (CartDbHelper.getmInstance(this).getItems(-1).size() > 0) {
                navigationView.getMenu().getItem(4).setActionView(R.layout.menu_dot);
                int count = 0;
                for (int i = 0; i < CartDbHelper.getmInstance(this).getItems(-1).size(); i++) {
                    count += CartDbHelper.getmInstance(this).getItems(-1).get(i).getQuantity();
                }
                ((TextView) navigationView.getMenu().getItem(4).getActionView().findViewById(R.id.cart_count_text)).setText(count + "");
            }
        }
    }


    public void setUpNavigation() {
        // Handle navigation menu items actions
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                Log.e("seifooo", navItemIndex + "??" + CURRENT_TAG);
                switch (item.getItemId()) {
                    case R.id.nav_my_cart:
                        startActivity(new Intent(BranchesActivity.this, MyCartActivity.class));
                        if (MySingleton.getmInstance(BranchesActivity.this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
                            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                        } else {
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        }
                        drawer.closeDrawers();
                        return true;
                    case R.id.nav_my_fav:
                        navItemIndex = 0;
                        CURRENT_TAG = TAG_MY_FAVORITES;
                        break;
                    case R.id.nav_make_order:
                        navItemIndex = 1;
                        CURRENT_TAG = TAG_MAKE_ORDER;
                        break;
                    case R.id.nav_book_table:
                        navItemIndex = 2;
                        CURRENT_TAG = TAG_BOOK_TABLE;
                        break;
                    case R.id.nav_tables:
                        startActivity(new Intent(BranchesActivity.this, TablesActivity.class));
                        if (MySingleton.getmInstance(BranchesActivity.this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
                            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                        } else {
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        }
                        drawer.closeDrawers();
                        return true;
                    case R.id.nav_on_way:
                        startActivity(new Intent(BranchesActivity.this, NavigationMap.class));
                        if (MySingleton.getmInstance(BranchesActivity.this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
                            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                        } else {
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        }
                        drawer.closeDrawers();
                        return true;
                    case R.id.nav_orders:
                        navItemIndex = 6;
                        CURRENT_TAG = TAG_ORDERS;
                        break;
                    case R.id.nav_log_in:
                        startActivity(new Intent(BranchesActivity.this, LoginActivity.class));
                        if (MySingleton.getmInstance(BranchesActivity.this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
                            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                        } else {
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        }
                        break;
                    case R.id.nav_lang:
                        if (MySingleton.getmInstance(BranchesActivity.this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
                            MySingleton.getmInstance(BranchesActivity.this).setAppLang(getString(R.string.settings_language_english_value));
                        } else if (MySingleton.getmInstance(BranchesActivity.this).getAppLang().equals(getString(R.string.settings_language_english_value))) {
                            MySingleton.getmInstance(BranchesActivity.this).setAppLang(getString(R.string.settings_language_arabic_value));
                        }
                        finish();
                        startActivity(getIntent());
                        drawer.closeDrawers();
                        return true;
                    default:
                        navItemIndex = 1;
                        CURRENT_TAG = TAG_MAKE_ORDER;
                        break;
                }

                loadMakeOrderFragment();

                return true;
            }
        });

        android.support.v7.app.ActionBarDrawerToggle actionBarDrawerToggle = new android.support.v7.app.ActionBarDrawerToggle(this, drawer, toolbar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                FrameLayout frame = (FrameLayout) findViewById(R.id.frame_container);
                if (getResources().getConfiguration().getLayoutDirection() == LayoutDirection.RTL) {
                    frame.setX(-slideOffset * drawerView.getWidth());
                } else {
                    frame.setX(slideOffset * drawerView.getWidth());
                }

            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);

            }
        };

        //Setting the actionbarToggle to drawer layout
        drawer.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
    }


    /*
     *  return selected fragment from navigation menu
     */
    public void loadMakeOrderFragment() {


        // selecting appropriate navigation menu item
        selectNavmenu();

        // set toolbar title
        setToolbarTitle();

        // close the navigation drawer if the user selected the current navigation menu item
        if (getSupportFragmentManager().findFragmentByTag(CURRENT_TAG) != null) {
            drawer.closeDrawers();
            return;
        }

        runnable = new Runnable() {
            @Override
            public void run() {
                Fragment fragment = getSelectedFragment();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                        android.R.anim.fade_out);
                fragmentTransaction.replace(R.id.frame, fragment);
                fragmentTransaction.commitAllowingStateLoss();
            }
        };

        if (runnable != null) {
            mHandler.post(runnable);
        }

        // closing drawer on item click
        drawer.closeDrawers();

        // refresh toolbar menu
        invalidateOptionsMenu();
    }


    public void selectNavmenu() {
        for (int i = 0; i < 10; i++) {
            if (i == navItemIndex) {

                navigationView.getMenu().getItem(i).setChecked(true);

            } else {
                navigationView.getMenu().getItem(i).setChecked(false);
            }
        }
    }


    public void setToolbarTitle() {
        getSupportActionBar().setTitle(activityTitles[navItemIndex]);
    }

    String category;
    String sort = "";
    String key = "";
    String type = "";
    double latitude = 0;
    double longitude = 0;

    public Fragment getSelectedFragment() {
        switch (navItemIndex) {
            case 0:
                fab.setVisibility(View.GONE);
                return new MyFavoritesFragment();
            case 1:
                fab.setVisibility(View.VISIBLE);
                MakeOrderFragment makeOrderFragment = new MakeOrderFragment();
                if (selectedCategories != null && selectedCategories.size() > 0) {
                    makeOrderFragment.setReqData(selectedCategories, type, String.valueOf(latitude), String.valueOf(longitude), sort, convertDigits1(key), false);
                } else {
                    selectedCategories = getCategories(linear);
                    makeOrderFragment.setReqData(selectedCategories, type, String.valueOf(latitude), String.valueOf(longitude), sort, convertDigits1(key), false);
                }

                key = "";
                return makeOrderFragment;
            case 2:
                fab.setVisibility(View.VISIBLE);
                MakeOrderFragment makeOrderFragment1 = new MakeOrderFragment();
                if (selectedCategories != null && selectedCategories.size() > 0) {
                    makeOrderFragment1.setReqData(selectedCategories, "1", String.valueOf(latitude), String.valueOf(longitude), sort, convertDigits1(key), true);
                } else {
                    selectedCategories = getCategories(linear);
                    makeOrderFragment1.setReqData(selectedCategories, "1", String.valueOf(latitude), String.valueOf(longitude), sort, convertDigits1(key), true);
                }
                key = "";
                return makeOrderFragment1;
            case 6:
                fab.setVisibility(View.GONE);
                return new OrdersFragment();
            default:
                fab.setVisibility(View.VISIBLE);
                return new MakeOrderFragment();
        }
    }


    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawers();
            return;
        }

        // load MakeOrderFragment if user pressed back in other fragment
        if (shouldLoadMakeOrderFragOnBackPress) {
            if (navItemIndex != 1) {
                navItemIndex = 1;
                CURRENT_TAG = TAG_MAKE_ORDER;
                checkMapPermission();
                loadMakeOrderFragment();
                return;
            }
        }

        if (!drawer.isDrawerOpen(GravityCompat.START) || !shouldLoadMakeOrderFragOnBackPress) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addCategory(Intent.CATEGORY_HOME);
            startActivity(intent);
            finish();
            System.exit(0);

        }

        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // make items shows in MakeOrderFragment only
        if (navItemIndex == 1 || navItemIndex == 2) {
            getMenuInflater().inflate(R.menu.main_menu, menu);
            MenuItem item = menu.findItem(R.id.action_search);
            setSearchView();
            searchView.setMenuItem(item);
        }
        return true;
    }

    MaterialSearchView searchView;

    private void setSearchView() {
        searchView = (MaterialSearchView) findViewById(R.id.search_view);
        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
                                              @Override
                                              public boolean onQueryTextSubmit(String query) {
                                                  //Do some magic
                                                  MakeOrderFragment makeOrderFragment = new MakeOrderFragment();
                                                  Log.e("search 2", query);
                                                  key = query;
                                                  if (selectedCategories.size() > 0 && selectedCategories != null) {
                                                      if (navItemIndex == 2) {
                                                          type = "1";
                                                      }
                                                      makeOrderFragment.setReqData(selectedCategories, type, String.valueOf(latitude), String.valueOf(longitude), sort, convertDigits1(key), true);
                                                  } else {
                                                      if (navItemIndex == 2) {
                                                          type = "1";
                                                      }
                                                      selectedCategories = getCategories(linear);
                                                      makeOrderFragment.setReqData(selectedCategories, type, String.valueOf(latitude), String.valueOf(longitude), sort, convertDigits1(key), true);
                                                  }

                                                  if (runnable != null) {
                                                      mHandler.post(runnable);
                                                  }
                                                  return true;
                                              }


                                              @Override
                                              public boolean onQueryTextChange(String newText) {
                                                  //Do some magic
                                                  MakeOrderFragment makeOrderFragment = new MakeOrderFragment();
                                                  Log.e("search 1", newText);
                                                  key += newText;
                                                  if (selectedCategories.size() > 0 && selectedCategories != null) {
                                                      if (navItemIndex == 2) {
                                                          type = "1";
                                                      }
                                                      makeOrderFragment.setReqData(selectedCategories, type, String.valueOf(latitude), String.valueOf(longitude), sort, convertDigits1(key), true);
                                                  } else {
                                                      if (navItemIndex == 2) {
                                                          type = "1";
                                                      }
                                                      selectedCategories = getCategories(linear);
                                                      makeOrderFragment.setReqData(selectedCategories, type, String.valueOf(latitude), String.valueOf(longitude), sort, convertDigits1(key), true);
                                                  }
                                                  if (runnable != null) {
                                                      mHandler.post(runnable);
                                                  }
                                                  return true;
                                              }
                                          }
        );

        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener()

                                           {
                                               @Override
                                               public void onSearchViewShown() {
                                                   //Do some magic
                                               }

                                               @Override
                                               public void onSearchViewClosed() {
                                                   //Do some magic
                                               }
                                           }

        );
    }

    private static final int MAP_REQUEST_CODE = 5050;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
//        if (id == R.id.action_search) {
//            Toast.makeText(BranchesActivity.this, "Search", Toast.LENGTH_LONG).show();
//            return true;
//        } else
//        if (id == R.id.action_filter) {
//            presenter.onFilterIconClicked(categories);
//            return true;
//        } else
        if (id == R.id.action_map) {
            if (categories != null && categories.size() > 0) {
                Intent intent = new Intent(BranchesActivity.this, MapsActivity.class);
                intent.putExtra("categories", categories);
                intent.putExtra("selectedCategories", selectedCategories);
                if (navItemIndex == 2) {
                    intent.putExtra("type", "1");
                } else {
                    intent.putExtra("type", type);
                }
                intent.putExtra("sort", sort);
                startActivityForResult(intent, MAP_REQUEST_CODE);
                if (MySingleton.getmInstance(BranchesActivity.this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                } else {
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                }
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    ArrayList<BranchCategories> categories;

    @Override
    public void getData(ArrayList<BranchCategories> branchCategories) {
        categories = branchCategories;
    }

    String preSort;

    Dialog dialog;

    @Override
    public void clickFilter(final ArrayList<BranchCategories> categories) {
        if (selectedCategories.size() > 0 && selectedCategories != null) {
            selectedCategories.clear();
        }
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.filter_dialog);
        final Button rate = (Button) dialog.findViewById(R.id.filter_rate_btn);
        final Button distance = (Button) dialog.findViewById(R.id.filter_distance_btn);
        final RadioGroup radioGroup = (RadioGroup) dialog.findViewById(R.id.filter_type_container);
        if (navItemIndex == 2) {
            radioGroup.setVisibility(View.GONE);
            ((TextView) dialog.findViewById(R.id.branches_type_text_view)).setVisibility(View.GONE);
            ((View) dialog.findViewById(R.id.view_id)).setVisibility(View.GONE);
        }
        ((RadioButton) radioGroup.getChildAt(0)).setChecked(true);
        distance.setBackgroundColor(rate.getContext().getResources().getColor(R.color.red_3));
        distance.setTextColor(rate.getContext().getResources().getColor(R.color.white));
        preSort = getString(R.string.sort_distance);
        setCategories(dialog, categories);
        rate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                rate.setBackgroundColor(rate.getContext().getResources().getColor(R.color.red_3));
                rate.setTextColor(rate.getContext().getResources().getColor(R.color.white));
                distance.setBackgroundResource(R.drawable.filter_btns);
                distance.setTextColor(rate.getContext().getResources().getColor(R.color.black));
                preSort = getString(R.string.sort_rate);
            }
        });
        distance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                distance.setBackgroundColor(rate.getContext().getResources().getColor(R.color.red_3));
                distance.setTextColor(rate.getContext().getResources().getColor(R.color.white));
                rate.setBackgroundResource(R.drawable.filter_btns);
                rate.setTextColor(rate.getContext().getResources().getColor(R.color.black));
                preSort = getString(R.string.sort_distance);
            }
        });

        Button done = (Button) dialog.findViewById(R.id.filter_done_btn);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (preSort.equals(getString(R.string.sort_rate))) {
                    sort = "rate";
                } else {
                    sort = "distance";
                }
                String typeString = getBranchType(dialog, radioGroup);
                if (typeString.equals(getString(R.string.type_1))) {
                    type = "0";
                } else if (typeString.equals(getString(R.string.type_2))) {
                    type = "2";
                } else if (typeString.equals(getString(R.string.type_3))) {
                    type = "1";
                }
                Log.e("typessss", type);
                if (navItemIndex == 1) {
                    MakeOrderFragment makeOrderFragment = new MakeOrderFragment();
                    if (selectedCategories.size() > 0 && selectedCategories != null) {
                        makeOrderFragment.setReqData(selectedCategories, type, String.valueOf(latitude), String.valueOf(longitude), sort, convertDigits1(key), false);
                    } else {
                        selectedCategories = getCategories(linear);
                        makeOrderFragment.setReqData(selectedCategories, type, String.valueOf(latitude), String.valueOf(longitude), sort, convertDigits1(key), false);
                    }
                } else if (navItemIndex == 2) {
                    MakeOrderFragment makeOrderFragment = new MakeOrderFragment();
                    if (selectedCategories.size() > 0 && selectedCategories != null) {
                        makeOrderFragment.setReqData(selectedCategories, "1", String.valueOf(latitude), String.valueOf(longitude), sort, convertDigits1(key), true);
                    } else {
                        selectedCategories = getCategories(linear);
                        makeOrderFragment.setReqData(selectedCategories, "1", String.valueOf(latitude), String.valueOf(longitude), sort, convertDigits1(key), true);
                    }
                }

                if (runnable != null) {
                    mHandler.post(runnable);
                }
                dialog.dismiss();
            }
        });
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                num = 0;
                if (linear.size() > 0 && linear != null) {
                    linear.clear();
                }
            }
        });
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }


    int num = 0;
    ArrayList<LinearLayout> linear = new ArrayList<>();

    public void setCategories(Dialog dialog, final ArrayList<BranchCategories> categories) {
        final ArrayList<ImageView> imgs = new ArrayList<>();
        final ArrayList<TextView> txts = new ArrayList<>();
        firstContainer = (LinearLayout) dialog.findViewById(R.id.first_category_container);
        secondContainer = (LinearLayout) dialog.findViewById(R.id.second_category_container);
        for (int i = 0; i < categories.size() + 1; i++) {
            final LinearLayout parent = new LinearLayout(this);
            parent.setBackground(new ColorDrawable(Color.WHITE));
            parent.setPadding(16, 16, 16, 16);
            parent.setOrientation(LinearLayout.HORIZONTAL);
            parent.setGravity(Gravity.CENTER_VERTICAL);
            ImageView imageView = new ImageView(this);
            imageView.setMinimumHeight(80);
            imageView.setMinimumWidth(80);
            TextView textView = new TextView(this);
            textView.setTextColor(getResources().getColor(R.color.black));
            LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params1.setMargins(40, 0, 40, 0);
            textView.setLayoutParams(params1);
            if (i == 0) {
                parent.setTag(0);
                textView.setText(getString(R.string.type_1));
                imageView.setImageResource(R.mipmap.all_food_c);
            } else {
                parent.setTag(categories.get(i - 1).getId());
                textView.setText(MySingleton.getmInstance(BranchesActivity.this).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? convertDigits(categories.get(i - 1).getName()) : categories.get(i - 1).getName());
                Glide.with(BranchesActivity.this).load(URLs.ROOT_URL + categories.get(i - 1).getPhoto())
                        .error(R.mipmap.default_category)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(imageView);
            }
            imgs.add(imageView);
            txts.add(textView);
            linear.add(parent);
            parent.addView(imageView);
            parent.addView(textView);
            if (i % 2 == 0) {
                firstContainer.addView(parent);
            } else {
                secondContainer.addView(parent);
            }
        }

        linear.get(0).setBackground(new ColorDrawable(getResources().getColor(R.color.test_1)));
//        imgs.get(0).setColorFilter(ContextCompat.getColor(BranchesActivity.this, R.color.white), PorterDuff.Mode.CLEAR);
        txts.get(0).setTextColor(getResources().getColor(R.color.white));
        category = "0";
        for (int i = 0; i < linear.size(); i++) {
            final int x = i;
            linear.get(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (x != 0) {
                        linear.get(0).setBackground(new ColorDrawable(Color.WHITE));
//                        imgs.get(0).setColorFilter(ContextCompat.getColor(BranchesActivity.this, R.color.red_3), PorterDuff.Mode.CLEAR);
                        txts.get(0).setTextColor(getResources().getColor(R.color.black));
                    } else if (x == 0) {
                        num = 0;
                        for (int z = 1; z < linear.size(); z++) {
                            linear.get(z).setBackground(new ColorDrawable(Color.WHITE));
//                            imgs.get(z).setColorFilter(ContextCompat.getColor(BranchesActivity.this, R.color.red_3), PorterDuff.Mode.CLEAR);
                            txts.get(z).setTextColor(getResources().getColor(R.color.black));
                        }
                    }
                    if (linear.get(x).getBackground() instanceof ColorDrawable) {
                        if (((ColorDrawable) linear.get(x).getBackground()).getColor() == Color.WHITE) {
                            linear.get(x).setBackground(new ColorDrawable(getResources().getColor(R.color.test_1)));
//                            imgs.get(x).setColorFilter(ContextCompat.getColor(BranchesActivity.this, R.color.white), android.graphics.PorterDuff.Mode.CLEAR);
                            txts.get(x).setTextColor(getResources().getColor(R.color.white));
                            if (x != 0) {
                                num++;
                            } else {
                                num = 0;
                            }

                        } else {
                            if (x != 0) {
                                linear.get(x).setBackground(new ColorDrawable(Color.WHITE));
//                                imgs.get(x).setColorFilter(ContextCompat.getColor(BranchesActivity.this, R.color.red_3), android.graphics.PorterDuff.Mode.CLEAR);
                                txts.get(x).setTextColor(getResources().getColor(R.color.black));
                                num--;
                            }

                        }
                    }
                    Log.e("dsds", num + "");
                    if (num == 0 || num == linear.size() - 1) {
                        num = 0;
                        for (int i = 0; i < linear.size(); i++) {
                            if (i == 0) {
                                linear.get(i).setBackground(new ColorDrawable(getResources().getColor(R.color.test_1)));
//                                imgs.get(i).setColorFilter(ContextCompat.getColor(BranchesActivity.this, R.color.white), PorterDuff.Mode.CLEAR);
                                txts.get(i).setTextColor(getResources().getColor(R.color.white));
                            } else {
                                linear.get(i).setBackground(new ColorDrawable(Color.WHITE));
//                                imgs.get(i).setColorFilter(ContextCompat.getColor(BranchesActivity.this, R.color.red_3), android.graphics.PorterDuff.Mode.CLEAR);
                                txts.get(i).setTextColor(getResources().getColor(R.color.black));
                            }
                        }
                    }
                }
            });

        }
    }

    private ArrayList<String> getCategories(ArrayList<LinearLayout> linear) {
        ArrayList<String> cats = new ArrayList<>();
        for (int i = 0; i < linear.size(); i++) {
            if (linear.get(i).getBackground() instanceof ColorDrawable) {
                if (((ColorDrawable) linear.get(i).getBackground()).getColor() == getResources().getColor(R.color.test_1)) {
                    cats.add(String.valueOf(linear.get(i).getTag()));
                }
            }
        }
        return cats;
    }

    private String getBranchType(Dialog dialog, RadioGroup radioGroup) {
        int selected = radioGroup.getCheckedRadioButtonId();
        RadioButton type = (RadioButton) dialog.findViewById(selected);
        if (type == null) {
            return null;
        }

        return String.valueOf(type.getText());
    }

    Dialog langDialog;

    @Override
    public void showLangDialog() {
        langDialog = new Dialog(this);
        langDialog.setContentView(R.layout.lang_dialog);
        Button en = (Button) langDialog.findViewById(R.id.lang_dialog_en_btn);
        Button ar = (Button) langDialog.findViewById(R.id.lang_dialog_ar_btn);
        en.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Vibrator v = (Vibrator) getSystemService(VIBRATOR_SERVICE);
                v.vibrate(50);
                MySingleton.getmInstance(BranchesActivity.this).setAppLang(getString(R.string.settings_language_english_value));
                langDialog.dismiss();
            }
        });
        ar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Vibrator v = (Vibrator) getSystemService(VIBRATOR_SERVICE);
                v.vibrate(50);
                MySingleton.getmInstance(BranchesActivity.this).setAppLang(getString(R.string.settings_language_arabic_value));
                langDialog.dismiss();

            }
        });
        langDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                MySingleton.getmInstance(BranchesActivity.this).firstTime();
                finish();
                Intent intent = new Intent(BranchesActivity.this, BranchesActivity.class);
                intent.putExtra("loginDialog", "loginDialog");
                startActivity(intent);
            }
        });
        langDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        langDialog.setCanceledOnTouchOutside(false);
        langDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        langDialog.show();
    }

    Dialog loginDialog;

    @Override
    public void showLoginDialog() {
        loginDialog = new Dialog(this);
        loginDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        loginDialog.setContentView(R.layout.login_dialog);
        Button login = (Button) loginDialog.findViewById(R.id.branches_login_btn);
        Button cont = (Button) loginDialog.findViewById(R.id.branches_continue_btn);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(BranchesActivity.this, LoginActivity.class));
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
            }
        });

        cont.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                checkMapPermission();
                loginDialog.dismiss();
            }
        });

        loginDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                // load navigation header
                loadHeader();
                // initialize navigation
                setUpNavigation();
                loadMakeOrderFragment();
            }
        });
        loginDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        loginDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        loginDialog.setCanceledOnTouchOutside(false);
        loginDialog.show();
    }

    LinearLayout firstContainer, secondContainer;


    private GoogleApiClient googleApiClient;
    private LocationRequest request;
    private boolean mRequestingLocationUpdates;
    private static final int REQUEST_CHECK_SETTINGS = 1000;

    private void checkMapPermission() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {

            if (ActivityCompat.checkSelfPermission(BranchesActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(BranchesActivity.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(BranchesActivity.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 1002);
            } else {

                setupLocationManager();
            }
        } else {
            setupLocationManager();
        }
    }

    private void setupLocationManager() {
        //buildGoogleApiClient();
        if (googleApiClient == null) {

            googleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .addApi(Places.GEO_DATA_API)
                    .addApi(Places.PLACE_DETECTION_API)
                    .build();
            //mGoogleApiClient = new GoogleApiClient.Builder(this);
        }
        googleApiClient.connect();
        createLocationRequest();
    }

    protected void createLocationRequest() {

        request = new LocationRequest();
        request.setSmallestDisplacement(10);
        request.setFastestInterval(300000);
        request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);


        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(request);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(googleApiClient,
                        builder.build());


        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates states = result.getLocationSettingsStates();

                switch (status.getStatusCode()) {

                    case LocationSettingsStatusCodes.SUCCESS:
                        setInitialLocation();
                        break;

                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied, but this can be fixed
                        // by showing the user a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(
                                    BranchesActivity.this,
                                    REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;

                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way
                        // to fix the settings so we won't show the dialog.

                        break;

                }


            }
        });
    }

    boolean flag;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("onActivityResult()", Integer.toString(resultCode));
        //final LocationSettingsStates states = LocationSettingsStates.fromIntent(data);
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK: {

                        // load navigation header
                        loadHeader();
                        // initialize navigation
                        setUpNavigation();


                        if (saveBundle == null) {
                            navItemIndex = 1;
                            CURRENT_TAG = TAG_MAKE_ORDER;
                            loadMakeOrderFragment();
                        }


                        setInitialLocation();

//                        Toast.makeText(BranchesActivity.this, "Location enabled by user!", Toast.LENGTH_LONG).show();
                        mRequestingLocationUpdates = true;
                        break;
                    }
                    case Activity.RESULT_CANCELED: {
                        latitude = -1;
                        longitude = -1;
                        // The user was asked to change settings, but chose not to
                        // load navigation header
                        loadHeader();
                        // initialize navigation
                        setUpNavigation();
                        if (saveBundle == null) {
                            navItemIndex = 1;
                            CURRENT_TAG = TAG_MAKE_ORDER;
                            loadMakeOrderFragment();
                        }

//                        Toast.makeText(BranchesActivity.this, "Location not enabled, user cancelled.", Toast.LENGTH_LONG).show();
                        mRequestingLocationUpdates = false;
                        break;
                    }
                    default: {
                        break;
                    }
                }
                break;
            case MAP_REQUEST_CODE:
                if (resultCode == RESULT_OK) {
                    latitude = data.getDoubleExtra("latitude", latitude);
                    longitude = data.getDoubleExtra("longitude", longitude);
                    sort = data.getStringExtra("sort");
                    if (navItemIndex == 2) {
                        type = "1";
                        CURRENT_TAG = TAG_BOOK_TABLE;
                    } else {
                        type = data.getStringExtra("type");
                        CURRENT_TAG = TAG_MAKE_ORDER;
                        navItemIndex = 1;
                    }
                    selectedCategories = data.getStringArrayListExtra("categories");
                    for (int i = 0; i < selectedCategories.size(); i++) {
                        Log.e("ssssssssssssssssssssss", selectedCategories.get(i));
                    }
                    flag = data.getBooleanExtra("flag", false);

                    loadMakeOrderFragment();
//                    MakeOrderFragment makeOrderFragment = new MakeOrderFragment();
//                    Log.e("timesss", time + "");
//                    makeOrderFragment.setReqData(selectedCategories, type, String.valueOf(latitude), String.valueOf(longitude), sort, key);
//                    if (runnable != null) {
//                        mHandler.post(runnable);
//                    }
                }
                break;
        }
    }

    ArrayList<String> selectedCategories = new ArrayList<>();
    int time = 0;

    private void setInitialLocation() {
        if (ActivityCompat.checkSelfPermission(BranchesActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(BranchesActivity.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, request, new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {

                if (time == 0) {
                    Log.e("sdsds", "dsdsd");
                    if (latitude == 0 && longitude == 0) {
                        latitude = location.getLatitude();
                        longitude = location.getLongitude();
                    }
//                    latitude = location.getLatitude();
//                    longitude = location.getLongitude();
                    // load navigation header
                    loadHeader();
                    // initialize navigation
                    setUpNavigation();
                    if (saveBundle == null) {
                        navItemIndex = 1;
                        CURRENT_TAG = TAG_MAKE_ORDER;
                        loadMakeOrderFragment();
                    }
                    time = 1;
                }
            }
        });
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1002: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        setupLocationManager();
                    }
                } else {
                    Toast.makeText(BranchesActivity.this, "Permission Denied", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
            break;
        }
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    private final static String CURRENT_STATE_KEY = "currentState";
    private final static String CURRENT_NUMBER_KEY = "currentNumber";
    private final static String LATITUDE_KEY = "latitude";
    private final static String LONGITUDE_KEY = "longitude";
    private final static String SORT_KEY = "sort";
    private final static String TYPE_KEY = "type";
    private final static String SEARCH_KEY = "search";
    private final static String CATEGORIES_KEY = "categories";

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(CURRENT_STATE_KEY, CURRENT_TAG);
        outState.putInt(CURRENT_NUMBER_KEY, navItemIndex);
        outState.putDouble(LATITUDE_KEY, latitude);
        outState.putDouble(LONGITUDE_KEY, longitude);
        outState.putString(SORT_KEY, sort);
        outState.putString(SEARCH_KEY, key);
        outState.putString(TYPE_KEY, type);
        outState.putStringArrayList(CATEGORIES_KEY, selectedCategories);
        Log.e("sssssssssss 1", CURRENT_TAG + "//" + navItemIndex);
    }


    public static View getView() {
        return MakeOrderFragment.getFView();
    }
}
