package seif.example.com.jahhizly.Branches;

import android.app.Dialog;

import org.json.JSONException;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import seif.example.com.jahhizly.Branches.MakeOrder.Branches;
import seif.example.com.jahhizly.Menus.MenuItemDetails;

/**
 * Created by seif on 10/22/2017.
 */
public interface BranchesMVP {

    interface viewMain {
        void getData(ArrayList<BranchCategories> branchCategories);

        void clickFilter(ArrayList<BranchCategories> categories);

        void showLangDialog();

        void showLoginDialog();
    }

    interface presenterMain {
        void onMainCreated();

        void onFilterIconClicked(ArrayList<BranchCategories> categories);
    }

    interface view {
        void loadData(ArrayList<Branches> branches);

        void goMenus(int position, Branches branch, boolean isBookTableOnly);

        void showProgress();

        void hideProgress();

        void showUnRevDialog(ArrayList<Branches> unReviewed, int i);

        void showEmptyText(String message);

        void onBranchReviewed(Dialog dialog, int index);

    }

    interface presenter {
        void CreateMakeOrder(ArrayList<String> category, String type, String latitude, String longitude, String sort, String key, boolean isLoggedIn) throws UnsupportedEncodingException;

        void onItemClicked(int position, Branches branch, boolean isBookTableOnly);

        void getUnReviewed();

        void addReview(int branchId, float rate, boolean isRated, String comment, Dialog dialog, int index, int orderId) throws JSONException;
    }

    interface viewOrders {
        void loadData(ArrayList<UserOrders> orders);

        void showProgress();

        void hideProgress();

        void showEmptyText(String message);
    }

    interface presenterOrders {
        void onOrdersCreated(int type);
    }

    interface viewFav {
        void loadBranchesData(ArrayList<Branches> branches);

        void loadMenusData(ArrayList<MenuItemDetails> menuItems);

        void showProgress();

        void hideProgress();

        void showLoadingMoreProgress();

        void hideLoadingMoreProgress();

        void showEmptyText(String message);
    }

    interface presenterFav {
        void onFavoritesCreated(int type, int page);
    }
}
