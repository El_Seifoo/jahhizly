package seif.example.com.jahhizly.Branches;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;
import seif.example.com.jahhizly.R;
import seif.example.com.jahhizly.Utils.Constants;
import seif.example.com.jahhizly.Utils.MySingleton;
import seif.example.com.jahhizly.Utils.URLs;

/**
 * Created by seif on 10/22/2017.
 */
public class BranchesModel {
    private Context context = null;
    private String url;

    public BranchesModel(Context context) {
        this.context = context;
    }

    //24.601450 + "/" + 46.691895
    protected void getAllBranches(final VolleyCallBack callBack, String category, String type, String latitude, String longitude, String sort, String key) throws UnsupportedEncodingException {
        Log.e("seifoooo location", latitude + "|||||||" + longitude);
        if (sort == null) {
            sort = "distance";
        }
        if (type == null) {
            type = "0";
        }
        if (Double.valueOf(latitude) != 0 && Double.valueOf(longitude) != 0) {
            if (category == null) {
                if (key.equals("")) {
                    url = URLs.BRANCHES + 24.601450 + "/" + 46.691895 + "/" + sort + "/" + type + "/" + 0;
                } else {
                    Log.e("keeeey", key);
                    url = URLs.BRANCHES + 24.601450 + "/" + 46.691895 + "/" + sort + "/" + type + "/" + 0 + "/" + URLEncoder.encode(key, "utf-8");
                }
            } else {
                if (key.equals("")) {
                    url = URLs.BRANCHES + 24.601450 + "/" + 46.691895 + "/" + sort + "/" + type + "/" + category;
                } else {
                    Log.e("keeeey", key);
                    url = URLs.BRANCHES + 24.601450 + "/" + 46.691895 + "/" + sort + "/" + type + "/" + category + "/" + URLEncoder.encode(key, "utf-8");
                }
            }

            Log.e("BranchesURL", url);

            StringRequest stringRequest = new StringRequest(Request.Method.GET,
                    url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                callBack.onSuccess(response, 1);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    callBack.onFail(error, 1);
                }
            });

            MySingleton.getmInstance(context).addToRQ(stringRequest);
        } else {
            try {
                callBack.onSuccess("wrong", 10);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    protected void getAllBranches(final VolleyCallBack callBack, ArrayList<String> categories, String type, String latitude, String longitude, String sort, String key) {

        try {
            if (sort == null || sort.equals("")) {
                sort = "distance";
            }
            if (type == null || type.equals("")) {
                type = "0";
            }
            JSONArray array = new JSONArray();
            JSONObject params = new JSONObject();
            if (categories == null || categories.isEmpty() || categories.get(0).equals("0")) {

                params.put("lat", latitude);
                params.put("lng", longitude);
                params.put("sort", sort);
                params.put("type", type);
                if (key != null && !key.equals("")) {
                    params.put("key", key);
                }
                params.put("categories", array);
                Log.e("sssssss", params.toString());

            } else {
                for (int i = 0; i < categories.size(); i++) {
                    array.put(categories.get(i));
                }
                params.put("lat", latitude);
                params.put("lng", longitude);
                params.put("sort", sort);
                params.put("type", type);
                if (!key.equals("")) {
                    params.put("key", key);
                }
                params.put("categories", array);
                Log.e("sssssss", params.toString());
            }

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                    URLs.SHOPPER_BRANCHES,
                    params,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                callBack.onSuccess(response.toString(), 1);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            callBack.onFail(error, 1);
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    return headers;
                }
            };
            MySingleton.getmInstance(context).addToRQ(jsonObjectRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected void getCategories(final VolleyCallBack callBack) {
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                URLs.ALL_CATEGORIES,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            callBack.onSuccess(response, 2);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callBack.onFail(error, 2);
                    }
                });
        MySingleton.getmInstance(context).addToRQ(stringRequest);
    }

    protected void getOrders(final VolleyCallBack callBack) {
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                URLs.PREVIOUS_ORDER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            callBack.onSuccess(response, 3);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("orders json error", e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callBack.onFail(error, 3);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                String token = "Bearer " + MySingleton.getmInstance(context).UserKey();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Authorization", token);
                return headers;
            }
        };

        MySingleton.getmInstance(context).addToRQ(stringRequest);
    }

    protected void getFavoriteBranches(final VolleyCallBack callBack) {
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                URLs.FAVORITE_BRANCHES + 1,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            callBack.onSuccess(response, 4);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callBack.onFail(error, 4);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                String token = "Bearer " + MySingleton.getmInstance(context).UserKey();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Authorization", token);
                return headers;
            }
        };
        MySingleton.getmInstance(context).addToRQ(stringRequest);
    }

    protected void getFavoriteBranches(final VolleyCallBack callBack, int page) {
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                URLs.FAVORITE_BRANCHES + page,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            callBack.onSuccess(response, 8);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callBack.onFail(error, 8);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                String token = "Bearer " + MySingleton.getmInstance(context).UserKey();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Authorization", token);
                return headers;
            }
        };
        MySingleton.getmInstance(context).addToRQ(stringRequest);
    }

    protected void getFavoriteMeals(final VolleyCallBack callBack) {
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                URLs.FAVORITE_MENUS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            callBack.onSuccess(response, 5);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callBack.onFail(error, 5);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                String token = "Bearer " + MySingleton.getmInstance(context).UserKey();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Authorization", token);
                return headers;
            }
        };
        MySingleton.getmInstance(context).addToRQ(stringRequest);
    }

    protected void getUnReviewedBranches(final VolleyCallBack callBack) {
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                URLs.UN_RATED,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            callBack.onSuccess(response, 6);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callBack.onFail(error, 6);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                String token = "Bearer " + MySingleton.getmInstance(context).UserKey();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Authorization", token);
                return headers;
            }
        };
        MySingleton.getmInstance(context).addToRQ(stringRequest);
    }

    protected void addReview(final VolleyCallBack callback, int branchId, float rate, boolean isRated, String comment, int orderId) throws JSONException {
        JSONObject review = new JSONObject();
        if (!isRated) {
            review.put("branch_id", branchId);
            review.put("is_rated", isRated);
            review.put("order_id", orderId);
        } else {
            review.put("branch_id", branchId);
            review.put("rate", rate);
            review.put("is_rated", isRated);
            review.put("comment", comment);
            review.put("order_id", orderId);
        }

        Log.e("review json", review.toString());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                URLs.ADD_USER_REVIEW,
                review,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            callback.onSuccess(response.toString(), 7);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onFail(error, 7);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                String token = "Bearer " + MySingleton.getmInstance(context).UserKey();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Authorization", token);
                return headers;
            }
        };
        MySingleton.getmInstance(context).addToRQ(jsonObjectRequest);
    }


    protected interface VolleyCallBack {
        void onSuccess(String response, int type) throws JSONException;

        void onFail(VolleyError error, int type);
    }
}
