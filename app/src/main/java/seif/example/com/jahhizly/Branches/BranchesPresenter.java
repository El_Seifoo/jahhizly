package seif.example.com.jahhizly.Branches;

import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.ArrayList;

import seif.example.com.jahhizly.Branches.MakeOrder.Branches;
import seif.example.com.jahhizly.Menus.MenuItemDetails;
import seif.example.com.jahhizly.R;
import seif.example.com.jahhizly.Utils.MySingleton;

/**
 * Created by seif on 10/22/2017.
 */
public class BranchesPresenter implements BranchesMVP.presenter, BranchesModel.VolleyCallBack, BranchesMVP.presenterMain,
        BranchesMVP.presenterOrders, BranchesMVP.presenterFav {

    private BranchesMVP.view view;
    private Context context;
    private BranchesModel model;
    private BranchesMVP.viewMain viewMain;
    private BranchesMVP.viewOrders viewOrders;
    private BranchesMVP.viewFav viewFav;

    public BranchesPresenter(BranchesMVP.viewMain viewMain, Context context, BranchesModel model) {
        this.viewMain = viewMain;
        this.context = context;
        this.model = model;
    }

    public BranchesPresenter(BranchesMVP.view view, Context context, BranchesModel model) {
        this.view = view;
        this.context = context;
        this.model = model;
    }

    public BranchesPresenter(BranchesMVP.viewOrders viewOrders, Context context, BranchesModel model) {
        this.viewOrders = viewOrders;
        this.context = context;
        this.model = model;
    }

    public BranchesPresenter(BranchesMVP.viewFav viewFav, Context context, BranchesModel model) {
        this.viewFav = viewFav;
        this.context = context;
        this.model = model;
    }


    @Override
    public void CreateMakeOrder(ArrayList<String> category, String type, String latitude, String longitude, String sort, String key, boolean isLoggedIn) throws UnsupportedEncodingException {
        isLogged = isLoggedIn;
        view.showProgress();
        if (isLoggedIn) {
            model.getFavoriteBranches(this);
        }
        model.getAllBranches(this, category, type, latitude, longitude, sort, key);
    }

    @Override
    public void onItemClicked(int position, Branches branch, boolean isBookTableOnly) {
        view.goMenus(position, branch, isBookTableOnly);
    }

    @Override
    public void getUnReviewed() {
        model.getUnReviewedBranches(this);
    }

    Dialog dialog;
    int index;

    @Override
    public void addReview(int branchId, float rate, boolean isRated, String comment, Dialog dialog, int index, int orderId) throws JSONException {
        this.dialog = dialog;
        this.index = index;
        model.addReview(this, branchId, rate, isRated, comment, orderId);
    }

    @Override
    public void onMainCreated() {
        if (MySingleton.getmInstance(context).isFirstTime()) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    viewMain.showLangDialog();
                }
            }, 1000);

        }
        model.getCategories(this);
    }

    @Override
    public void onFilterIconClicked(ArrayList<BranchCategories> categories) {
        if (categories != null && categories.size() > 0) {
            viewMain.clickFilter(categories);
        }
    }

    int prevORnew;

    @Override
    public void onOrdersCreated(int type) {
        viewOrders.showProgress();
        prevORnew = type;
        model.getOrders(this);
    }

    private int page;

    @Override
    public void onFavoritesCreated(int type, int page) {
        if (type == 0) {
            this.page = page;
            if (page > 1) viewFav.showLoadingMoreProgress();
            else viewFav.showProgress();

            model.getFavoriteBranches(this, page);
        } else if (type == 1) {
            viewFav.showProgress();
            model.getFavoriteMeals(this);
        }
    }

    private boolean isLogged;
    ArrayList<Branches> favBranchesList = new ArrayList<>();

    private ArrayList<Branches> getFinalBranches(ArrayList<Branches> branchesList, ArrayList<Branches> favBranchesList) {
        if (favBranchesList.size() > 0 && favBranchesList != null) {
            for (int i = 0; i < branchesList.size(); i++) {
                for (int j = 0; j < favBranchesList.size(); j++) {
                    if (branchesList.get(i).getId() == favBranchesList.get(j).getId()) {
                        branchesList.get(i).setChecked(true);
                    }
                }
            }
        }

        return branchesList;
    }

    @Override
    public void onSuccess(String response, int which) throws JSONException {
        if (which == 1) {
            ArrayList<Branches> branchesList = new ArrayList<>();
            Log.e("BranchesResponse", response);
            if (MySingleton.getmInstance(context).getAppLang().equals(context.getString(R.string.settings_language_english_value))) {
                JSONObject jsonResponse = new JSONObject(response);
                JSONArray branches = jsonResponse.getJSONArray("branches");
                int branchesLength = branches.length();
                for (int i = 0; i < branchesLength; i++) {
                    int id = branches.getJSONObject(i).getInt("id");
                    String name;
                    if (branches.getJSONObject(i).getString("name").equals("") ||
                            branches.getJSONObject(i).getString("name").equals("null")) {
                        name = context.getString(R.string.list_empty);
                    } else {
                        name = branches.getJSONObject(i).getString("name");
                    }
                    String address = "";
//                    if (branches.getJSONObject(i).getString("address").equals("") ||
//                            branches.getJSONObject(i).getString("address").equals("null")) {
//                        address = context.getString(R.string.list_empty);
//                    } else {
//                        address = branches.getJSONObject(i).getString("address");
//                    }

                    int brandId = branches.getJSONObject(i).getInt("brand_id");
                    String brandName;
                    if (branches.getJSONObject(i).getString("brand_name").equals("") ||
                            branches.getJSONObject(i).getString("brand_name").equals("null")) {
                        brandName = "no data available";
                    } else {
                        brandName = branches.getJSONObject(i).getString("brand_name");
                    }
                    String openHour;
                    if (branches.getJSONObject(i).getString("open_time").equals("") ||
                            branches.getJSONObject(i).getString("open_time").equals("null")) {
                        openHour = "";
                    } else {
                        openHour = branches.getJSONObject(i).getString("open_time");
                    }
                    String closeHour;
                    if (branches.getJSONObject(i).getString("close_time").equals("") ||
                            branches.getJSONObject(i).getString("close_time").equals("null")) {
                        closeHour = "";
                    } else {
                        closeHour = branches.getJSONObject(i).getString("close_time");
                    }

                    String brandPhoto;
                    if (branches.getJSONObject(i).getString("brand_photo").equals("") ||
                            branches.getJSONObject(i).getString("brand_photo").equals("null")) {
                        brandPhoto = "null";
                    } else {
                        brandPhoto = branches.getJSONObject(i).getString("brand_photo");
                    }
                    String brandCategory = branches.getJSONObject(i).getString("brand_category");
                    double distance;
                    if (branches.getJSONObject(i).getString("distance").equals("")
                            || branches.getJSONObject(i).getString("distance").equals("null")) {
                        distance = 0;
                    } else {

                        distance = branches.getJSONObject(i).getDouble("distance");
                        BigDecimal bigDecimal;
                        if (distance > 0) {
                            bigDecimal = new BigDecimal(String.valueOf(distance)).setScale(2, BigDecimal.ROUND_FLOOR);
                        } else {
                            bigDecimal = new BigDecimal(String.valueOf(distance)).setScale(2, BigDecimal.ROUND_CEILING);
                        }

                        distance = bigDecimal.doubleValue();
                    }
                    double rate;
                    if (branches.getJSONObject(i).getString("rate").equals("")
                            || branches.getJSONObject(i).getString("rate").equals("null")) {
                        rate = 0;
                    } else {
                        rate = branches.getJSONObject(i).getDouble("rate");
                        BigDecimal bigDecimal;
                        if (rate > 0) {
                            bigDecimal = new BigDecimal(String.valueOf(rate)).setScale(1, BigDecimal.ROUND_FLOOR);
                        } else {
                            bigDecimal = new BigDecimal(String.valueOf(rate)).setScale(1, BigDecimal.ROUND_CEILING);
                        }

                        rate = bigDecimal.doubleValue();
                    }
                    double latitude = branches.getJSONObject(i).getDouble("latitude");
                    double longitude = branches.getJSONObject(i).getDouble("longitude");


                    String tablesPosition;
                    if (branches.getJSONObject(i).getString("tables_positions").equals("") ||
                            branches.getJSONObject(i).getString("tables_positions").equals("null")) {
                        tablesPosition = "null";
                    } else {
                        tablesPosition = branches.getJSONObject(i).getString("tables_positions");
                    }
                    int type;
                    if (branches.getJSONObject(i).getString("type").equals("") ||
                            branches.getJSONObject(i).getString("type").equals("null")) {
                        type = -1;
                    } else {
                        type = branches.getJSONObject(i).getInt("type");
                    }
                    int maxGuests;
                    if (branches.getJSONObject(i).getString("max_guests").equals("") ||
                            branches.getJSONObject(i).getString("max_guests").equals("null")) {
                        maxGuests = 0;
                    } else {
                        maxGuests = branches.getJSONObject(i).getInt("max_guests");
                    }
                    Branches branch = new Branches(id, name, address, 0123, brandId, false, brandName, openHour, closeHour,
                            brandPhoto, brandCategory, distance, latitude, longitude, tablesPosition, rate, false, type, maxGuests);
                    branchesList.add(branch);
                }
            } else {
                JSONObject jsonResponse = new JSONObject(response);
                JSONArray branches = jsonResponse.getJSONArray("branches");
                int branchesLength = branches.length();
                for (int i = 0; i < branchesLength; i++) {
                    int id = branches.getJSONObject(i).getInt("id");
                    String name;
                    if (branches.getJSONObject(i).getString("arabic_name").equals("") ||
                            branches.getJSONObject(i).getString("arabic_name").equals("null")) {
                        name = context.getString(R.string.list_empty);
                    } else {
                        name = convertDigits(branches.getJSONObject(i).getString("arabic_name"));
                    }
                    String address = "";
//                    if (branches.getJSONObject(i).getString("arabic_address").equals("") ||
//                            branches.getJSONObject(i).getString("arabic_address").equals("null")) {
//                        address = "no data available";
//                    } else {
//                        address = convertDigits(branches.getJSONObject(i).getString("arabic_address"));
//                    }

                    int brandId = branches.getJSONObject(i).getInt("brand_id");
                    String brandName;
                    if (branches.getJSONObject(i).getString("brand_name").equals("") ||
                            branches.getJSONObject(i).getString("brand_name").equals("null")) {
                        brandName = "no data available";
                    } else {
                        brandName = branches.getJSONObject(i).getString("brand_name");
                    }
                    String openHour;
                    if (branches.getJSONObject(i).getString("open_time").equals("") ||
                            branches.getJSONObject(i).getString("open_time").equals("null")) {
                        openHour = "";
                    } else {
                        openHour = convertDigits(branches.getJSONObject(i).getString("open_time"));
                    }
                    String closeHour;
                    if (branches.getJSONObject(i).getString("close_time").equals("") ||
                            branches.getJSONObject(i).getString("close_time").equals("null")) {
                        closeHour = "";
                    } else {
                        closeHour = convertDigits(branches.getJSONObject(i).getString("close_time"));
                    }
                    String brandPhoto;
                    if (branches.getJSONObject(i).getString("brand_photo").equals("") ||
                            branches.getJSONObject(i).getString("brand_photo").equals("null")) {
                        brandPhoto = "null";
                    } else {
                        brandPhoto = branches.getJSONObject(i).getString("brand_photo");
                    }
                    String brandCategory = branches.getJSONObject(i).getString("arabic_brand_category");
                    double distance;
                    if (branches.getJSONObject(i).getString("distance").equals("")
                            || branches.getJSONObject(i).getString("distance").equals("null")) {
                        distance = 0;
                    } else {
                        distance = branches.getJSONObject(i).getDouble("distance");
                        BigDecimal bigDecimal;
                        if (distance > 0) {
                            bigDecimal = new BigDecimal(String.valueOf(distance)).setScale(2, BigDecimal.ROUND_FLOOR);
                        } else {
                            bigDecimal = new BigDecimal(String.valueOf(distance)).setScale(2, BigDecimal.ROUND_CEILING);
                        }

                        distance = bigDecimal.doubleValue();
                    }
                    double rate;
                    if (branches.getJSONObject(i).getString("rate").equals("")
                            || branches.getJSONObject(i).getString("rate").equals("null")) {
                        rate = 0;
                    } else {
                        rate = branches.getJSONObject(i).getDouble("rate");
                        BigDecimal bigDecimal;
                        if (rate > 0) {
                            bigDecimal = new BigDecimal(String.valueOf(rate)).setScale(1, BigDecimal.ROUND_FLOOR);
                        } else {
                            bigDecimal = new BigDecimal(String.valueOf(rate)).setScale(1, BigDecimal.ROUND_CEILING);
                        }

                        rate = bigDecimal.doubleValue();
                    }
                    double latitude = branches.getJSONObject(i).getDouble("latitude");
                    double longitude = branches.getJSONObject(i).getDouble("longitude");


                    String tablesPosition;
                    if (branches.getJSONObject(i).getString("tables_positions").equals("") ||
                            branches.getJSONObject(i).getString("tables_positions").equals("null")) {
                        tablesPosition = "null";
                    } else {
                        tablesPosition = branches.getJSONObject(i).getString("tables_positions");
                    }
                    int type;
                    if (branches.getJSONObject(i).getString("type").equals("") ||
                            branches.getJSONObject(i).getString("type").equals("null")) {
                        type = -1;
                    } else {
                        type = branches.getJSONObject(i).getInt("type");
                    }

                    int maxGuests;
                    if (branches.getJSONObject(i).getString("max_guests").equals("") ||
                            branches.getJSONObject(i).getString("max_guests").equals("null")) {
                        maxGuests = 0;
                    } else {
                        maxGuests = branches.getJSONObject(i).getInt("max_guests");
                    }
                    Branches branch = new Branches(id, name, address, 0123, brandId, false, brandName, openHour, closeHour,
                            brandPhoto, brandCategory, distance, latitude, longitude, tablesPosition, rate, false, type, maxGuests);
                    branchesList.add(branch);
                }
            }

            view.hideProgress();
            if (branchesList != null && branchesList.size() > 0) {
                if (isLogged) {
                    // compare between branches and favorites ;
                    view.loadData(getFinalBranches(branchesList, favBranchesList));
                } else {
                    view.loadData(branchesList);
                }
            } else {
                view.showEmptyText(context.getString(R.string.list_empty));
            }
        } else if (which == 2) {
            Log.e("categories response", response);
            ArrayList<BranchCategories> branchCategoriesArrayList = new ArrayList<>();
            if (MySingleton.getmInstance(context).getAppLang().equals(context.getString(R.string.settings_language_english_value))) {
                JSONObject jsonResponse = new JSONObject(response);
                JSONArray categories = jsonResponse.getJSONArray("categories");
                for (int i = 0; i < categories.length(); i++) {
                    int id = categories.getJSONObject(i).getInt("id");
                    String name;
                    if (categories.getJSONObject(i).getString("name").equals("") ||
                            categories.getJSONObject(i).getString("name").equals("null")) {
                        name = "no data available";
                    } else {
                        name = categories.getJSONObject(i).getString("name");
                    }
                    String photo;
                    if (categories.getJSONObject(i).getString("photo").equals("") ||
                            categories.getJSONObject(i).getString("photo").equals("null")) {
                        photo = "";
                    } else {
                        photo = categories.getJSONObject(i).getString("photo");
                    }
                    branchCategoriesArrayList.add(new BranchCategories(id, name, photo));
                }
            } else {
                JSONObject jsonResponse = new JSONObject(response);
                JSONArray categories = jsonResponse.getJSONArray("categories");
                for (int i = 0; i < categories.length(); i++) {
                    int id = categories.getJSONObject(i).getInt("id");
                    String name;
                    if (categories.getJSONObject(i).getString("arabic_name").equals("") ||
                            categories.getJSONObject(i).getString("arabic_name").equals("null")) {
                        name = "no data available";
                    } else {
                        name = categories.getJSONObject(i).getString("arabic_name");
                    }
                    String photo;
                    if (categories.getJSONObject(i).getString("photo").equals("") ||
                            categories.getJSONObject(i).getString("photo").equals("null")) {
                        photo = "";
                    } else {
                        photo = categories.getJSONObject(i).getString("photo");
                    }
                    branchCategoriesArrayList.add(new BranchCategories(id, name, photo));
                }

            }


            viewMain.getData(branchCategoriesArrayList);
        } else if (which == 3) {
            Log.e("orders response", response);
            ArrayList<UserOrders> ordersData = new ArrayList<>();
            JSONObject jsonObject = new JSONObject(response);
            JSONArray orders = jsonObject.getJSONArray("orders");
            if (MySingleton.getmInstance(context).getAppLang().equals(context.getString(R.string.settings_language_english_value))) {
                for (int i = 0; i < orders.length(); i++) {
                    int id = orders.getJSONObject(i).getInt("id");
                    int branchId = orders.getJSONObject(i).getInt("branch_id");
                    int statusInt = orders.getJSONObject(i).getInt("status");
                    String status;
                    // pending , inProgress , ready, delivered , canceled
                    if (statusInt == 0) {
                        status = "pending";
                    } else if (statusInt == 1) {
                        status = "in progress";
                    } else if (statusInt == 2) {
                        status = "ready";
                    } else if (statusInt == 3) {
                        status = "delivered";
                    } else if (statusInt == 4) {
                        status = "canceled";
                    } else {
                        status = "";
                    }
                    String[] createdAt = orders.getJSONObject(i).getString("created_at").split(" ");
                    String date = createdAt[0];
                    String time = createdAt[1];
                    String branchName;
                    if (orders.getJSONObject(i).getString("branch_name").equals("") ||
                            orders.getJSONObject(i).getString("branch_name").equals("null")) {
                        branchName = "";
                    } else {
                        branchName = orders.getJSONObject(i).getString("branch_name");
                    }
                    double price = 0;
                    if (orders.getJSONObject(i).get("price") != null) {
                        price = orders.getJSONObject(i).getDouble("price");
                    }
                    String brandPhoto;
                    if (orders.getJSONObject(i).getString("brand_photo").equals("") ||
                            orders.getJSONObject(i).getString("brand_photo").equals("null")) {
                        brandPhoto = "";
                    } else {
                        brandPhoto = orders.getJSONObject(i).getString("brand_photo");
                    }
                    int remain;
                    if (orders.getJSONObject(i).getString("countdown").equals("") ||
                            orders.getJSONObject(i).getString("countdown").equals("null")) {
                        remain = 0;
                    } else {
                        remain = orders.getJSONObject(i).getInt("countdown");
                    }
                    int prepTime;
                    if (orders.getJSONObject(i).getString("order_time").equals("") ||
                            orders.getJSONObject(i).getString("order_time").equals("null")) {
                        prepTime = 0;
                    } else {
                        prepTime = orders.getJSONObject(i).getInt("order_time");
                    }
                    UserOrders userOrders = new UserOrders(id, branchId,
                            branchName, status, price, brandPhoto, date, time, remain, prepTime);
                    if (prevORnew == 0) {
                        // prev orders
                        if (userOrders.getStatus().equals("delivered") || userOrders.getStatus().equals("canceled")) {
                            ordersData.add(userOrders);
                        }

                    } else if (prevORnew == 1) {
                        // new orders
                        if (userOrders.getStatus().equals("pending") || userOrders.getStatus().equals("in progress") || userOrders.getStatus().equals("ready")) {
                            ordersData.add(userOrders);
                        }
                    }
                }
            } else {
                for (int i = 0; i < orders.length(); i++) {
                    int id = orders.getJSONObject(i).getInt("id");
                    int branchId = orders.getJSONObject(i).getInt("branch_id");
                    int statusInt = orders.getJSONObject(i).getInt("status");
                    String status;
                    if (statusInt == 0) {
                        status = "pending";
                    } else if (statusInt == 1) {
                        status = "in progress";
                    } else if (statusInt == 2) {
                        status = "delivered";
                    } else if (statusInt == 3) {
                        status = "canceled";
                    } else {
                        status = "";
                    }
                    String[] createdAt = orders.getJSONObject(i).getString("created_at").split(" ");
                    String date = createdAt[0];
                    String time = createdAt[1];
                    String branchName;
                    if (orders.getJSONObject(i).getString("branch_arabic_name").equals("") ||
                            orders.getJSONObject(i).getString("branch_arabic_name").equals("null")) {
                        branchName = "";
                    } else {
                        branchName = orders.getJSONObject(i).getString("branch_arabic_name");
                    }
                    double price = orders.getJSONObject(i).getDouble("price");
                    String brandPhoto;
                    if (orders.getJSONObject(i).getString("brand_photo").equals("") ||
                            orders.getJSONObject(i).getString("brand_photo").equals("null")) {
                        brandPhoto = "";
                    } else {
                        brandPhoto = orders.getJSONObject(i).getString("brand_photo");
                    }
                    int remain;
                    if (orders.getJSONObject(i).getString("countdown").equals("") ||
                            orders.getJSONObject(i).getString("countdown").equals("null")) {
                        remain = 0;
                    } else {
                        remain = orders.getJSONObject(i).getInt("countdown");
                    }
                    int prepTime;
                    if (orders.getJSONObject(i).getString("order_time").equals("") ||
                            orders.getJSONObject(i).getString("order_time").equals("null")) {
                        prepTime = 0;
                    } else {
                        prepTime = orders.getJSONObject(i).getInt("order_time");
                    }
                    UserOrders userOrders = new UserOrders(id, branchId,
                            branchName, status, price, brandPhoto, date, time, remain, prepTime);
                    if (prevORnew == 0) {
                        // prev orders
                        if (userOrders.getStatus().equals("delivered") || userOrders.getStatus().equals("canceled")) {
                            ordersData.add(userOrders);
                        }

                    } else if (prevORnew == 1) {
                        // new orders
                        if (userOrders.getStatus().equals("pending") || userOrders.getStatus().equals("in progress")) {
                            ordersData.add(userOrders);
                        }
                    }
                }
            }
            viewOrders.hideProgress();
            if (ordersData != null && ordersData.size() > 0) {
                viewOrders.loadData(ordersData);
            } else {
                viewOrders.showEmptyText(context.getString(R.string.list_empty));
            }

        } else if (which == 4) {
            Log.e("favBranch", response);
            if (MySingleton.getmInstance(context).getAppLang().equals(context.getString(R.string.settings_language_english_value))) {
                JSONObject jsonResponse = new JSONObject(response);
                JSONArray branches = jsonResponse.getJSONArray("branches");
                int branchesLength = branches.length();
                for (int i = 0; i < branchesLength; i++) {
                    int id = branches.getJSONObject(i).getInt("id");
                    String name;
                    if (branches.getJSONObject(i).getString("name").equals("") ||
                            branches.getJSONObject(i).getString("name").equals("null")) {
                        name = context.getString(R.string.list_empty);
                    } else {
                        name = branches.getJSONObject(i).getString("name");
                    }
                    String address = "";
//                    if (branches.getJSONObject(i).getString("address").equals("") ||
//                            branches.getJSONObject(i).getString("address").equals("null")) {
//                        address = context.getString(R.string.list_empty);
//                    } else {
//                        address = branches.getJSONObject(i).getString("address");
//                    }

                    int brandId = branches.getJSONObject(i).getInt("brand_id");
                    String brandName;
                    if (branches.getJSONObject(i).getString("brand_name").equals("") ||
                            branches.getJSONObject(i).getString("brand_name").equals("null")) {
                        brandName = context.getString(R.string.list_empty);
                    } else {
                        brandName = branches.getJSONObject(i).getString("brand_name");
                    }
                    String openHour;
                    if (branches.getJSONObject(i).getString("open_time").equals("") ||
                            branches.getJSONObject(i).getString("open_time").equals("null")) {
                        openHour = "";
                    } else {
                        openHour = branches.getJSONObject(i).getString("open_time");
                    }
                    String closeHour;
                    if (branches.getJSONObject(i).getString("close_time").equals("") ||
                            branches.getJSONObject(i).getString("close_time").equals("null")) {
                        closeHour = "";
                    } else {
                        closeHour = branches.getJSONObject(i).getString("close_time");
                    }
                    String brandPhoto;
                    if (branches.getJSONObject(i).getString("brand_photo").equals("") ||
                            branches.getJSONObject(i).getString("brand_photo").equals("null")) {
                        brandPhoto = "null";
                    } else {
                        brandPhoto = branches.getJSONObject(i).getString("brand_photo");
                    }
                    String brandCategory = branches.getJSONObject(i).getString("brand_category");

                    double latitude = branches.getJSONObject(i).getDouble("latitude");
                    double longitude = branches.getJSONObject(i).getDouble("longitude");
                    double rate;
                    if (branches.getJSONObject(i).getString("rate").equals("")
                            || branches.getJSONObject(i).getString("rate").equals("null")) {
                        rate = 0;
                    } else {
                        rate = branches.getJSONObject(i).getDouble("rate");
                        BigDecimal bigDecimal;
                        if (rate > 0) {
                            bigDecimal = new BigDecimal(String.valueOf(rate)).setScale(1, BigDecimal.ROUND_FLOOR);
                        } else {
                            bigDecimal = new BigDecimal(String.valueOf(rate)).setScale(1, BigDecimal.ROUND_CEILING);
                        }

                        rate = bigDecimal.doubleValue();
                    }

                    int type;
                    if (branches.getJSONObject(i).getString("type").equals("") ||
                            branches.getJSONObject(i).getString("type").equals("null")) {
                        type = -1;
                    } else {
                        type = branches.getJSONObject(i).getInt("type");
                    }

                    String tablesPosition = branches.getJSONObject(i).getString("tables_positions");
                    int maxGuests;
                    if (branches.getJSONObject(i).getString("max_guests").equals("") ||
                            branches.getJSONObject(i).getString("max_guests").equals("null")) {
                        maxGuests = 0;
                    } else {
                        maxGuests = branches.getJSONObject(i).getInt("max_guests");
                    }

                    favBranchesList.add(new Branches(id, name, address, 0123, brandId, false, brandName, openHour, closeHour, brandPhoto, brandCategory, -1, latitude, longitude, tablesPosition, rate, true, type, maxGuests));
                }
            } else {
                JSONObject jsonResponse = new JSONObject(response);
                JSONArray branches = jsonResponse.getJSONArray("branches");
                int branchesLength = branches.length();
                for (int i = 0; i < branchesLength; i++) {
                    int id = branches.getJSONObject(i).getInt("id");
                    String name;
                    if (branches.getJSONObject(i).getString("arabic_name").equals("") ||
                            branches.getJSONObject(i).getString("arabic_name").equals("null")) {
                        name = context.getString(R.string.list_empty);
                    } else {
                        name = branches.getJSONObject(i).getString("arabic_name");
                    }
                    String address="";
//                    if (branches.getJSONObject(i).getString("arabic_address").equals("") ||
//                            branches.getJSONObject(i).getString("arabic_address").equals("null")) {
//                        address = context.getString(R.string.list_empty);
//                    } else {
//                        address = branches.getJSONObject(i).getString("arabic_address");
//                    }

                    int brandId = branches.getJSONObject(i).getInt("brand_id");
                    String brandName;
                    if (branches.getJSONObject(i).getString("brand_name").equals("") ||
                            branches.getJSONObject(i).getString("brand_name").equals("null")) {
                        brandName = context.getString(R.string.list_empty);
                    } else {
                        brandName = branches.getJSONObject(i).getString("brand_name");
                    }
                    String openHour;
                    if (branches.getJSONObject(i).getString("open_time").equals("") ||
                            branches.getJSONObject(i).getString("open_time").equals("null")) {
                        openHour = "";
                    } else {
                        openHour = branches.getJSONObject(i).getString("open_time");
                    }
                    String closeHour;
                    if (branches.getJSONObject(i).getString("close_time").equals("") ||
                            branches.getJSONObject(i).getString("close_time").equals("null")) {
                        closeHour = "";
                    } else {
                        closeHour = branches.getJSONObject(i).getString("close_time");
                    }

                    String brandPhoto;
                    if (branches.getJSONObject(i).getString("brand_photo").equals("") ||
                            branches.getJSONObject(i).getString("brand_photo").equals("null")) {
                        brandPhoto = "null";
                    } else {
                        brandPhoto = branches.getJSONObject(i).getString("brand_photo");
                    }
                    String brandCategory = branches.getJSONObject(i).getString("arabic_brand_category");

                    double latitude = branches.getJSONObject(i).getDouble("latitude");
                    double longitude = branches.getJSONObject(i).getDouble("longitude");
                    double rate;
                    if (branches.getJSONObject(i).getString("rate").equals("")
                            || branches.getJSONObject(i).getString("rate").equals("null")) {
                        rate = 0;
                    } else {
                        rate = branches.getJSONObject(i).getDouble("rate");
                        BigDecimal bigDecimal;
                        if (rate > 0) {
                            bigDecimal = new BigDecimal(String.valueOf(rate)).setScale(1, BigDecimal.ROUND_FLOOR);
                        } else {
                            bigDecimal = new BigDecimal(String.valueOf(rate)).setScale(1, BigDecimal.ROUND_CEILING);
                        }

                        rate = bigDecimal.doubleValue();
                    }

                    int type;
                    if (branches.getJSONObject(i).getString("type").equals("") ||
                            branches.getJSONObject(i).getString("type").equals("null")) {
                        type = -1;
                    } else {
                        type = branches.getJSONObject(i).getInt("type");
                    }

                    String tablesPosition = branches.getJSONObject(i).getString("tables_positions");
                    int maxGuests;
                    if (branches.getJSONObject(i).getString("max_guests").equals("") ||
                            branches.getJSONObject(i).getString("max_guests").equals("null")) {
                        maxGuests = 0;
                    } else {
                        maxGuests = branches.getJSONObject(i).getInt("max_guests");
                    }
                    favBranchesList.add(new Branches(id, name, address, 0123, brandId, false, brandName, openHour, closeHour, brandPhoto, brandCategory, -1, latitude, longitude, tablesPosition, rate, true, type, maxGuests));
                }
            }

            if (!isLogged) {
                if (page > 1) viewFav.hideLoadingMoreProgress();
                else viewFav.hideProgress();
                if (favBranchesList != null && favBranchesList.size() > 0) {
                    viewFav.loadBranchesData(favBranchesList);
                } else {
                    if (page <= 1)
                        viewFav.showEmptyText(context.getString(R.string.list_empty));
                }
            }
        } else if (which == 5) {
            Log.e("favMenu", response);
            ArrayList<MenuItemDetails> favMenuItemDetails = new ArrayList<>();
            if (MySingleton.getmInstance(context).getAppLang().equals(context.getString(R.string.settings_language_english_value))) {
                JSONObject jsonResponse = new JSONObject(response);
                JSONArray menus = jsonResponse.getJSONArray("menus");
                int branchesLength = menus.length();
                for (int i = 0; i < branchesLength; i++) {
                    int id = menus.getJSONObject(i).getInt("id");
                    int branchId = menus.getJSONObject(i).getInt("branch_id");
                    String name = menus.getJSONObject(i).getString("name");
                    String recipe = menus.getJSONObject(i).getString("recipe");
                    String photo;
                    if (menus.getJSONObject(i).getString("photo").equals("") ||
                            menus.getJSONObject(i).getString("photo").equals("null")) {
                        photo = "null";
                    } else {
                        photo = menus.getJSONObject(i).getString("photo");
                    }
                    String time = menus.getJSONObject(i).getString("time");
                    int statusInt = menus.getJSONObject(i).getInt("status");
                    boolean isAvailable;
                    if (statusInt == 1) {
                        isAvailable = true;
                    } else {
                        isAvailable = false;
                    }
                    int menuCategoryId = menus.getJSONObject(i).getInt("menu_category_id");
                    double price = menus.getJSONObject(i).getDouble("price");
                    favMenuItemDetails.add(new MenuItemDetails(id, branchId, name, recipe, photo, time, isAvailable, menuCategoryId, true, price));
                }
            } else {
                JSONObject jsonResponse = new JSONObject(response);
                JSONArray menus = jsonResponse.getJSONArray("menus");
                int branchesLength = menus.length();
                for (int i = 0; i < branchesLength; i++) {
                    int id = menus.getJSONObject(i).getInt("id");
                    int branchId = menus.getJSONObject(i).getInt("branch_id");
                    String name = menus.getJSONObject(i).getString("arabic_name");
                    String recipe = menus.getJSONObject(i).getString("arabic_recipe");
                    String photo;
                    if (menus.getJSONObject(i).getString("photo").equals("") ||
                            menus.getJSONObject(i).getString("photo").equals("null")) {
                        photo = "null";
                    } else {
                        photo = menus.getJSONObject(i).getString("photo");
                    }
                    String time = menus.getJSONObject(i).getString("time");
                    int statusInt = menus.getJSONObject(i).getInt("status");
                    boolean isAvailable;
                    if (statusInt == 1) {
                        isAvailable = true;
                    } else {
                        isAvailable = false;
                    }
                    int menuCategoryId = menus.getJSONObject(i).getInt("menu_category_id");
                    double price = menus.getJSONObject(i).getDouble("price");
                    favMenuItemDetails.add(new MenuItemDetails(id, branchId, name, recipe, photo, time, isAvailable, menuCategoryId, true, price));
                }
            }
            viewFav.hideProgress();
            if (favMenuItemDetails != null && favMenuItemDetails.size() > 0) {
                viewFav.loadMenusData(favMenuItemDetails);
            } else {
                viewFav.showEmptyText(context.getString(R.string.list_empty));
            }
        } else if (which == 6) {
            Log.e("unReviewedBranches", response);
            ArrayList<Branches> unReviewedBranches = new ArrayList<>();
            Log.e("ssssssss", response);
            JSONObject jsonResponse = new JSONObject(response);
            JSONArray branches = jsonResponse.getJSONArray("branches");
            int branchesLength = branches.length();
            if (MySingleton.getmInstance(context).getAppLang().equals(context.getString(R.string.settings_language_english_value))) {
                for (int i = 0; i < branchesLength; i++) {
                    int id = branches.getJSONObject(i).getInt("id");
                    String name;
                    if (branches.getJSONObject(i).getString("name").equals("") ||
                            branches.getJSONObject(i).getString("name").equals("null")) {
                        name = context.getString(R.string.list_empty);
                    } else {
                        name = branches.getJSONObject(i).getString("name");
                    }
                    String photo;
                    if (branches.getJSONObject(i).getString("photo").equals("") ||
                            branches.getJSONObject(i).getString("photo").equals("null")) {
                        photo = "";
                    } else {
                        photo = branches.getJSONObject(i).getString("photo");
                    }

                    int orderId = branches.getJSONObject(i).getInt("order_id");
                    unReviewedBranches.add(new Branches(id, name, photo, orderId));
                }
            } else {
                for (int i = 0; i < branchesLength; i++) {
                    int id = branches.getJSONObject(i).getInt("id");
                    String name;
                    if (branches.getJSONObject(i).getString("arabic_name").equals("") ||
                            branches.getJSONObject(i).getString("arabic_name").equals("null")) {
                        name = context.getString(R.string.list_empty);
                    } else {
                        name = branches.getJSONObject(i).getString("arabic_name");
                    }
                    String photo;
                    if (branches.getJSONObject(i).getString("photo").equals("") ||
                            branches.getJSONObject(i).getString("photo").equals("null")) {
                        photo = "";
                    } else {
                        photo = branches.getJSONObject(i).getString("photo");
                    }

                    int orderId = branches.getJSONObject(i).getInt("order_id");
                    unReviewedBranches.add(new Branches(id, name, photo, orderId));
                }
            }
            if (unReviewedBranches != null && unReviewedBranches.size() > 0) {
                view.showUnRevDialog(getFinal(unReviewedBranches), 0);
            }
        } else if (which == 7) {
            int x = index + 1;
            view.onBranchReviewed(dialog, x);
        } else if (which == 8) {
            Log.e("favBranch", response);
            if (MySingleton.getmInstance(context).getAppLang().equals(context.getString(R.string.settings_language_english_value))) {
                JSONObject jsonResponse = new JSONObject(response);
                JSONArray branches = jsonResponse.getJSONArray("branches");
                int branchesLength = branches.length();
                for (int i = 0; i < branchesLength; i++) {
                    int id = branches.getJSONObject(i).getInt("id");
                    String name;
                    if (branches.getJSONObject(i).getString("name").equals("") ||
                            branches.getJSONObject(i).getString("name").equals("null")) {
                        name = context.getString(R.string.list_empty);
                    } else {
                        name = branches.getJSONObject(i).getString("name");
                    }
                    String address="";
//                    if (branches.getJSONObject(i).getString("address").equals("") ||
//                            branches.getJSONObject(i).getString("address").equals("null")) {
//                        address = context.getString(R.string.list_empty);
//                    } else {
//                        address = branches.getJSONObject(i).getString("address");
//                    }

                    int brandId = branches.getJSONObject(i).getInt("brand_id");
                    String brandName;
                    if (branches.getJSONObject(i).getString("brand_name").equals("") ||
                            branches.getJSONObject(i).getString("brand_name").equals("null")) {
                        brandName = context.getString(R.string.list_empty);
                    } else {
                        brandName = branches.getJSONObject(i).getString("brand_name");
                    }
                    String openHour;
                    if (branches.getJSONObject(i).getString("open_time").equals("") ||
                            branches.getJSONObject(i).getString("open_time").equals("null")) {
                        openHour = "";
                    } else {
                        openHour = branches.getJSONObject(i).getString("open_time");
                    }
                    String closeHour;
                    if (branches.getJSONObject(i).getString("close_time").equals("") ||
                            branches.getJSONObject(i).getString("close_time").equals("null")) {
                        closeHour = "";
                    } else {
                        closeHour = branches.getJSONObject(i).getString("close_time");
                    }
                    String brandPhoto;
                    if (branches.getJSONObject(i).getString("brand_photo").equals("") ||
                            branches.getJSONObject(i).getString("brand_photo").equals("null")) {
                        brandPhoto = "null";
                    } else {
                        brandPhoto = branches.getJSONObject(i).getString("brand_photo");
                    }
                    String brandCategory = branches.getJSONObject(i).getString("brand_category");

                    double latitude = branches.getJSONObject(i).getDouble("latitude");
                    double longitude = branches.getJSONObject(i).getDouble("longitude");
                    double rate;
                    if (branches.getJSONObject(i).getString("rate").equals("")
                            || branches.getJSONObject(i).getString("rate").equals("null")) {
                        rate = 0;
                    } else {
                        rate = branches.getJSONObject(i).getDouble("rate");
                        BigDecimal bigDecimal;
                        if (rate > 0) {
                            bigDecimal = new BigDecimal(String.valueOf(rate)).setScale(1, BigDecimal.ROUND_FLOOR);
                        } else {
                            bigDecimal = new BigDecimal(String.valueOf(rate)).setScale(1, BigDecimal.ROUND_CEILING);
                        }

                        rate = bigDecimal.doubleValue();
                    }

                    int type;
                    if (branches.getJSONObject(i).getString("type").equals("") ||
                            branches.getJSONObject(i).getString("type").equals("null")) {
                        type = -1;
                    } else {
                        type = branches.getJSONObject(i).getInt("type");
                    }

                    String tablesPosition = branches.getJSONObject(i).getString("tables_positions");
                    int maxGuests;
                    if (branches.getJSONObject(i).getString("max_guests").equals("") ||
                            branches.getJSONObject(i).getString("max_guests").equals("null")) {
                        maxGuests = 0;
                    } else {
                        maxGuests = branches.getJSONObject(i).getInt("max_guests");
                    }
                    favBranchesList.add(new Branches(id, name, address, 0123, brandId, false, brandName, openHour, closeHour, brandPhoto, brandCategory, -1, latitude, longitude, tablesPosition, rate, true, type, maxGuests));
                }
            } else {
                JSONObject jsonResponse = new JSONObject(response);
                JSONArray branches = jsonResponse.getJSONArray("branches");
                int branchesLength = branches.length();
                for (int i = 0; i < branchesLength; i++) {
                    int id = branches.getJSONObject(i).getInt("id");
                    String name;
                    if (branches.getJSONObject(i).getString("arabic_name").equals("") ||
                            branches.getJSONObject(i).getString("arabic_name").equals("null")) {
                        name = context.getString(R.string.list_empty);
                    } else {
                        name = branches.getJSONObject(i).getString("arabic_name");
                    }
                    String address="";
//                    if (branches.getJSONObject(i).getString("arabic_address").equals("") ||
//                            branches.getJSONObject(i).getString("arabic_address").equals("null")) {
//                        address = context.getString(R.string.list_empty);
//                    } else {
//                        address = branches.getJSONObject(i).getString("arabic_address");
//                    }

                    int brandId = branches.getJSONObject(i).getInt("brand_id");
                    String brandName;
                    if (branches.getJSONObject(i).getString("brand_name").equals("") ||
                            branches.getJSONObject(i).getString("brand_name").equals("null")) {
                        brandName = context.getString(R.string.list_empty);
                    } else {
                        brandName = branches.getJSONObject(i).getString("brand_name");
                    }
                    String openHour;
                    if (branches.getJSONObject(i).getString("open_time").equals("") ||
                            branches.getJSONObject(i).getString("open_time").equals("null")) {
                        openHour = "";
                    } else {
                        openHour = branches.getJSONObject(i).getString("open_time");
                    }
                    String closeHour;
                    if (branches.getJSONObject(i).getString("close_time").equals("") ||
                            branches.getJSONObject(i).getString("close_time").equals("null")) {
                        closeHour = "";
                    } else {
                        closeHour = branches.getJSONObject(i).getString("close_time");
                    }

                    String brandPhoto;
                    if (branches.getJSONObject(i).getString("brand_photo").equals("") ||
                            branches.getJSONObject(i).getString("brand_photo").equals("null")) {
                        brandPhoto = "null";
                    } else {
                        brandPhoto = branches.getJSONObject(i).getString("brand_photo");
                    }
                    String brandCategory = branches.getJSONObject(i).getString("arabic_brand_category");

                    double latitude = branches.getJSONObject(i).getDouble("latitude");
                    double longitude = branches.getJSONObject(i).getDouble("longitude");
                    double rate;
                    if (branches.getJSONObject(i).getString("rate").equals("")
                            || branches.getJSONObject(i).getString("rate").equals("null")) {
                        rate = 0;
                    } else {
                        rate = branches.getJSONObject(i).getDouble("rate");
                        BigDecimal bigDecimal;
                        if (rate > 0) {
                            bigDecimal = new BigDecimal(String.valueOf(rate)).setScale(1, BigDecimal.ROUND_FLOOR);
                        } else {
                            bigDecimal = new BigDecimal(String.valueOf(rate)).setScale(1, BigDecimal.ROUND_CEILING);
                        }

                        rate = bigDecimal.doubleValue();
                    }

                    int type;
                    if (branches.getJSONObject(i).getString("type").equals("") ||
                            branches.getJSONObject(i).getString("type").equals("null")) {
                        type = -1;
                    } else {
                        type = branches.getJSONObject(i).getInt("type");
                    }

                    String tablesPosition = branches.getJSONObject(i).getString("tables_positions");

                    int maxGuests;
                    if (branches.getJSONObject(i).getString("max_guests").equals("") ||
                            branches.getJSONObject(i).getString("max_guests").equals("null")) {
                        maxGuests = 0;
                    } else {
                        maxGuests = branches.getJSONObject(i).getInt("max_guests");
                    }

                    favBranchesList.add(new Branches(id, name, address, 0123, brandId, false, brandName, openHour, closeHour, brandPhoto, brandCategory, -1, latitude, longitude, tablesPosition, rate, true, type, maxGuests));
                }
            }

//            if (!isLogged) {
            viewFav.hideProgress();
            if (favBranchesList != null && favBranchesList.size() > 0) {
                viewFav.loadBranchesData(favBranchesList);
            } else {
                if (page == 1) {
                    viewFav.showEmptyText(context.getString(R.string.list_empty));
                }
            }
//            }
        }

    }

    private String convertDigits(String string) {
        String rslt = "";
        for (int i = 0; i < string.length(); i++) {
            switch (string.charAt(i)) {
                case '0':
                    rslt += "٠";
                    break;
                case '1':
                    rslt += "١";
                    break;
                case '2':
                    rslt += "٢";
                    break;
                case '3':
                    rslt += "٣";
                    break;
                case '4':
                    rslt += "٤";
                    break;
                case '5':
                    rslt += "٥";
                    break;
                case '6':
                    rslt += "٦";
                    break;
                case '7':
                    rslt += "٧";
                    break;
                case '8':
                    rslt += "٨";
                    break;
                case '9':
                    rslt += "٩";
                    break;
                default:
                    rslt += string.charAt(i);
            }
        }
        return rslt;
    }

    /*
     *  filter unReviewed branches
     */
    private ArrayList<Branches> getFinal(ArrayList<Branches> unReviewed) {
        for (int i = 0; i < unReviewed.size(); i++) {
            if (i != unReviewed.size() - 1) {
                int j = i + 1;
                while (j < unReviewed.size()) {
                    if (unReviewed.get(i).getName().equals(unReviewed.get(j).getName())) {
                        Log.e("names", unReviewed.get(i).getName() + "////" + unReviewed.get(i).getName());
                        try {
                            model.addReview(this, unReviewed.get(j).getId(), -1, false, "", unReviewed.get(j).getOrderId());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        unReviewed.remove(j);
                    } else {
                        j++;
                    }
                }
            }
        }

        return unReviewed;
    }

    @Override
    public void onFail(VolleyError error, int type) {
        if (type == 1) {
            view.hideProgress();
            if (error instanceof NoConnectionError || error instanceof TimeoutError) {
                Toast.makeText(context, context.getString(R.string.internet_error), Toast.LENGTH_LONG).show();
            } else if (error instanceof ServerError) {
                Toast.makeText(context, context.getString(R.string.server_error), Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(context, context.getString(R.string.wrong), Toast.LENGTH_LONG).show();
            }
        } else if (type == 2) {
            model.getCategories(this);
        } else if (type == 3) {
            viewOrders.hideProgress();
            if (error instanceof NoConnectionError || error instanceof TimeoutError) {
                Toast.makeText(context, context.getString(R.string.internet_error), Toast.LENGTH_LONG).show();
            } else if (error instanceof ServerError) {
                Toast.makeText(context, context.getString(R.string.server_error), Toast.LENGTH_SHORT).show();
            }
        } else if (type == 4) {
            if (!MySingleton.getmInstance(context).isLoggedIn()) {
                if (error instanceof NoConnectionError || error instanceof TimeoutError) {
                    Toast.makeText(context, context.getString(R.string.internet_error), Toast.LENGTH_LONG).show();
                }
            }
        } else if (type == 5) {
            viewFav.hideProgress();
            if (error instanceof NoConnectionError || error instanceof TimeoutError) {
                Toast.makeText(context, context.getString(R.string.internet_error), Toast.LENGTH_LONG).show();
            }
        } else if (type == 6) {

        } else if (type == 7) {
            int x = index + 1;
            view.onBranchReviewed(dialog, x);
        } else if (type == 8) {
//            if (!MySingleton.getmInstance(context).isLoggedIn()) {
            if (page > 1) viewFav.hideLoadingMoreProgress();
            else
                viewFav.hideProgress();
            if (error instanceof NoConnectionError || error instanceof TimeoutError) {
                Toast.makeText(context, context.getString(R.string.internet_error), Toast.LENGTH_LONG).show();
            }
//            }
        }
    }


}
