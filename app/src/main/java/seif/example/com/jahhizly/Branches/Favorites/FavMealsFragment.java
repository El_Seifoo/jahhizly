package seif.example.com.jahhizly.Branches.Favorites;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;

import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;
import me.anwarshahriar.calligrapher.Calligrapher;
import seif.example.com.jahhizly.Branches.MakeOrder.Branches;
import seif.example.com.jahhizly.Branches.BranchesMVP;
import seif.example.com.jahhizly.Branches.BranchesModel;
import seif.example.com.jahhizly.Branches.BranchesPresenter;
import seif.example.com.jahhizly.Menus.MenuItemDetails;
import seif.example.com.jahhizly.R;
import seif.example.com.jahhizly.Utils.MySingleton;

/**
 * Created by seif on 11/3/2017.
 */
public class FavMealsFragment extends Fragment implements BranchesMVP.viewFav {
    View view;
    RecyclerView recyclerView;
    FavoriteMealsAdapter adapter;
    RecyclerView.LayoutManager layout;
    BranchesMVP.presenterFav presenter;
    MKLoader loading;
    TextView emptyText;

    public FavMealsFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fav_branch_meal_fragment, container, false);
        design();
        Calligrapher calligrapher = new Calligrapher(getContext());
        if (MySingleton.getmInstance(getContext()).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            calligrapher.setFont(getActivity(), "fonts/Arabic/arabic_font.ttf", true);
        } else {
            calligrapher.setFont(getActivity(), "fonts/English/Roboto_Regular.ttf", true);
        }
        loading = (MKLoader) view.findViewById(R.id.loading);
        emptyText = (TextView) view.findViewById(R.id.empty_txt);
        presenter = new BranchesPresenter(this, getContext(), new BranchesModel(getContext()));
        recyclerView = (RecyclerView) view.findViewById(R.id.fav_branch_meal_rec_view);
        adapter = new FavoriteMealsAdapter(MySingleton.getmInstance(getContext()).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? true : false);
        int screenSize = getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK;
        switch (screenSize) {
            case Configuration.SCREENLAYOUT_SIZE_XLARGE:
                layout = new GridLayoutManager(getContext(), 5, LinearLayoutManager.VERTICAL, false);
                break;
            case Configuration.SCREENLAYOUT_SIZE_LARGE:
                layout = new GridLayoutManager(getContext(), 3, LinearLayoutManager.VERTICAL, false);
                break;
            case Configuration.SCREENLAYOUT_SIZE_NORMAL:
                layout = new GridLayoutManager(getContext(), 2, LinearLayoutManager.VERTICAL, false);
                break;
            default:
                layout = new GridLayoutManager(getContext(), 2, LinearLayoutManager.VERTICAL, false);
        }
        recyclerView.setLayoutManager(layout);
        recyclerView.setHasFixedSize(true);
        presenter.onFavoritesCreated(1, 0);
        return view;
    }

    private float getScreenWidth() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        float scaleFactor = displayMetrics.density;
        return (width / scaleFactor);
    }

    private void design() {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        view.setLayoutParams(params);
    }

    @Override
    public void loadBranchesData(ArrayList<Branches> branches) {

    }

    @Override
    public void loadMenusData(ArrayList<MenuItemDetails> menuItems) {
        if (emptyText.getVisibility() == View.VISIBLE) {
            emptyText.setVisibility(View.GONE);
        }
        adapter.setFavorites(menuItems);
        ScaleInAnimationAdapter scaleInAnimationAdapter = new ScaleInAnimationAdapter(adapter);
        scaleInAnimationAdapter.setFirstOnly(false);
        recyclerView.setAdapter(scaleInAnimationAdapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void showProgress() {
//        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
//                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        loading.setVisibility(View.VISIBLE);
//        container.setForeground(new ColorDrawable(Color.parseColor("#66000000")));
    }

    @Override
    public void hideProgress() {
//        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        loading.setVisibility(View.GONE);
//        container.setForeground(new ColorDrawable(Color.TRANSPARENT));
    }

    @Override
    public void showLoadingMoreProgress() {

    }

    @Override
    public void hideLoadingMoreProgress() {

    }

    @Override
    public void showEmptyText(String message) {
        emptyText.setText(message);
    }
}
