package seif.example.com.jahhizly.Branches.Favorites;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Space;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.like.LikeButton;
import com.like.OnLikeListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import me.anwarshahriar.calligrapher.Calligrapher;
import seif.example.com.jahhizly.Branches.MakeOrder.Branches;
import seif.example.com.jahhizly.Other.CircleTransform;
import seif.example.com.jahhizly.R;
import seif.example.com.jahhizly.Utils.MySingleton;
import seif.example.com.jahhizly.Utils.URLs;

/**
 * Created by seif on 11/3/2017.
 */
public class FavoriteBranchesAdapter extends RecyclerView.Adapter<FavoriteBranchesAdapter.Holder> {
    ArrayList<Branches> branches = new ArrayList<>();
    private ListItemClickListener mOnClickListener;
    private boolean flag;

    public FavoriteBranchesAdapter() {
    }

    public FavoriteBranchesAdapter(ListItemClickListener mOnClickListener, boolean flag) {
        this.mOnClickListener = mOnClickListener;
        this.flag = flag;
    }

    public void setFavorites(ArrayList<Branches> branches) {
        if (branches != null && branches.size() > 0) {
            this.branches = branches;
            notifyDataSetChanged();
        }
    }

    public boolean isEmpty() {
        if (branches != null && branches.size() > 0) {
            return false;
        } else return true;
    }

    public void add(ArrayList<Branches> branches) {
        for (int i = 0; i < branches.size(); i++) {
            this.branches.add(branches.get(i));
        }
        notifyDataSetChanged();
    }

    public interface ListItemClickListener {
        void onListItemClick(int position);
    }

    @Override
    public FavoriteBranchesAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Holder(LayoutInflater.from(parent.getContext()).
                inflate(R.layout.branch_recview_item, parent, false));
    }

    @Override
    public void onBindViewHolder(final FavoriteBranchesAdapter.Holder holder, final int position) {
        Calligrapher calligrapher = new Calligrapher(holder.itemView.getContext());
        if (MySingleton.getmInstance(holder.itemView.getContext()).getAppLang().equals(holder.itemView.getContext().getString(R.string.settings_language_arabic_value))) {
            calligrapher.setFont(holder.itemView, "fonts/Arabic/arabic_font.ttf");
        } else {
            calligrapher.setFont(holder.itemView, "fonts/English/Roboto_Regular.ttf");
        }
        Glide.with(holder.itemView.getContext()).load(URLs.ROOT_URL + branches.get(position).getBrandPhoto())
                .error(R.mipmap.restaurant_icon_menu)
                .crossFade()
                .thumbnail(0.5f)
                .bitmapTransform(new CircleTransform(holder.itemView.getContext()))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.brandLogo);
        holder.branchName.setText(flag ? convertDigits(" " + branches.get(position).getName() + " ") : " " + branches.get(position).getName() + " ");
        if (branches.get(position).getRate() >= 0 && branches.get(position).getRate() <= 2) {
            holder.branchRate.setBackground(holder.itemView.getResources().getDrawable(R.mipmap.con_rate_restaurant));
        } else if (branches.get(position).getRate() > 2 && branches.get(position).getRate() <= 3.75) {
            holder.branchRate.setBackground(holder.itemView.getResources().getDrawable(R.mipmap.con_rate_restaurant_y));
        } else if (branches.get(position).getRate() > 3.75 && branches.get(position).getRate() <= 5) {
            holder.branchRate.setBackground(holder.itemView.getResources().getDrawable(R.mipmap.con_rate_restaurant_g));
        }
        holder.branchRate.setText(flag ? convertDigits(String.valueOf(branches.get(position).getRate())) : String.valueOf(branches.get(position).getRate()));
        holder.branchCategory.setText(flag ? convertDigits(" " + String.valueOf(branches.get(position).getBrandCategory()) + " ") : " " + String.valueOf(branches.get(position).getBrandCategory()) + " ");
        if (branches.get(position).getOpenHour().equals("") || branches.get(position).getCloseHour().equals("")) {
            holder.openCloseTime.setVisibility(View.GONE);
        } else {
            String[] open = branches.get(position).getOpenHour().split(":");
            String[] close = branches.get(position).getCloseHour().split(":");
            holder.openCloseTime.setText(flag ? convertDigits(" " + open[0] + ":" + open[1] + " - " + close[0] + ":" + close[1] + " ") : " " + open[0] + ":" + open[1] + " - " + close[0] + ":" + close[1] + " ");
        }
        holder.branchDistance.setVisibility(View.GONE);
        holder.space.setVisibility(View.VISIBLE);
        if (branches.get(position).getType() == 0) {
            holder.branchTypeMakeOrder.setVisibility(View.VISIBLE);
            holder.branchTypeBookTable.setVisibility(View.VISIBLE);
        } else if (branches.get(position).getType() == 1) {
            holder.branchTypeMakeOrder.setVisibility(View.GONE);
            holder.branchTypeBookTable.setVisibility(View.VISIBLE);
        } else if (branches.get(position).getType() == 2) {
            holder.branchTypeMakeOrder.setVisibility(View.VISIBLE);
            holder.branchTypeBookTable.setVisibility(View.GONE);
        }
        if (branches.get(position).isChecked()) {
            holder.addToFavorites.setLiked(true);
        } else {
            holder.addToFavorites.setLiked(false);
        }
        holder.addToFavorites.setOnLikeListener(new OnLikeListener() {
            @Override
            public void liked(LikeButton likeButton) {
            }

            @Override
            public void unLiked(LikeButton likeButton) {
                removeFromFavorites(branches.get(position).getId(), holder, position);
            }
        });
    }


    private void removeFromFavorites(int id, final Holder holder, final int position) {
        Map<String, String> param = new HashMap<>();
        param.put("branch_id", String.valueOf(id));
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                URLs.REMOVE_FAV_BRANCH,
                new JSONObject(param),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("remove branch", response.toString());
                        try {
                            String status = response.getString("status");
                            if (status.equals("success")) {
                                Toast.makeText(holder.itemView.getContext(), holder.itemView.getContext().getString(R.string.branch_removed), Toast.LENGTH_LONG).show();
                                branches.get(position).setChecked(false);
                                branches.remove(position);
                                notifyDataSetChanged();
                            } else {
                                Toast.makeText(holder.itemView.getContext(), holder.itemView.getContext().getString(R.string.remove_fav_error), Toast.LENGTH_LONG).show();
                                holder.addToFavorites.setLiked(true);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                holder.addToFavorites.setLiked(true);
                if (error instanceof NoConnectionError || error instanceof TimeoutError) {
                    Toast.makeText(holder.itemView.getContext(), holder.itemView.getContext().getString(R.string.internet_error), Toast.LENGTH_LONG).show();
                    return;
                } else if (error instanceof ServerError) {
                    Toast.makeText(holder.itemView.getContext(), holder.itemView.getContext().getString(R.string.server_error), Toast.LENGTH_LONG).show();
                    return;
                } else {
                    Toast.makeText(holder.itemView.getContext(), holder.itemView.getContext().getString(R.string.wrong), Toast.LENGTH_LONG).show();
                    return;
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                String token = "Bearer " + MySingleton.getmInstance(holder.itemView.getContext()).UserKey();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Authorization", token);
                return headers;
            }
        };
        MySingleton.getmInstance(holder.itemView.getContext()).addToRQ(jsonObjectRequest);
    }


    @Override
    public int getItemCount() {
        return (null != branches ? branches.size() : 0);
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView branchName, branchDistance, branchCategory, branchRate, openCloseTime;
        LikeButton addToFavorites;
        ImageView brandLogo, branchTypeBookTable, branchTypeMakeOrder;
        Space space;

        public Holder(View itemView) {
            super(itemView);
            branchName = (TextView) itemView.findViewById(R.id.branch_name);
            branchCategory = (TextView) itemView.findViewById(R.id.branch_category);
            branchDistance = (TextView) itemView.findViewById(R.id.branch_distance);
            openCloseTime = (TextView) itemView.findViewById(R.id.branch_open_close_time);
            branchRate = (TextView) itemView.findViewById(R.id.branch_rate);
            addToFavorites = (LikeButton) itemView.findViewById(R.id.add_branch_to_fav_list);
            brandLogo = (ImageView) itemView.findViewById(R.id.branch_img);
            branchTypeBookTable = (ImageView) itemView.findViewById(R.id.branch_type_book_table);
            branchTypeMakeOrder = (ImageView) itemView.findViewById(R.id.branch_type_make_order);
            space = (Space) itemView.findViewById(R.id.space);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int clickedPosition = getAdapterPosition();
            mOnClickListener.onListItemClick(clickedPosition);
        }
    }

    private String convertDigits(String string) {
        if (string==null) return null;
        String rslt = "";
        for (int i = 0; i < string.length(); i++) {
            switch (string.charAt(i)) {
                case '0':
                    rslt += "٠";
                    break;
                case '1':
                    rslt += "١";
                    break;
                case '2':
                    rslt += "٢";
                    break;
                case '3':
                    rslt += "٣";
                    break;
                case '4':
                    rslt += "٤";
                    break;
                case '5':
                    rslt += "٥";
                    break;
                case '6':
                    rslt += "٦";
                    break;
                case '7':
                    rslt += "٧";
                    break;
                case '8':
                    rslt += "٨";
                    break;
                case '9':
                    rslt += "٩";
                    break;
                default:
                    rslt += string.charAt(i);
            }
        }
        return rslt;
    }

}
