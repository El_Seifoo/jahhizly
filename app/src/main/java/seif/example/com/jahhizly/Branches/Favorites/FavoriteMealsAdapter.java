package seif.example.com.jahhizly.Branches.Favorites;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.like.LikeButton;
import com.like.OnLikeListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import me.anwarshahriar.calligrapher.Calligrapher;
import seif.example.com.jahhizly.Menus.MenuItemDetails;
import seif.example.com.jahhizly.Other.CircleTransform;
import seif.example.com.jahhizly.R;
import seif.example.com.jahhizly.Utils.MySingleton;
import seif.example.com.jahhizly.Utils.URLs;

/**
 * Created by seif on 11/3/2017.
 */
public class FavoriteMealsAdapter extends RecyclerView.Adapter<FavoriteMealsAdapter.Holder> {

    private ArrayList<MenuItemDetails> menuItem = new ArrayList<>();
    private boolean flag;

    public FavoriteMealsAdapter(boolean flag) {
        this.flag = flag;
    }

    public void setFavorites(ArrayList<MenuItemDetails> menuItem) {
        if (menuItem != null && menuItem.size() > 0) {
            this.menuItem = menuItem;
            notifyDataSetChanged();
        }
    }

    @Override
    public FavoriteMealsAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Holder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.menu_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(final FavoriteMealsAdapter.Holder holder, final int position) {
        Calligrapher calligrapher = new Calligrapher(holder.itemView.getContext());
        if (MySingleton.getmInstance(holder.itemView.getContext()).getAppLang().equals(holder.itemView.getContext().getString(R.string.settings_language_arabic_value))) {
            calligrapher.setFont(holder.itemView, "fonts/Arabic/arabic_font.ttf");
        } else {
            calligrapher.setFont(holder.itemView, "fonts/English/Roboto_Regular.ttf");
        }
        Glide.with(holder.itemView.getContext().getApplicationContext())
                .load(URLs.ROOT_URL + menuItem.get(position).getPhoto())
                .error(R.mipmap.restaurant_meal_icon)
                .thumbnail(0.5f)
                .bitmapTransform(new CircleTransform(holder.itemView.getContext().getApplicationContext()))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.menuItemImg);

        holder.menuItemName.setText(flag ? convertDigits(" " + menuItem.get(position).getName() + " ") : " " + menuItem.get(position).getName() + " ");
        holder.menuItemPrice.setText(flag ? convertDigits(" " + menuItem.get(position).getPrice() + holder.itemView.getContext().getString(R.string.currency)) : " " + menuItem.get(position).getPrice() + holder.itemView.getContext().getString(R.string.currency));
        if (menuItem.get(position).isChecked()) {
            holder.addToFavorites.setLiked(true);
        } else {
            holder.addToFavorites.setLiked(false);
        }
        holder.addToFavorites.setOnLikeListener(new OnLikeListener() {
            @Override
            public void liked(LikeButton likeButton) {
            }

            @Override
            public void unLiked(LikeButton likeButton) {
                removeFromFavorites(menuItem.get(position).getId(), holder, position);
            }
        });
    }

    private void removeFromFavorites(int id, final Holder holder, final int position) {
        Map<String, String> param = new HashMap<>();
        param.put("menu_id", String.valueOf(id));
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                URLs.REMOVE_FAV_MENU,
                new JSONObject(param),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("remove meal response", String.valueOf(response));
                        try {
                            String status = response.getString("status");
                            if (status.equals("success")) {
                                Toast.makeText(holder.itemView.getContext(), holder.itemView.getContext().getString(R.string.meal_removed), Toast.LENGTH_LONG).show();
                                menuItem.get(position).setChecked(false);
                                menuItem.remove(position);
                                notifyDataSetChanged();
                            } else {
                                Toast.makeText(holder.itemView.getContext().getApplicationContext(), holder.itemView.getContext().getString(R.string.remove_fav_error), Toast.LENGTH_LONG).show();
                                holder.addToFavorites.setLiked(true);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        holder.addToFavorites.setLiked(true);
                        if (error instanceof AuthFailureError) {
                            Toast.makeText(holder.itemView.getContext(), "Cant add to favorites, Login first", Toast.LENGTH_LONG).show();
                            return;
                        } else if (error instanceof NoConnectionError) {
                            Toast.makeText(holder.itemView.getContext(), "no Internet connection!", Toast.LENGTH_LONG).show();
                            return;
                        } else if (error instanceof ServerError) {
                            Toast.makeText(holder.itemView.getContext(), "something went wrong, try later!", Toast.LENGTH_LONG).show();
                            return;
                        } else if (error instanceof TimeoutError) {
                            Toast.makeText(holder.itemView.getContext(), "Time out error", Toast.LENGTH_LONG).show();
                            return;
                        } else {
                            Toast.makeText(holder.itemView.getContext(), "Error, try later!", Toast.LENGTH_LONG).show();
                            return;
                        }
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                String token = "Bearer " + MySingleton.getmInstance(holder.itemView.getContext()).UserKey();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Authorization", token);
                return headers;
            }
        };
        MySingleton.getmInstance(holder.itemView.getContext()).addToRQ(jsonObjectRequest);
    }


    @Override
    public int getItemCount() {
        return (null != menuItem ? menuItem.size() : 0);
    }

    public class Holder extends RecyclerView.ViewHolder {
        ImageView menuItemImg;
        LikeButton addToFavorites;
        TextView menuItemName, menuItemPrice;

        public Holder(View itemView) {
            super(itemView);
            menuItemImg = (ImageView) itemView.findViewById(R.id.menu_item_img);
            addToFavorites = (LikeButton) itemView.findViewById(R.id.add_meal_to_fav_list);
            menuItemName = (TextView) itemView.findViewById(R.id.menu_item_name);
            menuItemPrice = (TextView) itemView.findViewById(R.id.menu_item_price);
        }
    }

    private String convertDigits(String string) {
        String rslt = "";
        for (int i = 0; i < string.length(); i++) {
            switch (string.charAt(i)) {
                case '0':
                    rslt += "٠";
                    break;
                case '1':
                    rslt += "١";
                    break;
                case '2':
                    rslt += "٢";
                    break;
                case '3':
                    rslt += "٣";
                    break;
                case '4':
                    rslt += "٤";
                    break;
                case '5':
                    rslt += "٥";
                    break;
                case '6':
                    rslt += "٦";
                    break;
                case '7':
                    rslt += "٧";
                    break;
                case '8':
                    rslt += "٨";
                    break;
                case '9':
                    rslt += "٩";
                    break;
                default:
                    rslt += string.charAt(i);
            }
        }
        return rslt;
    }

}
