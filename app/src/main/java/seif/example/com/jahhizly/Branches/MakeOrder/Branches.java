package seif.example.com.jahhizly.Branches.MakeOrder;

import java.io.Serializable;

/**
 * Created by seif on 10/23/2017.
 */
public class Branches implements Serializable {
    private int id;
    private String name;
    private String address;
    private int phone;
    private int brandId;
    private boolean isOpen;
    private String brandName;
    private String openHour;
    private String closeHour;
    private String brandPhoto;
    private String brandCategory;
    private double distance;
    private double latitude;
    private double longitude;
    private String tablesPosition;
    private double rate;
    private boolean isChecked = false;
    private int type;
    private int orderId;
    private int maxGuests;

    public Branches() {
    }

    public Branches(int id, String name, String brandPhoto, int orderId) {
        this.id = id;
        this.name = name;
        this.brandPhoto = brandPhoto;
        this.orderId = orderId;
    }

    //  name .. adress .. lat .. long .. photo


    public Branches(String name, String address, double latitude, double longitude) {
        this.name = name;
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Branches(int id, String name, String address, int phone, int brandId, boolean isOpen,
                    String brandName, String openHour, String closeHour, String brandPhoto, String brandCategory,
                    double distance, double latitude, double longitude, String tablesPosition, double rate, boolean isChecked, int type, int maxGuests) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.phone = phone;
        this.brandId = brandId;
        this.isOpen = isOpen;
        this.brandName = brandName;
        this.openHour = openHour;
        this.closeHour = closeHour;
        this.brandPhoto = brandPhoto;
        this.brandCategory = brandCategory;
        this.distance = distance;
        this.latitude = latitude;
        this.longitude = longitude;
        this.tablesPosition = tablesPosition;
        this.rate = rate;
        this.isChecked = isChecked;
        this.type = type;
        this.maxGuests = maxGuests;
    }

    public int getMaxGuests() {
        return maxGuests;
    }

    public void setMaxGuests(int maxGuests) {
        this.maxGuests = maxGuests;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    public int getBrandId() {
        return brandId;
    }

    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getOpenHour() {
        return openHour;
    }

    public void setOpenHour(String openHour) {
        this.openHour = openHour;
    }

    public String getCloseHour() {
        return closeHour;
    }

    public void setCloseHour(String closeHour) {
        this.closeHour = closeHour;
    }

    public String getBrandPhoto() {
        return brandPhoto;
    }

    public void setBrandPhoto(String brandPhoto) {
        this.brandPhoto = brandPhoto;
    }

    public String getBrandCategory() {
        return brandCategory;
    }

    public void setBrandCategory(String brandCategory) {
        this.brandCategory = brandCategory;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getTablesPosition() {
        return tablesPosition;
    }

    public void setTablesPosition(String tablesPosition) {
        this.tablesPosition = tablesPosition;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }
}
