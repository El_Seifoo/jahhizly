package seif.example.com.jahhizly.Branches.MakeOrder;

import android.content.Context;
import android.media.MediaPlayer;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.like.LikeButton;
import com.like.OnLikeListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import me.anwarshahriar.calligrapher.Calligrapher;
import seif.example.com.jahhizly.Other.CircleTransform;
import seif.example.com.jahhizly.R;
import seif.example.com.jahhizly.Utils.MySingleton;
import seif.example.com.jahhizly.Utils.URLs;

/**
 * Created by seif on 10/24/2017.
 */
public class MakeOrderAdapter extends RecyclerView.Adapter<MakeOrderAdapter.Holder> {

    private ArrayList<Branches> branchData = new ArrayList<>();
    final private ListItemClickListener mOnClickListener;
    private boolean flag;


    public void setBranchesData(ArrayList<Branches> branchData) {
        if (branchData != null && branchData.size() > 0) {
            this.branchData = branchData;
            notifyDataSetChanged();
        }
    }

    public void clear() {
        branchData.clear();
        notifyDataSetChanged();
    }

    public MakeOrderAdapter(ListItemClickListener listener, boolean flag) {
        mOnClickListener = listener;
        this.flag = flag;
    }

    public interface ListItemClickListener {
        void onListItemClick(int position);
    }

    @Override
    public MakeOrderAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        int menuItemId = R.layout.branch_recview_item;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachToParentImmediately = false;
        View view = inflater.inflate(menuItemId, parent, shouldAttachToParentImmediately);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(final MakeOrderAdapter.Holder holder, final int position) {
        Calligrapher calligrapher = new Calligrapher(holder.itemView.getContext());
        if (MySingleton.getmInstance(holder.itemView.getContext()).getAppLang().equals(holder.itemView.getContext().getString(R.string.settings_language_arabic_value))) {
            calligrapher.setFont(holder.itemView, "fonts/Arabic/arabic_font.ttf");
        } else {
            calligrapher.setFont(holder.itemView, "fonts/English/Roboto_Regular.ttf");
        }
        Glide.with(holder.itemView.getContext()).load(URLs.ROOT_URL + branchData.get(position).getBrandPhoto())
                .error(R.mipmap.restaurant_icon_menu)
                .crossFade()
                .thumbnail(1f)
                .bitmapTransform(new CircleTransform(holder.itemView.getContext()))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.brandLogo);
        holder.branchName.setText(flag ? convertDigits(" " + branchData.get(position).getName() + " ") : " " + branchData.get(position).getName() + " ");
        if (branchData.get(position).getRate() >= 0 && branchData.get(position).getRate() <= 2) {
            holder.branchRate.setBackground(holder.itemView.getResources().getDrawable(R.mipmap.con_rate_restaurant));
        } else if (branchData.get(position).getRate() > 2 && branchData.get(position).getRate() <= 3.75) {
            holder.branchRate.setBackground(holder.itemView.getResources().getDrawable(R.mipmap.con_rate_restaurant_y));
        } else if (branchData.get(position).getRate() > 3.75 && branchData.get(position).getRate() <= 5) {
            holder.branchRate.setBackground(holder.itemView.getResources().getDrawable(R.mipmap.con_rate_restaurant_g));
        }
        holder.branchRate.setText(String.valueOf(branchData.get(position).getRate()));
        if (branchData.get(position).getOpenHour().equals("") || branchData.get(position).getCloseHour().equals("")) {
            holder.openCloseTime.setVisibility(View.GONE);
        } else {
            String[] open = branchData.get(position).getOpenHour().split(":");
            String[] close = branchData.get(position).getCloseHour().split(":");
            holder.openCloseTime.setText(flag ? convertDigits(" " + open[0] + ":" + open[1] + " - " + close[0] + ":" + close[1] + " ") : " " + open[0] + ":" + open[1] + " - " + close[0] + ":" + close[1] + " ");
        }
        holder.branchDistance.setText(flag ? convertDigits(" " + branchData.get(position).getDistance() + " " + holder.itemView.getContext().getString(R.string.dist)) : " " + branchData.get(position).getDistance() + " " + holder.itemView.getContext().getString(R.string.dist));
        holder.branchCategory.setText(flag ? convertDigits(" " + branchData.get(position).getBrandCategory() + " ") : " " + branchData.get(position).getBrandCategory() + " ");
        if (branchData.get(position).getType() == 0) {
            holder.branchTypeMakeOrder.setVisibility(View.VISIBLE);
            holder.branchTypeBookTable.setVisibility(View.VISIBLE);
        } else if (branchData.get(position).getType() == 1) {
            holder.branchTypeMakeOrder.setVisibility(View.GONE);
            holder.branchTypeBookTable.setVisibility(View.VISIBLE);
        } else if (branchData.get(position).getType() == 2) {
            holder.branchTypeMakeOrder.setVisibility(View.VISIBLE);
            holder.branchTypeBookTable.setVisibility(View.GONE);
        }

        Log.e("branch id " + position, branchData.get(position).getId() + "");

        if (branchData.get(position).isChecked()) {
            holder.addToFavorites.setLiked(true);
        } else {
            holder.addToFavorites.setLiked(false);
        }
        holder.addToFavorites.setOnLikeListener(new OnLikeListener() {
            @Override
            public void liked(LikeButton likeButton) {
                // Added to your branches favorite list
                final MediaPlayer mediaPlayer = MediaPlayer.create(holder.itemView.getContext(), R.raw.sound_effect);
                mediaPlayer.start();
                addToFavorites(branchData.get(position).getId(), holder, position);
            }

            @Override
            public void unLiked(LikeButton likeButton) {
                removeFromFavorites(branchData.get(position).getId(), holder, position);
            }
        });
    }

    private void addToFavorites(int id, final Holder holder, final int position) {
        Map<String, String> param = new HashMap<>();
        param.put("branch_id", String.valueOf(id));
        final JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                URLs.ADD_FAV_BRANCH,
                new JSONObject(param),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("add branch", response.toString());
                        try {
                            String status = response.getString("status");
                            if (status.equals("success")) {
                                Toast.makeText(holder.itemView.getContext(), holder.itemView.getContext().getString(R.string.branch_added), Toast.LENGTH_LONG).show();
                                branchData.get(position).setChecked(true);
                            } else {
                                Toast.makeText(holder.itemView.getContext(), response.getString("desc"), Toast.LENGTH_LONG).show();
                                holder.addToFavorites.setLiked(false);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        holder.addToFavorites.setLiked(false);
                        if (error instanceof AuthFailureError) {
                            Toast.makeText(holder.itemView.getContext(), holder.itemView.getContext().getString(R.string.auth_failure_error_2), Toast.LENGTH_LONG).show();
                            return;
                        } else if (error instanceof NoConnectionError || error instanceof TimeoutError) {
                            Toast.makeText(holder.itemView.getContext(), holder.itemView.getContext().getString(R.string.internet_error), Toast.LENGTH_LONG).show();
                            return;
                        } else if (error instanceof ServerError) {
                            Toast.makeText(holder.itemView.getContext(), holder.itemView.getContext().getString(R.string.server_error), Toast.LENGTH_LONG).show();
                            return;
                        } else {
                            Toast.makeText(holder.itemView.getContext(), holder.itemView.getContext().getString(R.string.wrong), Toast.LENGTH_LONG).show();
                            return;
                        }
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                String token = "Bearer " + MySingleton.getmInstance(holder.itemView.getContext()).UserKey();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Authorization", token);
                return headers;
            }
        };
        MySingleton.getmInstance(holder.itemView.getContext()).addToRQ(jsonObjectRequest);
    }

    private void removeFromFavorites(int id, final Holder holder, final int position) {
        Map<String, String> param = new HashMap<>();
        param.put("branch_id", String.valueOf(id));
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                URLs.REMOVE_FAV_BRANCH,
                new JSONObject(param),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("remove branch", response.toString());
                        try {
                            String status = response.getString("status");
                            if (status.equals("success")) {
                                Toast.makeText(holder.itemView.getContext(), holder.itemView.getContext().getString(R.string.branch_removed), Toast.LENGTH_LONG).show();
                                branchData.get(position).setChecked(false);
                            } else {
                                Toast.makeText(holder.itemView.getContext(), holder.itemView.getContext().getString(R.string.remove_fav_error), Toast.LENGTH_LONG).show();
                                holder.addToFavorites.setLiked(true);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                holder.addToFavorites.setLiked(true);
                if (error instanceof NoConnectionError || error instanceof TimeoutError) {
                    Toast.makeText(holder.itemView.getContext(), holder.itemView.getContext().getString(R.string.internet_error), Toast.LENGTH_LONG).show();
                    return;
                } else if (error instanceof ServerError) {
                    Toast.makeText(holder.itemView.getContext(), holder.itemView.getContext().getString(R.string.server_error), Toast.LENGTH_LONG).show();
                    return;
                } else {
                    Toast.makeText(holder.itemView.getContext(), holder.itemView.getContext().getString(R.string.wrong), Toast.LENGTH_LONG).show();
                    return;
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                String token = "Bearer " + MySingleton.getmInstance(holder.itemView.getContext()).UserKey();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Authorization", token);
                return headers;
            }
        };
        MySingleton.getmInstance(holder.itemView.getContext()).addToRQ(jsonObjectRequest);
    }


    @Override
    public int getItemCount() {
        return (null != branchData ? branchData.size() : 0);
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView branchName, branchRate, branchDistance, openCloseTime, branchCategory;
        LikeButton addToFavorites;
        ImageView brandLogo, branchTypeBookTable, branchTypeMakeOrder;

        public Holder(View itemView) {
            super(itemView);
            branchName = (TextView) itemView.findViewById(R.id.branch_name);
            branchCategory = (TextView) itemView.findViewById(R.id.branch_category);
            branchRate = (TextView) itemView.findViewById(R.id.branch_rate);
            openCloseTime = (TextView) itemView.findViewById(R.id.branch_open_close_time);
            branchDistance = (TextView) itemView.findViewById(R.id.branch_distance);
            addToFavorites = (LikeButton) itemView.findViewById(R.id.add_branch_to_fav_list);
            brandLogo = (ImageView) itemView.findViewById(R.id.branch_img);
//            branchStatus = (ImageView) itemView.findViewById(R.id.branch_status);
            branchTypeBookTable = (ImageView) itemView.findViewById(R.id.branch_type_book_table);
            branchTypeMakeOrder = (ImageView) itemView.findViewById(R.id.branch_type_make_order);
            itemView.setOnClickListener(this);


        }

        @Override
        public void onClick(View view) {
            int clickedPosition = getAdapterPosition();
            mOnClickListener.onListItemClick(clickedPosition);
        }
    }

    private String convertDigits(String string) {
        String rslt = "";
        for (int i = 0; i < string.length(); i++) {
            switch (string.charAt(i)) {
                case '0':
                    rslt += "٠";
                    break;
                case '1':
                    rslt += "١";
                    break;
                case '2':
                    rslt += "٢";
                    break;
                case '3':
                    rslt += "٣";
                    break;
                case '4':
                    rslt += "٤";
                    break;
                case '5':
                    rslt += "٥";
                    break;
                case '6':
                    rslt += "٦";
                    break;
                case '7':
                    rslt += "٧";
                    break;
                case '8':
                    rslt += "٨";
                    break;
                case '9':
                    rslt += "٩";
                    break;
                default:
                    rslt += string.charAt(i);
            }
        }
        return rslt;
    }
}
