package seif.example.com.jahhizly.Branches.MakeOrder;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.tuyenmonkey.mkloader.MKLoader;

import org.json.JSONException;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;
import me.anwarshahriar.calligrapher.Calligrapher;
import seif.example.com.jahhizly.BookTable.BookTableActivity;
import seif.example.com.jahhizly.Branches.BranchesMVP;
import seif.example.com.jahhizly.Branches.BranchesModel;
import seif.example.com.jahhizly.Branches.BranchesPresenter;
import seif.example.com.jahhizly.Menus.MenusActivity;
import seif.example.com.jahhizly.Other.CircleTransform;
import seif.example.com.jahhizly.R;
import seif.example.com.jahhizly.Utils.MySingleton;
import seif.example.com.jahhizly.Utils.URLs;

import static android.content.Context.VIBRATOR_SERVICE;

/**
 * Created by seif on 10/23/2017.
 */
public class MakeOrderFragment extends Fragment implements BranchesMVP.view, MakeOrderAdapter.ListItemClickListener {
    private static View view;
    private int times = 0;

    public MakeOrderFragment() {
    }

    BranchesPresenter presenter;
    //    PullRefreshLayout pullRefreshLayout;
    RecyclerView branchesRecView;
    MakeOrderAdapter adapter;
    RecyclerView.LayoutManager layout;
    private ArrayList<String> category;
    private String sort;
    private String query;
    private String type;
    private String latitude;
    private String longitude;
    private boolean isBookTableOnly;
    private ArrayList<Branches> branchesArrayList;
    MKLoader loading;
    TextView emptyText;

    public void setReqData(ArrayList<String> category, String type, String latitude, String longitude, String sort, String key, boolean isBookTableOnly) {
        this.category = category;
        this.type = type;
        this.latitude = latitude;
        this.longitude = longitude;
        this.sort = sort;
        this.query = key;
        this.isBookTableOnly = isBookTableOnly;
    }

    @Override
    public void onDestroyView() {
        branchesRecView.setAdapter(null);
        super.onDestroyView();
    }

    public static View getFView() {
        if (view != null) {
            return view;
        } else {
            return null;
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_make_order, container, false);
        Calligrapher calligrapher = new Calligrapher(getContext());
        if (MySingleton.getmInstance(getContext()).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            calligrapher.setFont(getActivity(), "fonts/Arabic/arabic_font.ttf", true);
        } else {
            calligrapher.setFont(getActivity(), "fonts/English/Roboto_Regular.ttf", true);
        }
        times = 1;
        emptyText = (TextView) view.findViewById(R.id.empty_txt);
        loading = (MKLoader) view.findViewById(R.id.loading);
        presenter = new BranchesPresenter(this,
                getActivity().getApplicationContext(),
                new BranchesModel(getActivity().getApplicationContext()));
        if (MySingleton.getmInstance(getContext()).isLoggedIn()) {
            presenter.getUnReviewed();
        }
        try {
            presenter.CreateMakeOrder(category, type, latitude, longitude, sort, query, MySingleton.getmInstance(getContext()).isLoggedIn());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        branchesRecView = (RecyclerView) view.findViewById(R.id.branches_rec_view);
        adapter = new MakeOrderAdapter(this, MySingleton.getmInstance(getContext()).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? true : false);
        int screenSize = getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK;
        switch (screenSize) {
            case Configuration.SCREENLAYOUT_SIZE_XLARGE:
                layout = new GridLayoutManager(getContext(), 2, LinearLayoutManager.VERTICAL, false);
                break;
            case Configuration.SCREENLAYOUT_SIZE_LARGE:
                layout = new GridLayoutManager(getContext(), 2, LinearLayoutManager.VERTICAL, false);
                break;
            case Configuration.SCREENLAYOUT_SIZE_NORMAL:
                layout = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
                break;
            default:
                layout = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        }
        branchesRecView.setLayoutManager(layout);
        branchesRecView.setHasFixedSize(true);
        design();
        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        if (times == 0) {

        }
    }

    private void design() {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        view.setLayoutParams(params);
    }

    @Override
    public void loadData(ArrayList<Branches> branches) {
        branchesArrayList = branches;
        adapter.setBranchesData(branches);
        ScaleInAnimationAdapter scaleInAnimationAdapter = new ScaleInAnimationAdapter(adapter);
        scaleInAnimationAdapter.setFirstOnly(false);
        branchesRecView.setAdapter(scaleInAnimationAdapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void goMenus(int position, Branches branch, boolean isBookTableOnly) {
        Intent intent;

        if (isBookTableOnly) {
            intent = new Intent(getActivity().getApplicationContext(), BookTableActivity.class);
            intent.putExtra("BookTableBranch", branch);
        } else {
            intent = new Intent(getActivity().getApplicationContext(), MenusActivity.class);
            intent.putExtra("branchObject", branch);
        }
        startActivity(intent);
        if (MySingleton.getmInstance(getContext()).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            getActivity().overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        } else {
            getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }
    }

    @Override
    public void showProgress() {
//        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
//                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        loading.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
//        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        loading.setVisibility(View.GONE);
    }

    String comment = "";
    double rate;
    int y;
    ArrayList<Branches> unReviewed;

    @Override
    public void showUnRevDialog(final ArrayList<Branches> unReviewedBranches, final int i) {
        unReviewed = unReviewedBranches;
        y = i;
        final Dialog dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.un_review_branches);
        Glide.with(getContext()).load(URLs.ROOT_URL + unReviewedBranches.get(y).getBrandPhoto())
                .error(R.mipmap.restaurant_meal_icon)
                .thumbnail(0.5f)
                .bitmapTransform(new CircleTransform(getContext()))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into((ImageView) dialog.findViewById(R.id.branch_logo));
        ((TextView) dialog.findViewById(R.id.branch_name)).setText(unReviewedBranches.get(y).getName());
        ((EditText) dialog.findViewById(R.id.review_add_comment)).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                comment = editable.toString().trim();
            }
        });

        SimpleRatingBar simpleRatingBar = (SimpleRatingBar) dialog.findViewById(R.id.review_add_rate);
        simpleRatingBar.setOnRatingBarChangeListener(new SimpleRatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(SimpleRatingBar simpleRatingBar, float rating, boolean fromUser) {
                if (rating >= 0 && rating <= 2) {
                    simpleRatingBar.setFillColor(getResources().getColor(R.color.red_3));
                    simpleRatingBar.setPressedBorderColor(getResources().getColor(R.color.red_3));
                    simpleRatingBar.setPressedFillColor(getResources().getColor(R.color.red_3));
                    simpleRatingBar.setBorderColor(getResources().getColor(R.color.red_3));

                } else if (rating > 2 && rating <= 3.75) {
                    simpleRatingBar.setFillColor(getResources().getColor(R.color.yellow));
                    simpleRatingBar.setPressedBorderColor(getResources().getColor(R.color.yellow));
                    simpleRatingBar.setPressedFillColor(getResources().getColor(R.color.yellow));
                    simpleRatingBar.setBorderColor(getResources().getColor(R.color.yellow));
                } else if (rating > 3.75 && rating <= 5) {
                    simpleRatingBar.setFillColor(getResources().getColor(R.color.green_1));
                    simpleRatingBar.setPressedBorderColor(getResources().getColor(R.color.green_1));
                    simpleRatingBar.setPressedFillColor(getResources().getColor(R.color.green_1));
                    simpleRatingBar.setBorderColor(getResources().getColor(R.color.green_1));
                }
                rate = rating;
            }
        });
        ((Button) dialog.findViewById(R.id.review_submit_btn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    presenter.addReview(unReviewedBranches.get(y).getId(), (float) rate, true, comment, dialog, i, unReviewedBranches.get(y).getOrderId());
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        });

        ((Button) dialog.findViewById(R.id.dialog_cancel_btn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    presenter.addReview(unReviewedBranches.get(y).getId(), -1, false, "", dialog, i, unReviewedBranches.get(y).getOrderId());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    @Override
    public void showEmptyText(String message) {
        emptyText.setText(message);
    }

    @Override
    public void onBranchReviewed(Dialog dialog, int i) {
        if (dialog != null) {
            dialog.dismiss();
            if (i < unReviewed.size()) {
                showUnRevDialog(unReviewed, i);
            }

        }
    }


    @Override
    public void onListItemClick(int position) {
        presenter.onItemClicked(position, branchesArrayList.get(position), isBookTableOnly);
    }
}
