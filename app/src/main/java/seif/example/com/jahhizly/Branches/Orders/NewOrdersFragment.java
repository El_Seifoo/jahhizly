package seif.example.com.jahhizly.Branches.Orders;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;

import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;
import me.anwarshahriar.calligrapher.Calligrapher;
import seif.example.com.jahhizly.Branches.BranchesMVP;
import seif.example.com.jahhizly.Branches.BranchesModel;
import seif.example.com.jahhizly.Branches.BranchesPresenter;
import seif.example.com.jahhizly.Branches.Orders.PrevOrders.PrevOrdersDetailsActivity;
import seif.example.com.jahhizly.Branches.UserOrders;
import seif.example.com.jahhizly.R;
import seif.example.com.jahhizly.Utils.MySingleton;

/**
 * Created by seif on 11/2/2017.
 */
public class NewOrdersFragment extends Fragment implements BranchesMVP.viewOrders, OrdersAdapter.OrdersListItemClickListener {
    View view;
    RecyclerView prevRecView;
    OrdersAdapter adapter;
    RecyclerView.LayoutManager layout;
    BranchesMVP.presenterOrders presenter;
    ArrayList<UserOrders> ordersData;

    public NewOrdersFragment() {
    }

    MKLoader loading;
    TextView emptyText;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_new_prev_orders, container, false);
        design();
        Calligrapher calligrapher = new Calligrapher(getContext());
        if (MySingleton.getmInstance(getContext()).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            calligrapher.setFont(getActivity(), "fonts/Arabic/arabic_font.ttf", true);
        } else {
            calligrapher.setFont(getActivity(), "fonts/English/Roboto_Regular.ttf", true);
        }
        loading = (MKLoader) view.findViewById(R.id.loading);
        presenter = new BranchesPresenter(this, getContext(), new BranchesModel(getContext()));
        emptyText = (TextView) view.findViewById(R.id.empty_txt);
        prevRecView = (RecyclerView) view.findViewById(R.id.prev_new_rec_view);
        layout = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        int screenSize = getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK;
        switch (screenSize) {
            case Configuration.SCREENLAYOUT_SIZE_XLARGE:
                prevRecView.setLayoutManager(new GridLayoutManager(getContext(), 2, LinearLayoutManager.VERTICAL, false));
                break;
            case Configuration.SCREENLAYOUT_SIZE_LARGE:
                prevRecView.setLayoutManager(new GridLayoutManager(getContext(), 2, LinearLayoutManager.VERTICAL, false));
                break;
            case Configuration.SCREENLAYOUT_SIZE_NORMAL:
                prevRecView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                break;
            default:
                prevRecView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        }
        prevRecView.setHasFixedSize(true);
        adapter = new OrdersAdapter(this, MySingleton.getmInstance(getContext()).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? true : false);
        presenter.onOrdersCreated(1);
        return view;
    }

    private float getScreenWidth() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        float scaleFactor = displayMetrics.density;
        return (width / scaleFactor);
    }


    private void design() {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        view.setLayoutParams(params);
    }

    @Override
    public void onListItemClickListener(int position) {
        Intent intent = new Intent(getContext(), PrevOrdersDetailsActivity.class);
        intent.putExtra("menuId", ordersData.get(position).getId());
        intent.putExtra("brandLogo", ordersData.get(position).getPhoto());
        Log.e("logo 1", ordersData.get(position).getPhoto());
        startActivity(intent);
        if (MySingleton.getmInstance(getContext()).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            getActivity().overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        } else {
            getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }
    }

    @Override
    public void loadData(ArrayList<UserOrders> orders) {
        if (emptyText.getVisibility() == View.VISIBLE) {
            emptyText.setVisibility(View.GONE);
        }
        ordersData = orders;
        adapter.setOrders(orders);
        ScaleInAnimationAdapter scaleInAnimationAdapter = new ScaleInAnimationAdapter(adapter);
        scaleInAnimationAdapter.setFirstOnly(false);
        prevRecView.setAdapter(scaleInAnimationAdapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void showProgress() {
        loading.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        loading.setVisibility(View.GONE);
    }

    @Override
    public void showEmptyText(String message) {
        emptyText.setText(message);
    }
}
