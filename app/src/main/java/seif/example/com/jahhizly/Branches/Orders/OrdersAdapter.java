package seif.example.com.jahhizly.Branches.Orders;

import android.os.CountDownTimer;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import me.anwarshahriar.calligrapher.Calligrapher;
import seif.example.com.jahhizly.Branches.UserOrders;
import seif.example.com.jahhizly.R;
import seif.example.com.jahhizly.Utils.MySingleton;
import seif.example.com.jahhizly.Utils.URLs;

/**
 * Created by seif on 11/2/2017.
 */
public class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.Holder> {

    private ArrayList<UserOrders> userOrders;
    final private OrdersListItemClickListener listener;
    private boolean flag;

    public OrdersAdapter(OrdersListItemClickListener listener, boolean flag) {
        this.listener = listener;
        this.flag = flag;
    }

    public interface OrdersListItemClickListener {
        void onListItemClickListener(int position);
    }

    public void setOrders(ArrayList<UserOrders> userOrders) {
        if (userOrders != null && userOrders.size() > 0) {
            this.userOrders = userOrders;
            notifyDataSetChanged();
        }
    }

    @Override
    public OrdersAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Holder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.orders_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(final OrdersAdapter.Holder holder, final int position) {
        Calligrapher calligrapher = new Calligrapher(holder.itemView.getContext());
        if (MySingleton.getmInstance(holder.itemView.getContext()).getAppLang().equals(holder.itemView.getContext().getString(R.string.settings_language_arabic_value))) {
            calligrapher.setFont(holder.itemView, "fonts/Arabic/arabic_font.ttf");
        } else {
            calligrapher.setFont(holder.itemView, "fonts/English/Roboto_Regular.ttf");
        }
        Glide.with(holder.itemView.getContext()).load(URLs.ROOT_URL + userOrders.get(position).getPhoto())
                .error(R.mipmap.restaurant_icon_menu)
                .crossFade()
                .thumbnail(0.5f)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.brandLogo);
        holder.branchName.setText(flag ? convertDigits(userOrders.get(position).getBranchName()) : userOrders.get(position).getBranchName());
        holder.orderId.setText(flag ? convertDigits(holder.itemView.getContext().getString(R.string.order_num) + userOrders.get(position).getId() + "  ") : holder.itemView.getContext().getString(R.string.order_num) + userOrders.get(position).getId() + "  ");
        holder.itemPrice.setText(flag ? convertDigits("" + userOrders.get(position).getPrice()) : "" + userOrders.get(position).getPrice());
        String[] time = userOrders.get(position).getDate().split("-");
        holder.orderDate.setText(flag ? convertDigits(" " + time[2] + " / " + time[1] + " / " + time[0] + "  ") : " " + time[0] + " / " + time[1] + " / " + time[2] + "  ");

        if (userOrders.get(position).getStatus().equals("delivered") || userOrders.get(position).getStatus().equals("canceled")) {
            holder.itemTime.setVisibility(View.GONE);
        } else {
            int remainingTime = userOrders.get(position).getRemain() * 60 * 1000;
            Log.e("remaining", String.valueOf(remainingTime));
            new CountDownTimer(remainingTime, 1000) {
                @Override
                public void onTick(long countDownTime) {
                    Date date = new Date(countDownTime);
                    SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
                    formatter.setTimeZone(TimeZone.getTimeZone("GMT-2"));
                    String dateFormatted = formatter.format(date);
                    holder.itemTime.setText(dateFormatted);
                }

                @Override
                public void onFinish() {
                    holder.itemTime.setText(holder.itemView.getContext().getString(R.string.rdy));
                }
            }.start();
        }
    }


    @Override
    public int getItemCount() {
        return (null != userOrders ? userOrders.size() : 0);
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView brandLogo;
        TextView branchName, orderDate, itemTime, itemPrice, orderId;

        public Holder(View itemView) {
            super(itemView);
            brandLogo = (ImageView) itemView.findViewById(R.id.order_branch_logo);
            branchName = (TextView) itemView.findViewById(R.id.orders_branch_name);
            orderId = (TextView) itemView.findViewById(R.id.orders_num);
            orderDate = (TextView) itemView.findViewById(R.id.orders_date);
            itemTime = (TextView) itemView.findViewById(R.id.orders_item_time);
            itemPrice = (TextView) itemView.findViewById(R.id.orders_item_price);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            listener.onListItemClickListener(position);
        }
    }

    private String convertDigits(String string) {
        String rslt = "";
        for (int i = 0; i < string.length(); i++) {
            switch (string.charAt(i)) {
                case '0':
                    rslt += "٠";
                    break;
                case '1':
                    rslt += "١";
                    break;
                case '2':
                    rslt += "٢";
                    break;
                case '3':
                    rslt += "٣";
                    break;
                case '4':
                    rslt += "٤";
                    break;
                case '5':
                    rslt += "٥";
                    break;
                case '6':
                    rslt += "٦";
                    break;
                case '7':
                    rslt += "٧";
                    break;
                case '8':
                    rslt += "٨";
                    break;
                case '9':
                    rslt += "٩";
                    break;
                default:
                    rslt += string.charAt(i);
            }
        }
        return rslt;
    }
}
