package seif.example.com.jahhizly.Branches.Orders;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.eftimoff.viewpagertransformers.CubeOutTransformer;

import java.util.ArrayList;

import me.anwarshahriar.calligrapher.Calligrapher;
import seif.example.com.jahhizly.Branches.BranchesMVP;
import seif.example.com.jahhizly.Branches.UserOrders;
import seif.example.com.jahhizly.R;
import seif.example.com.jahhizly.Utils.MySingleton;

/**
 * Created by seif on 10/23/2017.
 */
public class OrdersFragment extends Fragment implements BranchesMVP.viewOrders {
    private View view;

    public OrdersFragment() {
    }

    ViewPager viewPager = null;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_orders, container, false);
        design();
        Calligrapher calligrapher = new Calligrapher(getContext());
        if (MySingleton.getmInstance(getContext()).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            calligrapher.setFont(getActivity(), "fonts/Arabic/arabic_font.ttf", true);
        } else {
            calligrapher.setFont(getActivity(), "fonts/English/Roboto_Regular.ttf", true);
        }
        viewPager = (ViewPager) view.findViewById(R.id.pager);
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        viewPager.setAdapter(new MyAdapter(fragmentManager));
        viewPager.setPageTransformer(true, new CubeOutTransformer());
        return view;
    }

    private void design() {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        view.setLayoutParams(params);
    }


    @Override
    public void loadData(ArrayList<UserOrders> orders) {
    }

    @Override
    public void showProgress() {
    }

    @Override
    public void hideProgress() {
    }

    @Override
    public void showEmptyText(String message) {
    }

    class MyAdapter extends FragmentStatePagerAdapter {

        @Override
        public Parcelable saveState() {
            return null;
        }

        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            if (position == 0) {
                fragment = new NewOrdersFragment();

            } else if (position == 1) {
                fragment = new PrevOrdersFragment();

            }
            return fragment;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if (position == 0) {
                return "  " + getString(R.string.newOrders) + "  ";
            } else if (position == 1) {
                return "  " + getString(R.string.prevOrders) + "  ";
            }

            return null;
        }
    }


}


