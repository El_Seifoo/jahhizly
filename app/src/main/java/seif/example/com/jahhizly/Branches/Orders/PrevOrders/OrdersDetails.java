package seif.example.com.jahhizly.Branches.Orders.PrevOrders;

import java.io.Serializable;

/**
 * Created by seif on 11/2/2017.
 */
public class OrdersDetails implements Serializable {
    private int quantity;
    private double price;
    private String itemName;
    private String extraName;
    private String extraPrice;
    private String size;
    private int id;
    private int orderId;
    private int menuId;
    private String photo;

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public OrdersDetails() {
    }

    public OrdersDetails(int quantity, double price, String itemName, String extraName,
                         String extraPrice, String size, int id, int orderId, int menuId) {
        this.quantity = quantity;
        this.price = price;
        this.itemName = itemName;
        this.extraName = extraName;
        this.extraPrice = extraPrice;
        this.size = size;
        this.id = id;
        this.orderId = orderId;
        this.menuId = menuId;
    }


    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getExtraName() {
        return extraName;
    }

    public void setExtraName(String extraName) {
        this.extraName = extraName;
    }

    public String getExtraPrice() {
        return extraPrice;
    }

    public void setExtraPrice(String extraPrice) {
        this.extraPrice = extraPrice;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getMenuId() {
        return menuId;
    }

    public void setMenuId(int menuId) {
        this.menuId = menuId;
    }
}
