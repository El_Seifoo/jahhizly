package seif.example.com.jahhizly.Branches.Orders.PrevOrders;

import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

import me.anwarshahriar.calligrapher.Calligrapher;
import seif.example.com.jahhizly.Other.CircleTransform;
import seif.example.com.jahhizly.R;
import seif.example.com.jahhizly.Utils.MySingleton;
import seif.example.com.jahhizly.Utils.URLs;

/**
 * Created by seif on 11/2/2017.
 */
public class OrdersDetailsAdapter extends RecyclerView.Adapter<OrdersDetailsAdapter.Holder> {

    private ArrayList<OrdersDetails> orders = new ArrayList<>();
    private boolean flag;

    public OrdersDetailsAdapter(boolean flag) {
        this.flag = flag;
    }

    public void setOrdersDetails(ArrayList<OrdersDetails> orders) {
        this.orders = orders;
    }


    @Override
    public OrdersDetailsAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Holder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_details_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(OrdersDetailsAdapter.Holder holder, int position) {
        Calligrapher calligrapher = new Calligrapher(holder.itemView.getContext());
        if (flag)
            calligrapher.setFont(holder.itemView, "fonts/Arabic/arabic_font.ttf");
        else
            calligrapher.setFont(holder.itemView, "fonts/English/Roboto_Regular.ttf");


        Glide.with(holder.itemView.getContext()).load(URLs.ROOT_URL + orders.get(position).getPhoto())
                .error(R.mipmap.restaurant_icon_menu)
                .thumbnail(0.1f)
                .transform(new CircleTransform(holder.itemView.getContext()))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.orderItemLogo);
        holder.orderItemName.setText(orders.get(position).getItemName());
        String txt = holder.itemView.getContext().getString(R.string.sizes) + "<font color = '#eccd33'> " + orders.get(position).getSize() + " </font>";
        holder.orderItemSize.setText(Html.fromHtml(txt), TextView.BufferType.SPANNABLE);
        holder.orderItemPrice.setText(orders.get(position).getPrice() + holder.itemView.getContext().getString(R.string.currency));
        String quantity = holder.itemView.getContext().getString(R.string.quantity) + "<font color = '#eccd33'> " + orders.get(position).getQuantity() + " </font>";
        holder.orderQuantity.setText(Html.fromHtml(flag ? convertDigits(quantity) : quantity), TextView.BufferType.SPANNABLE);
        if (orders.get(position).getExtraName().contains(",")) {
            String[] extras = orders.get(position).getExtraName().split(",");
            String text1 = holder.itemView.getContext().getString(R.string.extras);
            text1 += "<br>";
            String text = "";
            for (int i = 0; i < extras.length; i++) {
                text += extras[i] + " " + orders.get(position).getQuantity();
                text += "<br>";
            }
            String finalTxt = text1 + "<font color = '#eccd33'> " + text + " </font>";
            Log.e("textaya", finalTxt);
            holder.orderExtrasPrice.setText(Html.fromHtml(flag ? convertDigits(finalTxt) : finalTxt), TextView.BufferType.SPANNABLE);
        } else {

            if (orders.get(position).getExtraName().trim().equals("")) {
                holder.orderExtrasPrice.setText("");
            } else {
                String text = holder.itemView.getContext().getString(R.string.extras) + "<br>" + "<font color = '#eccd33'> " + orders.get(position).getExtraName() + " " + orders.get(position).getQuantity() + " </font>";
                holder.orderExtrasPrice.setText(Html.fromHtml(flag ? convertDigits(text) : text), TextView.BufferType.SPANNABLE);
            }
        }
    }

    @Override
    public int getItemCount() {
        return (null != orders ? orders.size() : 0);
    }

    public class Holder extends RecyclerView.ViewHolder {
        ImageView orderItemLogo;
        TextView orderItemName, orderItemSize, orderItemPrice, orderQuantity, orderExtrasPrice;

        public Holder(View itemView) {
            super(itemView);
            orderItemLogo = (ImageView) itemView.findViewById(R.id.prev_order_details_branch_logo);
            orderItemName = (TextView) itemView.findViewById(R.id.prev_order_details_item_name);
            orderItemSize = (TextView) itemView.findViewById(R.id.prev_order_details_item_size);
            orderItemPrice = (TextView) itemView.findViewById(R.id.prev_order_details_item_price);
            orderQuantity = (TextView) itemView.findViewById(R.id.prev_order_details_item_quantity);
            orderExtrasPrice = (TextView) itemView.findViewById(R.id.prev_order_details_item_extras);

        }
    }

    private String convertDigits(String string) {
        if (string==null) return null;
        String rslt = "";
        for (int i = 0; i < string.length(); i++) {
            switch (string.charAt(i)) {
                case '0':
                    rslt += "٠";
                    break;
                case '1':
                    rslt += "١";
                    break;
                case '2':
                    rslt += "٢";
                    break;
                case '3':
                    rslt += "٣";
                    break;
                case '4':
                    rslt += "٤";
                    break;
                case '5':
                    rslt += "٥";
                    break;
                case '6':
                    rslt += "٦";
                    break;
                case '7':
                    rslt += "٧";
                    break;
                case '8':
                    rslt += "٨";
                    break;
                case '9':
                    rslt += "٩";
                    break;
                default:
                    rslt += string.charAt(i);
            }
        }
        return rslt;
    }
}
