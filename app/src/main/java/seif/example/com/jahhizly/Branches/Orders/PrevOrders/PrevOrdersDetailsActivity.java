package seif.example.com.jahhizly.Branches.Orders.PrevOrders;

import android.content.res.Configuration;
import android.support.v4.app.ShareCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;

import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;
import me.anwarshahriar.calligrapher.Calligrapher;
import seif.example.com.jahhizly.Branches.MakeOrder.Branches;
import seif.example.com.jahhizly.R;
import seif.example.com.jahhizly.Utils.MySingleton;

public class PrevOrdersDetailsActivity extends AppCompatActivity implements PrevOrdersMVP.view {

    RecyclerView recyclerView;
    OrdersDetailsAdapter adapter;
    PrevOrdersPresenter presenter;
    MKLoader loading;
    TextView emptyText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prev_orders_details);
        Calligrapher calligrapher = new Calligrapher(this);
        if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            calligrapher.setFont(this, "fonts/Arabic/arabic_font.ttf", true);
        } else {
            calligrapher.setFont(this, "fonts/English/Roboto_Regular.ttf", true);
        }
        getSupportActionBar().setTitle(getString(R.string.prev_order_details_activity));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        loading = (MKLoader) findViewById(R.id.loading);
        emptyText = (TextView) findViewById(R.id.empty_txt);
        presenter = new PrevOrdersPresenter(this, new PrevOrdersModel(this), this);
        recyclerView = (RecyclerView) findViewById(R.id.prev_order_details_rec_view);
        adapter = new OrdersDetailsAdapter(MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? true : false);
        int screenSize = getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK;
        switch (screenSize) {
            case Configuration.SCREENLAYOUT_SIZE_XLARGE:
                recyclerView.setLayoutManager(new GridLayoutManager(this, 2, LinearLayoutManager.VERTICAL, false));
                break;
            case Configuration.SCREENLAYOUT_SIZE_LARGE:
                recyclerView.setLayoutManager(new GridLayoutManager(this, 2, LinearLayoutManager.VERTICAL, false));
                break;
            case Configuration.SCREENLAYOUT_SIZE_NORMAL:
                recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
                break;
            default:
                recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        }
        recyclerView.setHasFixedSize(true);
        Log.e("ssdsds", getIntent().getExtras().getInt("menuId") + "");
        presenter.onPrevOrderDetailsCreated(getIntent().getExtras().getInt("menuId"));


    }

    private float getScreenWidth() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        float scaleFactor = displayMetrics.density;
        return (width / scaleFactor);
    }

    ArrayList<OrdersDetails> ordersDetails;
    Branches branch;

    @Override
    public void loadData(ArrayList<OrdersDetails> orders, Branches branches) {
        branch = branches;
        String photo = getIntent().getExtras().getString("brandLogo");
        for (int i = 0; i < orders.size(); i++) {
            orders.get(i).setPhoto(photo);
        }
        ordersDetails = orders;
        adapter.setOrdersDetails(orders);
        ScaleInAnimationAdapter scaleInAnimationAdapter = new ScaleInAnimationAdapter(adapter);
        scaleInAnimationAdapter.setFirstOnly(false);
        recyclerView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void showProgress() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        loading.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        loading.setVisibility(View.GONE);
    }

    @Override
    public void showEmptyText(String message) {
        emptyText.setText(message);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.share_order, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            } else {
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            }
            return true;
        } else if (id == R.id.action_share) {

            shareText(getSharedData(ordersDetails, branch));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void shareText(String textToShare) {
        String mimeType = "text/plain";

        String title = "Jahhizly";
        ShareCompat.IntentBuilder
                .from(this)
                .setType(mimeType)
                .setChooserTitle(title)
                .setText(textToShare)
                .startChooser();
    }

    private String getSharedData(ArrayList<OrdersDetails> order, Branches branches) {
        String data = "Branch name: " + branches.getName() +
                "\nBranch Location: http://maps.google.com/maps?saddr=" + branches.getLatitude() + "," + branches.getLongitude() +
                "\nOrder number: " + order.get(0).getId() + "\n ";
        for (int i = 0; i < order.size(); i++) {
            data += "Item (" + (i + 1) + "): " + order.get(i).getItemName() + " \n";
            data += "Size: " + order.get(i).getSize() + " \n";
            data += "Extras: " + order.get(i).getExtraName() + " \n";
            data += "Quantity: " + order.get(i).getQuantity() + " \n";
            data += "Price: " + order.get(i).getPrice() + " \n";
        }
        Log.e("dataaaaaaaaa", data);
        return data;
    }
}
