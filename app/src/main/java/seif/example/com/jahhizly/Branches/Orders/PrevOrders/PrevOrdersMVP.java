package seif.example.com.jahhizly.Branches.Orders.PrevOrders;

import java.util.ArrayList;

import seif.example.com.jahhizly.Branches.MakeOrder.Branches;

/**
 * Created by seif on 11/2/2017.
 */
public interface PrevOrdersMVP {

    interface view {
        void loadData(ArrayList<OrdersDetails> orders, Branches branches);

        void showProgress();

        void hideProgress();

        void showEmptyText(String message);
    }

    interface presenter {
        void onPrevOrderDetailsCreated(int orderId);
    }
}
