package seif.example.com.jahhizly.Branches.Orders.PrevOrders;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import seif.example.com.jahhizly.Branches.MakeOrder.Branches;
import seif.example.com.jahhizly.R;
import seif.example.com.jahhizly.Utils.MySingleton;

/**
 * Created by seif on 11/2/2017.
 */
public class PrevOrdersPresenter implements PrevOrdersMVP.presenter, PrevOrdersModel.VolleyCallback {

    Context context;
    PrevOrdersMVP.view view;
    PrevOrdersModel model;

    public PrevOrdersPresenter(PrevOrdersMVP.view view, PrevOrdersModel model, Context context) {
        this.view = view;
        this.model = model;
        this.context = context;
    }

    @Override
    public void onPrevOrderDetailsCreated(int orderId) {
        view.showProgress();
        model.getOrderDetails(this, orderId);
    }

    @Override
    public void onSuccess(String response) throws JSONException {
        Log.e("response", response);
        ArrayList<OrdersDetails> order = new ArrayList<>();
        Branches branches;
        JSONObject jsonObject = new JSONObject(response);
        JSONArray orders = jsonObject.getJSONArray("orders");
        if (MySingleton.getmInstance(context).getAppLang().equals(context.getString(R.string.settings_language_english_value))) {
            for (int i = 0; i < orders.length(); i++) {
                int id = orders.getJSONObject(i).getInt("id");
                int orderId = orders.getJSONObject(i).getInt("order_id");
                int menuId = orders.getJSONObject(i).getInt("menu_id");
                int quantity = orders.getJSONObject(i).getInt("quantity");
                double price = orders.getJSONObject(i).getDouble("price");
                String itemName = orders.getJSONObject(i).getString("item_name");
                String extraName;
                if (orders.getJSONObject(i).getString("extra_name").equals("") ||
                        orders.getJSONObject(i).getString("extra_name").equals("null")) {
                    extraName = "";
                } else {
                    extraName = orders.getJSONObject(i).getString("extra_name");
                }
                String extraPrice;
                if (orders.getJSONObject(i).getString("extra_price").equals("") ||
                        orders.getJSONObject(i).getString("extra_price").equals("null")) {
                    extraPrice = "";
                } else {
                    extraPrice = orders.getJSONObject(i).getString("extra_price");
                }
                String size = orders.getJSONObject(i).getString("size");

                order.add(new OrdersDetails(quantity, price, itemName,
                        extraName, extraPrice, size, id, orderId, menuId));
            }

            JSONObject branchObject = jsonObject.getJSONObject("branch");
            String name;
            if (branchObject.getString("name").equals("")
                    || branchObject.getString("name").equals("null")) {
                name = context.getString(R.string.list_empty);
            } else {
                name = branchObject.getString("name");
            }
            String address = "";
//            if (branchObject.getString("address").equals("")
//                    || branchObject.getString("address").equals("null")) {
//                address = context.getString(R.string.list_empty);
//            } else {
//                address = branchObject.getString("address");
//            }
            double latitude = branchObject.getDouble("latitude");
            double longitude = branchObject.getDouble("longitude");
            branches = new Branches(name, address, latitude, longitude);
        } else {
            for (int i = 0; i < orders.length(); i++) {
                int id = orders.getJSONObject(i).getInt("id");
                int orderId = orders.getJSONObject(i).getInt("order_id");
                int menuId = orders.getJSONObject(i).getInt("menu_id");
                int quantity = orders.getJSONObject(i).getInt("quantity");
                double price = orders.getJSONObject(i).getDouble("price");
                String itemName = orders.getJSONObject(i).getString("item_arabic_name");
                String extraName;
                if (orders.getJSONObject(i).getString("extra_arabic_name").equals("") ||
                        orders.getJSONObject(i).getString("extra_arabic_name").equals("null")) {
                    extraName = "";
                } else {
                    extraName = orders.getJSONObject(i).getString("extra_arabic_name");
                }
                String extraPrice;
                if (orders.getJSONObject(i).getString("extra_price").equals("") ||
                        orders.getJSONObject(i).getString("extra_price").equals("null")) {
                    extraPrice = "";
                } else {
                    extraPrice = orders.getJSONObject(i).getString("extra_price");
                }
                String size = orders.getJSONObject(i).getString("arabic_size");

                order.add(new OrdersDetails(quantity, price, itemName,
                        extraName, extraPrice, size, id, orderId, menuId));
            }
            JSONObject branchObject = jsonObject.getJSONObject("branch");
            String name;
            if (branchObject.getString("arabic_name").equals("")
                    || branchObject.getString("arabic_name").equals("null")) {
                name = context.getString(R.string.list_empty);
            } else {
                name = branchObject.getString("arabic_name");
            }
            String address = "";
//            if (branchObject.getString("arabic_address").equals("")
//                    || branchObject.getString("arabic_address").equals("null")) {
//                address = context.getString(R.string.list_empty);
//            } else {
//                address = branchObject.getString("arabic_address");
//            }
            double latitude = branchObject.getDouble("latitude");
            double longitude = branchObject.getDouble("longitude");
            branches = new Branches(name, address, latitude, longitude);
        }
        view.hideProgress();
        if (order != null && order.size() > 0) {
            arrangeData(order, branches);
//            view.loadData(order);
        } else {
            view.showEmptyText(context.getString(R.string.list_empty));
        }
    }

    private void arrangeData(ArrayList<OrdersDetails> orders, Branches branches) {
        int i = 0;
        int x = 1;
        if (orders.size() > 1) {
            while (i < orders.size()) {
                if (orders.get(i).getId() == orders.get(x).getId() &&
                        orders.get(i).getMenuId() == orders.get(x).getMenuId()) {
                    orders.get(i).setExtraName(orders.get(i).getExtraName() + "," + orders.get(x).getExtraName());
                    orders.get(i).setExtraPrice(orders.get(i).getExtraPrice() + "," + orders.get(x).getExtraPrice());
                    orders.remove(x);
                } else {
                    x++;
                }

                if (x == orders.size()) {
                    i++;
                    x = i + 1;
                    if (x == orders.size()) {
                        break;
                    }
                }
            }
        }

        for (int y = 0; y < orders.size(); y++) {
            Log.e("order (" + y + ")    ", orders.get(y).getExtraName());
            Log.e("order (" + y + ")    ", orders.get(y).getExtraPrice());
        }
        view.loadData(orders, branches);
    }


    @Override
    public void onFail(VolleyError error) {
        view.hideProgress();
        if (error instanceof NoConnectionError || error instanceof TimeoutError) {
            Toast.makeText(context, context.getString(R.string.internet_error), Toast.LENGTH_LONG).show();
        } else if (error instanceof ServerError) {
            Toast.makeText(context, context.getString(R.string.server_error), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(context, context.getString(R.string.wrong), Toast.LENGTH_LONG).show();
        }
    }
}
