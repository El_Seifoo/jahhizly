package seif.example.com.jahhizly.Branches.Tables;

import java.io.Serializable;

/**
 * Created by seif on 11/30/2017.
 */
public class ReservedTables implements Serializable {
    private int id;
    private int userId;
    private int branch_id;
    private int tableNumber;
    private int orderNumber;
    private int chairsCount;
    private String status;
    private String reservationTime;
    private String branchName;
    private String brandLogo;

    public ReservedTables() {
    }

    public ReservedTables(int id, int userId, int branch_id, int tableNumber, int orderNumber,
                          int chairsCount, String status, String reservationTime, String branchName, String brandLogo) {
        this.id = id;
        this.userId = userId;
        this.branch_id = branch_id;
        this.tableNumber = tableNumber;
        this.orderNumber = orderNumber;
        this.chairsCount = chairsCount;
        this.status = status;
        this.reservationTime = reservationTime;
        this.branchName = branchName;
        this.brandLogo = brandLogo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getBranch_id() {
        return branch_id;
    }

    public void setBranch_id(int branch_id) {
        this.branch_id = branch_id;
    }

    public int getTableNumber() {
        return tableNumber;
    }

    public void setTableNumber(int tableNumber) {
        this.tableNumber = tableNumber;
    }

    public int getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(int orderNumber) {
        this.orderNumber = orderNumber;
    }

    public int getChairsCount() {
        return chairsCount;
    }

    public void setChairsCount(int chairsCount) {
        this.chairsCount = chairsCount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReservationTime() {
        return reservationTime;
    }

    public void setReservationTime(String reservationTime) {
        this.reservationTime = reservationTime;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getBrandLogo() {
        return brandLogo;
    }

    public void setBrandLogo(String brandLogo) {
        this.brandLogo = brandLogo;
    }
}
