package seif.example.com.jahhizly.Branches.Tables;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

import me.anwarshahriar.calligrapher.Calligrapher;
import seif.example.com.jahhizly.Branches.Orders.PrevOrders.PrevOrdersDetailsActivity;
import seif.example.com.jahhizly.Other.CircleTransform;
import seif.example.com.jahhizly.R;
import seif.example.com.jahhizly.Utils.MySingleton;
import seif.example.com.jahhizly.Utils.URLs;

/**
 * Created by seif on 11/29/2017.
 */
public class ReservedTablesAdapter extends RecyclerView.Adapter<ReservedTablesAdapter.Holder> {
    private ArrayList<ReservedTables> tables;
    private boolean flag;

    public ReservedTablesAdapter(boolean flag) {
        this.flag = flag;
    }

    public void setTables(ArrayList<ReservedTables> tables) {
        this.tables = tables;
        notifyDataSetChanged();
    }

    @Override
    public ReservedTablesAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Holder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.reserved_tables_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(final ReservedTablesAdapter.Holder holder, final int position) {
        Calligrapher calligrapher = new Calligrapher(holder.itemView.getContext());
        if (MySingleton.getmInstance(holder.itemView.getContext()).getAppLang().equals(holder.itemView.getContext().getString(R.string.settings_language_arabic_value))) {
            calligrapher.setFont(holder.itemView, "fonts/Arabic/arabic_font.ttf");
        } else {
            calligrapher.setFont(holder.itemView, "fonts/English/Roboto_Regular.ttf");
        }

        Glide.with(holder.itemView.getContext()).load(URLs.ROOT_URL + tables.get(position).getBrandLogo())
                .error(R.mipmap.restaurant_icon_menu)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .thumbnail(0.1f)
                .transform(new CircleTransform(holder.itemView.getContext()))
                .into(holder.logo);

        holder.name.setText(flag ? convertDigits(" " + tables.get(position).getBranchName() + " ") : " " + tables.get(position).getBranchName() + " ");
        holder.chairsCount.setText(flag ? convertDigits(" " + tables.get(position).getChairsCount() + " ") : " " + tables.get(position).getChairsCount() + " ");
        holder.reservationNumber.setText(flag ? convertDigits(holder.itemView.getContext().getString(R.string.reserv_num) + tables.get(position).getId()) : holder.itemView.getContext().getString(R.string.reserv_num) + tables.get(position).getId());
        String[] time = tables.get(position).getReservationTime().split(" ");
//        holder.reservationTime.setText(" " + time[0] + " , " + time[1].split(":")[0] + ":" + time[1].split(":")[1] + " ");
        holder.reservationTime.setText(flag ? convertDigits(" " + time[1].split(":")[0] + ":" + time[1].split(":")[1] + " , " + time[0].split("-")[2] + " - " + time[0].split("-")[1] + " - " + time[0].split("-")[0] + " ") : " " + time[1].split(":")[0] + ":" + time[1].split(":")[1] + " , " + time[0] + " ");
        if (tables.get(position).getOrderNumber() == -1) {
            holder.orderIcon.setVisibility(View.GONE);
        } else {
            holder.orderIcon.setVisibility(View.VISIBLE);
            holder.orderIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(holder.itemView.getContext(), PrevOrdersDetailsActivity.class);
                    intent.putExtra("menuId", tables.get(position).getOrderNumber());
                    holder.itemView.getContext().startActivity(intent);
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return (null != tables ? tables.size() : 0);
    }

    public class Holder extends RecyclerView.ViewHolder {
        ImageView logo, orderIcon;
        TextView name, chairsCount, reservationTime, reservationNumber;

        public Holder(View itemView) {
            super(itemView);
            logo = (ImageView) itemView.findViewById(R.id.table_brand_logo);
            orderIcon = (ImageView) itemView.findViewById(R.id.tables_order_icon);
            name = (TextView) itemView.findViewById(R.id.tables_branch_name);
            reservationNumber = (TextView) itemView.findViewById(R.id.tables_reservation_number);
            chairsCount = (TextView) itemView.findViewById(R.id.tables_chairs_count);
            reservationTime = (TextView) itemView.findViewById(R.id.tables_reservation_date);
        }
    }

    private String convertDigits(String string) {
        if (string==null) return null;
        String rslt = "";
        for (int i = 0; i < string.length(); i++) {
            switch (string.charAt(i)) {
                case '0':
                    rslt += "٠";
                    break;
                case '1':
                    rslt += "١";
                    break;
                case '2':
                    rslt += "٢";
                    break;
                case '3':
                    rslt += "٣";
                    break;
                case '4':
                    rslt += "٤";
                    break;
                case '5':
                    rslt += "٥";
                    break;
                case '6':
                    rslt += "٦";
                    break;
                case '7':
                    rslt += "٧";
                    break;
                case '8':
                    rslt += "٨";
                    break;
                case '9':
                    rslt += "٩";
                    break;
                default:
                    rslt += string.charAt(i);
            }
        }
        return rslt;
    }

}
