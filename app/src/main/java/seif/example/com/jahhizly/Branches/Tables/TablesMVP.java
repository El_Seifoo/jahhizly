package seif.example.com.jahhizly.Branches.Tables;

import java.util.ArrayList;

/**
 * Created by seif on 11/29/2017.
 */
public interface TablesMVP {
    interface view {
        void showEmptyText(String message);
        void loadData(ArrayList<ReservedTables> tables);
        void showProgress();
        void hideProgress();
    }

    interface presenter {
        void onTablesCreated();
    }
}
