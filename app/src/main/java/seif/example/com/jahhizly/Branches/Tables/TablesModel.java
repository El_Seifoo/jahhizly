package seif.example.com.jahhizly.Branches.Tables;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;

import java.util.HashMap;
import java.util.Map;

import seif.example.com.jahhizly.Utils.MySingleton;
import seif.example.com.jahhizly.Utils.URLs;

/**
 * Created by seif on 11/29/2017.
 */
public class TablesModel {
    private Context context;

    public TablesModel(Context context) {
        this.context = context;
    }

    protected void getTables(final VolleyCallback callback) {
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                URLs.TABLES,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("rssrsrs", response);
                        try {
                            callback.onSuccess(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("tables json", e.toString());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.onFail(error);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                String token = "Bearer " + MySingleton.getmInstance(context).UserKey();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Authorization", token);
                return headers;
            }
        };
        MySingleton.getmInstance(context).addToRQ(stringRequest);
    }


    protected interface VolleyCallback {
        void onSuccess(String response) throws JSONException;

        void onFail(VolleyError error);
    }
}
