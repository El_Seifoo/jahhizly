package seif.example.com.jahhizly.Branches.Tables;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import seif.example.com.jahhizly.R;
import seif.example.com.jahhizly.Utils.MySingleton;

/**
 * Created by seif on 11/29/2017.
 */
public class TablesPresenter implements TablesMVP.presenter, TablesModel.VolleyCallback {
    private TablesMVP.view view;
    private TablesModel model;
    private Context context;

    public TablesPresenter(TablesMVP.view view, TablesModel model, Context context) {
        this.view = view;
        this.model = model;
        this.context = context;
    }

    @Override
    public void onTablesCreated() {
        view.showProgress();
        model.getTables(this);
    }

    @Override
    public void onSuccess(String response) throws JSONException {
        ArrayList<ReservedTables> reservedTables = new ArrayList<>();
        JSONObject responseJson = new JSONObject(response);
        JSONArray tables = responseJson.getJSONArray("tables");
        if (MySingleton.getmInstance(context).getAppLang().equals(context.getString(R.string.settings_language_english_value))) {
            for (int i = 0; i < tables.length(); i++) {
                int id = tables.getJSONObject(i).getInt("id");
                int userId = tables.getJSONObject(i).getInt("user_id");
                int branchId = tables.getJSONObject(i).getInt("branch_id");
                int status = tables.getJSONObject(i).getInt("status");
                int chairsCount = tables.getJSONObject(i).getInt("chairs_count");
                int orderID;
                if (tables.getJSONObject(i).getString("order_id").equals("")
                        || tables.getJSONObject(i).getString("order_id").equals("null")) {
                    orderID = -1;
                } else {
                    orderID = tables.getJSONObject(i).getInt("order_id");
                }
                int tableNumber = 0;
                String branchName;
                if (tables.getJSONObject(i).getString("branch_name").equals("")
                        || tables.getJSONObject(i).getString("branch_name").equals("null")) {
                    branchName = context.getString(R.string.list_empty);
                } else {
                    branchName = tables.getJSONObject(i).getString("branch_name");
                }
                String brandLogo;
                if (tables.getJSONObject(i).getString("brand_photo").equals("")
                        || tables.getJSONObject(i).getString("brand_photo").equals("null")) {
                    brandLogo = "";
                } else {
                    brandLogo = tables.getJSONObject(i).getString("brand_photo");
                }
                String reservationTime;
                if (tables.getJSONObject(i).getString("reservation_time").equals("")
                        || tables.getJSONObject(i).getString("reservation_time").equals("null")) {
                    reservationTime = "";
                } else {
                    reservationTime = tables.getJSONObject(i).getString("reservation_time");
                }

                reservedTables.add(new ReservedTables(id, userId, branchId,
                        tableNumber, orderID, chairsCount, "", reservationTime, branchName, brandLogo));
            }
        } else {
            for (int i = 0; i < tables.length(); i++) {
                int id = tables.getJSONObject(i).getInt("id");
                int userId = tables.getJSONObject(i).getInt("user_id");
                int branchId = tables.getJSONObject(i).getInt("branch_id");

                int status = tables.getJSONObject(i).getInt("status");
                int chairsCount = tables.getJSONObject(i).getInt("chairs_count");
                int orderID;
                if (tables.getJSONObject(i).getString("order_id").equals("")
                        || tables.getJSONObject(i).getString("order_id").equals("null")) {
                    orderID = -1;
                } else {
                    orderID = tables.getJSONObject(i).getInt("order_id");
                }
                int tableNumber = tables.getJSONObject(i).getInt("table_number");
                String branchName;
                if (tables.getJSONObject(i).getString("branch_arabic_name").equals("")
                        || tables.getJSONObject(i).getString("branch_arabic_name").equals("null")) {
                    branchName = context.getString(R.string.list_empty);
                } else {
                    branchName = tables.getJSONObject(i).getString("branch_arabic_name");
                }
                String brandLogo;
                if (tables.getJSONObject(i).getString("brand_photo").equals("")
                        || tables.getJSONObject(i).getString("brand_photo").equals("null")) {
                    brandLogo = "";
                } else {
                    brandLogo = tables.getJSONObject(i).getString("brand_photo");
                }
                String reservationTime;
                if (tables.getJSONObject(i).getString("reservation_time").equals("")
                        || tables.getJSONObject(i).getString("reservation_time").equals("null")) {
                    reservationTime = "";
                } else {
                    reservationTime = tables.getJSONObject(i).getString("reservation_time");
                }

                reservedTables.add(new ReservedTables(id, userId, branchId,
                        tableNumber, orderID, chairsCount, "", reservationTime, branchName, brandLogo));
            }
        }
        view.hideProgress();
        if (reservedTables.isEmpty()) {
            view.showEmptyText(context.getString(R.string.list_empty));
        } else {
            view.loadData(reservedTables);
        }
    }

    @Override
    public void onFail(VolleyError error) {
        view.hideProgress();
        if (error instanceof NoConnectionError || error instanceof TimeoutError) {
            Toast.makeText(context, context.getString(R.string.internet_error), Toast.LENGTH_LONG).show();
        } else if (error instanceof ServerError) {
            Toast.makeText(context, context.getString(R.string.server_error), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(context, context.getString(R.string.wrong), Toast.LENGTH_LONG).show();
        }
    }

}
