package seif.example.com.jahhizly.Branches;

import java.io.Serializable;

/**
 * Created by seif on 10/24/2017.
 */
public class UserOrders implements Serializable {
    // id , branch_id , user_id , reservation_id , status , price , order_time , address , city , createdAt
    private int id;
    private int branchId;
    private String branchName;
    private String status;
    private double price;
    private String photo;
    private String date;
    private String time;
    private int remain;
    private int prepTime;

    public UserOrders() {
    }

    public UserOrders(int id, int branchId, String branchName, String status,
                      double price, String photo, String date, String time, int remain, int prepTime) {
        this.id = id;
        this.branchId = branchId;
        this.branchName = branchName;
        this.status = status;
        this.price = price;
        this.photo = photo;
        this.date = date;
        this.time = time;
        this.remain = remain;
        this.prepTime = prepTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getRemain() {
        return remain;
    }

    public void setRemain(int remain) {
        this.remain = remain;
    }

    public int getPrepTime() {
        return prepTime;
    }

    public void setPrepTime(int prepTime) {
        this.prepTime = prepTime;
    }
}
