package seif.example.com.jahhizly.Cart;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

import me.anwarshahriar.calligrapher.Calligrapher;
import seif.example.com.jahhizly.Menus.MenuItem.CartDbHelper;
import seif.example.com.jahhizly.Menus.MenuItem.CartItemDetails;
import seif.example.com.jahhizly.R;
import seif.example.com.jahhizly.Utils.MySingleton;
import seif.example.com.jahhizly.Utils.URLs;

/**
 * Created by seif on 10/30/2017.
 */
public class CartAdapter extends RecyclerView.Adapter<CartAdapter.Holder> {
    private ArrayList<CartItemDetails> cartItems = new ArrayList<>();
    private boolean flag;

    public CartAdapter(boolean flag) {
        this.flag = flag;
    }


    public void setCartData(ArrayList<CartItemDetails> cartItems) {
        if (cartItems != null && cartItems.size() > 0) {
            this.cartItems = cartItems;
            notifyDataSetChanged();
        }

    }

    public void clear() {
        cartItems.clear();
        notifyDataSetChanged();
    }

    @Override
    public CartAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.cart_list_item, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(final CartAdapter.Holder holder, final int position) {
        Calligrapher calligrapher = new Calligrapher(holder.itemView.getContext());
        if (MySingleton.getmInstance(holder.itemView.getContext()).getAppLang().equals(holder.itemView.getContext().getString(R.string.settings_language_arabic_value))) {
            calligrapher.setFont(holder.itemView, "fonts/Arabic/arabic_font.ttf");
        } else {
            calligrapher.setFont(holder.itemView, "fonts/English/Roboto_Regular.ttf");
        }
        Glide.with(holder.itemView.getContext()).load(URLs.ROOT_URL + cartItems.get(position).getPhoto())
                .error(R.mipmap.restaurant_icon_menu)
                .crossFade()
                .thumbnail(0.5f)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.cartItemImg);
        holder.cartItemSize.setText(holder.itemView.getContext().getString(R.string.sizes));
        if (MySingleton.getmInstance(holder.itemView.getContext()).getAppLang().equals(holder.itemView.getContext().getString(R.string.settings_language_arabic_value))) {
            holder.cartItemName.setText(convertDigits(cartItems.get(position).getItemArabicName()));
            holder.cartItemSize.append(convertDigits(cartItems.get(position).getArabicSize() + " "));
        } else {
            holder.cartItemName.setText(cartItems.get(position).getItemName());
            holder.cartItemSize.append(cartItems.get(position).getSize() + " ");
        }
        String priceText = holder.itemView.getContext().getString(R.string.price) + "<font color = '#65db73'> " + String.valueOf(cartItems.get(position).getPrice()) + " " + holder.itemView.getContext().getString(R.string.currency) + " </font>";
        holder.cartItemPrice.setText(Html.fromHtml(flag ? convertDigits(priceText) : priceText), TextView.BufferType.SPANNABLE);
        holder.deleteCartItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                CartDbHelper.getmInstance(holder.itemView.getContext()).removeByMenuId(cartItems.get(position).getId());
                cartItems.remove(position);
                if (holder.itemView.getContext() instanceof MyCartActivity) {
                    ((MyCartActivity) holder.itemView.getContext()).totPriceMaxTime(cartItems);
                }
                if (cartItems.size() == 0) {
                    MySingleton.getmInstance(holder.itemView.getContext()).clearCart();
                    if (holder.itemView.getContext() instanceof MyCartActivity) {
                        ((MyCartActivity) holder.itemView.getContext()).hideButtons();
                        ((MyCartActivity) holder.itemView.getContext()).showEmptyText(holder.itemView.getContext().getString(R.string.cart_empty));
                    }
                }
                notifyDataSetChanged();
            }
        });
    }


    @Override
    public int getItemCount() {
        return (null != cartItems ? cartItems.size() : 0);
    }

    public class Holder extends RecyclerView.ViewHolder {
        public TextView cartItemName, cartItemPrice, cartItemSize;
        ImageView cartItemImg, deleteCartItem;
        View view;
        LinearLayout container;

        public Holder(View itemView) {
            super(itemView);
            cartItemImg = (ImageView) itemView.findViewById(R.id.cart_item_img);
            container = (LinearLayout) itemView.findViewById(R.id.container);
            deleteCartItem = (ImageView) itemView.findViewById(R.id.delete_cart_item);
            cartItemName = (TextView) itemView.findViewById(R.id.cart_item_name);
            cartItemSize = (TextView) itemView.findViewById(R.id.cart_item_size);
            cartItemPrice = (TextView) itemView.findViewById(R.id.cart_item_price);
            view = (View) itemView.findViewById(R.id.view);
        }
    }

    private String convertDigits(String string) {
        if (string==null) return null;
        String rslt = "";
        for (int i = 0; i < string.length(); i++) {
            switch (string.charAt(i)) {
                case '0':
                    rslt += "٠";
                    break;
                case '1':
                    rslt += "١";
                    break;
                case '2':
                    rslt += "٢";
                    break;
                case '3':
                    rslt += "٣";
                    break;
                case '4':
                    rslt += "٤";
                    break;
                case '5':
                    rslt += "٥";
                    break;
                case '6':
                    rslt += "٦";
                    break;
                case '7':
                    rslt += "٧";
                    break;
                case '8':
                    rslt += "٨";
                    break;
                case '9':
                    rslt += "٩";
                    break;
                default:
                    rslt += string.charAt(i);
            }
        }
        return rslt;
    }

}
