package seif.example.com.jahhizly.Cart;

import org.json.JSONException;

import java.util.ArrayList;

import seif.example.com.jahhizly.Branches.MakeOrder.Branches;
import seif.example.com.jahhizly.Menus.MenuItem.CartItemDetails;

/**
 * Created by seif on 10/30/2017.
 */
public interface CartMVP {
    interface view {
        void loadData(ArrayList<CartItemDetails> cartItemDetails);

        void ShowMessage(String message);

        void clearCart(String string);

        void setButtonsState(Branches branches);

        void hideButtons();

        void onMakeOrderSuccess(int orderId);

        void goHome();

        void goLogin();

        void goBookTable(Branches branches, int orderId);

        void showProgress();

        void hideProgress();

        void showEmptyText(String message);
    }

    interface presenter {
        void onCartCreated();

        void onClearCartClicked();

        void onMakeOrderClicked() throws JSONException;

        void onBookTableClicked(Branches branch, int orderId);
    }
}
