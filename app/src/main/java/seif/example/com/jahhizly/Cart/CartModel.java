package seif.example.com.jahhizly.Cart;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import seif.example.com.jahhizly.Menus.MenuItem.CartDbHelper;
import seif.example.com.jahhizly.Menus.MenuItem.CartItemDetails;
import seif.example.com.jahhizly.Utils.MySingleton;
import seif.example.com.jahhizly.Utils.URLs;

/**
 * Created by seif on 10/30/2017.
 */
public class CartModel {
    Context context;
    private boolean flag;

    public CartModel(Context context, boolean flag) {
        this.context = context;
        this.flag = flag;
    }

    protected ArrayList<CartItemDetails> getUserCartData() {
        ArrayList<CartItemDetails> cart;
        if (MySingleton.getmInstance(context).isLoggedIn()) {
            cart = CartDbHelper.getmInstance(context)
                    .getItems(MySingleton.getmInstance(context).userData().getId());
        } else {
            cart = CartDbHelper.getmInstance(context).getItems(-1);
        }
        return cart;
    }

    protected void makeOrder(final VolleyCallBack callBack) throws JSONException {
        ArrayList<CartItemDetails> cart = CartDbHelper.getmInstance(context)
                .getItems(MySingleton.getmInstance(context).userData().getId());
        int maxTime = Integer.parseInt(cart.get(0).getTime());
        double totalPrice = 0;
        for (int i = 1; i < cart.size(); i++) {
            if (maxTime < Integer.parseInt(cart.get(i).getTime())) {
                maxTime = Integer.parseInt(cart.get(i).getTime());
            }
        }
        for (int i = 0; i < cart.size(); i++) {
            totalPrice += cart.get(i).getPrice();
        }
        JSONObject order = new JSONObject();
        order.put("branch_id", MySingleton.getmInstance(context).getCartData().getId());
        order.put("order_time", convertDigits(String.valueOf(maxTime)));
        order.put("order_price", convertDigits(String.valueOf(totalPrice)));
        JSONArray items = new JSONArray();
        for (int i = 0; i < cart.size(); i++) {
            JSONObject data = new JSONObject();
            data.put("menu_id", cart.get(i).getMenuID());
            data.put("quantity", convertDigits(String.valueOf(cart.get(i).getQuantity())));
            data.put("size_id", cart.get(i).getSizeID());

            String[] extras = cart.get(i).getExtrasID().split(",");
            JSONArray extrasArray = new JSONArray();
            for (int x = 0; x < extras.length; x++) {
                extrasArray.put(extras[x]);
            }
            data.put("extra_ids", extrasArray);
            items.put(i, data);
        }
        order.put("items", items);
        Log.e("order object", order.toString());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                URLs.MAKE_ORDER,
                order,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("el Seifoooo", response.toString());
                        try {
                            callBack.onSuccess(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callBack.onFail(error);
////                        callBack.onFail(error);
//                        String body;
//                        //get status code here
//                        String statusCode = String.valueOf(error.networkResponse.statusCode);
//                        //get response body and parse with appropriate encoding
//                        if(error.networkResponse.data!=null) {
//                            try {
//                                body = new String(error.networkResponse.data,"UTF-8");
//                                Log.e("body error" , body);
//                            } catch (UnsupportedEncodingException e) {
//                                e.printStackTrace();
//                            }
//                        }
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                String token = "Bearer " + MySingleton.getmInstance(context).UserKey();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Authorization", token);
                return headers;
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getmInstance(context).addToRQ(jsonObjectRequest);
    }


    protected interface VolleyCallBack {
        void onSuccess(JSONObject response) throws JSONException;

        void onFail(VolleyError error);
    }

    private String convertDigits(String string) {
        String rslt = "";
        for (int i = 0; i < string.length(); i++) {
            switch (string.charAt(i)) {
                case '٠':
                    rslt += "0";
                    break;
                case '١':
                    rslt += "1";
                    break;
                case '٢':
                    rslt += "2";
                    break;
                case '٣':
                    rslt += "3";
                    break;
                case '٤':
                    rslt += "4";
                    break;
                case '٥':
                    rslt += "5";
                    break;
                case '٦':
                    rslt += "6";
                    break;
                case '٧':
                    rslt += "7";
                    break;
                case '٨':
                    rslt += "8";
                    break;
                case '٩':
                    rslt += "9";
                    break;
                default:
                    rslt += string.charAt(i);
            }
        }
        return rslt;
    }

}
