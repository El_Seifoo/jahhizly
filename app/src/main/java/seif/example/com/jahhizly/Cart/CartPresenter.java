package seif.example.com.jahhizly.Cart;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import seif.example.com.jahhizly.Branches.MakeOrder.Branches;
import seif.example.com.jahhizly.Menus.MenuItem.CartDbHelper;
import seif.example.com.jahhizly.R;
import seif.example.com.jahhizly.Utils.MySingleton;

/**
 * Created by seif on 10/30/2017.
 */
public class CartPresenter implements CartMVP.presenter, CartModel.VolleyCallBack {

    Context context;
    CartModel model;
    CartMVP.view view;

    public CartPresenter(Context context, CartModel model, CartMVP.view view) {
        this.context = context;
        this.model = model;
        this.view = view;
    }


    @Override
    public void onCartCreated() {
        if (model.getUserCartData().size() > 0) {
            if (MySingleton.getmInstance(context).isCartFull()) {
                view.setButtonsState(MySingleton.getmInstance(context).getCartData());
            }
            view.loadData(model.getUserCartData());
        } else {
            view.hideButtons();
            view.showEmptyText(context.getString(R.string.cart_empty));
        }
    }

    @Override
    public void onClearCartClicked() {
        view.showProgress();
        CartDbHelper.getmInstance(context).deleteAll();
        MySingleton.getmInstance(context).clearCart();
        view.clearCart(context.getString(R.string.cart_empty));
    }

    @Override
    public void onMakeOrderClicked() throws JSONException {
        if (!MySingleton.getmInstance(context).isLoggedIn()) {
            view.goLogin();
            return;
        }
        view.showProgress();
        model.makeOrder(this);
    }

    @Override
    public void onBookTableClicked(Branches branch, int orderId) {
        if (!MySingleton.getmInstance(context).isLoggedIn()) {
            view.goLogin();
        } else {
            view.goBookTable(branch, orderId);
        }
    }

    @Override
    public void onSuccess(JSONObject response) throws JSONException {
        String status = response.getString("status");
        view.hideProgress();
        if (status.equals("success")) {
            view.onMakeOrderSuccess(response.getInt("order_id"));
        } else {
            view.ShowMessage(context.getString(R.string.wrong));
        }
    }

    @Override
    public void onFail(VolleyError error) {
        view.hideProgress();
        if (error instanceof NoConnectionError || error instanceof TimeoutError) {
            Toast.makeText(context, context.getString(R.string.internet_error), Toast.LENGTH_LONG).show();
        } else if (error instanceof ServerError) {
            Toast.makeText(context, context.getString(R.string.server_error), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(context, context.getString(R.string.wrong), Toast.LENGTH_LONG).show();
        }
        Log.e("sesesesese", error.toString());
    }
}
