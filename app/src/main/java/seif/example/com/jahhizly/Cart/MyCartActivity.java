package seif.example.com.jahhizly.Cart;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.LayoutDirection;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.tuyenmonkey.mkloader.MKLoader;

import org.json.JSONException;

import java.util.ArrayList;

import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;
import me.anwarshahriar.calligrapher.Calligrapher;
import seif.example.com.jahhizly.BookTable.BookTableActivity;
import seif.example.com.jahhizly.Branches.MakeOrder.Branches;
import seif.example.com.jahhizly.Branches.BranchesActivity;
import seif.example.com.jahhizly.Login.LoginActivity;
import seif.example.com.jahhizly.Menus.MenuItem.CartDbHelper;
import seif.example.com.jahhizly.Menus.MenuItem.CartItemDetails;
import seif.example.com.jahhizly.R;
import seif.example.com.jahhizly.Utils.MySingleton;

public class MyCartActivity extends AppCompatActivity implements CartMVP.view {

    TextView totalPrice, maxTime, clearCart;
    Button bookTable, makeOrder;
    LinearLayout linearLayout;
    CartPresenter presenter;
    RecyclerView cartRecView;
    CartAdapter adapter;
    double maximumTime = 0;
    double totalPriceF = 0;
    MKLoader loading;
    TextView emptyTxt;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_cart);
        Calligrapher calligrapher = new Calligrapher(this);
        if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            calligrapher.setFont(this, "fonts/Arabic/arabic_font.ttf", true);
        } else {
            calligrapher.setFont(this, "fonts/English/Roboto_Regular.ttf", true);
        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.cart_activity));
        if (getIntent().hasExtra("CartIntent")) {
            if (getIntent().getExtras().getString("CartIntent").equals("FromLogin")) {
                CartDbHelper.getmInstance(this).updateData(CartDbHelper.getmInstance(this).getItems(-1),
                        MySingleton.getmInstance(this).userData().getId());
                ArrayList<CartItemDetails> cart = CartDbHelper.getmInstance(this).getItems(MySingleton.getmInstance(this).userData().getId());
                if (cart.size() > 0) {
                    for (int i = 0; i < cart.size(); i++) {
                        Log.e("cart user", cart.get(i).getItemName());
                    }
                } else {
                    ArrayList<CartItemDetails> cart1 = CartDbHelper.getmInstance(this).getItems(-1);
                    if (cart1.size() > 0) {
                        for (int i = 0; i < cart1.size(); i++) {
                            Log.e("cart user 1", cart1.get(i).getItemName());
                        }
                    } else {
                        Log.e("cart user 2", "bl7");
                    }
                }
            }
        }

        emptyTxt = (TextView) findViewById(R.id.empty_txt);
        loading = (MKLoader) findViewById(R.id.loading);
        presenter = new CartPresenter(this, new CartModel(this, MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? true : false), this);
        linearLayout = (LinearLayout) findViewById(R.id.btns_container);
        bookTable = (Button) findViewById(R.id.book_table_btn);
        makeOrder = (Button) findViewById(R.id.make_order_btn);
        makeOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    presenter.onMakeOrderClicked();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
//        bookTable.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                presenter.onBookTableClicked(MySingleton.getmInstance(MyCartActivity.this).getCartData());
//            }
//        });
        totalPrice = (TextView) findViewById(R.id.orders_price);
        maxTime = (TextView) findViewById(R.id.orders_time);
        clearCart = (TextView) findViewById(R.id.clear_cart);
        clearCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                presenter.onClearCartClicked();
            }
        });
        cartRecView = (RecyclerView) findViewById(R.id.cart_rec_view);
        adapter = new CartAdapter(MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? true : false);
        int screenSize = getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK;
        switch (screenSize) {
            case Configuration.SCREENLAYOUT_SIZE_XLARGE:
                cartRecView.setLayoutManager(new GridLayoutManager(this, 2, LinearLayoutManager.VERTICAL, false));
                break;
            case Configuration.SCREENLAYOUT_SIZE_LARGE:
                cartRecView.setLayoutManager(new GridLayoutManager(this, 2, LinearLayoutManager.VERTICAL, false));
                break;
            case Configuration.SCREENLAYOUT_SIZE_NORMAL:
                cartRecView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
                break;
            default:
                cartRecView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        }
        cartRecView.setHasFixedSize(true);

        presenter.onCartCreated();
        if (getResources().getConfiguration().getLayoutDirection() == LayoutDirection.RTL) {
            clearCart.setTextDirection(View.TEXT_DIRECTION_RTL);
            maxTime.setTextDirection(View.TEXT_DIRECTION_RTL);
            totalPrice.setTextDirection(View.TEXT_DIRECTION_RTL);
        }
    }

    private float getScreenWidth() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;
        float scaleFactor = displayMetrics.density;
        return (width / scaleFactor);
    }

    public void totPriceMaxTime(ArrayList<CartItemDetails> cartItems) {
        maximumTime = 0;
        totalPriceF = 0;
        if (cartItems.size() > 0) {
            for (int i = 0; i < cartItems.size(); i++) {
                totalPriceF += cartItems.get(i).getPrice();
                if (Integer.valueOf(cartItems.get(i).getTime()) > maximumTime) {
                    maximumTime = Integer.valueOf(cartItems.get(i).getTime());
                }
            }
        }

        totalPrice.setText(MySingleton.getmInstance(MyCartActivity.this).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? convertDigits(totalPriceF + "") : totalPriceF + "");
        maxTime.setText(MySingleton.getmInstance(MyCartActivity.this).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? convertDigits(maximumTime + getString(R.string.time)) : maximumTime + getString(R.string.time));
    }

    @Override
    public void loadData(ArrayList<CartItemDetails> cartItemDetails) {
        if (cartItemDetails != null && cartItemDetails.size() > 0) {
            totPriceMaxTime(cartItemDetails);
            adapter.setCartData(cartItemDetails);
            ScaleInAnimationAdapter scaleInAnimationAdapter = new ScaleInAnimationAdapter(adapter);
            scaleInAnimationAdapter.setFirstOnly(false);
            cartRecView.setAdapter(scaleInAnimationAdapter);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void ShowMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void clearCart(String string) {
        totalPriceF = 0;
        maximumTime = 0;
        hideProgress();
        adapter.clear();
        showEmptyText(string);
        hideButtons();
        totalPrice.setText(MySingleton.getmInstance(MyCartActivity.this).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? convertDigits(totalPriceF + "") : totalPriceF + "");
        maxTime.setText(MySingleton.getmInstance(MyCartActivity.this).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? convertDigits(maximumTime + getString(R.string.time)) : maximumTime + getString(R.string.time));
    }

    @Override
    public void setButtonsState(Branches branches) {
        if (branches.getType() == 0) {
            bookTable.setVisibility(View.GONE);
            makeOrder.setVisibility(View.VISIBLE);
        } else if (branches.getType() == 1) {
            bookTable.setVisibility(View.GONE);
            makeOrder.setVisibility(View.GONE);
        } else if (branches.getType() == 2) {
            bookTable.setVisibility(View.GONE);
            makeOrder.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void hideButtons() {
        linearLayout.setVisibility(View.GONE);
    }

//    @Override
//    public void onMakeOrderSuccess(String orderId) {
//        final Dialog dialog = new Dialog(this);
//        dialog.setContentView(R.layout.book_table_done_dialog);
//        TextView txt = (TextView) dialog.findViewById(R.id.message);
//        if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
//            dialog.getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
//        }
//        txt.setText(getString(R.string.make_order_done));
//        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
//            @Override
//            public void onDismiss(DialogInterface dialogInterface) {
//                CartDbHelper.getmInstance(MyCartActivity.this).deleteAll();
//                MySingleton.getmInstance(MyCartActivity.this).clearCart();
//                adapter.clear();
//                goHome();
//            }
//        });
//        dialog.setCanceledOnTouchOutside(false);
//        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        dialog.show();
//        Thread th = new Thread() {
//            public void run() {
//                try {
//                    sleep(1500);
//                    dialog.dismiss();
//                } catch (Exception e) {
//
//                }
//            }
//        };
//        th.start();
//    }

    @Override
    public void onMakeOrderSuccess(final int orderId) {
        adapter.clear();
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.book_table_done_dialog);
        TextView txt = (TextView) dialog.findViewById(R.id.message);
        if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            dialog.getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }
        txt.setText(getString(R.string.make_order_done));
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                showOptionDialog(orderId);
            }
        });
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
        Thread th = new Thread() {
            public void run() {
                try {
                    sleep(1500);
                    dialog.dismiss();
                } catch (Exception e) {

                }
            }
        };
        th.start();
    }

    private void showOptionDialog(final int orderId) {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.book_table_done_dialog);
        TextView txt = (TextView) dialog.findViewById(R.id.message);
        ((TextView) dialog.findViewById(R.id.SuccButton)).setVisibility(View.GONE);
        ((LinearLayout) dialog.findViewById(R.id.btns_container)).setVisibility(View.VISIBLE);
        ((RelativeLayout) dialog.findViewById(R.id.container)).setVisibility(View.GONE);
        if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            dialog.getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }
        txt.setText(getString(R.string.make_order_done_two));
        txt.setPadding(32, 32, 32, 32);
        ((Button) dialog.findViewById(R.id.ok_btn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
                presenter.onBookTableClicked(MySingleton.getmInstance(MyCartActivity.this).getCartData(), orderId);

            }
        });
        ((Button) dialog.findViewById(R.id.cancel_btn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
                goHome();
            }
        });

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                CartDbHelper.getmInstance(MyCartActivity.this).deleteAll();
                MySingleton.getmInstance(MyCartActivity.this).clearCart();
            }
        });
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    @Override
    public void goHome() {
        Intent intent = new Intent(this, BranchesActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.putExtra("GoToOrders", "GoToOrders");
        startActivity(intent);
        if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        } else {
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }

    }

    @Override
    public void goLogin() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.putExtra("LoginIntent", "FromCart");
        startActivity(intent);
        finish();
        if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        } else {
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }
    }

    @Override
    public void goBookTable(Branches branches, int orderId) {
        Intent intent = new Intent(this, BookTableActivity.class);
        intent.putExtra("BookTableBranch", branches);
        intent.putExtra("OrderIDFromCart", orderId);
        startActivity(intent);
        CartDbHelper.getmInstance(MyCartActivity.this).deleteAll();
        MySingleton.getmInstance(MyCartActivity.this).clearCart();
        if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        } else {
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }
    }

    @Override
    public void showProgress() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        loading.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        loading.setVisibility(View.GONE);
    }

    @Override
    public void showEmptyText(String message) {
        emptyTxt.setText(message);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            } else {
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private String convertDigits(String string) {
        if (string==null) return null;
        String rslt = "";
        for (int i = 0; i < string.length(); i++) {
            switch (string.charAt(i)) {
                case '0':
                    rslt += "٠";
                    break;
                case '1':
                    rslt += "١";
                    break;
                case '2':
                    rslt += "٢";
                    break;
                case '3':
                    rslt += "٣";
                    break;
                case '4':
                    rslt += "٤";
                    break;
                case '5':
                    rslt += "٥";
                    break;
                case '6':
                    rslt += "٦";
                    break;
                case '7':
                    rslt += "٧";
                    break;
                case '8':
                    rslt += "٨";
                    break;
                case '9':
                    rslt += "٩";
                    break;
                default:
                    rslt += string.charAt(i);
            }
        }
        return rslt;
    }

}
