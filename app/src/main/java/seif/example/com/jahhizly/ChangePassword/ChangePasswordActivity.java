package seif.example.com.jahhizly.ChangePassword;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.LayoutDirection;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.tuyenmonkey.mkloader.MKLoader;

import de.hdodenhof.circleimageview.CircleImageView;
import me.anwarshahriar.calligrapher.Calligrapher;
import seif.example.com.jahhizly.Other.CircleTransform;
import seif.example.com.jahhizly.R;
import seif.example.com.jahhizly.Utils.MySingleton;
import seif.example.com.jahhizly.Utils.URLs;

public class ChangePasswordActivity extends AppCompatActivity implements ChngPwMVP.view {
    EditText oldPassword, newPassword, confirmPassword;
    Button saveBtn;
    CircleImageView profilePic;
    TextView username;
    ChngPwPresenter presenter;
    MKLoader loading;
    ScrollView container;


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        Calligrapher calligrapher = new Calligrapher(this);
        if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            calligrapher.setFont(this, "fonts/Arabic/arabic_font.ttf", true);
        } else {
            calligrapher.setFont(this, "fonts/English/Roboto_Regular.ttf", true);
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        loading = (MKLoader) findViewById(R.id.loading);
        container = (ScrollView) findViewById(R.id.container);
        profilePic = (CircleImageView) findViewById(R.id.profile_pic);
        username = (TextView) findViewById(R.id.profile_name);

        oldPassword = (EditText) findViewById(R.id.old_password);
        newPassword = (EditText) findViewById(R.id.new_password);
        saveBtn = (Button) findViewById(R.id.chng_pw_save_btn);
        confirmPassword = (EditText) findViewById(R.id.confirm_password);
        presenter = new ChngPwPresenter(this, new ChngPwModel(this), this);
        setData();
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onSaveBtnClicked(convertDigits(getOldPassword()), convertDigits(getNewPassword()), convertDigits(getConfirmPassword()));
            }
        });
        if (getResources().getConfiguration().getLayoutDirection() == LayoutDirection.RTL) {
            username.setTextDirection(View.TEXT_DIRECTION_RTL);
            oldPassword.setGravity(Gravity.RIGHT);
            newPassword.setGravity(Gravity.RIGHT);
            confirmPassword.setGravity(Gravity.RIGHT);
        }
    }

    @Override
    public String getOldPassword() {
        return oldPassword.getText().toString().trim();
    }

    @Override
    public String getNewPassword() {
        return newPassword.getText().toString().trim();
    }

    @Override
    public String getConfirmPassword() {
        return confirmPassword.getText().toString().trim();
    }

    @Override
    public void showErrorMessage(String error) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void backProfileWithSuccesMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        finish();
        if (MySingleton.getmInstance(this).getAppLang().equals(R.string.settings_language_arabic_value)) {
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        } else {
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }
    }

    @Override
    public void setData() {
        username.setText(MySingleton.getmInstance(this).userData().getName());
        if ((MySingleton.getmInstance(this).userData().getGender().equals("Male"))) {
            Glide.with(this).load(URLs.ROOT_URL + (MySingleton.getmInstance(this).userData().getPhoto()))
                    .error(R.mipmap.sidemenu_pic_profile_man)
                    .thumbnail(0.5f)
                    .transform(new CircleTransform(this))
                    .into(profilePic);
        } else {
            Glide.with(this).load(URLs.ROOT_URL + (MySingleton.getmInstance(this).userData().getPhoto()))
                    .error(R.mipmap.sidemenu_pic_profile_woman)
                    .thumbnail(0.5f)
                    .transform(new CircleTransform(this))
                    .into(profilePic);
        }
    }

    @Override
    public void showProgress() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        loading.setVisibility(View.VISIBLE);
        container.setForeground(new ColorDrawable(Color.parseColor("#66000000")));
    }

    @Override
    public void hideProgress() {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        loading.setVisibility(View.GONE);
        container.setForeground(new ColorDrawable(Color.TRANSPARENT));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            } else {
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private String convertDigits(String string) {
        String rslt = "";
        for (int i = 0; i < string.length(); i++) {
            switch (string.charAt(i)) {
                case '٠':
                    rslt += "0";
                    break;
                case '١':
                    rslt += "1";
                    break;
                case '٢':
                    rslt += "2";
                    break;
                case '٣':
                    rslt += "3";
                    break;
                case '٤':
                    rslt += "4";
                    break;
                case '٥':
                    rslt += "5";
                    break;
                case '٦':
                    rslt += "6";
                    break;
                case '٧':
                    rslt += "7";
                    break;
                case '٨':
                    rslt += "8";
                    break;
                case '٩':
                    rslt += "9";
                    break;
                default:
                    rslt += string.charAt(i);
            }
        }
        return rslt;
    }

}
