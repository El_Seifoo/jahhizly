package seif.example.com.jahhizly.ChangePassword;

/**
 * Created by seif on 10/30/2017.
 */
public interface ChngPwMVP {
    interface view {
        String getOldPassword();
        String getNewPassword();
        String getConfirmPassword();
        void showErrorMessage(String error);
        void backProfileWithSuccesMessage(String message);
        void setData();
        void showProgress();
        void hideProgress();
    }

    interface presenter {
        void onSaveBtnClicked(String oldPassword , String newPassword , String confirmPassword);
    }
}
