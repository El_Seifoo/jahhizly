package seif.example.com.jahhizly.ChangePassword;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import seif.example.com.jahhizly.R;

/**
 * Created by seif on 10/30/2017.
 */
public class ChngPwPresenter implements ChngPwMVP.presenter, ChngPwModel.VolleyCallback {

    Context context;
    ChngPwModel model;
    ChngPwModel.VolleyCallback callback;
    ChngPwMVP.view view;

    public ChngPwPresenter(Context context, ChngPwModel model, ChngPwMVP.view view) {
        this.context = context;
        this.model = model;
        this.callback = this;
        this.view = view;
    }

    @Override
    public void onSaveBtnClicked(String oldPassword, String newPassword, String confirmPassword) {
        if (oldPassword.equals("") || newPassword.equals("") || confirmPassword.equals("")) {
            view.showErrorMessage(context.getString(R.string.error_1));
            return;
        }
        view.showProgress();
        model.changePassword(callback, oldPassword, newPassword, confirmPassword);
    }

    @Override
    public void onSuccess(JSONObject response) throws JSONException {
        String status = response.getString("status");
        view.hideProgress();
        if (status.equals("success")) {
            view.backProfileWithSuccesMessage(context.getString(R.string.done));
        } else {
            view.showErrorMessage(context.getString(R.string.wrong));
        }

    }

    @Override
    public void onFail(VolleyError error) {
        view.hideProgress();
        if (error instanceof NoConnectionError || error instanceof TimeoutError) {
            Toast.makeText(context, context.getString(R.string.internet_error), Toast.LENGTH_LONG).show();
        } else if (error instanceof ServerError) {
            Toast.makeText(context, context.getString(R.string.server_error), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(context, context.getString(R.string.wrong), Toast.LENGTH_LONG).show();
        }
    }
}
