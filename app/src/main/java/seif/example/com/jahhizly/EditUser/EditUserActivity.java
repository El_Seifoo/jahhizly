package seif.example.com.jahhizly.EditUser;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;

import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.LayoutDirection;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.tuyenmonkey.mkloader.MKLoader;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.hdodenhof.circleimageview.CircleImageView;
import me.anwarshahriar.calligrapher.Calligrapher;
import seif.example.com.jahhizly.Login.UserInfo;
import seif.example.com.jahhizly.Other.CircleTransform;
import seif.example.com.jahhizly.Other.FixedHoloDatePickerDialog;
import seif.example.com.jahhizly.R;
import seif.example.com.jahhizly.SignUp.SignUpActivity;
import seif.example.com.jahhizly.Utils.MySingleton;
import seif.example.com.jahhizly.Utils.URLs;

public class EditUserActivity extends AppCompatActivity implements EditUserMVP.view, DatePickerDialog.OnDateSetListener {

    EditUserPresenter presenter;
    EditText username, email, phone, dob;
    AutoCompleteTextView city;
    CircleImageView pic, profilePic;
    Button saveEditedData;
    RadioGroup radioGroup;
    RadioButton gRBtn;
    Calendar myCalendar;
    DatePickerDialog date;
    MKLoader loading;
    ScrollView container;


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_user);
        Calligrapher calligrapher = new Calligrapher(this);
        if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            calligrapher.setFont(this, "fonts/Arabic/arabic_font.ttf", true);
        } else {
            calligrapher.setFont(this, "fonts/English/Roboto_Regular.ttf", true);
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        loading = (MKLoader) findViewById(R.id.loading);
        container = (ScrollView) findViewById(R.id.container);
        saveEditedData = (Button) findViewById(R.id.edit_data_save_btn);
        profilePic = (CircleImageView) findViewById(R.id.edt_profile_pic);
        pic = (CircleImageView) findViewById(R.id.profile_pic);
        username = (EditText) findViewById(R.id.edit_user_name);
        email = (EditText) findViewById(R.id.edt_user_email);
        phone = (EditText) findViewById(R.id.edt_user_phone);
        dob = (EditText) findViewById(R.id.edt_user_dob);
        String[] cities = getResources().getStringArray(R.array.cities);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, cities);
        city = (AutoCompleteTextView) findViewById(R.id.edt_user_city);
        city.setAdapter(adapter);
        city.setThreshold(1);
        radioGroup = (RadioGroup) findViewById(R.id.edt_gender_rd_group);
        presenter = new EditUserPresenter(this, new EditUserModel(EditUserActivity.this), this);
        presenter.onEditUserCreated();

        myCalendar = Calendar.getInstance();
        dob.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                showDatePicker();
            }
        });

        saveEditedData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (bitmap != null) {
                    presenter.saveProfPic(getPath(filePath), convertDigits(getName()), convertDigits(getEmail()), convertDigits(getPhone()), convertDigits(getGender()), convertDigits(getDOB()), convertDigits(getCity()));
                } else {
                    presenter.onSaveBtnClicked(convertDigits(getName()), convertDigits(getEmail()), convertDigits(getPhone()), convertDigits(getGender()), convertDigits(getDOB()), convertDigits(getCity()));
                }
            }
        });

        profilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectProfilePic();
            }
        });

        if (getResources().getConfiguration().getLayoutDirection() == LayoutDirection.RTL) {
            username.setGravity(Gravity.RIGHT);
            email.setGravity(Gravity.RIGHT);
            phone.setGravity(Gravity.RIGHT);
            dob.setGravity(Gravity.RIGHT);
            city.setGravity(Gravity.RIGHT);
            ((RadioButton) findViewById(R.id.edt_male_rd_btn)).setGravity(Gravity.RIGHT);
            ((RadioButton) findViewById(R.id.edt_female_rd_btn)).setGravity(Gravity.RIGHT);
        }
    }

    private final static int SELECT_IMG_REQUEST = 1010;

    private void selectProfilePic() {
        ActivityCompat.requestPermissions(
                EditUserActivity.this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                SELECT_IMG_REQUEST
        );
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == SELECT_IMG_REQUEST) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, "Select Image"), SELECT_IMG_REQUEST);
            } else {
                Toast.makeText(getApplicationContext(), "permission", Toast.LENGTH_LONG).show();
            }
            return;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    Bitmap bitmap;
    Uri filePath;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SELECT_IMG_REQUEST && resultCode == RESULT_OK && data != null) {
            filePath = data.getData();
            Log.e("path", filePath + "");
            try {
                InputStream inputStream = getContentResolver().openInputStream(filePath);
                bitmap = BitmapFactory.decodeStream(inputStream);
                if (MySingleton.getmInstance(this).userData().getGender().equals("Male")) {
                    Glide.with(this)
                            .load(bitmapToByte(bitmap))
                            .asBitmap()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .placeholder(R.mipmap.sidemenu_pic_profile_man)
                            .transform(new CircleTransform(this))
                            .thumbnail(0.1f)
                            .into(pic);
                } else {
                    Glide.with(this)
                            .load(bitmapToByte(bitmap))
                            .asBitmap()
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .placeholder(R.mipmap.sidemenu_pic_profile_woman)
                            .transform(new CircleTransform(this))
                            .into(pic);
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private byte[] bitmapToByte(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

    public String getPath(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
        cursor.close();

        cursor = getContentResolver().query(
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();

        return path;
    }

    private void updateLabel() {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        String DOB = sdf.format(myCalendar.getTime());
        dob.setText(DOB);
    }

    @Override
    public void showErrorMessage(String error) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void setUserData(UserInfo userInfo) {
        username.setText(!userInfo.getName().equals(getString(R.string.list_empty)) ? userInfo.getName() : "");
        email.setText(!userInfo.getEmail().equals(getString(R.string.list_empty)) ? userInfo.getEmail() : "");
        phone.setText(!userInfo.getPhone().equals(getString(R.string.list_empty)) ? userInfo.getPhone() : "");
        city.setText(!userInfo.getCity().equals(getString(R.string.list_empty)) ? userInfo.getCity() : "");

        RadioButton male = (RadioButton) radioGroup.getChildAt(0);
        RadioButton female = (RadioButton) radioGroup.getChildAt(1);
        if (userInfo.getBirthDate().contains("T")) {
            String[] dateObirth = userInfo.getBirthDate().split("T")[0].split("-");
            dob.setText(dateObirth[2] + "-" + dateObirth[1] + "-" + dateObirth[0]);
        } else {
            dob.setText(" " + userInfo.getBirthDate() + " ");
        }
        if (userInfo.getGender().equals("Male")) {
            male.setChecked(true);
            female.setChecked(false);
            Glide.with(this).load(URLs.ROOT_URL + userInfo.getPhoto())
                    .placeholder(R.mipmap.sidemenu_pic_profile_man)
                    .thumbnail(0.5f)
                    .transform(new CircleTransform(this))
                    .into(pic);
        } else {
            male.setChecked(false);
            female.setChecked(true);
            Glide.with(this).load(URLs.ROOT_URL + userInfo.getPhoto())
                    .placeholder(R.mipmap.sidemenu_pic_profile_woman)
                    .thumbnail(0.5f)
                    .transform(new CircleTransform(this))
                    .into(pic);
        }

    }

    @Override
    public String getName() {
        return username.getText().toString().trim();
    }

    @Override
    public String getEmail() {
        return email.getText().toString().trim();
    }

    @Override
    public String getPhone() {
        return phone.getText().toString().trim();
    }

    @Override
    public String getGender() {
        int selected = radioGroup.getCheckedRadioButtonId();
        gRBtn = (RadioButton) findViewById(selected);
        if (gRBtn == null) {
            return null;
        }
        return String.valueOf(gRBtn.getText());
    }

    @Override
    public String getDOB() {
        return dob.getText().toString().trim();
    }

    @Override
    public String getCity() {
        return city.getText().toString().trim();
    }

    @Override
    public boolean checkMailValidation(String email) {
        String regExpn =
                "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                        + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                        + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";
        CharSequence inputStr = email;
        Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        if (matcher.matches())
            return true;
        else
            return false;
    }

    @Override
    public void showDatePicker() {
        // TODO Auto-generated method stub
        date = new FixedHoloDatePickerDialog(
                new ContextThemeWrapper(
                        EditUserActivity.this,
                        R.style.DatePickerDialogStyle),
                this,
                myCalendar.get(Calendar.YEAR),
                myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)
        );
        date.show();
    }

    @Override
    public void backProfile(UserInfo userInfo) {
        Log.e("dob", userInfo.getBirthDate());
//        String[] birthday = userInfo.getBirthDate().split("/");
//        userInfo.setGender(birthday[0]+"-"+birthday[1]+"-"+birthday[2]+"T22:00:00.0007");
        MySingleton.getmInstance(getApplicationContext()).saveUserData(userInfo);
        finish();
        if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        } else {
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }
    }

    @Override
    public void showProgress() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        loading.setVisibility(View.VISIBLE);
        container.setForeground(new ColorDrawable(Color.parseColor("#66000000")));
    }

    @Override
    public void hideProgress() {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        loading.setVisibility(View.GONE);
        container.setForeground(new ColorDrawable(Color.TRANSPARENT));
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            } else {
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private String convertDigits(String string) {
        if (string==null) return null;
        String rslt = "";
        for (int i = 0; i < string.length(); i++) {
            switch (string.charAt(i)) {
                case '٠':
                    rslt += "0";
                    break;
                case '١':
                    rslt += "1";
                    break;
                case '٢':
                    rslt += "2";
                    break;
                case '٣':
                    rslt += "3";
                    break;
                case '٤':
                    rslt += "4";
                    break;
                case '٥':
                    rslt += "5";
                    break;
                case '٦':
                    rslt += "6";
                    break;
                case '٧':
                    rslt += "7";
                    break;
                case '٨':
                    rslt += "8";
                    break;
                case '٩':
                    rslt += "9";
                    break;
                default:
                    rslt += string.charAt(i);
            }
        }
        return rslt;
    }


    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        myCalendar.set(Calendar.YEAR, year);
        myCalendar.set(Calendar.MONTH, month);
        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        updateLabel();
    }
}
