package seif.example.com.jahhizly.EditUser;

import seif.example.com.jahhizly.Login.UserInfo;

/**
 * Created by seif on 10/30/2017.
 */
public interface EditUserMVP {
    interface view {
        void showErrorMessage(String error);

        void setUserData(UserInfo userInfo);

        String getName();

        String getEmail();

        String getPhone();

        String getGender();

        String getDOB();

        String getCity();

        boolean checkMailValidation(String email);

        void showDatePicker();

        void backProfile(UserInfo userInfo);

        void showProgress();

        void hideProgress();
    }

    interface presenter {
        void onEditUserCreated();

        void onSaveBtnClicked(String username, String email, String phone, String gender, String birthDay, String city);

        void saveProfPic(String img, String username, String email, String phone, String gender, String birthDay, String city);
    }
}
