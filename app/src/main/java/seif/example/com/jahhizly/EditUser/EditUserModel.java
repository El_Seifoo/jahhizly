package seif.example.com.jahhizly.EditUser;

import android.app.Activity;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.ServerResponse;
import net.gotev.uploadservice.UploadInfo;
import net.gotev.uploadservice.UploadNotificationConfig;
import net.gotev.uploadservice.UploadService;
import net.gotev.uploadservice.UploadStatusDelegate;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import seif.example.com.jahhizly.Login.UserInfo;
import seif.example.com.jahhizly.R;
import seif.example.com.jahhizly.Utils.MySingleton;
import seif.example.com.jahhizly.Utils.URLs;

/**
 * Created by seif on 10/30/2017.
 */
public class EditUserModel {

    Activity activity;

    public EditUserModel(Activity activity) {
        this.activity = activity;
    }

    protected UserInfo getIntentExtra() {
        return (UserInfo) activity.getIntent().getExtras().getSerializable("UserInfo");
    }

    protected void edtUser(final VolleyCallback callback, String name, String email, String phone, String gender, String birthday, String city) {
        if (gender.equals(activity.getApplicationContext().getString(R.string.male))) {
            gender = "0";
        } else {
            gender = "1";
        }
        Map<String, String> params = new HashMap<String, String>();
        params.put("name", name);
        params.put("email", email);
        params.put("phone", phone);
        params.put("gender", gender);
        params.put("birthday", birthday);
        Log.e("city", city);
        params.put("city", city);
        Log.e("params", params.toString());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                URLs.EDIT_USER,
                new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            callback.onSuccess(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("eorrororor", error.toString());
                        callback.onFail(error);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                String token = "Bearer " + MySingleton.getmInstance(activity.getApplicationContext()).UserKey();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Authorization", token);
                return headers;
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getmInstance(activity.getApplicationContext()).addToRQ(jsonObjectRequest);


    }

    protected void uploadProfPic(final VolleyCallback callback, String img, final String username, final String email, final String phone, final String gender, final String birthDay, final String city) {
        Log.e("imgsssssss", img);
        final String uploadID = UUID.randomUUID().toString();
        try {
            UploadService.NAMESPACE = "seif.example.com.jahhizly";
            String token = "Bearer " + MySingleton.getmInstance(activity.getApplicationContext()).UserKey();
            new MultipartUploadRequest(activity.getApplicationContext(), uploadID, URLs.ADD_PROF_PIC)
                    .addFileToUpload(img, "file")
                    .addHeader("Authorization", token)
                    .setNotificationConfig(new UploadNotificationConfig())
                    .setMaxRetries(2)
                    .setDelegate(new UploadStatusDelegate() {
                        @Override
                        public void onProgress(UploadInfo uploadInfo) {
                            Log.e("progress", uploadInfo.getProgressPercent() + "");
                        }

                        @Override
                        public void onError(UploadInfo uploadInfo, Exception exception) {
                            callback.onUploadFailed();
                        }

                        @Override
                        public void onCompleted(UploadInfo uploadInfo, ServerResponse serverResponse) {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    edtUser(callback, username, email, phone, gender, birthDay, city);
                                }
                            }, 1500);

                        }

                        @Override
                        public void onCancelled(UploadInfo uploadInfo) {

                        }
                    })
                    .startUpload();
        } catch (Exception e) {
            Toast.makeText(activity.getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }


    protected interface VolleyCallback {
        void onSuccess(JSONObject response) throws JSONException;

        void onFail(VolleyError error);

        void onUploadFailed();

    }
}
