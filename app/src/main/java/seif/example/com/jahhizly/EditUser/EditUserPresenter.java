package seif.example.com.jahhizly.EditUser;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import seif.example.com.jahhizly.Login.UserInfo;
import seif.example.com.jahhizly.R;

/**
 * Created by seif on 10/30/2017.
 */
public class EditUserPresenter implements EditUserMVP.presenter, EditUserModel.VolleyCallback {

    Context context;
    EditUserModel model;
    EditUserMVP.view view;
    EditUserModel.VolleyCallback callback;

    public EditUserPresenter(Context context, EditUserModel model, EditUserMVP.view view) {
        this.context = context;
        this.model = model;
        this.view = view;
        callback = this;
    }

    @Override
    public void onEditUserCreated() {
        if (model.getIntentExtra() != null) {
            view.setUserData(model.getIntentExtra());
        } else {
            view.showErrorMessage(context.getString(R.string.wrong));
        }
    }

    @Override
    public void onSaveBtnClicked(String username, String email, String phone, String gender, String birthDay, String city) {
        if (username.equals("") || email.equals("") || phone.equals("") || gender.equals("") || birthDay.equals("") || city.equals("")) {
            view.showErrorMessage(context.getString(R.string.error_1));
        } else {
            if (!view.checkMailValidation(email)) {
                view.showErrorMessage(context.getString(R.string.check_mail));
                return;
            }
            view.showProgress();
            model.edtUser(callback, username, email, phone, gender, birthDay, city);
        }
    }

    @Override
    public void saveProfPic(String img, String username, String email, String phone, String gender, String birthDay, String city) {
        if (username.equals("") || email.equals("") || phone.equals("") || gender.equals("") || birthDay.equals("") || city.equals("")) {
            view.showErrorMessage(context.getString(R.string.error_1));
        } else {
            if (!view.checkMailValidation(email)) {
                view.showErrorMessage(context.getString(R.string.check_mail));
                return;
            }
            view.showProgress();
            model.uploadProfPic(this, img, username, email, phone, gender, birthDay, city);
        }

    }

    @Override
    public void onSuccess(JSONObject response) throws JSONException {
        Log.e("edit user response", response.toString());
        JSONObject user = response.getJSONObject("user");
        int id = user.getInt("id");
        String name;
        if (user.getString("name").equals("") || user.getString("name").equals("null")) {
            name = "No data";
        } else {
            name = user.getString("name");
        }
        String email;
        if (user.getString("email").equals("") || user.getString("email").equals("null")) {
            email = "No data";
        } else {
            email = user.getString("email");
        }

        String phone;
        if (user.getString("phone").equals("") || user.getString("phone").equals("null")) {
            phone = "No data";
        } else {
            phone = user.getString("phone");
        }

        String photo;
        if (user.getString("photo").equals("") || user.getString("photo").equals("null")) {
            photo = null;
        } else {
            photo = user.getString("photo");
        }


        String birthDate;
        if (user.getString("birthday").equals("") || user.getString("birthday").equals("null")) {
            birthDate = "No data";
        } else {
            birthDate = user.getString("birthday");
        }


        String city;
        if (user.getString("city").equals("") || user.getString("city").equals("null")) {
            city = "No data";
        } else {
            city = user.getString("city");
        }


        String gender;
        if (user.getString("gender").equals("") || user.getString("gender").equals("null")) {
            gender = "No data";
        } else {
            if (user.getString("gender").equals("0")) {
                gender = "Male";
            } else {
                gender = "Female";
            }
        }
        UserInfo userInfo = new UserInfo(id, name, email, phone,
                photo, birthDate, city, gender);
        view.hideProgress();
        view.backProfile(userInfo);


    }

    @Override
    public void onFail(VolleyError error) {
        view.hideProgress();
        if (error instanceof NoConnectionError || error instanceof TimeoutError) {
            Toast.makeText(context, context.getString(R.string.internet_error), Toast.LENGTH_LONG).show();
        } else if (error instanceof ServerError) {
            Toast.makeText(context, context.getString(R.string.server_error), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(context, context.getString(R.string.wrong), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onUploadFailed() {
        view.hideProgress();
        view.showErrorMessage(context.getString(R.string.error_5));
    }
}
