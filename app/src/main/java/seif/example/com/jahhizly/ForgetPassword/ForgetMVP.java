package seif.example.com.jahhizly.ForgetPassword;

/**
 * Created by seif on 11/28/2017.
 */
public interface ForgetMVP {
    interface view {
        void showErrorMessage(String message);
        void goLogin();
        void showSecondContainer();
    }

    interface presenter {
        void onSendSMSClicked(String phone);

        void onResetPassclicked(String phone, String code, String newPass);
    }
}
