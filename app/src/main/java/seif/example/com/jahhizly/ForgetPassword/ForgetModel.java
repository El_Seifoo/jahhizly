package seif.example.com.jahhizly.ForgetPassword;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import seif.example.com.jahhizly.R;
import seif.example.com.jahhizly.Utils.MySingleton;
import seif.example.com.jahhizly.Utils.URLs;

/**
 * Created by seif on 11/28/2017.
 */
public class ForgetModel {
    private Context context;

    public ForgetModel(Context context) {
        this.context = context;
    }

    protected void sendSMS(final VolleyCallback callback, String phone) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("phone", (context.getString(R.string.phone_code) + phone).trim());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                URLs.USER_VERIFICATION,
                new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            callback.onSuccess(response, 1);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("errrrrrrrrrrr", error.toString());
                        callback.onFail(error, 1);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getmInstance(context).addToRQ(jsonObjectRequest);
    }

    protected void resetPassword(final VolleyCallback callback, String phone, String code, String newPass) {
        Map<String, String> params = new HashMap<String, String>();
        Log.e("scscscscs", phone + "\n" + code + "\n" + newPass);
        params.put("phone", (context.getString(R.string.phone_code) + phone).trim());
        params.put("code", code);
        params.put("password", newPass);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                URLs.RESET_PASSWORD,
                new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("dsdsdsds", response.toString());
                        try {
                            callback.onSuccess(response, 2);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("erororroror", error.toString());
                        callback.onFail(error, 2);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getmInstance(context).addToRQ(jsonObjectRequest);
    }

    protected interface VolleyCallback {
        void onSuccess(JSONObject response, int which) throws JSONException;

        void onFail(VolleyError error, int which);
    }
}
