package seif.example.com.jahhizly.ForgetPassword;

import android.content.Context;
import android.content.Intent;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.LayoutDirection;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

import me.anwarshahriar.calligrapher.Calligrapher;
import seif.example.com.jahhizly.Login.LoginActivity;
import seif.example.com.jahhizly.R;
import seif.example.com.jahhizly.Utils.MySingleton;

public class ForgetPassActivity extends AppCompatActivity implements ForgetMVP.view {
    Vibrator v;
    EditText phoneNumber, code, newPassword;
    Button sendSMS, confirm;
    TextView resendCode;
    FrameLayout firstContainer;
    LinearLayout secondContainer;
    ForgetPresenter presenter;
    String phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_pass);
        Calligrapher calligrapher = new Calligrapher(this);
        if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            calligrapher.setFont(this, "fonts/Arabic/arabic_font.ttf", true);
            ((LinearLayout) findViewById(R.id.phone_container)).setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        } else {
            calligrapher.setFont(this, "fonts/English/Roboto_Regular.ttf", true);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        presenter = new ForgetPresenter(this, new ForgetModel(this), this);
        phoneNumber = (EditText) findViewById(R.id.forget_pass_phone_number);
        code = (EditText) findViewById(R.id.forget_pass_code);
        newPassword = (EditText) findViewById(R.id.forget_pass_new_pass);
        sendSMS = (Button) findViewById(R.id.forget_pass_phone_number_btn);
        confirm = (Button) findViewById(R.id.forgetPass_btn);
        resendCode = (TextView) findViewById(R.id.resend_code);
        firstContainer = (FrameLayout) findViewById(R.id.first_container);
        secondContainer = (LinearLayout) findViewById(R.id.second_container);
        phoneNumber.setTextLocale(Locale.ENGLISH);
        code.setTextLocale(Locale.ENGLISH);
        newPassword.setTextLocale(Locale.ENGLISH);
        if (getResources().getConfiguration().getLayoutDirection() == LayoutDirection.RTL) {
            phoneNumber.setGravity(Gravity.RIGHT);
            code.setGravity(Gravity.RIGHT);
            newPassword.setGravity(Gravity.RIGHT);
        }


        sendSMS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                phone = phoneNumber.getText().toString().trim();
                presenter.onSendSMSClicked(convertDigits(phone));
            }
        });

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onResetPassclicked(convertDigits(phone), convertDigits(code.getText().toString().trim()), convertDigits(newPassword.getText().toString().trim()));
            }
        });

    }

    @Override
    public void showErrorMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void goLogin() {
        startActivity(new Intent(this, LoginActivity.class));
        if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        } else {
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }
    }

    @Override
    public void showSecondContainer() {
        secondContainer.setVisibility(View.VISIBLE);
        firstContainer.setVisibility(View.GONE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            } else {
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private String convertDigits(String string) {
        if (string==null) return null;
        String rslt = "";
        for (int i = 0; i < string.length(); i++) {
            switch (string.charAt(i)) {
                case '٠':
                    rslt += "0";
                    break;
                case '١':
                    rslt += "1";
                    break;
                case '٢':
                    rslt += "2";
                    break;
                case '٣':
                    rslt += "3";
                    break;
                case '٤':
                    rslt += "4";
                    break;
                case '٥':
                    rslt += "5";
                    break;
                case '٦':
                    rslt += "6";
                    break;
                case '٧':
                    rslt += "7";
                    break;
                case '٨':
                    rslt += "8";
                    break;
                case '٩':
                    rslt += "9";
                    break;
                default:
                    rslt += string.charAt(i);
            }
        }
        return rslt;
    }

}
