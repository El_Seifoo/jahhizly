package seif.example.com.jahhizly.ForgetPassword;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import seif.example.com.jahhizly.R;

/**
 * Created by seif on 11/28/2017.
 */
public class ForgetPresenter implements ForgetMVP.presenter, ForgetModel.VolleyCallback {
    private Context context;
    private ForgetModel model;
    private ForgetMVP.view view;

    public ForgetPresenter(Context context, ForgetModel model, ForgetMVP.view view) {
        this.context = context;
        this.model = model;
        this.view = view;
    }


    @Override
    public void onSendSMSClicked(String phone) {
        if (phone.equals("")) {
            view.showErrorMessage(context.getString(R.string.error_4));
            return;
        }
        model.sendSMS(this, phone);
    }

    @Override
    public void onResetPassclicked(String phone, String code, String newPass) {
        if (phone.equals("") || code.equals("") || newPass.equals("")) {
            view.showErrorMessage(context.getString(R.string.error_1));
            return;
        }
        model.resetPassword(this, phone, code, newPass);
    }

    @Override
    public void onSuccess(JSONObject response, int which) throws JSONException {
        if (which == 1) {
            Log.e("sms response" , response.toString());
            String status = response.getString("status");
            if (status.equals("error")){
                view.showErrorMessage(response.getString("desc"));
            }else {
                view.showErrorMessage(response.getString("desc"));
                view.showSecondContainer();
            }
        } else if (which == 2) {
            Log.e("reset response" , response.toString());
            String stats = response.getString("status");
            if (stats.equals("error")) {
                view.showErrorMessage(response.getString("desc"));
            } else {
                view.showErrorMessage(context.getString(R.string.done));
                view.goLogin();
            }
        }
    }

    @Override
    public void onFail(VolleyError error, int which) {
        if (which == 1) {
            if (error instanceof NoConnectionError || error instanceof TimeoutError) {
                Toast.makeText(context, context.getString(R.string.internet_error), Toast.LENGTH_LONG).show();
            } else if (error instanceof ServerError) {
                Toast.makeText(context, context.getString(R.string.server_error), Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(context, context.getString(R.string.wrong), Toast.LENGTH_LONG).show();
            }
        } else if (which == 2) {
            if (error instanceof NoConnectionError || error instanceof TimeoutError) {
                Toast.makeText(context, context.getString(R.string.internet_error), Toast.LENGTH_LONG).show();
            } else if (error instanceof ServerError) {
                Toast.makeText(context, context.getString(R.string.server_error), Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(context, context.getString(R.string.wrong), Toast.LENGTH_LONG).show();
            }
        }
    }
}
