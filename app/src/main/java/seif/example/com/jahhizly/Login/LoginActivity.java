package seif.example.com.jahhizly.Login;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.LayoutDirection;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.podcopic.animationlib.library.AnimationType;
import com.podcopic.animationlib.library.SmartAnimation;
import com.podcopic.animationlib.library.StartSmartAnimation;
import com.tuyenmonkey.mkloader.MKLoader;

import me.anwarshahriar.calligrapher.Calligrapher;
import retrofit2.Call;
import seif.example.com.jahhizly.BookTable.BookTableActivity;
import seif.example.com.jahhizly.Branches.BranchesActivity;
import seif.example.com.jahhizly.Cart.MyCartActivity;
import seif.example.com.jahhizly.ForgetPassword.ForgetPassActivity;
import seif.example.com.jahhizly.Menus.MenusActivity;
import seif.example.com.jahhizly.R;
import seif.example.com.jahhizly.SignUp.SignUpActivity;
import seif.example.com.jahhizly.Utils.MySingleton;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.tuyenmonkey.mkloader.model.Line;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.Session;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.twitter.sdk.android.core.models.User;
import com.twitter.sdk.android.core.services.AccountService;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

public class
LoginActivity extends AppCompatActivity implements LoginMVP.view {

    EditText phoneEdtTxt, passwordEdtTxt;
    Button logBtn;
    TextView newAcc, forgPass;
    LoginPresenter presenter;
    MKLoader loading;
    FrameLayout container;
    LoginButton fbLoginBtn;
    TextView fb, twitter;
    TwitterLoginButton twtLoginBtn;
    private boolean isTwitter = false;

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Twitter.initialize(this);
        setContentView(R.layout.activity_login);
        FacebookSdk.sdkInitialize(this);
        Calligrapher calligrapher = new Calligrapher(this);
        if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            calligrapher.setFont(this, "fonts/Arabic/arabic_font.ttf", true);
            ((LinearLayout) findViewById(R.id.phone_container)).setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        } else {
            calligrapher.setFont(this, "fonts/English/Roboto_Regular.ttf", true);
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        fbLoginBtn = (LoginButton) findViewById(R.id.fb_login_button);
        twtLoginBtn = (TwitterLoginButton) findViewById(R.id.twt_login_button);
        fb = (TextView) findViewById(R.id.fb);
        twitter = (TextView) findViewById(R.id.twitter);
        presenter = new LoginPresenter(this, new LoginModel(this), this);
        loading = (MKLoader) findViewById(R.id.loading);
        container = (FrameLayout) findViewById(R.id.container);
        phoneEdtTxt = (EditText) findViewById(R.id.login_phone_edt_txt);
        phoneEdtTxt.setTextLocale(Locale.ENGLISH);
        passwordEdtTxt = (EditText) findViewById(R.id.login_pw_edt_txt);
        passwordEdtTxt.setTextLocale(Locale.ENGLISH);
        logBtn = (Button) findViewById(R.id.login_btn);
        newAcc = (TextView) findViewById(R.id.new_account_txt);
        forgPass = (TextView) findViewById(R.id.forget_pass_txt);
        callbackManager = CallbackManager.Factory.create();
        facebook(callbackManager);
        twitterS();
        fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                StartSmartAnimation.startAnimation(fb, AnimationType.FadeIn, 2000, 0, true);
                isTwitter = false;
                fbLoginBtn.performClick();
            }
        });

        twitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                StartSmartAnimation.startAnimation(twitter, AnimationType.FadeIn, 2000, 0, true);
                isTwitter = true;
                twtLoginBtn.performClick();
            }
        });

        logBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Vibrator v = (Vibrator) getSystemService(VIBRATOR_SERVICE);
                v.vibrate(50);
                presenter.onLoginBtnClicked(convertDigits(getPhone()), convertDigits(getPassword()));
            }
        });

        newAcc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                presenter.onNewAccountClicked();
            }
        });

        forgPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(LoginActivity.this, ForgetPassActivity.class));
                if (MySingleton.getmInstance(LoginActivity.this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                } else {
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                }
            }
        });
        if (getResources().getConfiguration().getLayoutDirection() == LayoutDirection.RTL) {
            phoneEdtTxt.setGravity(Gravity.RIGHT);
            passwordEdtTxt.setGravity(Gravity.RIGHT);
        }

    }


    // twitter login
    private void twitterS() {
        twtLoginBtn.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                final TwitterSession session = TwitterCore.getInstance().getSessionManager().getActiveSession();
                // get user email
                TwitterAuthClient authClient = new TwitterAuthClient();
                authClient.requestEmail(session, new Callback<String>() {
                    @Override
                    public void success(Result<String> result) {
                        String username = session.getUserName();
                        long socialId = session.getUserId();
                        String email = result.data;
                        twitterSignup(username, email, socialId);
                    }

                    @Override
                    public void failure(TwitterException exception) {
                        Toast.makeText(LoginActivity.this, exception.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });

            }

            @Override
            public void failure(TwitterException exception) {
                Toast.makeText(LoginActivity.this, exception.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void twitterSignup(String username, String email, long socialId) {
        String uName = "", eMail = "", socialIdString = String.valueOf(socialId);
        if (username != null) {
            uName = username;
        }
        if (email != null) {
            eMail = email;
        }
        presenter.requestSocial(new UserInfo(uName, eMail, socialIdString, "2"),false);
//        Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
//        intent.putExtra("LoginViaTwt", new UserInfo(uName, eMail, socialIdString, "2"));
//        startActivity(intent);
    }


    // facebook login
    CallbackManager callbackManager;

    private void facebook(CallbackManager callbackManager) {
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                GraphRequest graphRequest = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.e("userInfo", object.toString());
                        try {
                            goSignup(object);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(LoginActivity.this, getString(R.string.wrong), Toast.LENGTH_LONG).show();
                            LoginManager.getInstance().logOut();
                        }
                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "first_name ,middle_name, last_name , email ,birthday ,gender,id");
                graphRequest.setParameters(parameters);
                graphRequest.executeAsync();
            }

            @Override
            public void onCancel() {
                Toast.makeText(LoginActivity.this, "cancel", Toast.LENGTH_LONG).show();
                LoginManager.getInstance().logOut();
            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(LoginActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
                LoginManager.getInstance().logOut();
            }
        });

    }

    private void goSignup(JSONObject object) throws JSONException {
        String name = "", birthdayString = "", gender = "", email = "", socialId = object.getString("id");
        if (object.has("first_name")) {
            name = object.getString("first_name");
        }
        if (object.has("middle_name")) {
            name += " " + object.getString("middle_name");
        }
        if (object.has("last_name")) {
            name += " " + object.getString("last_name");
        }
        if (object.has("birthday")) {
            String[] birthday = object.getString("birthday").split("/");
            birthdayString = (birthday[2] + "-" + birthday[0] + "-" + birthday[1]);
        }
        if (object.has("email")) {
            email = object.getString("email");
        }
        if (object.has("gender")) {
            gender = object.getString("gender");
        }


        UserInfo userInfo = new UserInfo(name.trim(), email.trim(), birthdayString.trim(), gender.trim(), socialId, "1");
        FacebookSdk.sdkInitialize(getApplicationContext());
        LoginManager.getInstance().logOut();
        presenter.requestSocial(userInfo , true);
//        Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
//        intent.putExtra("LoginViaFB", userInfo);
//        startActivity(intent);
//        if (MySingleton.getmInstance(LoginActivity.this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
//            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
//        } else {
//            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
//        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (isTwitter) {
            twtLoginBtn.onActivityResult(requestCode, resultCode, data);
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }


    }

    @Override
    public String getPhone() {
        return phoneEdtTxt.getText().toString().trim();
    }

    @Override
    public String getPassword() {
        return passwordEdtTxt.getText().toString().trim();
    }

    @Override
    public void showErrorMessage(String error) {
        Toast.makeText(LoginActivity.this, error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void clearPw() {
        passwordEdtTxt.setText("");
    }

    @Override
    public void goRegister() {
        startActivity(new Intent(LoginActivity.this, SignUpActivity.class));
        if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        } else {
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }
    }

    @Override
    public void goHome() {
        // intent to home activity
        if (getIntent().hasExtra("LoginIntent")) {
            String intentDataString = getIntent().getExtras().getString("LoginIntent");
            if (intentDataString.equals("FromCart")) {
                Intent intent = new Intent(this, MyCartActivity.class);
                intent.putExtra("CartIntent", "FromLogin");
                startActivity(intent);
                finish();
                if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                } else {
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                }
            } else if (intentDataString.equals("FromReview")) {
                Intent intent = new Intent(this, MenusActivity.class);
                intent.putExtra("ReviewIntent", "FromLogin");
                intent.putExtra("ReviewObject", getIntent().getExtras().getSerializable("ReviewBranchObject"));
                startActivity(intent);
                finish();
                if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                } else {
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                }
            } else if (intentDataString.equals("FromMenuToBookTable")) {
                Intent intent = new Intent(this, BookTableActivity.class);
                intent.putExtra("BookTableBranch", getIntent().getExtras().getSerializable("BookTableBranch"));
                startActivity(intent);
                if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                } else {
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                }
            }
        } else {
            Intent intent = new Intent(getApplicationContext(), BranchesActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            } else {
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        }

    }

    @Override
    public void showProgressDialog() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        loading.setVisibility(View.VISIBLE);
        container.setForeground(new ColorDrawable(Color.parseColor("#66000000")));
    }

    @Override
    public void hideProgressDialog() {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        loading.setVisibility(View.GONE);
        container.setForeground(new ColorDrawable(Color.TRANSPARENT));
    }

    @Override
    public void onSocialNotExist(UserInfo userInfo, boolean isFacebook) {
        Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
        intent.putExtra(isFacebook ? "LoginViaFB" : "LoginViaTwt", userInfo);
        startActivity(intent);
        if (MySingleton.getmInstance(LoginActivity.this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        } else {
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            } else {
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private String convertDigits(String string) {
        String rslt = "";
        for (int i = 0; i < string.length(); i++) {
            switch (string.charAt(i)) {
                case '٠':
                    rslt += "0";
                    break;
                case '١':
                    rslt += "1";
                    break;
                case '٢':
                    rslt += "2";
                    break;
                case '٣':
                    rslt += "3";
                    break;
                case '٤':
                    rslt += "4";
                    break;
                case '٥':
                    rslt += "5";
                    break;
                case '٦':
                    rslt += "6";
                    break;
                case '٧':
                    rslt += "7";
                    break;
                case '٨':
                    rslt += "8";
                    break;
                case '٩':
                    rslt += "9";
                    break;
                default:
                    rslt += string.charAt(i);
            }
        }
        return rslt;
    }


}
