package seif.example.com.jahhizly.Login;

import android.content.Context;

/**
 * Created by seif on 10/21/2017.
 */
public interface LoginMVP {

    interface view {
        String getPhone();

        String getPassword();

        void showErrorMessage(String error);

        void clearPw();

        void goRegister();

        void goHome();

        void showProgressDialog();

        void hideProgressDialog();

        void onSocialNotExist(UserInfo userInfo, boolean isFacebook);

    }

    interface presenter {
        void onLoginBtnClicked(String phone, String password);

        void onNewAccountClicked();

        void onForgetPasswordClicked();

        void requestSocial(UserInfo userInfo, boolean isFacebook);
    }
}
