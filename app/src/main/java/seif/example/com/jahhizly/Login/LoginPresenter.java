package seif.example.com.jahhizly.Login;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import seif.example.com.jahhizly.Other.MyAndroidFirebaseInstanceIdService;
import seif.example.com.jahhizly.R;
import seif.example.com.jahhizly.Utils.MySingleton;

/**
 * Created by seif on 10/21/2017.
 */
public class LoginPresenter implements LoginMVP.presenter, LoginModel.VolleyCallback {

    LoginMVP.view view;
    LoginModel model;
    Context context;
    LoginModel.VolleyCallback callback;

    public LoginPresenter(LoginMVP.view view, LoginModel model, Context context) {
        this.view = view;
        this.model = model;
        this.context = context;
        callback = this;
    }

    @Override
    public void onLoginBtnClicked(String phone, String password) {
        if (phone.equals("") || password.equals("")) {
            view.showErrorMessage(context.getString(R.string.error_1));
            view.clearPw();
            return;
        }
        ////////////////////////
        // Login Api at model //
        ////////////////////////
        view.showProgressDialog();
        model.Login(callback, phone, password);
    }

    @Override
    public void onNewAccountClicked() {
        view.goRegister();
    }

    @Override
    public void onForgetPasswordClicked() {

    }

    @Override
    public void requestSocial(UserInfo userInfo, boolean isFacebook) {
        view.showProgressDialog();
        model.socialLogin(this, userInfo, isFacebook);
    }

    @Override
    public void onSuccess(String response, int position, UserInfo userInfo, boolean isFacebook) throws JSONException {
        if (position == 1) {
            Log.e("login response ", response.toString());
            JSONObject responseJson = new JSONObject(response);
            String token = responseJson.getString("token");
            MySingleton.getmInstance(context).saveUser(token);
            MyAndroidFirebaseInstanceIdService seif = new MyAndroidFirebaseInstanceIdService(context);
            seif.onTokenRefresh();
            model.getProfile(callback);
        } else if (position == 2) {
            JSONObject jsonResponse = new JSONObject(response);
            JSONObject user = jsonResponse.getJSONObject("user");
            int id = user.getInt("id");
            String name;
            if (user.getString("name").equals("") || user.getString("name").equals("null")) {
                name = context.getString(R.string.list_empty);
            } else {
                name = user.getString("name");
            }
            String email;
            if (user.getString("email").equals("") || user.getString("email").equals("null")) {
                email = context.getString(R.string.list_empty);
            } else {
                email = user.getString("email");
            }

            String phone;
            if (user.getString("phone").equals("") || user.getString("phone").equals("null")) {
                phone = context.getString(R.string.list_empty);
            } else {
                phone = user.getString("phone");
            }

            String photo;
            if (user.getString("photo").equals("") || user.getString("photo").equals("null")) {
                photo = null;
            } else {
                photo = user.getString("photo");
            }
            String birthDate;
            if (user.getString("birthday").equals("") || user.getString("birthday").equals("null")) {
                birthDate = context.getString(R.string.list_empty);
            } else {
                birthDate = user.getString("birthday");
            }


            String city;
            if (user.getString("city").equals("") || user.getString("city").equals("null")) {
                city = context.getString(R.string.list_empty);
            } else {
                city = user.getString("city");
            }


            String gender;
            if (user.getString("gender").equals("") || user.getString("gender").equals("null")) {
                gender = context.getString(R.string.list_empty);
            } else {
                if (user.getString("gender").equals("0")) {
                    gender = "Male";
                } else {
                    gender = "Female";
                }
            }
            UserInfo profileData = new UserInfo(id, name, email, phone,
                    photo, birthDate, city, gender);
            Log.e("first req", profileData.getName() + "  Seifoo");
            MySingleton.getmInstance(context).saveUserData(profileData);
            MySingleton.getmInstance(context).loginUser();
            view.hideProgressDialog();
            view.goHome();
        } else if (position == 3) {
            view.hideProgressDialog();
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.has("status")) {
                view.onSocialNotExist(userInfo, isFacebook);
            } else {
                String token = jsonObject.getString("token");
                MySingleton.getmInstance(context).saveUser(token);
                model.getProfile(this);
            }

        }
    }

    @Override
    public void onFail(VolleyError error) {
        view.hideProgressDialog();
        if (error instanceof AuthFailureError) {
            Toast.makeText(context, context.getString(R.string.auth_failure_error_1), Toast.LENGTH_LONG).show();
            return;
        }
        if (error instanceof NoConnectionError || error instanceof TimeoutError) {
            Toast.makeText(context, context.getString(R.string.internet_error), Toast.LENGTH_LONG).show();
            return;
        }
        if (error instanceof ServerError) {
            Toast.makeText(context, context.getString(R.string.server_error), Toast.LENGTH_LONG).show();
            return;
        }
        Toast.makeText(context, context.getString(R.string.wrong), Toast.LENGTH_LONG).show();
        return;

    }
}
