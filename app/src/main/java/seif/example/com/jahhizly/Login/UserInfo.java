package seif.example.com.jahhizly.Login;

import java.io.Serializable;

/**
 * Created by seif on 10/23/2017.
 */
public class UserInfo implements Serializable {

    private int id;
    private String name;
    private String email;
    private String phone;
    private String photo;
    private String birthDate;
    private String city;
    private String gender;
    private String password;
    private String socialId;
    private String loginType;

    public String getSocialId() {
        return socialId;
    }

    public String getLoginType() {
        return loginType;
    }

    public UserInfo() {
    }


    // facebook
    public UserInfo(String name, String email, String birthDate, String gender, String socialId, String loginType) {
        this.name = name;
        this.email = email;
        this.birthDate = birthDate;
        this.gender = gender;
        this.socialId = socialId;
        this.loginType = loginType;
    }

    // twitter
    public UserInfo(String name, String email, String socialId, String loginType) {
        this.name = name;
        this.email = email;
        this.socialId = socialId;
        this.loginType = loginType;
    }

    // edituser , login , verification
    public UserInfo(int id, String name, String email, String phone,
                    String photo, String birthDate, String city, String gender) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.photo = photo;
        this.birthDate = birthDate;
        this.city = city;
        this.gender = gender;
    }

    // sign-up
    public UserInfo(String name, String email, String phone, String birthDate, String city, String gender, String password, String loginType, String socialId) {
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.birthDate = birthDate;
        this.city = city;
        this.gender = gender;
        this.password = password;
        this.loginType = loginType;
        this.socialId = socialId;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    // sign-up
    public UserInfo(String photo, String name, String email, String phone, String birthDate, String city, String gender, String password, String loginType, String socialId) {
        this.photo = photo;
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.birthDate = birthDate;
        this.city = city;
        this.gender = gender;
        this.password = password;
        this.loginType = loginType;
        this.socialId = socialId;
    }

    public String getGender() {
        return gender;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
