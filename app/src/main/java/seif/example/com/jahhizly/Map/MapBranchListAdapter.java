package seif.example.com.jahhizly.Map;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import seif.example.com.jahhizly.Branches.MakeOrder.Branches;
import seif.example.com.jahhizly.Other.CircleTransform;
import seif.example.com.jahhizly.R;
import seif.example.com.jahhizly.Utils.URLs;

/**
 * Created by seif on 11/14/2017.
 */
public class MapBranchListAdapter extends RecyclerView.Adapter<MapBranchListAdapter.Holder> {
    private ArrayList<Branches> branchData = new ArrayList<>();
    final private ListItemClickListener mOnClickListener;
    private boolean flag;

    public void setMapBranches(ArrayList<Branches> branchData) {
        if (branchData != null && branchData.size() > 0) {
            this.branchData = branchData;
            notifyDataSetChanged();
        }
    }

    public void clear() {
        branchData.clear();
        notifyDataSetChanged();
    }

    public MapBranchListAdapter(ListItemClickListener listener, boolean flag) {
        mOnClickListener = listener;
        this.flag = flag;
    }

    public interface ListItemClickListener {
        void onListItemClick(int position);
    }

    @Override
    public MapBranchListAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Holder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.map_branch_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(MapBranchListAdapter.Holder holder, int position) {
        Glide.with(holder.itemView.getContext()).load(URLs.ROOT_URL + branchData.get(position).getBrandPhoto())
                .error(R.mipmap.restaurant_meal_icon)
                .crossFade()
                .thumbnail(0.5f)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.logo);
        holder.name.setText(flag ? convertDigits(branchData.get(position).getName()) : branchData.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return (null != branchData ? branchData.size() : 0);
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        CircleImageView logo;
        TextView name;

        public Holder(View itemView) {
            super(itemView);
            logo = (CircleImageView) itemView.findViewById(R.id.map_list_branch_logo);
            name = (TextView) itemView.findViewById(R.id.map_list_branch_name);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int clickedPosition = getAdapterPosition();
            mOnClickListener.onListItemClick(clickedPosition);
        }
    }

    private String convertDigits(String string) {
        String rslt = "";
        for (int i = 0; i < string.length(); i++) {
            switch (string.charAt(i)) {
                case '0':
                    rslt += "٠";
                    break;
                case '1':
                    rslt += "١";
                    break;
                case '2':
                    rslt += "٢";
                    break;
                case '3':
                    rslt += "٣";
                    break;
                case '4':
                    rslt += "٤";
                    break;
                case '5':
                    rslt += "٥";
                    break;
                case '6':
                    rslt += "٦";
                    break;
                case '7':
                    rslt += "٧";
                    break;
                case '8':
                    rslt += "٨";
                    break;
                case '9':
                    rslt += "٩";
                    break;
                default:
                    rslt += string.charAt(i);
            }
        }
        return rslt;
    }

}
