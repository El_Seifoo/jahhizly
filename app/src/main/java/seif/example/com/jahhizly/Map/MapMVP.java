package seif.example.com.jahhizly.Map;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import seif.example.com.jahhizly.Branches.MakeOrder.Branches;

/**
 * Created by seif on 11/8/2017.
 */
public interface MapMVP {
    interface view {
        void loadData(ArrayList<Branches> data);

        void showEmptyText(String message);
    }

    interface presenter {
        void onPickLocation(double latitude, double longitude, String type, ArrayList<String> category, String sort, String key) throws UnsupportedEncodingException;
    }
}
