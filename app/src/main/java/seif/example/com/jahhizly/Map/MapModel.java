package seif.example.com.jahhizly.Map;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import seif.example.com.jahhizly.Utils.MySingleton;
import seif.example.com.jahhizly.Utils.URLs;

/**
 * Created by seif on 11/8/2017.
 */
public class MapModel {
    Context context;

    public MapModel(Context context) {
        this.context = context;
    }

    protected void getNearestBranches(final VolleyCallback callback, double latitude, double longitude, String type, String category, String sort, String key) throws UnsupportedEncodingException {
        String url;
        if (sort == null) {
            sort = "distance";
        }
        if (type == null) {
            type = "0";
        }
        if (category == null) {
            if (key.equals("")) {
                url = URLs.BRANCHES + latitude + "/" + longitude + "/" + sort + "/" + type + "/" + 0;
            } else {
                url = URLs.BRANCHES + latitude + "/" + longitude + "/" + sort + "/" + type + "/" + 0 + "/" + URLEncoder.encode(key, "utf-8");
            }
        } else {
            if (key.equals("")) {
                url = URLs.BRANCHES + latitude + "/" + longitude + "/" + sort + "/" + type + "/" + category;
            } else {
                url = URLs.BRANCHES + latitude + "/" + longitude + "/" + sort + "/" + type + "/" + category + "/" + URLEncoder.encode(key, "utf-8");
            }
        }
        Log.e("map branches", url);
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("branches", response);
                        try {
                            callback.onSuccess(response);
                        } catch (JSONException e) {
                            Log.e("json error", e.toString());
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", error.toString());
                callback.onFail(error);
            }
        });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getmInstance(context).addToRQ(stringRequest);
    }

    protected void getNearestBranches(final VolleyCallback callback, double latitude, double longitude, String type, ArrayList<String> categories, String sort, String key) {
        try {
            if (sort.equals("")) {
                sort = "distance";
            }
            if (type == null) {
                type = "0";
            }
            JSONArray array = new JSONArray();
            JSONObject params = new JSONObject();
            if (categories.isEmpty() || categories.get(0).equals("0")) {

                params.put("lat", latitude);
                params.put("lng", longitude);
                params.put("sort", sort);
                params.put("type", type);
                if (!key.equals("")) {
                    params.put("key", key);
                }
                params.put("categories", array);
                Log.e("sssssss", params.toString());

            } else {
                for (int i = 0; i < categories.size(); i++) {
                    array.put(categories.get(i));
                }
                params.put("lat", latitude);
                params.put("lng", longitude);
                params.put("sort", sort);
                params.put("type", type);
                if (!key.equals("")) {
                    params.put("key", key);
                }
                params.put("categories", array);
                Log.e("sssssss", params.toString());
            }

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                    URLs.SHOPPER_BRANCHES,
                    params,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e("aaaaaaaa",response.toString());
                            try {
                                callback.onSuccess(response.toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Log.e("eeeeee",e.toString());
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e("ssssss",error.toString());
                            callback.onFail(error);
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    return headers;
                }
            };
            MySingleton.getmInstance(context).addToRQ(jsonObjectRequest);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected interface VolleyCallback {
        void onSuccess(String response) throws JSONException;

        void onFail(VolleyError error);
    }
}
