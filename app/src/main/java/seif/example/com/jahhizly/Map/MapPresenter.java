package seif.example.com.jahhizly.Map;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.ArrayList;

import seif.example.com.jahhizly.Branches.MakeOrder.Branches;
import seif.example.com.jahhizly.R;
import seif.example.com.jahhizly.Utils.MySingleton;

/**
 * Created by seif on 11/8/2017.
 */
public class MapPresenter implements MapMVP.presenter, MapModel.VolleyCallback {

    Context context;
    MapModel model;
    MapMVP.view view;

    public MapPresenter(Context context, MapModel model, MapMVP.view view) {
        this.context = context;
        this.model = model;
        this.view = view;
    }

    @Override
    public void onPickLocation(double latitude, double longitude, String type, ArrayList<String> category, String sort, String key) throws UnsupportedEncodingException {
        model.getNearestBranches(this, latitude, longitude, type, category, sort, key);
    }

    @Override
    public void onSuccess(String response) throws JSONException {
        ArrayList<Branches> data = new ArrayList<>();
        if (MySingleton.getmInstance(context).getAppLang().equals(context.getString(R.string.settings_language_english_value))) {
            JSONObject jsonResponse = new JSONObject(response);
            JSONArray branches = jsonResponse.getJSONArray("branches");
            int branchesLength = branches.length();
            for (int i = 0; i < branchesLength; i++) {
                int id = branches.getJSONObject(i).getInt("id");
                Log.e("position id", String.valueOf(id));
                String name;
                if (branches.getJSONObject(i).getString("name").equals("") ||
                        branches.getJSONObject(i).getString("name").equals("null")) {
                    name = "-";
                } else {
                    name = branches.getJSONObject(i).getString("name");
                }
                String address = "";
//                if (branches.getJSONObject(i).getString("address").equals("") ||
//                        branches.getJSONObject(i).getString("address").equals("null")) {
//                    address = "-";
//                } else {
//                    address = branches.getJSONObject(i).getString("address");
//                }
                int brandId = branches.getJSONObject(i).getInt("brand_id");
                String brandName = branches.getJSONObject(i).getString("brand_name");
                String openHour;
                if (branches.getJSONObject(i).getString("open_time").equals("") ||
                        branches.getJSONObject(i).getString("open_time").equals("null")) {
                    openHour = "";
                } else {
                    openHour = branches.getJSONObject(i).getString("open_time");
                }
                String closeHour;
                if (branches.getJSONObject(i).getString("close_time").equals("") ||
                        branches.getJSONObject(i).getString("close_time").equals("null")) {
                    closeHour = "";
                } else {
                    closeHour = branches.getJSONObject(i).getString("close_time");
                }
                String brandPhoto = branches.getJSONObject(i).getString("brand_photo");
                String brandCategory = branches.getJSONObject(i).getString("brand_category");
                double distance;
                if (branches.getJSONObject(i).getString("distance").equals("")
                        || branches.getJSONObject(i).getString("distance").equals("null")) {
                    distance = 0;
                } else {

                    distance = branches.getJSONObject(i).getDouble("distance");
                    BigDecimal bigDecimal;
                    if (distance > 0) {
                        bigDecimal = new BigDecimal(String.valueOf(distance)).setScale(2, BigDecimal.ROUND_FLOOR);
                    } else {
                        bigDecimal = new BigDecimal(String.valueOf(distance)).setScale(2, BigDecimal.ROUND_CEILING);
                    }

                    distance = bigDecimal.doubleValue();
                }
                double latitude = branches.getJSONObject(i).getDouble("latitude");
                double longitude = branches.getJSONObject(i).getDouble("longitude");
                double rate;
                if (branches.getJSONObject(i).getString("rate").equals("")
                        || branches.getJSONObject(i).getString("rate").equals("null")) {
                    rate = 0;
                } else {
                    rate = branches.getJSONObject(i).getDouble("rate");
                    BigDecimal bigDecimal;
                    if (rate > 0) {
                        bigDecimal = new BigDecimal(String.valueOf(rate)).setScale(1, BigDecimal.ROUND_FLOOR);
                    } else {
                        bigDecimal = new BigDecimal(String.valueOf(rate)).setScale(1, BigDecimal.ROUND_CEILING);
                    }

                    rate = bigDecimal.doubleValue();
                }

                String tablesPosition;
                if (branches.getJSONObject(i).getString("tables_positions").equals("") ||
                        branches.getJSONObject(i).getString("tables_positions").equals("null")) {
                    tablesPosition = "null";
                } else {
                    tablesPosition = branches.getJSONObject(i).getString("tables_positions");
                }
                int type;
                if (branches.getJSONObject(i).getString("type").equals("") ||
                        branches.getJSONObject(i).getString("type").equals("null")) {
                    type = -1;
                } else {
                    type = branches.getJSONObject(i).getInt("type");
                }
                int maxGuests;
                if (branches.getJSONObject(i).getString("max_guests").equals("") ||
                        branches.getJSONObject(i).getString("max_guests").equals("null")) {
                    maxGuests = 0;
                } else {
                    maxGuests = branches.getJSONObject(i).getInt("max_guests");
                }
                data.add(new Branches(id, name, address, 0123, brandId, false, brandName, openHour, closeHour,
                        brandPhoto, brandCategory, distance, latitude, longitude, tablesPosition, rate, false, type, maxGuests));
            }
        } else {
            JSONObject jsonResponse = new JSONObject(response);
            JSONArray branches = jsonResponse.getJSONArray("branches");
            int branchesLength = branches.length();
            for (int i = 0; i < branchesLength; i++) {
                int id = branches.getJSONObject(i).getInt("id");
                Log.e("position id", String.valueOf(id));
                String name;
                if (branches.getJSONObject(i).getString("arabic_name").equals("") ||
                        branches.getJSONObject(i).getString("arabic_name").equals("null")) {
                    name = "-";
                } else {
                    name = branches.getJSONObject(i).getString("arabic_name");
                }
                String address = "";
//                if (branches.getJSONObject(i).getString("arabic_address").equals("") ||
//                        branches.getJSONObject(i).getString("arabic_address").equals("null")) {
//                    address = "-";
//                } else {
//                    address = branches.getJSONObject(i).getString("arabic_address");
//                }
                int brandId = branches.getJSONObject(i).getInt("brand_id");
                String brandName = branches.getJSONObject(i).getString("brand_name");
                String openHour;
                if (branches.getJSONObject(i).getString("open_hour").equals("") ||
                        branches.getJSONObject(i).getString("open_hour").equals("null")) {
                    openHour = "";
                } else {
                    openHour = branches.getJSONObject(i).getString("open_hour");
                }
                String closeHour;
                if (branches.getJSONObject(i).getString("close_hour").equals("") ||
                        branches.getJSONObject(i).getString("close_hour").equals("null")) {
                    closeHour = "";
                } else {
                    closeHour = branches.getJSONObject(i).getString("close_hour");
                }
                String brandPhoto = branches.getJSONObject(i).getString("brand_photo");
                String brandCategory = branches.getJSONObject(i).getString("arabic_brand_category");
                double distance;
                if (branches.getJSONObject(i).getString("distance").equals("")
                        || branches.getJSONObject(i).getString("distance").equals("null")) {
                    distance = 0;
                } else {

                    distance = branches.getJSONObject(i).getDouble("distance");
                    BigDecimal bigDecimal;
                    if (distance > 0) {
                        bigDecimal = new BigDecimal(String.valueOf(distance)).setScale(2, BigDecimal.ROUND_FLOOR);
                    } else {
                        bigDecimal = new BigDecimal(String.valueOf(distance)).setScale(2, BigDecimal.ROUND_CEILING);
                    }

                    distance = bigDecimal.doubleValue();
                }
                double latitude = branches.getJSONObject(i).getDouble("latitude");
                double longitude = branches.getJSONObject(i).getDouble("longitude");
                double rate;
                if (branches.getJSONObject(i).getString("rate").equals("")
                        || branches.getJSONObject(i).getString("rate").equals("null")) {
                    rate = 0;
                } else {
                    rate = branches.getJSONObject(i).getDouble("rate");
                    BigDecimal bigDecimal;
                    if (rate > 0) {
                        bigDecimal = new BigDecimal(String.valueOf(rate)).setScale(1, BigDecimal.ROUND_FLOOR);
                    } else {
                        bigDecimal = new BigDecimal(String.valueOf(rate)).setScale(1, BigDecimal.ROUND_CEILING);
                    }

                    rate = bigDecimal.doubleValue();
                }

                String tablesPosition;
                if (branches.getJSONObject(i).getString("tables_positions").equals("") ||
                        branches.getJSONObject(i).getString("tables_positions").equals("null")) {
                    tablesPosition = "null";
                } else {
                    tablesPosition = branches.getJSONObject(i).getString("tables_positions");
                }
                int type;
                if (branches.getJSONObject(i).getString("type").equals("") ||
                        branches.getJSONObject(i).getString("type").equals("null")) {
                    type = -1;
                } else {
                    type = branches.getJSONObject(i).getInt("type");
                }
                int maxGuests;
                if (branches.getJSONObject(i).getString("max_guests").equals("") ||
                        branches.getJSONObject(i).getString("max_guests").equals("null")) {
                    maxGuests = 0;
                } else {
                    maxGuests = branches.getJSONObject(i).getInt("max_guests");
                }
                data.add(new Branches(id, name, address, 0123, brandId, false, brandName, openHour, closeHour,
                        brandPhoto, brandCategory, distance, latitude, longitude, tablesPosition, rate, false, type, maxGuests));
            }
        }
        if (data != null && data.size() > 0) {
            view.loadData(data);
        } else {
            view.showEmptyText(context.getString(R.string.list_empty));
        }
    }

    @Override
    public void onFail(VolleyError error) {
        if (error instanceof NoConnectionError || error instanceof TimeoutError) {
            Toast.makeText(context, context.getString(R.string.internet_error), Toast.LENGTH_LONG).show();
        } else if (error instanceof ServerError) {
            Toast.makeText(context, context.getString(R.string.server_error), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(context, context.getString(R.string.wrong), Toast.LENGTH_LONG).show();
        }
    }
}
