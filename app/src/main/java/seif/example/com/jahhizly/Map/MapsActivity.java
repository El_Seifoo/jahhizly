package seif.example.com.jahhizly.Map;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;
import me.anwarshahriar.calligrapher.Calligrapher;
import seif.example.com.jahhizly.Branches.BranchCategories;
import seif.example.com.jahhizly.Branches.MakeOrder.Branches;
import seif.example.com.jahhizly.Menus.MenusActivity;
import seif.example.com.jahhizly.R;
import seif.example.com.jahhizly.Utils.MySingleton;
import seif.example.com.jahhizly.Utils.URLs;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener, MapMVP.view, MapBranchListAdapter.ListItemClickListener {

    private GoogleMap mMap;
    GoogleApiClient googleApiClient;
    Location pickupLocation;
    LocationRequest locationRequest;
    SupportMapFragment mapFragment;
    MapPresenter presenter;
    MapBranchListAdapter adapter;
    RecyclerView mapRecView;
    RecyclerView.LayoutManager layout;
    ArrayList<BranchCategories> categories;
    private String type = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(seif.example.com.jahhizly.R.layout.activity_maps);
        Log.e("create", "created");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.title_activity_maps));
        Calligrapher calligrapher = new Calligrapher(this);
        if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            calligrapher.setFont(this, "fonts/Arabic/arabic_font.ttf", true);
        } else {
            calligrapher.setFont(this, "fonts/English/Roboto_Regular.ttf", true);
        }
        categories = (ArrayList<BranchCategories>) getIntent().getExtras().get("categories");
        for (int i = 0; i < categories.size(); i++) {
            Log.e("cat", categories.get(i).getName());
        }
        type = getIntent().getExtras().getString("type");
        sort = getIntent().getExtras().getString("sort");
        selectedCategories = (ArrayList<String>) getIntent().getExtras().get("selectedCategories");
        for (int x = 0; x > selectedCategories.size(); x++) {
            Log.e("seesesesese", selectedCategories.get(x));
        }
        pickupLocation = new Location("");
        pickupLocation.setLatitude(-1);
        pickupLocation.setLongitude(-1);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        presenter = new MapPresenter(this, new MapModel(this), this);
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        container = (FrameLayout) findViewById(R.id.map_list_container);
        mapRecView = (RecyclerView) findViewById(R.id.map_branch_rec_view);
        layout = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mapRecView.setLayoutManager(layout);
        mapRecView.setHasFixedSize(true);
        adapter = new MapBranchListAdapter(this, MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? true : false);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MapsActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST_CODE);
        } else {
            mapFragment.getMapAsync(this);
        }

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    private LatLng pickedLocation;
    View view;

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.e("ready", "ready");
        mMap = googleMap;
        mMap.animateCamera(CameraUpdateFactory.zoomTo(12.0f));
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        buildGoogleApiClient();
        mMap.setMyLocationEnabled(true);
        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoContents(Marker marker) {
                return null;
            }

            @Override
            public View getInfoWindow(final Marker marker) {

                for (int i = 0; i < branches.size(); i++) {
                    if (branchesMarker.get(i).equals(marker)) {
                        Log.e("zzzzzzzzz", "7mada");
                        view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.map_info_window, null);
                        ImageView logo = (ImageView) view.findViewById(R.id.map_branch_logo);
                        Glide.with(getApplicationContext()).load(URLs.ROOT_URL + branches.get(i).getBrandPhoto())
                                .error(R.mipmap.ic_launcher)
                                .crossFade()
                                .thumbnail(0.5f)
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .listener(new RequestListener<String, GlideDrawable>() {
                                    @Override
                                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                        return false;
                                    }

                                    @Override
                                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                        if (!isFromMemoryCache) marker.showInfoWindow();
                                        return false;
                                    }
                                })
                                .into(logo);
                        TextView branchName = (TextView) view.findViewById(R.id.map_branch_name);
                        branchName.setText(branches.get(i).getName());

                        TextView branchRate = (TextView) view.findViewById(R.id.map_branch_rate);
                        if (branches.get(i).getRate() >= 0 && branches.get(i).getRate() <= 3) {
                            branchRate.setBackground(getResources().getDrawable(R.mipmap.con_rate_restaurant));
                        } else if (branches.get(i).getRate() > 3 && branches.get(i).getRate() <= 4) {
                            branchRate.setBackground(getResources().getDrawable(R.mipmap.con_rate_restaurant_y));
                        } else if (branches.get(i).getRate() > 4 && branches.get(i).getRate() <= 5) {
                            branchRate.setBackground(getResources().getDrawable(R.mipmap.con_rate_restaurant_g));
                        }
                        branchRate.setText(String.valueOf((float) branches.get(i).getRate()));
                        ImageView bookTable = (ImageView) view.findViewById(R.id.map_branch_book_table_type);
                        ImageView makeOrder = (ImageView) view.findViewById(R.id.map_branch_make_order_type);
                        if (branches.get(i).getType() == 0) {
                            makeOrder.setVisibility(View.VISIBLE);
                            bookTable.setVisibility(View.VISIBLE);
                        } else if (branches.get(i).getType() == 1) {
                            makeOrder.setVisibility(View.GONE);
                            bookTable.setVisibility(View.VISIBLE);
                        } else if (branches.get(i).getType() == 2) {
                            makeOrder.setVisibility(View.VISIBLE);
                            bookTable.setVisibility(View.GONE);
                        }
                        break;
                    }
                }
                return view;
            }
        });
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {

            @Override
            public boolean onMarkerClick(Marker marker) {
                Marker lastOpened = null;
                Log.e("zzzzzzzzz", "7mada 0");
                for (int i = 0; i < branches.size(); i++) {
                    Log.e("zzzzzzzzz", "7mada 1");
                    if (branchesMarker.get(i).equals(marker)) {
                        Log.e("zzzzzzzzz", "7mada 2");
                        if (lastOpened != null) {
                            lastOpened.hideInfoWindow();
                            if (lastOpened.equals(marker)) {
                                lastOpened = null;
                                return true;
                            }
                        }
                        marker.showInfoWindow();
                        lastOpened = marker;
                        return true;

                    }
                }

                return true;
            }
        });

        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                for (int i = 0; i < branches.size(); i++) {
                    if (branchesMarker.get(i).equals(marker)) {
                        Intent intent = new Intent(getApplicationContext(), MenusActivity.class);
                        intent.putExtra("branchObject", branches.get(i));
                        startActivity(intent);
                        if (MySingleton.getmInstance(MapsActivity.this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
                            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                        } else {
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        }
                    }
                }
            }
        });
        mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
                mMap.clear();
                showEmptyText("");
                pickedLocation = cameraPosition.target;


                pickupLocation.setLatitude(pickedLocation.latitude);
                pickupLocation.setLongitude(pickedLocation.longitude);
                Log.e("picked up location", pickupLocation.getLatitude() + "()()()()" + pickupLocation.getLongitude());
                try {
                    if (selectedCategories.size() > 0 && selectedCategories != null) {
                        presenter.onPickLocation(pickupLocation.getLatitude(), pickupLocation.getLongitude(), type, selectedCategories, sort, key);
                    } else {
                        presenter.onPickLocation(pickupLocation.getLatitude(), pickupLocation.getLongitude(), type, getCategories(linear), sort, key);
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    private synchronized void buildGoogleApiClient() {
        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        locationRequest = new LocationRequest();
        locationRequest.setInterval(1000);
        locationRequest.setFastestInterval(1000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MapsActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST_CODE);
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    int times = 0;

    @Override
    public void onLocationChanged(Location location) {
//        lastLocation = location;
        Log.e("location", location.getLatitude() + "//////" + location.getLongitude());
        if (times == 0) {
            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(11));
            times = 1;
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    final int LOCATION_REQUEST_CODE = 1;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case LOCATION_REQUEST_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mapFragment.getMapAsync(this);
                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.add_permission), Toast.LENGTH_LONG).show();
                }
                break;
            }
        }
    }

    ArrayList<Marker> branchesMarker = new ArrayList<>();
    ArrayList<Branches> branches;

    @Override
    public void loadData(ArrayList<Branches> data) {
        branches = data;
        branchesMarker.clear();
        Log.e("data", "" + data.size());
        for (int i = 0; i < data.size(); i++) {
            branchesMarker.add(mMap.addMarker(new MarkerOptions().position(new LatLng(data.get(i).getLatitude(), data.get(i).getLongitude())).icon(BitmapDescriptorFactory.fromResource(R.mipmap.map_near_mark))));
            Log.e("bbb", data.get(i).getName());
        }

        container.setVisibility(View.VISIBLE);
        adapter.setMapBranches(data);
        ScaleInAnimationAdapter scaleInAnimationAdapter = new ScaleInAnimationAdapter(adapter);
        scaleInAnimationAdapter.setFirstOnly(false);
        mapRecView.setAdapter(scaleInAnimationAdapter);
        adapter.notifyDataSetChanged();

    }

    FrameLayout container;

    @Override
    public void showEmptyText(String message) {
        adapter.clear();
        container.setVisibility(View.GONE);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.map_menu_icons, menu);
        MenuItem item = menu.findItem(R.id.action_search);
        setSearchView();
        searchView.setMenuItem(item);
        return true;
    }

    MaterialSearchView searchView;

    private void setSearchView() {
        searchView = (MaterialSearchView) findViewById(R.id.search_view);
        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
                                              @Override
                                              public boolean onQueryTextSubmit(String query) {
                                                  //Do some magic
                                                  Log.e("search 2", query);
                                                  key = query;
                                                  try {
                                                      selectedCategories = getCategories(linear);
                                                      presenter.onPickLocation(pickupLocation.getLatitude(), pickupLocation.getLongitude(), type, selectedCategories, sort, query);
                                                  } catch (UnsupportedEncodingException e) {
                                                      e.printStackTrace();
                                                  }
                                                  return true;
                                              }


                                              @Override
                                              public boolean onQueryTextChange(String newText) {
                                                  //Do some magic

                                                  Log.e("search 1", newText);
                                                  key += newText;
                                                  try {
                                                      selectedCategories = getCategories(linear);
                                                      presenter.onPickLocation(pickupLocation.getLatitude(), pickupLocation.getLongitude(), type, selectedCategories, sort, newText);
                                                  } catch (UnsupportedEncodingException e) {
                                                      e.printStackTrace();
                                                  }

                                                  return true;
                                              }
                                          }

        );

        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener()

                                           {
                                               @Override
                                               public void onSearchViewShown() {
                                                   //Do some magic

                                               }

                                               @Override
                                               public void onSearchViewClosed() {
                                                   //Do some magic

                                               }
                                           }

        );
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent();
                intent.putExtra("latitude", pickupLocation.getLatitude());
                intent.putExtra("longitude", pickupLocation.getLongitude());
                intent.putExtra("sort", sort);
                for (int i = 0; i < selectedCategories.size(); i++) {
                    Log.e("ssssssssssssss", selectedCategories.get(i) + "");
                }
                intent.putStringArrayListExtra("categories", selectedCategories);
                intent.putExtra("type", type);
                intent.putExtra("flag", true);
                setResult(RESULT_OK, intent);
                finish();
                if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                } else {
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                }
                return true;
            case R.id.action_filter:
                createFilterDialog();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    String preSort;
    String sort = "";
    String category;
    String key = "";

    private void createFilterDialog() {
        if (selectedCategories.size() > 0 && selectedCategories != null) {
            selectedCategories.clear();
            type = "";
            sort = "";
        }
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.filter_dialog);
        final Button rate = (Button) dialog.findViewById(R.id.filter_rate_btn);
        final Button distance = (Button) dialog.findViewById(R.id.filter_distance_btn);
        final RadioGroup radioGroup = (RadioGroup) dialog.findViewById(R.id.filter_type_container);
        ((RadioButton) radioGroup.getChildAt(0)).setChecked(true);
        distance.setBackgroundColor(rate.getContext().getResources().getColor(R.color.red_3));
        distance.setTextColor(rate.getContext().getResources().getColor(R.color.white));
        preSort = getString(R.string.sort_distance);
        setCategories(dialog, categories);
        rate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rate.setBackgroundColor(rate.getContext().getResources().getColor(R.color.red_3));
                rate.setTextColor(rate.getContext().getResources().getColor(R.color.white));
                distance.setBackgroundResource(R.drawable.filter_btns);
                distance.setTextColor(rate.getContext().getResources().getColor(R.color.black));
                preSort = getString(R.string.sort_rate);
            }
        });
        distance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                distance.setBackgroundColor(rate.getContext().getResources().getColor(R.color.red_3));
                distance.setTextColor(rate.getContext().getResources().getColor(R.color.white));
                rate.setBackgroundResource(R.drawable.filter_btns);
                rate.setTextColor(rate.getContext().getResources().getColor(R.color.black));
                preSort = getString(R.string.sort_distance);
            }
        });

        Button done = (Button) dialog.findViewById(R.id.filter_done_btn);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (preSort.equals(getString(R.string.sort_rate))) {
                    sort = "rate";
                } else {
                    sort = "distance";
                }
                String typeString = getBranchType(dialog, radioGroup);
                if (typeString.equals(getString(R.string.type_1))) {
                    type = "0";
                } else if (typeString.equals(getString(R.string.type_2))) {
                    type = "2";
                } else if (typeString.equals(getString(R.string.type_3))) {
                    type = "1";
                }
                Log.e("typessss", type);
                try {
                    selectedCategories = getCategories(linear);
                    presenter.onPickLocation(pickupLocation.getLatitude(), pickupLocation.getLongitude(), type, selectedCategories, sort, key);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                dialog.dismiss();
            }
        });
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                num = 0;
                linear.clear();
//                selectedCategories.clear();
            }
        });
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    private String getBranchType(Dialog dialog, RadioGroup radioGroup) {
        int selected = radioGroup.getCheckedRadioButtonId();
        RadioButton type = (RadioButton) dialog.findViewById(selected);
        if (type == null) {
            return null;
        }

        return String.valueOf(type.getText());
    }

    LinearLayout firstContainer, secondContainer;

    int num = 0;
    ArrayList<LinearLayout> linear = new ArrayList<>();

    public void setCategories(Dialog dialog, final ArrayList<BranchCategories> categories) {
        final ArrayList<ImageView> imgs = new ArrayList<>();
        final ArrayList<TextView> txts = new ArrayList<>();
        firstContainer = (LinearLayout) dialog.findViewById(R.id.first_category_container);
        secondContainer = (LinearLayout) dialog.findViewById(R.id.second_category_container);
        for (int i = 0; i < categories.size() + 1; i++) {
            final LinearLayout parent = new LinearLayout(this);
            parent.setBackground(new ColorDrawable(Color.WHITE));
            parent.setPadding(16, 16, 16, 16);
            parent.setOrientation(LinearLayout.HORIZONTAL);
            parent.setGravity(Gravity.CENTER_VERTICAL);
            ImageView imageView = new ImageView(this);
            imageView.setMinimumHeight(80);
            imageView.setMinimumWidth(80);
            TextView textView = new TextView(this);
            LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params1.setMargins(40, 0, 40, 0);
            textView.setLayoutParams(params1);
            if (i == 0) {
                parent.setTag(0);
                textView.setText(getString(R.string.type_1));
                imageView.setImageResource(R.mipmap.all_food_c);
            } else {
                parent.setTag(categories.get(i - 1).getId());
                textView.setText(categories.get(i - 1).getName());
                Glide.with(MapsActivity.this).load(URLs.ROOT_URL + categories.get(i - 1).getPhoto())
                        .error(R.mipmap.default_category)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(imageView);
            }
            linear.add(parent);
            txts.add(textView);
            parent.addView(imageView);
            parent.addView(textView);
            if (i % 2 == 0) {
                firstContainer.addView(parent);
            } else {
                secondContainer.addView(parent);
            }
        }

        linear.get(0).setBackground(new ColorDrawable(getResources().getColor(R.color.test_1)));
        txts.get(0).setTextColor(getResources().getColor(R.color.white));
        category = "0";
        for (int i = 0; i < linear.size(); i++) {
            final int x = i;
            linear.get(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (x != 0) {
                        linear.get(0).setBackground(new ColorDrawable(Color.WHITE));
                        txts.get(0).setTextColor(getResources().getColor(R.color.black));
                    } else if (x == 0) {
                        num = 0;
                        for (int z = 1; z < linear.size(); z++) {
                            linear.get(z).setBackground(new ColorDrawable(Color.WHITE));
                            txts.get(z).setTextColor(getResources().getColor(R.color.black));
                        }
                    }
                    if (linear.get(x).getBackground() instanceof ColorDrawable) {
                        if (((ColorDrawable) linear.get(x).getBackground()).getColor() == Color.WHITE) {
                            linear.get(x).setBackground(new ColorDrawable(getResources().getColor(R.color.test_1)));
                            txts.get(x).setTextColor(getResources().getColor(R.color.white));
                            if (x != 0) {
                                num++;
                            } else {
                                num = 0;
                            }

                        } else {
                            if (x != 0) {
                                linear.get(x).setBackground(new ColorDrawable(Color.WHITE));
                                txts.get(x).setTextColor(getResources().getColor(R.color.black));
                                num--;
                            }

                        }
                    }
                    Log.e("dsds", num + "");
                    if (num == 0 || num == linear.size() - 1) {
                        num = 0;
                        for (int i = 0; i < linear.size(); i++) {
                            if (i == 0) {
                                linear.get(i).setBackground(new ColorDrawable(getResources().getColor(R.color.test_1)));
                                txts.get(i).setTextColor(getResources().getColor(R.color.white));
                            } else {
                                linear.get(i).setBackground(new ColorDrawable(Color.WHITE));
                                txts.get(i).setTextColor(getResources().getColor(R.color.black));
                            }
                        }
                    }
                }
            });

        }
    }

    ArrayList<String> selectedCategories;

    private ArrayList<String> getCategories(ArrayList<LinearLayout> linear) {
        ArrayList<String> cats = new ArrayList<>();
        for (int i = 0; i < linear.size(); i++) {
            if (linear.get(i).getBackground() instanceof ColorDrawable) {
                if (((ColorDrawable) linear.get(i).getBackground()).getColor() == getResources().getColor(R.color.test_1)) {
                    cats.add(String.valueOf(linear.get(i).getTag()));
                }
            }
        }
        return cats;
    }


    @Override
    public void onListItemClick(int position) {
        Intent intent = new Intent(this, MenusActivity.class);
        intent.putExtra("branchObject", branches.get(position));
        startActivity(intent);
        if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        } else {
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }
    }
}
