package seif.example.com.jahhizly.Map;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;
import me.anwarshahriar.calligrapher.Calligrapher;
import seif.example.com.jahhizly.Branches.MakeOrder.Branches;
import seif.example.com.jahhizly.Menus.MenusActivity;
import seif.example.com.jahhizly.R;
import seif.example.com.jahhizly.Utils.MySingleton;
import seif.example.com.jahhizly.Utils.URLs;

public class NavigationMap extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener, RoutingListener, MapMVP.view, MapBranchListAdapter.ListItemClickListener {

    private GoogleMap mMap;
    private static final int REQUEST_CHECK_SETTINGS = 1000;
    private SupportMapFragment mapFragment;
    private GoogleApiClient googleApiClient;
    private Location mLastLocation;
    private LocationRequest request;
    private boolean mRequestingLocationUpdates;
    private LatLng destination;
    private MapMVP.presenter presenter;
    private View view;
    MapBranchListAdapter adapter;
    RecyclerView mapRecView;
    RecyclerView.LayoutManager layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(seif.example.com.jahhizly.R.layout.activity_navigation_map);
        getSupportActionBar().setTitle(getString(R.string.title_activity_navigation_map));
        Calligrapher calligrapher = new Calligrapher(this);
        if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            calligrapher.setFont(this, "fonts/Arabic/arabic_font.ttf", true);
        } else {
            calligrapher.setFont(this, "fonts/English/Roboto_Regular.ttf", true);
        }
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        container = (FrameLayout) findViewById(R.id.map_list_container);
        mapRecView = (RecyclerView) findViewById(R.id.map_branch_rec_view);
        layout = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mapRecView.setLayoutManager(layout);
        mapRecView.setHasFixedSize(true);
        adapter = new MapBranchListAdapter(this, MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? true : false);
        mapFragment.getMapAsync(this);
        presenter = new MapPresenter(getApplicationContext(), new MapModel(getApplicationContext()), this);
        checkMapPermission();
        ///////////////////////////
        PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.
                destination = place.getLatLng();

                getRout(mLastLocation, destination);
                Log.e("destination", destination.latitude + "(((())))" + destination.longitude);
            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.e("error seifffff", status.getStatusMessage());
                Log.e("errorrr 1", status.toString());
            }
        });
        ///////////////////////
    }

    private void getRout(Location origin, LatLng destination) {
        mMap.clear();
        clearRoute();
        Routing routing = new Routing.Builder()
                .travelMode(AbstractRouting.TravelMode.DRIVING)
                .withListener(this)
                .alternativeRoutes(false)
                .waypoints(new LatLng(origin.getLatitude(), origin.getLongitude()), destination)
                .build();
        routing.execute();
        mMap.addMarker(new MarkerOptions().position(destination).title("destination"))
                .setIcon(BitmapDescriptorFactory.fromResource(R.mipmap.map_near_mark));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(destination));
//        mMap.animateCamera(CameraUpdateFactory.zoomTo(12));
    }

    private void getExternalLocation(Location origin) {
        try {
            String receivedData = getIntent().getData().toString();
            Log.e("location", receivedData);
            ArrayList<String> latLong = extractLocation(receivedData);
            for (int i = 0; i < latLong.size(); i++) {
                Log.e("location " + i, latLong.get(i));
            }
            LatLng destination = new LatLng(Double.parseDouble(latLong.get(0)), Double.parseDouble(latLong.get(1)));
//            mMap.addMarker(new MarkerOptions().position(destination).title("destination"));
//            LatLng positionUpdate = new LatLng(Double.parseDouble(latLong.get(0)), Double.parseDouble(latLong.get(1)));
//            CameraUpdate update = CameraUpdateFactory.newLatLngZoom(positionUpdate, 15);
//            mMap.animateCamera(update);
            getRout(origin, destination);

        } catch (Exception ex) {

        }
    }

    private ArrayList<String> extractLocation(String url) {
        ArrayList<String> location = new ArrayList<>();
        Pattern pattern = Pattern.compile("(-?[0-9]+(?:[,.][0-9]+)?)");
        Matcher matcher = pattern.matcher(url);
        while (matcher.find()) {
            location.add(matcher.group());
        }
        return location;
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(final Marker marker) {

                for (int i = 0; i < branches.size(); i++) {
                    if (branchesMarker.get(i).equals(marker)) {
                        Log.e("zzzzzzzzz", "7mada");
                        view = LayoutInflater.from(getApplicationContext()).inflate(R.layout.map_info_window, null);
                        ImageView logo = (ImageView) view.findViewById(R.id.map_branch_logo);
                        Glide.with(getApplicationContext()).load(URLs.ROOT_URL + branches.get(i).getBrandPhoto())
                                .error(R.mipmap.ic_launcher)
                                .crossFade()
                                .thumbnail(0.5f)
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .listener(new RequestListener<String, GlideDrawable>() {
                                    @Override
                                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                        return false;
                                    }

                                    @Override
                                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                        if (!isFromMemoryCache) marker.showInfoWindow();
                                        return false;
                                    }
                                })
                                .into(logo);
                        TextView branchName = (TextView) view.findViewById(R.id.map_branch_name);
                        branchName.setText(branches.get(i).getName());
                        TextView branchRate = (TextView) view.findViewById(R.id.map_branch_rate);
                        branchRate.setText(String.valueOf((float) branches.get(i).getRate()));
                        ImageView bookTable = (ImageView) view.findViewById(R.id.map_branch_book_table_type);
                        ImageView makeOrder = (ImageView) view.findViewById(R.id.map_branch_make_order_type);
                        if (branches.get(i).getType() == 0) {
                            makeOrder.setVisibility(View.VISIBLE);
                            bookTable.setVisibility(View.VISIBLE);
                        } else if (branches.get(i).getType() == 1) {
                            makeOrder.setVisibility(View.GONE);
                            bookTable.setVisibility(View.VISIBLE);
                        } else if (branches.get(i).getType() == 2) {
                            makeOrder.setVisibility(View.VISIBLE);
                            bookTable.setVisibility(View.GONE);
                        }
                        break;
                    }
                }
                return view;
            }
        });

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {

            @Override
            public boolean onMarkerClick(Marker marker) {
                Marker lastOpened = null;
                Log.e("zzzzzzzzz", "7mada 0");
                if (branches != null && branches.size() > 0) {
                    for (int i = 0; i < branches.size(); i++) {
                        Log.e("zzzzzzzzz", "7mada 1");
                        if (branchesMarker.get(i).equals(marker)) {
                            Log.e("zzzzzzzzz", "7mada 2");
                            if (lastOpened != null) {
                                lastOpened.hideInfoWindow();
                                if (lastOpened.equals(marker)) {
                                    lastOpened = null;
                                    return true;
                                }
                            }
                            marker.showInfoWindow();
                            lastOpened = marker;
                            return true;

                        }
                    }
                }
                return true;
            }
        });

        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                for (int i = 0; i < branches.size(); i++) {
                    if (branchesMarker.get(i).equals(marker)) {
                        Intent intent = new Intent(getApplicationContext(), MenusActivity.class);
                        intent.putExtra("branchObject", branches.get(i));
                        startActivity(intent);
                        finish();
                        if (MySingleton.getmInstance(NavigationMap.this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
                            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                        } else {
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        }
                    }
                }
            }
        });
    }

    private void checkMapPermission() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {

            if (ActivityCompat.checkSelfPermission(NavigationMap.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(NavigationMap.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(NavigationMap.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 1002);
            } else {

                setupLocationManager();
            }
        } else {
            setupLocationManager();
        }
    }

    private void setupLocationManager() {
        //buildGoogleApiClient();
        if (googleApiClient == null) {

            googleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .addApi(Places.GEO_DATA_API)
                    .addApi(Places.PLACE_DETECTION_API)
                    .build();
            //mGoogleApiClient = new GoogleApiClient.Builder(this);
        }
        googleApiClient.connect();
        createLocationRequest();
    }

    protected void createLocationRequest() {

        request = new LocationRequest();
        request.setSmallestDisplacement(10);
        request.setFastestInterval(5000);
        request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);


        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(request);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(googleApiClient,
                        builder.build());


        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates states = result.getLocationSettingsStates();

                switch (status.getStatusCode()) {

                    case LocationSettingsStatusCodes.SUCCESS:
                        setInitialLocation();
                        break;

                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        // Location settings are not satisfied, but this can be fixed
                        // by showing the user a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(
                                    NavigationMap.this,
                                    REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;

                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        // Location settings are not satisfied. However, we have no way
                        // to fix the settings so we won't show the dialog.

                        break;

                }


            }
        });
    }

    int time = 0;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("onActivityResult()", Integer.toString(resultCode));
        //final LocationSettingsStates states = LocationSettingsStates.fromIntent(data);
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK: {

                        setInitialLocation();

                        Toast.makeText(NavigationMap.this, "Location enabled by user!", Toast.LENGTH_LONG).show();
                        mRequestingLocationUpdates = true;
                        break;
                    }
                    case Activity.RESULT_CANCELED: {
                        // The user was asked to change settings, but chose not to
                        Toast.makeText(NavigationMap.this, "Location not enabled, user cancelled.", Toast.LENGTH_LONG).show();
                        finish();
                        mRequestingLocationUpdates = false;
                        break;
                    }
                    default: {
                        break;
                    }
                }
                break;
        }
    }

    String sort = "";
    String key = "";
    ArrayList<String> categories = new ArrayList<>();

    private void setInitialLocation() {
        if (ActivityCompat.checkSelfPermission(NavigationMap.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(NavigationMap.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, request, new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                mLastLocation = location;
                try {
                    presenter.onPickLocation(mLastLocation.getLatitude(), mLastLocation.getLongitude(), "0", categories, sort, key);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                Log.e("User location : ", mLastLocation.getLatitude() + " ((())) " + mLastLocation.getLongitude());

                try {
                    if (time == 0) {
                        getExternalLocation(mLastLocation);
                        LatLng positionUpdate = new LatLng(location.getLatitude(), location.getLongitude());
                        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(positionUpdate, 15);
                        mMap.animateCamera(update);
                        time = 1;
                    }

                } catch (Exception ex) {

                    ex.printStackTrace();
                    Log.e("MapException", ex.getMessage());
                }
            }
        });
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 1002: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        setupLocationManager();
                    }
                } else {
                    Toast.makeText(NavigationMap.this, "Permission Denied", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
            break;
        }
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onLocationChanged(Location location) {
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }



    /*
     *  Routing .....
     */

    @Override
    public void onRoutingFailure(RouteException e) {
        if (e != null) {
            Toast.makeText(this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "Something went wrong, Try again", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRoutingStart() {
    }

    private List<Polyline> polylines = new ArrayList<>();
    private static final int[] COLORS = new int[]{R.color.red_3};

    @Override
    public void onRoutingSuccess(ArrayList<Route> route, int shortestRouteIndex) {
        if (polylines.size() > 0) {
            for (Polyline poly : polylines) {
                poly.remove();
            }
        }

        polylines = new ArrayList<>();
        //add route(s) to the map.
        for (int i = 0; i < route.size(); i++) {

            //In case of more than 5 alternative routes
            int colorIndex = i % COLORS.length;
            PolylineOptions polyOptions = new PolylineOptions();
            polyOptions.color(getResources().getColor(COLORS[colorIndex]));
            polyOptions.width(10 + i * 5);
            polyOptions.addAll(route.get(i).getPoints());
            Polyline polyline = mMap.addPolyline(polyOptions);
            polylines.add(polyline);
//            Toast.makeText(getApplicationContext(), "Route " + (i + 1) + ": distance - " + route.get(i).getDistanceValue() + ": duration - " + route.get(i).getDurationValue(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRoutingCancelled() {
    }

    private void clearRoute() {
        for (Polyline line : polylines) {
            line.remove();
        }
        polylines.clear();
    }

    ArrayList<Marker> branchesMarker = new ArrayList<>();
    ArrayList<Branches> branches;

    @Override
    public void loadData(ArrayList<Branches> data) {
        branches = data;
        branchesMarker.clear();
        Log.e("data", "" + data.size());
        for (int i = 0; i < data.size(); i++) {
            branchesMarker.add(mMap.addMarker(new MarkerOptions().position(new LatLng(data.get(i).getLatitude(), data.get(i).getLongitude())).icon(BitmapDescriptorFactory.fromResource(R.mipmap.map_near_mark))));
            Log.e("bbb", data.get(i).getName());
        }


        container.setVisibility(View.VISIBLE);
        adapter.setMapBranches(data);
        ScaleInAnimationAdapter scaleInAnimationAdapter = new ScaleInAnimationAdapter(adapter);
        scaleInAnimationAdapter.setFirstOnly(false);
        mapRecView.setAdapter(scaleInAnimationAdapter);
        adapter.notifyDataSetChanged();
    }

    FrameLayout container;

    @Override
    public void showEmptyText(String message) {
        adapter.clear();
        container.setVisibility(View.GONE);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            } else {
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onListItemClick(int position) {
        Intent intent = new Intent(this, MenusActivity.class);
        intent.putExtra("branchObject", branches.get(position));
        startActivity(intent);
        if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        } else {
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }
    }
}
