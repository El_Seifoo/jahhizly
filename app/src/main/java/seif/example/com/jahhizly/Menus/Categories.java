package seif.example.com.jahhizly.Menus;

import java.io.Serializable;

/**
 * Created by seif on 10/24/2017.
 */
public class Categories implements Serializable{
    private int id ;
    private String name ;
    private String photo ;
    private int brandId ;

    public Categories() {}

    public Categories(int id, String name, String photo, int brandId) {
        this.id = id;
        this.name = name;
        this.photo = photo;
        this.brandId = brandId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public int getBrandId() {
        return brandId;
    }

    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }
}
