package seif.example.com.jahhizly.Menus;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.like.LikeButton;
import com.like.OnLikeListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import me.anwarshahriar.calligrapher.Calligrapher;
import seif.example.com.jahhizly.Other.CircleTransform;
import seif.example.com.jahhizly.R;
import seif.example.com.jahhizly.Utils.MySingleton;
import seif.example.com.jahhizly.Utils.URLs;

/**
 * Created by seif on 10/25/2017.
 */
public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.Holder> {
    private ArrayList<MenuItemDetails> menu;
    final private ListItemClickListener mOnClickListener;
    private boolean flag;

    public MenuAdapter(ListItemClickListener mOnClickListener, boolean flag) {
        this.mOnClickListener = mOnClickListener;
        this.flag = flag;
    }

    public interface ListItemClickListener {
        void onListItemClick(int clickedItemIndex);
    }

    public void setMenuData(ArrayList<MenuItemDetails> menu) {
        if (menu != null && menu.size() > 0) {
            this.menu = menu;
        }
        notifyDataSetChanged();
    }

    public void clear() {
        if (menu != null && menu.size() > 0) {
            menu.clear();
        }
        notifyDataSetChanged();
    }

    @Override
    public MenuAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        int menuLayoutId = R.layout.menu_list_item;
        boolean shouldAttachToParentImmediately = false;
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(menuLayoutId, parent, shouldAttachToParentImmediately);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(final MenuAdapter.Holder holder, final int position) {
        Calligrapher calligrapher = new Calligrapher(holder.itemView.getContext());
        if (flag)
            calligrapher.setFont(holder.itemView, "fonts/Arabic/arabic_font.ttf");
        else
            calligrapher.setFont(holder.itemView, "fonts/English/Roboto_Regular.ttf");

        Glide.with(holder.itemView.getContext().getApplicationContext())
                .load(URLs.ROOT_URL + menu.get(position).getPhoto())
                .error(R.mipmap.restaurant_meal_icon)
                .thumbnail(0.5f)
                .bitmapTransform(new CircleTransform(holder.itemView.getContext().getApplicationContext()))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.menuItemImg);

        holder.menuItemName.setText(flag ? convertDigits(" " + menu.get(position).getArabicName() + " ") : " " + menu.get(position).getName() + " ");
        holder.menuItemPrice.setText(flag ? convertDigits(" " + menu.get(position).getPrice() + holder.itemView.getContext().getString(R.string.currency)) : " " + menu.get(position).getPrice() + holder.itemView.getContext().getString(R.string.currency));
        if (menu.get(position).isChecked()) {
            holder.addToFavorites.setLiked(true);
        } else {
            holder.addToFavorites.setLiked(false);
        }
        holder.addToFavorites.setOnLikeListener(new OnLikeListener() {
            @Override
            public void liked(LikeButton likeButton) {
                addToFavorites(menu.get(position).getId(), holder, position);
            }

            @Override
            public void unLiked(LikeButton likeButton) {
                removeFromFavorites(menu.get(position).getId(), holder, position);
            }
        });
    }

    private void addToFavorites(int id, final MenuAdapter.Holder holder, final int position) {
        Map<String, String> param = new HashMap<>();
        param.put("menu_id", String.valueOf(id));
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                URLs.ADD_FAV_MENU,
                new JSONObject(param),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("add meal", response.toString());

                        try {
                            String status = response.getString("status");
                            if (status.equals("success")) {
                                Toast.makeText(holder.itemView.getContext().getApplicationContext(), holder.itemView.getContext().getString(R.string.meal_added), Toast.LENGTH_LONG).show();
                                menu.get(position).setChecked(true);
                            } else {
                                Toast.makeText(holder.itemView.getContext(), response.getString("desc"), Toast.LENGTH_LONG).show();
                                holder.addToFavorites.setLiked(false);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        holder.addToFavorites.setLiked(false);
                        if (error instanceof AuthFailureError) {
                            Toast.makeText(holder.itemView.getContext(), holder.itemView.getContext().getString(R.string.auth_failure_error_2), Toast.LENGTH_LONG).show();
                            return;
                        } else if (error instanceof NoConnectionError || error instanceof TimeoutError) {
                            Toast.makeText(holder.itemView.getContext(), holder.itemView.getContext().getString(R.string.internet_error), Toast.LENGTH_LONG).show();
                            return;
                        } else if (error instanceof ServerError) {
                            Toast.makeText(holder.itemView.getContext(), holder.itemView.getContext().getString(R.string.server_error), Toast.LENGTH_LONG).show();
                            return;
                        } else {
                            Toast.makeText(holder.itemView.getContext(), holder.itemView.getContext().getString(R.string.wrong), Toast.LENGTH_LONG).show();
                            return;
                        }

                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                String token = "Bearer " + MySingleton.getmInstance(holder.itemView.getContext()).UserKey();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Authorization", token);
                return headers;
            }
        };
        MySingleton.getmInstance(holder.itemView.getContext()).addToRQ(jsonObjectRequest);
    }

    private void removeFromFavorites(int id, final MenuAdapter.Holder holder, final int position) {
        Map<String, String> param = new HashMap<>();
        param.put("menu_id", String.valueOf(id));
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                URLs.REMOVE_FAV_MENU,
                new JSONObject(param),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("remove meal response", String.valueOf(response));
                        try {
                            String status = response.getString("status");
                            if (status.equals("success")) {
                                Toast.makeText(holder.itemView.getContext().getApplicationContext(), holder.itemView.getContext().getString(R.string.meal_removed), Toast.LENGTH_LONG).show();
                                menu.get(position).setChecked(false);
                            } else {
                                Toast.makeText(holder.itemView.getContext().getApplicationContext(), holder.itemView.getContext().getString(R.string.remove_fav_error), Toast.LENGTH_LONG).show();
                                holder.addToFavorites.setLiked(true);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        holder.addToFavorites.setLiked(true);
                        if (error instanceof NoConnectionError || error instanceof TimeoutError) {
                            Toast.makeText(holder.itemView.getContext(), holder.itemView.getContext().getString(R.string.internet_error), Toast.LENGTH_LONG).show();
                            return;
                        } else if (error instanceof ServerError) {
                            Toast.makeText(holder.itemView.getContext(), holder.itemView.getContext().getString(R.string.server_error), Toast.LENGTH_LONG).show();
                            return;
                        } else {
                            Toast.makeText(holder.itemView.getContext(), holder.itemView.getContext().getString(R.string.wrong), Toast.LENGTH_LONG).show();
                            return;
                        }
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                String token = "Bearer " + MySingleton.getmInstance(holder.itemView.getContext()).UserKey();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Authorization", token);
                return headers;
            }
        };
        MySingleton.getmInstance(holder.itemView.getContext()).addToRQ(jsonObjectRequest);
    }

    @Override
    public int getItemCount() {
        return (null != menu ? menu.size() : 0);
    }

    public class Holder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView menuItemImg;
        LikeButton addToFavorites;
        TextView menuItemName, menuItemPrice;

        public Holder(View itemView) {
            super(itemView);
            menuItemImg = (ImageView) itemView.findViewById(R.id.menu_item_img);
            addToFavorites = (LikeButton) itemView.findViewById(R.id.add_meal_to_fav_list);
            menuItemName = (TextView) itemView.findViewById(R.id.menu_item_name);
            menuItemPrice = (TextView) itemView.findViewById(R.id.menu_item_price);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            mOnClickListener.onListItemClick(position);
        }
    }

    private String convertDigits(String string) {
        String rslt = "";
        for (int i = 0; i < string.length(); i++) {
            switch (string.charAt(i)) {
                case '0':
                    rslt += "٠";
                    break;
                case '1':
                    rslt += "١";
                    break;
                case '2':
                    rslt += "٢";
                    break;
                case '3':
                    rslt += "٣";
                    break;
                case '4':
                    rslt += "٤";
                    break;
                case '5':
                    rslt += "٥";
                    break;
                case '6':
                    rslt += "٦";
                    break;
                case '7':
                    rslt += "٧";
                    break;
                case '8':
                    rslt += "٨";
                    break;
                case '9':
                    rslt += "٩";
                    break;
                default:
                    rslt += string.charAt(i);
            }
        }
        return rslt;
    }

}
