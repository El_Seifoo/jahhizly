package seif.example.com.jahhizly.Menus;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.eftimoff.viewpagertransformers.RotateDownTransformer;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;
import java.util.List;

import me.anwarshahriar.calligrapher.Calligrapher;
import seif.example.com.jahhizly.BookTable.BookTableActivity;
import seif.example.com.jahhizly.BranchLocationActivity;
import seif.example.com.jahhizly.Branches.MakeOrder.Branches;
import seif.example.com.jahhizly.Login.LoginActivity;
import seif.example.com.jahhizly.Menus.MenuItem.MenuFragment;
import seif.example.com.jahhizly.Other.CircleTransform;
import seif.example.com.jahhizly.R;
import seif.example.com.jahhizly.Utils.MySingleton;
import seif.example.com.jahhizly.Utils.URLs;

/**
 * Created by seif on 10/24/2017.
 */
public class MenuDetailsFragment extends Fragment implements MenusMVP.view {
    private View view;

    public MenuDetailsFragment() {
    }

    TabLayout tabLayout;
    ViewPager viewPager;
    private Branches branch;
    MenusPresenter presenter;
    ImageView branchLogo;
    TextView branchName, branDistance, branchCategory;
    Button bookTableBtn;
    MKLoader loading;
    CoordinatorLayout container1;

    public void sendBranchObj(Branches branch) {
        if (branch != null) {
            this.branch = branch;
        }
    }

    int time = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_menu_details, container, false);
//        time = 1 ;
        Calligrapher calligrapher = new Calligrapher(getContext());
        if (MySingleton.getmInstance(getContext()).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            calligrapher.setFont(getActivity(), "fonts/Arabic/arabic_font.ttf", true);
        } else {
            calligrapher.setFont(getActivity(), "fonts/English/Roboto_Regular.ttf", true);
        }
        loading = (MKLoader) view.findViewById(R.id.loading);
        container1 = (CoordinatorLayout) view.findViewById(R.id.container);
        presenter = new MenusPresenter(this, new MenusModel(getActivity().getApplicationContext()), getActivity().getApplicationContext());
        viewPager = (ViewPager) view.findViewById(R.id.viewpager_details);
        tabLayout = (TabLayout) view.findViewById(R.id.tabs_details);
        Log.e("seifoo", String.valueOf(branch.getBrandId()));
        presenter.onMenuDetailsCreated(viewPager, branch.getBrandId());
        time = 1;
        branchLogo = (ImageView) view.findViewById(R.id.menu_branch_logo);
        branchName = (TextView) view.findViewById(R.id.menu_branch_name);
        branchCategory = (TextView) view.findViewById(R.id.menu_branch_category);
        branDistance = (TextView) view.findViewById(R.id.menu_branch_distance);
        bookTableBtn = (Button) view.findViewById(R.id.menu_book_table_btn);
        bookTableBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (branch.getType() == 0 || branch.getType() == 1) {
                    if (!MySingleton.getmInstance(getContext()).isLoggedIn()) {
                        Intent intent = new Intent(getContext(), LoginActivity.class);
                        intent.putExtra("LoginIntent", "FromMenuToBookTable");
                        intent.putExtra("BookTableBranch", branch);
                        startActivity(intent);
                        if (MySingleton.getmInstance(getContext()).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
                            getActivity().overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                        } else {
                            getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        }
                    } else {
                        Intent intent = new Intent(getContext(), BookTableActivity.class);
                        intent.putExtra("BookTableBranch", branch);
                        startActivity(intent);
                        if (MySingleton.getmInstance(getContext()).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
                            getActivity().overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                        } else {
                            getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        }
                    }
                }
            }
        });
        if (branch.getType() == 2) {
            bookTableBtn.setVisibility(View.GONE);
        }
        presenter.setBranchData(branch.getBrandPhoto(), branch.getName(), branch.getBrandCategory(), branch.getDistance());
        design();
        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        BranchLocationActivity.time = 0;
        if (time == 0) {
            presenter.onMenuDetailsCreated(viewPager, branch.getBrandId());
        }

        Log.e("resume", "resume");
    }

    @Override
    public void setupViewPager(ViewPager viewPager, ArrayList<Categories> categories) {
        Activity activity = getActivity();
        if (activity != null) {
            ViewPagerAdapter adapter = new ViewPagerAdapter(getFragmentManager());
            for (int i = 0; i < categories.size() + 1; i++) {
                if (i == 0) {
                    adapter.addFragment(MenuFragment.newInstance(0, branch), getString(R.string.type_1));
                } else {
                    adapter.addFragment(MenuFragment.newInstance(categories.get(i - 1).getId(), branch), categories.get(i - 1).getName());
                }
            }
            viewPager.setAdapter(adapter);
            viewPager.setCurrentItem(0);
            viewPager.setPageTransformer(true, new RotateDownTransformer());
            tabLayout.setupWithViewPager(viewPager);
        }

    }

    @Override
    public void setBranchName(String name) {
        branchName.setText(MySingleton.getmInstance(getContext()).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? convertDigits(" " + name + " ") : " " + name + " ");
    }

    @Override
    public void setBranchLogo(String logo) {
        Glide.with(getActivity().getApplication())
                .load(URLs.ROOT_URL + logo)
                .error(R.mipmap.restaurant_icon_menu)
                .crossFade()
                .thumbnail(0.1f)
                .bitmapTransform(new CircleTransform(getActivity().getApplicationContext()))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(branchLogo);
    }

    @Override
    public void setBranchCategory(String category) {
        branchCategory.setText(MySingleton.getmInstance(getContext()).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? convertDigits(" " + category + " ") : " " + category + " ");
    }


    @Override
    public void setBranchDistance(Double distance) {
        if (distance == -1) {
            branDistance.setVisibility(View.GONE);
        } else {
            branDistance.setText(MySingleton.getmInstance(getContext()).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? convertDigits(" " + distance + " " + getString(R.string.dist)) : " " + distance + " " + getString(R.string.dist));
        }

    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void showProgress() {
//        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
//                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        loading.setVisibility(View.VISIBLE);
        container1.setForeground(new ColorDrawable(Color.parseColor("#66000000")));
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void hideProgress() {
//        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        loading.setVisibility(View.GONE);
        container1.setForeground(new ColorDrawable(Color.TRANSPARENT));
    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        @Override
        public Parcelable saveState() {
            return null;
        }

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private void design() {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        view.setLayoutParams(params);
    }

    private String convertDigits(String string) {
        if (string==null) return null;
        String rslt = "";
        for (int i = 0; i < string.length(); i++) {
            switch (string.charAt(i)) {
                case '0':
                    rslt += "٠";
                    break;
                case '1':
                    rslt += "١";
                    break;
                case '2':
                    rslt += "٢";
                    break;
                case '3':
                    rslt += "٣";
                    break;
                case '4':
                    rslt += "٤";
                    break;
                case '5':
                    rslt += "٥";
                    break;
                case '6':
                    rslt += "٦";
                    break;
                case '7':
                    rslt += "٧";
                    break;
                case '8':
                    rslt += "٨";
                    break;
                case '9':
                    rslt += "٩";
                    break;
                default:
                    rslt += string.charAt(i);
            }
        }
        return rslt;
    }

}
