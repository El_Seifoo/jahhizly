package seif.example.com.jahhizly.Menus.MenuItem;

import android.provider.BaseColumns;

/**
 * Created by seif on 10/29/2017.
 */
public class CartContract {

    public class CartEntry implements BaseColumns {
        public static final String ID = "id";
        public static final String TABLE_NAME = "items";
        public static final String USER_ID = "userID";
        public static final String MENU_ID = "menuID";
        public static final String BRANCH_ID = "branchID";
        public static final String ITEM_NAME = "name";
        public static final String ITEM_ARABIC_NAME = "arabicName";
        public static final String PHOTO = "photo";
        public static final String QUANTITY = "quantity";
        public static final String SIZE_ID = "sizeId";
        public static final String SIZE = "size";
        public static final String ARABIC_SIZE = "arabicSize";
        public static final String EXTRAS_ID = "extrasID";
        public static final String EXTRAS = "extras";
        public static final String PRICE = "price";
        public static final String TIME = "time";
    }
}
