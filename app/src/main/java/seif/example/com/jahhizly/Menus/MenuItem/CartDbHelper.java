package seif.example.com.jahhizly.Menus.MenuItem;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by seif on 10/29/2017.
 */


public class CartDbHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "cartItems.db";
    private static final int DATABASE_VERSION = 5070;
    private static CartDbHelper mInstance;

    private CartDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static synchronized CartDbHelper getmInstance(Context context) {
        if (mInstance == null) {
            mInstance = new CartDbHelper(context);
        }

        return mInstance;
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String SQL_CREATE_CART_TABLE = "CREATE TABLE " + CartContract.CartEntry.TABLE_NAME + " ( " +
                CartContract.CartEntry.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                CartContract.CartEntry.USER_ID + " INTEGER, " +
                CartContract.CartEntry.BRANCH_ID + " INTEGER, " +
                CartContract.CartEntry.MENU_ID + " INTEGER, " +
                CartContract.CartEntry.PHOTO + " TEXT, " +
                CartContract.CartEntry.QUANTITY + " INTEGER, " +
                CartContract.CartEntry.SIZE_ID + " INTEGER, " +
                CartContract.CartEntry.SIZE + " TEXT, " +
                CartContract.CartEntry.ARABIC_SIZE + " TEXT, " +
                CartContract.CartEntry.ITEM_NAME + " TEXT, " +
                CartContract.CartEntry.ITEM_ARABIC_NAME + " TEXT, " +
                CartContract.CartEntry.EXTRAS_ID + " TEXT," +
                CartContract.CartEntry.EXTRAS + " TEXT," +
                CartContract.CartEntry.PRICE + " TEXT," +
                CartContract.CartEntry.TIME + " TEXT" +
                " );";
        sqLiteDatabase.execSQL(SQL_CREATE_CART_TABLE);
    }


    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + CartContract.CartEntry.TABLE_NAME);
        onCreate(sqLiteDatabase);
    }


    public void addItem(CartItemDetails cart) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        Log.e("iddddddddddd", CartContract.CartEntry.ID);
        values.put(CartContract.CartEntry.USER_ID, cart.getUserID());
        values.put(CartContract.CartEntry.BRANCH_ID, cart.getBranchID());
        values.put(CartContract.CartEntry.MENU_ID, cart.getMenuID());
        values.put(CartContract.CartEntry.PHOTO, cart.getPhoto());
        values.put(CartContract.CartEntry.QUANTITY, cart.getQuantity());
        values.put(CartContract.CartEntry.SIZE_ID, cart.getSizeID());
        values.put(CartContract.CartEntry.SIZE, cart.getSize());
        values.put(CartContract.CartEntry.ARABIC_SIZE, cart.getArabicSize());
        values.put(CartContract.CartEntry.ITEM_NAME, cart.getItemName());
        values.put(CartContract.CartEntry.ITEM_ARABIC_NAME, cart.getItemArabicName());
        values.put(CartContract.CartEntry.PRICE, cart.getPrice());
        values.put(CartContract.CartEntry.EXTRAS_ID, cart.getExtrasID());
        Log.e("db extras id", cart.getExtrasID());
        values.put(CartContract.CartEntry.EXTRAS, cart.getExtras());
        Log.e("db extras ", cart.getExtras());
        values.put(CartContract.CartEntry.TIME, cart.getTime());


        db.insert(CartContract.CartEntry.TABLE_NAME, null, values);
        db.close();
    }


    public ArrayList<CartItemDetails> getItems(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(CartContract.CartEntry.TABLE_NAME,
                new String[]{CartContract.CartEntry.ID, CartContract.CartEntry.USER_ID, CartContract.CartEntry.BRANCH_ID,
                        CartContract.CartEntry.MENU_ID, CartContract.CartEntry.PHOTO, CartContract.CartEntry.QUANTITY,
                        CartContract.CartEntry.SIZE_ID, CartContract.CartEntry.SIZE, CartContract.CartEntry.ARABIC_SIZE,
                        CartContract.CartEntry.ITEM_NAME, CartContract.CartEntry.ITEM_ARABIC_NAME, CartContract.CartEntry.EXTRAS_ID,
                        CartContract.CartEntry.EXTRAS, CartContract.CartEntry.PRICE, CartContract.CartEntry.TIME},
                CartContract.CartEntry.USER_ID + "=?",
                new String[]{String.valueOf(id)},
                null, null, null);
        ArrayList<CartItemDetails> cartDetails = new ArrayList<>();
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {

                    CartItemDetails cart = new CartItemDetails();
                    cart.setId(Integer.parseInt(cursor.getString(0)));
                    cart.setUserID(Integer.parseInt(cursor.getString(1)));
                    cart.setBranchID(Integer.parseInt(cursor.getString(2)));
                    cart.setMenuID(Integer.parseInt(cursor.getString(3)));
                    cart.setPhoto(cursor.getString(4));
                    cart.setQuantity(Integer.parseInt(cursor.getString(5)));
                    cart.setSizeID(Integer.parseInt(cursor.getString(6)));
                    cart.setSize(cursor.getString(7));
                    cart.setArabicSize(cursor.getString(8));
                    cart.setItemName(cursor.getString(9));
                    cart.setItemArabicName(cursor.getString(10));
                    cart.setExtrasID(cursor.getString(11));
                    cart.setExtras(cursor.getString(12));
                    cart.setPrice(Double.valueOf(cursor.getString(13)));
                    cart.setTime(cursor.getString(14));
                    cartDetails.add(cart);
                } while (cursor.moveToNext());
            }
        }
        cursor.close();
        db.close();
        return cartDetails;
    }

    public void updateData(ArrayList<CartItemDetails> cart, int id) {
        SQLiteDatabase db = this.getWritableDatabase();

        for (int i = 0; i < cart.size(); i++) {
            ContentValues values = new ContentValues();
            values.put(CartContract.CartEntry.USER_ID, id);
            values.put(CartContract.CartEntry.BRANCH_ID, cart.get(i).getBranchID());
            values.put(CartContract.CartEntry.MENU_ID, cart.get(i).getMenuID());
            values.put(CartContract.CartEntry.PHOTO, cart.get(i).getPhoto());
            values.put(CartContract.CartEntry.QUANTITY, cart.get(i).getQuantity());
            values.put(CartContract.CartEntry.SIZE_ID, cart.get(i).getSizeID());
            values.put(CartContract.CartEntry.SIZE, cart.get(i).getSize());
            values.put(CartContract.CartEntry.SIZE, cart.get(i).getArabicSize());
            values.put(CartContract.CartEntry.ITEM_NAME, cart.get(i).getItemName());
            values.put(CartContract.CartEntry.ITEM_ARABIC_NAME, cart.get(i).getItemArabicName());
            values.put(CartContract.CartEntry.PRICE, cart.get(i).getPrice());
            values.put(CartContract.CartEntry.EXTRAS_ID, cart.get(i).getExtrasID());
            Log.e("db extras id", cart.get(i).getExtrasID());
            values.put(CartContract.CartEntry.EXTRAS, cart.get(i).getExtras());
            Log.e("db extras ", cart.get(i).getExtras());
            values.put(CartContract.CartEntry.TIME, cart.get(i).getTime());
            db.update(CartContract.CartEntry.TABLE_NAME, values, CartContract.CartEntry.USER_ID + " = ?",
                    new String[]{String.valueOf(-1)});
        }
        db.close();
    }

    public void removeByMenuId(int id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(CartContract.CartEntry.TABLE_NAME, CartContract.CartEntry.ID + " = ?",
                new String[]{String.valueOf(id)});
        Log.e("deleted succ", String.valueOf(id));
        db.close();
    }

    public void deleteItemsByUserId(int userId) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(CartContract.CartEntry.TABLE_NAME, CartContract.CartEntry.USER_ID + " = ?",
                new String[]{String.valueOf(userId)});
        Log.e("deleted succ", String.valueOf(userId));
        db.close();
    }

    public void deleteAll() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + CartContract.CartEntry.TABLE_NAME);
        db.close();
    }

}
