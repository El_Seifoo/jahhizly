package seif.example.com.jahhizly.Menus.MenuItem;

import java.io.Serializable;

/**
 * Created by seif on 10/29/2017.
 */
public class CartItemDetails implements Serializable {
    private int id;
    private int userID;
    private int branchID;
    private int menuID;
    private String photo;
    private int quantity;
    private int sizeID;
    private String size;
    private String arabicSize;
    private String itemName;
    private String itemArabicName;
    private String extrasID;
    private String extras;
    private double price;
    private String time;

    public CartItemDetails() {
    }

    public CartItemDetails(int userID, int branchID, int menuID, String photo, int quantity, int sizeID,
                           String size, String itemName, String extrasID, String extras, double price, String time) {
        this.userID = userID;
        this.branchID = branchID;
        this.menuID = menuID;
        this.photo = photo;
        this.quantity = quantity;
        this.sizeID = sizeID;
        this.size = size;
        this.itemName = itemName;
        this.extrasID = extrasID;
        this.extras = extras;
        this.price = price;
        this.time = time;
    }

    public CartItemDetails(int userID, int branchID, int menuID, String photo, int quantity, int sizeID,
                           String size, String arabicSize, String itemName, String itemArabicName, String extrasID, String extras, double price, String time) {
        this.userID = userID;
        this.branchID = branchID;
        this.menuID = menuID;
        this.photo = photo;
        this.quantity = quantity;
        this.sizeID = sizeID;
        this.size = size;
        this.arabicSize = arabicSize;
        this.itemName = itemName;
        this.itemArabicName = itemArabicName;
        this.extrasID = extrasID;
        this.extras = extras;
        this.price = price;
        this.time = time;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public int getBranchID() {
        return branchID;
    }

    public void setBranchID(int branchID) {
        this.branchID = branchID;
    }

    public int getMenuID() {
        return menuID;
    }

    public void setMenuID(int menuID) {
        this.menuID = menuID;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getSizeID() {
        return sizeID;
    }

    public void setSizeID(int sizeID) {
        this.sizeID = sizeID;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getArabicSize() {
        return arabicSize;
    }

    public void setArabicSize(String arabicSize) {
        this.arabicSize = arabicSize;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemArabicName() {
        return itemArabicName;
    }

    public void setItemArabicName(String itemArabicName) {
        this.itemArabicName = itemArabicName;
    }

    public String getExtrasID() {
        return extrasID;
    }

    public void setExtrasID(String extrasID) {
        this.extrasID = extrasID;
    }

    public String getExtras() {
        return extras;
    }

    public void setExtras(String extras) {
        this.extras = extras;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
