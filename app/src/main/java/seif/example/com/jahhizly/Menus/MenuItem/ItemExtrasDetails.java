package seif.example.com.jahhizly.Menus.MenuItem;

import java.io.Serializable;

/**
 * Created by seif on 10/29/2017.
 */
public class ItemExtrasDetails implements Serializable {
    private int id;
    private String name;
    private String arabicName;
    private double price;

    public ItemExtrasDetails() {
    }

    public ItemExtrasDetails(int id, String name, double price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }

    public ItemExtrasDetails(int id, String name, String arabicName, double price) {
        this.id = id;
        this.name = name;
        this.arabicName = arabicName;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArabicName() {
        return arabicName;
    }

    public void setArabicName(String arabicName) {
        this.arabicName = arabicName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
