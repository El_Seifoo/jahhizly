package seif.example.com.jahhizly.Menus.MenuItem;

import java.io.Serializable;

/**
 * Created by seif on 10/29/2017.
 */
public class ItemSizeDetails implements Serializable {
    private int id;
    private String size;
    private String arabicSize;
    private double price;

    public ItemSizeDetails() {
    }

    public ItemSizeDetails(int id, String size, double price) {
        this.id = id;
        this.size = size;
        this.price = price;
    }

    public ItemSizeDetails(int id, String size, String arabicSize, double price) {
        this.id = id;
        this.size = size;
        this.arabicSize = arabicSize;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getArabicSize() {
        return arabicSize;
    }

    public void setArabicSize(String arabicSize) {
        this.arabicSize = arabicSize;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
