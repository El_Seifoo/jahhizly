package seif.example.com.jahhizly.Menus.MenuItem;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.LayoutDirection;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.dk.animation.circle.CircleAnimationUtil;
import com.like.LikeButton;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;
import me.anwarshahriar.calligrapher.Calligrapher;
import seif.example.com.jahhizly.BookTable.BookTableActivity;
import seif.example.com.jahhizly.Branches.MakeOrder.Branches;
import seif.example.com.jahhizly.Menus.MenuAdapter;
import seif.example.com.jahhizly.Menus.MenuItemDetails;
import seif.example.com.jahhizly.Menus.MenusActivity;
import seif.example.com.jahhizly.Menus.MenusMVP;
import seif.example.com.jahhizly.Menus.MenusModel;
import seif.example.com.jahhizly.Menus.MenusPresenter;
import seif.example.com.jahhizly.MyApp;
import seif.example.com.jahhizly.Other.CircleTransform;
import seif.example.com.jahhizly.R;
import seif.example.com.jahhizly.Utils.MySingleton;
import seif.example.com.jahhizly.Utils.URLs;

/**
 * Created by seif on 10/25/2017.
 */
public class MenuFragment extends Fragment implements MenusMVP.viewMenu, SizesAdapter.SizesListItemClickListener, MenuAdapter.ListItemClickListener {
    View view;
    Branches mBranches;
    ArrayList<MenuItemDetails> menu = new ArrayList<>();
    MKLoader loading, mainloading;
    FrameLayout container;


    public MenuFragment() {
    }

    MenusPresenter presenter;
    RecyclerView menuRecView;
    MenuAdapter adapter;
    SizesAdapter sizesAdapter;
    LinearLayout checkBoxContainer;
    ArrayList<String> sizes = new ArrayList<>();
    ArrayList<Double> prices = new ArrayList<>();
    ArrayList<Integer> sizesIds = new ArrayList<>();
    ArrayList<ItemExtrasDetails> extras = new ArrayList<>();
    private String size;
    private String arabicSize;
    private double price;
    private int sizeId;
    private int menuIndexClicked;
    ImageView decrease, increase;
    TextView counter, cartDialogPrice;
    int counterInteger = 1;
    double extraPrice = 0;
    TextView emptyTxt;

    public static MenuFragment newInstance(int categoryId, Branches branch) {
        MenuFragment fragment = new MenuFragment();
        Bundle args = new Bundle();
        args.putInt("categId", categoryId);
        args.putSerializable("branchObject", branch);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_menu, container, false);
        Calligrapher calligrapher = new Calligrapher(getContext());
        if (MySingleton.getmInstance(getContext()).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            calligrapher.setFont(getActivity(), "fonts/Arabic/arabic_font.ttf", true);
        } else {
            calligrapher.setFont(getActivity(), "fonts/English/Roboto_Regular.ttf", true);
        }
        int categoryId = getArguments().getInt("categId");
        mBranches = (Branches) getArguments().getSerializable("branchObject");
        mainloading = (MKLoader) view.findViewById(R.id.loading);
        presenter = new MenusPresenter(this, new MenusModel(getActivity().getApplicationContext()), getActivity().getApplicationContext());
        presenter.onMenuFragmentCreated(categoryId, mBranches.getId(), mBranches.getType());
        design();
        emptyTxt = (TextView) view.findViewById(R.id.empty_txt);
        menuRecView = (RecyclerView) view.findViewById(R.id.menus_rec_view);
        menuRecView.setHasFixedSize(true);
        menuRecView.setNestedScrollingEnabled(false);
        int screenSize = getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK;
        switch (screenSize) {
            case Configuration.SCREENLAYOUT_SIZE_XLARGE:
                menuRecView.setLayoutManager(new GridLayoutManager(getContext(), 5, LinearLayoutManager.VERTICAL, false));
                break;
            case Configuration.SCREENLAYOUT_SIZE_LARGE:
                menuRecView.setLayoutManager(new GridLayoutManager(getContext(), 3, LinearLayoutManager.VERTICAL, false));
                break;
            case Configuration.SCREENLAYOUT_SIZE_NORMAL:
                menuRecView.setLayoutManager(new GridLayoutManager(getContext(), 2, LinearLayoutManager.VERTICAL, false));
                break;
            default:
                menuRecView.setLayoutManager(new GridLayoutManager(getContext(), 2, LinearLayoutManager.VERTICAL, false));
        }


        adapter = new MenuAdapter(this, MySingleton.getmInstance(getContext()).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? true : false);
        return view;
    }

    private float getScreenWidth() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;

        float scaleFactor = displayMetrics.density;
        return width / scaleFactor;
    }

    private void design() {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        view.setLayoutParams(params);
    }

    @Override
    public void loadData(ArrayList<MenuItemDetails> menus) {
        menu = menus;
        if (menus != null && menus.size() > 0) {
            adapter.setMenuData(menus);
            ScaleInAnimationAdapter scaleInAnimationAdapter = new ScaleInAnimationAdapter(adapter);
            scaleInAnimationAdapter.setFirstOnly(false);
            menuRecView.setAdapter(scaleInAnimationAdapter);
            adapter.notifyDataSetChanged();
        }
    }

    CircleImageView cartDialogImg;

    @Override
    public void onListItemClick(int clickedItemIndex) {
        menuIndexClicked = clickedItemIndex;
        final Dialog dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.menu_item_dialoge);
        loading = (MKLoader) dialog.findViewById(R.id.dialog_loading);
        container = (FrameLayout) dialog.findViewById(R.id.dialog_container);
        presenter.onDialogCreated(menu.get(clickedItemIndex).getId(), dialog, loading, container);
        increase = (ImageView) dialog.findViewById(R.id.cart_dialog_increase);
        decrease = (ImageView) dialog.findViewById(R.id.cart_dialog_decrease);
        counter = (TextView) dialog.findViewById(R.id.cart_dialog_counter);
        TextView cartDialogName = (TextView) dialog.findViewById(R.id.menu_item_name);
        TextView cartDialogTime = (TextView) dialog.findViewById(R.id.cart_dialog_time);
        cartDialogPrice = (TextView) dialog.findViewById(R.id.cart_dialog_price);
        String priceTxt = getString(R.string.price) +
                "<font color = '#65db73'>" + String.valueOf((extraPrice * getQuantity()) + (price * getQuantity())) + getString(R.string.currency) + "</font>";
        cartDialogPrice.setText(Html.fromHtml(MySingleton.getmInstance(getContext()).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? convertDigits(priceTxt) : priceTxt), TextView.BufferType.SPANNABLE);
        cartDialogImg = (CircleImageView) dialog.findViewById(R.id.menu_item_img);
        if (MySingleton.getmInstance(getContext()).getAppLang().equals(getContext().getString(R.string.settings_language_arabic_value))) {
            cartDialogName.setText(convertDigits(" " + menu.get(clickedItemIndex).getArabicName() + " "));
        } else {
            cartDialogName.setText(" " + menu.get(clickedItemIndex).getName() + " ");
        }
        cartDialogTime.setText(MySingleton.getmInstance(getContext()).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? convertDigits(" " + menu.get(clickedItemIndex).getTime() + getString(R.string.time)) : " " + menu.get(clickedItemIndex).getTime() + getString(R.string.time));
        Glide.with(getContext()).load(URLs.ROOT_URL + menu.get(menuIndexClicked).getPhoto())
                .error(R.mipmap.restaurant_meal_icon)
                .thumbnail(0.5f)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(cartDialogImg);
        increase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onPlusClicked();

            }
        });

        decrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onMinusClicked();
            }
        });

        LikeButton isFavorite = (LikeButton) dialog.findViewById(R.id.menu_item_like_btn);
        if (menu.get(clickedItemIndex).isChecked()) {
            isFavorite.setLiked(true);
        } else {
            isFavorite.setLiked(false);
        }
        Button addToCart = (Button) dialog.findViewById(R.id.add_to_cart_btn);
        addToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                presenter.onAddToCartClicked(getMenuId(), getBranchId(), getUserId(), getPhoto(), getQuantity(),
                        getSizeId(), getSize(), getArabicSize(), getItemName(), getItemArabicName(), getExtrasId(), getExtras(), getPrice(), getTime());
            }
        });

        Button cancel = (Button) dialog.findViewById(R.id.dialog_cancel_btn);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                if (sizes != null && sizes.size() > 0) {
                    sizes.clear();
                }
                if (arabicSizes != null && arabicSizes.size() > 0) {
                    arabicSizes.clear();
                }
                if (prices != null && prices.size() > 0) {
                    prices.clear();
                }
                if (extras != null && extras.size() > 0) {
                    extras.clear();
                }

                extraPrice = 0;
                size = "";
                arabicSize = "";
                price = 0;
                Thread th = new Thread() {
                    public void run() {
                        try {
                            sleep(1000);
                            counterInteger = 1;
                        } catch (Exception e) {

                        }
                    }
                };
                th.start();
            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        int screenSize = getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK;

        DisplayMetrics metrics = getResources().getDisplayMetrics();
//        metrics.widthPixels, metrics.heightPixels
        switch (screenSize) {
            case Configuration.SCREENLAYOUT_SIZE_XLARGE:
                dialog.getWindow().setLayout(2 * (metrics.widthPixels) / 3, LinearLayout.LayoutParams.WRAP_CONTENT);
                break;
            case Configuration.SCREENLAYOUT_SIZE_LARGE:
                dialog.getWindow().setLayout(2 * (metrics.widthPixels) / 2, LinearLayout.LayoutParams.WRAP_CONTENT);
                break;
            default:
                dialog.getWindow().setLayout((metrics.widthPixels), LinearLayout.LayoutParams.WRAP_CONTENT);
        }
        dialog.show();
    }

    ArrayList<String> arabicSizes = new ArrayList<>();

    @Override
    public void sizesList(Dialog dialog, ArrayList<ItemSizeDetails> itemSizeDetails) {
        for (int i = 0; i < itemSizeDetails.size(); i++) {
            size = itemSizeDetails.get(0).getSize();
            arabicSize = itemSizeDetails.get(0).getArabicSize();
            price = itemSizeDetails.get(0).getPrice();
            sizeId = itemSizeDetails.get(0).getId();
            sizes.add(itemSizeDetails.get(i).getSize());
            arabicSizes.add(itemSizeDetails.get(i).getArabicSize());
            prices.add(itemSizeDetails.get(i).getPrice());
            sizesIds.add(itemSizeDetails.get(i).getId());
        }
        String priceTxt = getString(R.string.price) +
                "<font color = '#65db73'>" + String.valueOf((extraPrice * counterInteger) + (price * counterInteger)) + getString(R.string.currency) + "</font>";
        cartDialogPrice.setText(Html.fromHtml(MySingleton.getmInstance(getContext()).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? convertDigits(priceTxt) : priceTxt), TextView.BufferType.SPANNABLE);
//        cartDialogPrice.setText(getString(R.string.price) + String.valueOf((extraPrice * counterInteger) + (price * counterInteger)) + getString(R.string.currency));
        itemSizeDetails.clear();
        RecyclerView rv = (RecyclerView) dialog.findViewById(R.id.sizes_rec_view);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        rv.setLayoutManager(layoutManager);
        rv.setHasFixedSize(true);
        sizesAdapter = new SizesAdapter(getContext(), sizes, arabicSizes, prices, this,
                MySingleton.getmInstance(getContext()).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? true : false);
        rv.setAdapter(sizesAdapter);
    }


    LinearLayout container1;

    @Override
    public void checkBoxExtra(Dialog dialog, final ArrayList<ItemExtrasDetails> itemExtrasDetails) {
        extras = itemExtrasDetails;
        container1 = (LinearLayout) dialog.findViewById(R.id.extras_container);
        for (int i = 0; i < extras.size(); i++) {
            LinearLayout linearLayout = new LinearLayout(getContext());
            linearLayout.setOrientation(LinearLayout.HORIZONTAL);
            final CheckBox checkBox = new CheckBox(new ContextThemeWrapper(getContext(), R.style.customRadioBtn));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            checkBox.setLayoutParams(params);
            checkBox.setTag(extras.get(i).getPrice());
            if (MySingleton.getmInstance(getContext()).getAppLang().equals(getString(R.string.settings_language_arabic_value)))
                checkBox.setText(convertDigits(extras.get(i).getArabicName()));
            else checkBox.setText(extras.get(i).getName());

            if (getResources().getConfiguration().getLayoutDirection() == LayoutDirection.RTL) {
                checkBox.setGravity(Gravity.RIGHT | Gravity.CENTER);
            }
            checkBox.setTextSize(15);
            checkBox.setChecked(false);
            checkBox.setTextColor(getResources().getColor(R.color.grey_5));
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                    if (isChecked) {
                        extraPrice += Double.parseDouble(String.valueOf(compoundButton.getTag()));
                        String priceTxt = getString(R.string.price) +
                                "<font color = '#65db73'>" + String.valueOf((extraPrice * counterInteger) + (price * counterInteger)) + getString(R.string.currency) + "</font>";
                        cartDialogPrice.setText(Html.fromHtml(MySingleton.getmInstance(getContext()).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? convertDigits(priceTxt) : priceTxt), TextView.BufferType.SPANNABLE);

                    } else {
                        extraPrice -= Double.parseDouble(String.valueOf(compoundButton.getTag()));
                        String priceTxt = getString(R.string.price) +
                                "<font color = '#65db73'>" + String.valueOf((extraPrice * counterInteger) + (price * counterInteger)) + getString(R.string.currency) + "</font>";
                        cartDialogPrice.setText(Html.fromHtml(MySingleton.getmInstance(getContext()).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? convertDigits(priceTxt) : priceTxt), TextView.BufferType.SPANNABLE);
                    }
                }
            });
            linearLayout.addView(checkBox, 0);

            TextView txt = new TextView(getContext());
            LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1f);
            checkBox.setLayoutParams(params1);
            txt.setText(MySingleton.getmInstance(getContext()).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? convertDigits(String.valueOf(extras.get(i).getPrice()) + getString(R.string.currency)) : String.valueOf(extras.get(i).getPrice()) + getString(R.string.currency));
            txt.setTextColor(getResources().getColor(R.color.green_1));
            txt.setTextSize(15);
            txt.setGravity(Gravity.START);
            linearLayout.addView(txt, 1);
            container1.addView(linearLayout);

        }
    }


    @Override
    public void increaseClick() {
        if (counterInteger >= 99) {
            counterInteger = 99;
        } else {
            counterInteger++;
        }
        counter.setText(MySingleton.getmInstance(getContext()).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? convertDigits(String.valueOf(counterInteger)) : String.valueOf(counterInteger));
        String priceTxt = getString(R.string.price) +
                "<font color = '#65db73'>" + String.valueOf((extraPrice * counterInteger) + (price * counterInteger)) + getString(R.string.currency) + "</font>";
        cartDialogPrice.setText(Html.fromHtml(MySingleton.getmInstance(getContext()).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? convertDigits(priceTxt) : priceTxt), TextView.BufferType.SPANNABLE);
//        cartDialogPrice.setText(getString(R.string.price) + String.valueOf((extraPrice * counterInteger) + (price * counterInteger)) + getString(R.string.currency));
    }

    @Override
    public void decreaseClick() {
        if (counterInteger <= 1) {
            counterInteger = 1;
        } else {
            counterInteger--;
        }
        counter.setText(String.valueOf(counterInteger));
        String priceTxt = getString(R.string.price) +
                "<font color = '#65db73'>" + String.valueOf((extraPrice * counterInteger) + (price * counterInteger)) + getString(R.string.currency) + "</font>";
        cartDialogPrice.setText(Html.fromHtml(MySingleton.getmInstance(getContext()).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? convertDigits(priceTxt) : priceTxt), TextView.BufferType.SPANNABLE);
//        cartDialogPrice.setText(getString(R.string.price) + String.valueOf((extraPrice * counterInteger) + (price * counterInteger)) + getString(R.string.currency));
    }

    @Override
    public int getBranchId() {
        return mBranches.getId();
    }

    @Override
    public int getMenuId() {
        return menu.get(menuIndexClicked).getId();
    }

    @Override
    public int getUserId() {
        if (!MySingleton.getmInstance(getContext()).isLoggedIn()) {
            return -1;
        } else {
            return MySingleton.getmInstance(getContext()).userData().getId();
        }
    }

    @Override
    public String getPhoto() {
        return menu.get(menuIndexClicked).getPhoto();
    }

    @Override
    public int getQuantity() {
        return counterInteger;
    }

    @Override
    public int getSizeId() {
        Log.e("sizeId", sizeId + "");
        return sizeId;
    }

    @Override
    public String getSize() {
        return size;
    }

    @Override
    public String getArabicSize() {
        return arabicSize;
    }

    @Override
    public String getItemName() {
        return menu.get(menuIndexClicked).getName();
    }

    @Override
    public String getItemArabicName() {
        return menu.get(menuIndexClicked).getArabicName();
    }

    double extrasPrice = 0;

    @Override
    public String getExtrasId() {
        String extrasId = "";
        Log.e("sdsdsd", "SDsdsdsd");
        ArrayList<String> extrasData = getExtrasData();
        for (int i = 0; i < extras.size(); i++) {
            for (int y = 0; y < extrasData.size(); y++) {
                if (MySingleton.getmInstance(getContext()).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
                    if (extras.get(i).getArabicName().equals(extrasData.get(y))) {
                        extrasId += extras.get(i).getId();
                        Log.e("extras ", extrasId);
                        extrasPrice += extras.get(i).getPrice();
                        if (y != extras.size() - 1) {
                            extrasId += ",";
                        }
                    }
                } else {
                    if (extras.get(i).getName().equals(extrasData.get(y))) {
                        extrasId += extras.get(i).getId();
                        Log.e("extras ", extrasId);
                        extrasPrice += extras.get(i).getPrice();
                        if (y != extras.size() - 1) {
                            extrasId += ",";
                        }
                    }
                }
            }
        }
        Log.e("extras id", extrasId);
        return extrasId;
    }


    @Override
    public String getExtras() {
        String extrasNames = "";
        if (getExtrasData() != null && getExtrasData().size() > 0) {
            ArrayList<String> extras = getExtrasData();
            for (int i = 0; i < extras.size(); i++) {
                extrasNames += extras.get(i);
                if (i != extras.size() - 1) {
                    extrasNames += ",";
                }
            }
        }
        return extrasNames;
    }

    private ArrayList<String> getExtrasData() {
        ArrayList<String> checkBoxData = new ArrayList<>();
        int extraSize = container1.getChildCount();
        for (int i = 0; i < extraSize; i++) {
            CheckBox extraCheckBox = (CheckBox) ((LinearLayout) container1.getChildAt(i)).getChildAt(0);
            if (extraCheckBox.isChecked()) {
                checkBoxData.add(extraCheckBox.getText().toString().trim());
            }
        }
        return checkBoxData;
    }

    @Override
    public double getPrice() {
        Log.e("prices 1", price + "\n" + extraPrice + "\n" + getQuantity());
        double one = price + extraPrice;
        double two = one * getQuantity();
        return two;
    }

    @Override
    public String getTime() {
        return menu.get(menuIndexClicked).getTime();
    }


    @Override
    public void addToCartClick(Dialog dialog) {
        // Add to cart and make animation .......
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        if (activity instanceof MenusActivity) {
            final MenusActivity myActivity = (MenusActivity) activity;
            Log.e("counter frag", String.valueOf(counterInteger));
            MySingleton.getmInstance(getContext()).saveCartData(mBranches);
            MySingleton.getmInstance(getContext()).addToCart();
            new CircleAnimationUtil().attachActivity(getActivity())
                    .setTargetView(cartDialogImg)
                    .setDestView(myActivity.getDd())
                    .setCircleDuration(200)
                    .setMoveDuration(200)
                    .startAnimation();
            final MediaPlayer mediaPlayer = MediaPlayer.create(getContext(), R.raw.add_to_cart_sound_effect);
            mediaPlayer.start();
            Vibrator v = (Vibrator) getContext().getSystemService(Context.VIBRATOR_SERVICE);
            // Vibrate for 500 milliseconds
            v.vibrate(200);
            dialog.dismiss();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    myActivity.setCartCount(counterInteger);
                    getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                }
            }, 400);
        }
    }


    @Override
    public void goBookTable() {
        Intent intent = new Intent(getContext(), BookTableActivity.class);
        intent.putExtra("BookTableBranch", mBranches);
        startActivity(intent);
        if (MySingleton.getmInstance(getContext()).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            getActivity().overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        } else {
            getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }
    }

    @Override
    public void showErrorMessage(String error) {
        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        Toast.makeText(getContext(), error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showMainProgress() {
//        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
//                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        mainloading.setVisibility(View.VISIBLE);

    }

    @Override
    public void hideMainProgress() {
//        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        mainloading.setVisibility(View.GONE);

    }

    @Override
    public void showProgress(MKLoader loader, FrameLayout frameLayout, Dialog dialog) {
        loading = loader;
        container = frameLayout;
        dialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        loader.setVisibility(View.VISIBLE);
        frameLayout.setForeground(new ColorDrawable(Color.parseColor("#66000000")));
    }

    @Override
    public void hideProgress(Dialog dialog) {
        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        loading.setVisibility(View.GONE);
        container.setForeground(new ColorDrawable(Color.TRANSPARENT));
    }

    @Override
    public void showEmptyText(String message) {
        emptyTxt.setText(message);
    }


    @Override
    public void onSizeListItemClicked(int position) {
        if (mBranches.getType() == 1) {
            /////////book table button animation ......
            Toast.makeText(getContext(), "Go book table", Toast.LENGTH_LONG).show();
        } else {
            size = sizes.get(position);
            arabicSize = arabicSizes.get(position);
            price = prices.get(position);
            sizeId = sizesIds.get(position);
            String priceTxt = getString(R.string.price) +
                    "<font color = '#65db73'>" + String.valueOf((extraPrice * counterInteger) + (price * counterInteger)) + getString(R.string.currency) + "</font>";
            cartDialogPrice.setText(Html.fromHtml(MySingleton.getmInstance(getContext()).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? convertDigits(priceTxt) : priceTxt), TextView.BufferType.SPANNABLE);
//            cartDialogPrice.setText(getString(R.string.price) + String.valueOf((extraPrice * counterInteger) + (price * counterInteger)) + getString(R.string.currency));
        }

    }

    private String convertDigits(String string) {
        if (string == null) return null;
        String rslt = "";
        for (int i = 0; i < string.length(); i++) {
            switch (string.charAt(i)) {
                case '0':
                    rslt += "٠";
                    break;
                case '1':
                    rslt += "١";
                    break;
                case '2':
                    rslt += "٢";
                    break;
                case '3':
                    rslt += "٣";
                    break;
                case '4':
                    rslt += "٤";
                    break;
                case '5':
                    rslt += "٥";
                    break;
                case '6':
                    rslt += "٦";
                    break;
                case '7':
                    rslt += "٧";
                    break;
                case '8':
                    rslt += "٨";
                    break;
                case '9':
                    rslt += "٩";
                    break;
                default:
                    rslt += string.charAt(i);
            }
        }
        return rslt;
    }


}
