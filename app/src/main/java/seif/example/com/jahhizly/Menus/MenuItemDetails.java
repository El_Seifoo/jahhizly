package seif.example.com.jahhizly.Menus;

import java.io.Serializable;
import java.util.ArrayList;

import seif.example.com.jahhizly.Menus.MenuItem.ItemExtrasDetails;
import seif.example.com.jahhizly.Menus.MenuItem.ItemSizeDetails;

/**
 * Created by seif on 10/25/2017.
 */
public class MenuItemDetails implements Serializable {
    //    id - branch_id - name - recipe - photo - time - status - menu_category_id
//     [size][extras]
    private int id;
    private int branchId;
    private String name;
    private String arabicName;
    private String recipe;
    private String photo;
    private String time;
    private boolean status;
    private int menuCatId;
    private boolean isChecked;
    private double price;
    private ArrayList<ItemSizeDetails> itemSizeDetails;
    private ArrayList<ItemExtrasDetails> itemExtrasDetails;


    public MenuItemDetails() {
    }


    public MenuItemDetails(int id, int branchId, String name, String recipe, String photo, String time, boolean status, int menuCatId, boolean isChecked, double price) {
        this.id = id;
        this.branchId = branchId;
        this.name = name;
        this.recipe = recipe;
        this.photo = photo;
        this.time = time;
        this.status = status;
        this.menuCatId = menuCatId;
        this.isChecked = isChecked;
        this.price = price;
    }

    public MenuItemDetails(int id, int branchId, String name, String arabicName, String recipe, String photo, String time, boolean status, int menuCatId, boolean isChecked, double price) {
        this.id = id;
        this.branchId = branchId;
        this.name = name;
        this.arabicName = arabicName;
        this.recipe = recipe;
        this.photo = photo;
        this.time = time;
        this.status = status;
        this.menuCatId = menuCatId;
        this.isChecked = isChecked;
        this.price = price;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBranchId() {
        return branchId;
    }

    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArabicName() {
        return arabicName;
    }

    public void setArabicName(String arabicName) {
        this.arabicName = arabicName;
    }

    public String getRecipe() {
        return recipe;
    }

    public void setRecipe(String recipe) {
        this.recipe = recipe;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getMenuCatId() {
        return menuCatId;
    }

    public void setMenuCatId(int menuCatId) {
        this.menuCatId = menuCatId;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public ArrayList<ItemSizeDetails> getItemSizeDetails() {
        return itemSizeDetails;
    }

    public void setItemSizeDetails(ArrayList<ItemSizeDetails> itemSizeDetails) {
        this.itemSizeDetails = itemSizeDetails;
    }

    public ArrayList<ItemExtrasDetails> getItemExtrasDetails() {
        return itemExtrasDetails;
    }

    public void setItemExtrasDetails(ArrayList<ItemExtrasDetails> itemExtrasDetails) {
        this.itemExtrasDetails = itemExtrasDetails;
    }
}
