package seif.example.com.jahhizly.Menus;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Parcelable;
import android.os.Vibrator;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.dk.animation.circle.CircleAnimationUtil;
import com.eftimoff.viewpagertransformers.CubeOutTransformer;

import java.util.ArrayList;
import java.util.List;

import me.anwarshahriar.calligrapher.Calligrapher;
import seif.example.com.jahhizly.BookTable.BookTableActivity;
import seif.example.com.jahhizly.BranchLocationActivity;
import seif.example.com.jahhizly.Branches.BranchesActivity;
import seif.example.com.jahhizly.Branches.MakeOrder.Branches;
import seif.example.com.jahhizly.Cart.MyCartActivity;
import seif.example.com.jahhizly.Login.LoginActivity;
import seif.example.com.jahhizly.Menus.MenuItem.CartDbHelper;
import seif.example.com.jahhizly.Menus.MenuItem.CartItemDetails;
import seif.example.com.jahhizly.Other.NonSwipeableViewPager;
import seif.example.com.jahhizly.R;
import seif.example.com.jahhizly.Menus.Review.ReviewsFragment;
import seif.example.com.jahhizly.Utils.MySingleton;


public class MenusActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    public NonSwipeableViewPager viewPager;

    private String[] fragTitles;
    private int count;

    public void setCartCount(int count) {
        this.count += count;
        invalidateOptionsMenu();
        Log.e("counter", String.valueOf(count));
    }

    public View getDd() {
        return findViewById(R.id.test_action);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menus);
        Calligrapher calligrapher = new Calligrapher(this);
        if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            calligrapher.setFont(this, "fonts/Arabic/arabic_font.ttf", true);
        } else {
            calligrapher.setFont(this, "fonts/English/Roboto_Regular.ttf", true);
        }
        addCartCount();
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.menus_activity));


        viewPager = (NonSwipeableViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        viewPager.setPageTransformer(true, new CubeOutTransformer());
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        if (getIntent().hasExtra("ReviewIntent")) {
            if (((Branches) getIntent().getSerializableExtra("ReviewObject")).getType() == 0) {
//                showPickActionDialog();
            }
        } else {
            if (((Branches) getIntent().getSerializableExtra("branchObject")).getType() == 0) {
//                showPickActionDialog();
            }
        }


    }


    private void showPickActionDialog() {
        final Dialog dialog = new Dialog(MenusActivity.this);
        dialog.setContentView(R.layout.pick_action_dialog);
        Button makeOrderBtn = (Button) dialog.findViewById(R.id.pick_action_make_order_btn);
        Button bookTableBtn = (Button) dialog.findViewById(R.id.pick_action_book_table_btn);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCanceledOnTouchOutside(false);
        makeOrderBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Vibrator v = (Vibrator) getSystemService(VIBRATOR_SERVICE);
                v.vibrate(50);
            }
        });

        bookTableBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Vibrator v = (Vibrator) getSystemService(VIBRATOR_SERVICE);
                v.vibrate(50);
                if (!MySingleton.getmInstance(MenusActivity.this).isLoggedIn()) {
                    if (getIntent().hasExtra("ReviewIntent")) {
                        Intent intent = new Intent(MenusActivity.this, LoginActivity.class);
                        intent.putExtra("LoginIntent", "FromMenuToBookTable");
                        intent.putExtra("BookTableBranch", getIntent().getSerializableExtra("ReviewObject"));
                        startActivity(intent);
                        if (MySingleton.getmInstance(MenusActivity.this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
                            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                        } else {
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        }
                    } else {
                        Intent intent = new Intent(MenusActivity.this, LoginActivity.class);
                        intent.putExtra("LoginIntent", "FromMenuToBookTable");
                        intent.putExtra("BookTableBranch", getIntent().getSerializableExtra("branchObject"));
                        startActivity(intent);
                        if (MySingleton.getmInstance(MenusActivity.this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
                            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                        } else {
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        }
                    }
                } else {
                    if (getIntent().hasExtra("ReviewIntent")) {
                        Intent intent = new Intent(MenusActivity.this, BookTableActivity.class);
                        intent.putExtra("BookTableBranch", getIntent().getSerializableExtra("ReviewObject"));
                        startActivity(intent);
                        if (MySingleton.getmInstance(MenusActivity.this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
                            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                        } else {
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        }
                    } else {
                        Intent intent = new Intent(MenusActivity.this, BookTableActivity.class);
                        intent.putExtra("BookTableBranch", getIntent().getSerializableExtra("branchObject"));
                        startActivity(intent);
                        if (MySingleton.getmInstance(MenusActivity.this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
                            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                        } else {
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        }
                    }
                }

            }
        });
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        count = 0;
        addCartCount();
    }

    private void addCartCount() {
        if (MySingleton.getmInstance(MenusActivity.this).isLoggedIn()) {
            ArrayList<CartItemDetails> cart = CartDbHelper.getmInstance(MenusActivity.this)
                    .getItems(MySingleton.getmInstance(MenusActivity.this).userData().getId());
            if (cart.size() > 0) {
                for (int i = 0; i < cart.size(); i++) {
                    count += cart.get(i).getQuantity();
                }
            } else {
                count = 0;
            }
        } else {
            ArrayList<CartItemDetails> cart = CartDbHelper.getmInstance(MenusActivity.this)
                    .getItems(-1);
            if (cart.size() > 0) {
                for (int i = 0; i < cart.size(); i++) {
                    count += cart.get(i).getQuantity();
                }
            } else {
                count = 0;
            }
        }
        invalidateOptionsMenu();

    }

    @Override
    public void onBackPressed() {
        if (viewPager.getCurrentItem() != 0) {
            viewPager.setCurrentItem(0);
        } else {
            super.onBackPressed();
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        fragTitles = getResources().getStringArray(R.array.menu_activity_tabs_titles);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        if (getIntent().hasExtra("ReviewIntent")) {
            MenuDetailsFragment menuDetailsFragment = new MenuDetailsFragment();
            menuDetailsFragment.sendBranchObj((Branches) getIntent()
                    .getSerializableExtra("ReviewObject"));
            adapter.addFragment(menuDetailsFragment, fragTitles[0]);
            ReviewsFragment reviewsFragment = new ReviewsFragment();
            reviewsFragment.sendBranchObj((Branches) getIntent().getSerializableExtra("ReviewObject"));
            adapter.addFragment(reviewsFragment, fragTitles[1]);
            BranchLocationActivity branchLocationActivity = new BranchLocationActivity();
            branchLocationActivity.setBranch((Branches) getIntent().getSerializableExtra("ReviewObject"));
            adapter.addFragment(branchLocationActivity, fragTitles[2]);
        } else {
            MenuDetailsFragment menuDetailsFragment = new MenuDetailsFragment();
            menuDetailsFragment.sendBranchObj((Branches) getIntent()
                    .getSerializableExtra("branchObject"));
            adapter.addFragment(menuDetailsFragment, fragTitles[0]);
            ReviewsFragment reviewsFragment = new ReviewsFragment();
            reviewsFragment.sendBranchObj((Branches) getIntent().getSerializableExtra("branchObject"));
            adapter.addFragment(reviewsFragment, fragTitles[1]);
            BranchLocationActivity branchLocationActivity = new BranchLocationActivity();
            branchLocationActivity.setBranch((Branches) getIntent().getSerializableExtra("branchObject"));
            adapter.addFragment(branchLocationActivity, fragTitles[2]);
        }

        viewPager.setAdapter(adapter);
        if (getIntent().hasExtra("ReviewIntent")) {
            viewPager.setCurrentItem(1);
        } else {
            viewPager.setCurrentItem(0);
        }
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        @Override
        public Parcelable saveState() {
            return null;
        }

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private Drawable buildCounterDrawable(int count, int backgroundImageId) {
        LayoutInflater inflater = LayoutInflater.from(MenusActivity.this);
        View view = inflater.inflate(R.layout.counter_menu_item_layout, null);
        view.setBackgroundResource(backgroundImageId);

        if (count == 0) {
            View counterTextPanel = view.findViewById(R.id.counter_value_panel);
            counterTextPanel.setVisibility(View.GONE);
        } else {
            TextView textView = (TextView) view.findViewById(R.id.count);
            textView.setText(MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? convertDigits("" + count) : "" + count);
        }

        view.measure(
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());

        view.setDrawingCacheEnabled(true);
        view.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        Bitmap bitmap = Bitmap.createBitmap(view.getDrawingCache());
        view.setDrawingCacheEnabled(false);

        return new BitmapDrawable(getResources(), bitmap);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.cart_menu, menu);
        MenuItem menuItem = menu.findItem(R.id.test_action);
        menuItem.setIcon(buildCounterDrawable(count, R.mipmap.nav_cart));
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            } else {
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            }
            return true;
        }
        if (id == R.id.test_action) {
            startActivity(new Intent(MenusActivity.this, MyCartActivity.class));
            if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            } else {
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private String convertDigits(String string) {
        if (string==null) return null;
        String rslt = "";
        for (int i = 0; i < string.length(); i++) {
            switch (string.charAt(i)) {
                case '0':
                    rslt += "٠";
                    break;
                case '1':
                    rslt += "١";
                    break;
                case '2':
                    rslt += "٢";
                    break;
                case '3':
                    rslt += "٣";
                    break;
                case '4':
                    rslt += "٤";
                    break;
                case '5':
                    rslt += "٥";
                    break;
                case '6':
                    rslt += "٦";
                    break;
                case '7':
                    rslt += "٧";
                    break;
                case '8':
                    rslt += "٨";
                    break;
                case '9':
                    rslt += "٩";
                    break;
                default:
                    rslt += string.charAt(i);
            }
        }
        return rslt;
    }


}
