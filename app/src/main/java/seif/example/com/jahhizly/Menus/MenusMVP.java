package seif.example.com.jahhizly.Menus;

import android.app.Dialog;
import android.support.v4.view.ViewPager;
import android.widget.FrameLayout;

import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;

import seif.example.com.jahhizly.Menus.MenuItem.ItemExtrasDetails;
import seif.example.com.jahhizly.Menus.MenuItem.ItemSizeDetails;
import seif.example.com.jahhizly.Menus.Review.ReviewDetails;

/**
 * Created by seif on 10/25/2017.
 */
public interface MenusMVP {
    interface view {
        void setupViewPager(ViewPager viewPager, ArrayList<Categories> categories);

        void setBranchName(String name);

        void setBranchLogo(String logo);

        void setBranchCategory(String category);

        void setBranchDistance(Double distance);

        void showProgress();

        void hideProgress();
    }

    interface presenter {
        void onMenuDetailsCreated(ViewPager viewPager, int id);

        void setBranchData(String logo, String name, String category, double distance);
    }


    interface viewMenu {
        void loadData(ArrayList<MenuItemDetails> menus);

        void sizesList(Dialog dialog, ArrayList<ItemSizeDetails> itemSizeDetails);

        void checkBoxExtra(Dialog dialog, ArrayList<ItemExtrasDetails> itemExtrasDetails);

        void increaseClick();

        void decreaseClick();

        int getBranchId();

        int getMenuId();

        int getUserId();

        String getPhoto();

        int getQuantity();

        int getSizeId();

        String getSize();

        String getArabicSize();

        String getItemName();

        String getItemArabicName();

        String getExtrasId();

        String getExtras();

        double getPrice();

        String getTime();

        void addToCartClick(Dialog dialog);

        void goBookTable();

        void showErrorMessage(String error);

        void showMainProgress();

        void hideMainProgress();

        void showProgress(MKLoader loader, FrameLayout container, Dialog dialog);

        void hideProgress(Dialog dialog);

        void showEmptyText(String message);

    }

    interface presenterMenu {
        void onMenuFragmentCreated(int categoryId, int branchId, int type);

        void onDialogCreated(int menuId, Dialog dialog, MKLoader loader, FrameLayout container);

        void onPlusClicked();

        void onMinusClicked();

        void onAddToCartClicked(int menuId, int branchId, int userId, String photo, int count, int sizeId,
                                String size, String arabicSize, String itemName, String arabicItemName, String extrasId, String extras, double price, String time);
    }

    interface viewRev {
        void loadData(ArrayList<ReviewDetails> reviewDetails);

        void showErrorMessage(String error);

        void showProgress();

        void hideProgress();

        void goLogin();

        void showEmptyText(String message);

        void clearAdapter();
    }

    interface presenterRev {
        void onReviewCreated(int branchId);

        void onSubmitReviewClicked(int branchId, float rate, boolean isRated, String comment);
    }
}
