package seif.example.com.jahhizly.Menus;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;
import seif.example.com.jahhizly.Menus.MenuItem.CartDbHelper;
import seif.example.com.jahhizly.Menus.MenuItem.CartItemDetails;
import seif.example.com.jahhizly.R;
import seif.example.com.jahhizly.Utils.MySingleton;
import seif.example.com.jahhizly.Utils.URLs;

/**
 * Created by seif on 10/24/2017.
 */
public class MenusModel {

    private Context context;

    public MenusModel(Context context) {
        this.context = context;
    }

    protected void getAllCategories(final VolleyCallback callback, int brandId) {
        String url = URLs.MENU_CATEGORIES + brandId;
        Log.e("urlllll", url);
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            callback.onSuccess(response, 1);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.onFail(error, 1);
                    }
                });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getmInstance(context).addToRQ(stringRequest);
    }


    protected void getMenus(final VolleyCallback callback, int categoryId, int branchId) {
        String url;
        if (categoryId <= 0) {
            url = URLs.BRANCH_MENU + branchId;
        } else {
            url = URLs.BRANCH_MENU + branchId + "/" + categoryId;
        }
        Log.e("menu URLLLL", url);
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            callback.onSuccess(response, 2);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.onFail(error, 2);
                    }
                });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getmInstance(context).addToRQ(stringRequest);

    }

    protected void getSizesExtras(final VolleyCallback callback, int menuId) {
        String url = URLs.MENU_ITEM_DETAILS + menuId;
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            callback.onSuccess(response, 3);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.onFail(error, 3);
                    }
                });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getmInstance(context).addToRQ(stringRequest);
    }

    protected void getAllReviews(final VolleyCallback callback, int branchId) {
        Log.e("urllllll", URLs.USERS_REVIEWS + branchId);
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                URLs.USERS_REVIEWS + branchId,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("response reviews", response.toString());
                        try {
                            callback.onSuccess(response, 4);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.onFail(error, 4);
                    }
                });
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getmInstance(context).addToRQ(stringRequest);
    }

    protected void addReview(final VolleyCallback callback , int branchId, float rate, boolean isRated, String comment) throws JSONException {
        JSONObject review = new JSONObject();
        review.put("branch_id", branchId);
        review.put("rate", rate);
        review.put("is_rated", isRated);
        review.put("comment", comment);
        Log.e("review json", review.toString());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                URLs.ADD_USER_REVIEW,
                review,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            callback.onSuccess(response.toString() , 5);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onFail(error , 5);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                String token = "Bearer " + MySingleton.getmInstance(context).UserKey();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Authorization", token);
                return headers;
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getmInstance(context).addToRQ(jsonObjectRequest);
    }

    protected void getFavoriteMeals(final VolleyCallback callBack) {
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                URLs.FAVORITE_MENUS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            callBack.onSuccess(response, 6);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callBack.onFail(error, 6);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                String token = "Bearer " + MySingleton.getmInstance(context).UserKey();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Authorization", token);
                return headers;
            }
        };
        MySingleton.getmInstance(context).addToRQ(stringRequest);
    }

    protected void addToCartDB(DBCallback callback, CartItemDetails cart) {
        CartDbHelper.getmInstance(context).addItem(cart);
        callback.onSuccess("done");
    }

    protected interface DBCallback {
        void onSuccess(String done);
    }

    protected interface VolleyCallback {
        void onSuccess(String response, int type) throws JSONException;

        void onFail(VolleyError error, int type);
    }
}
