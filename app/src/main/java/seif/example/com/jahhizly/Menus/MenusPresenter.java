package seif.example.com.jahhizly.Menus;

import android.app.Dialog;
import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.tuyenmonkey.mkloader.MKLoader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import seif.example.com.jahhizly.Menus.MenuItem.CartItemDetails;
import seif.example.com.jahhizly.Menus.MenuItem.ItemExtrasDetails;
import seif.example.com.jahhizly.Menus.MenuItem.ItemSizeDetails;
import seif.example.com.jahhizly.Menus.Review.ReviewDetails;
import seif.example.com.jahhizly.R;
import seif.example.com.jahhizly.Utils.MySingleton;

/**
 * Created by seif on 10/24/2017.
 */
public class MenusPresenter implements MenusMVP.presenter, MenusModel.VolleyCallback, MenusMVP.presenterMenu, MenusMVP.presenterRev, MenusModel.DBCallback {

    private MenusModel model;
    private Context context;
    MenusMVP.view view;
    Dialog dialog;
    MenusMVP.viewMenu viewMenu;
    MenusModel.VolleyCallback callback;
    MenusModel.DBCallback dbCallback;
    ArrayList<Categories> categories = new ArrayList<>();

    ArrayList<ItemSizeDetails> itemSizeDetails = new ArrayList<>();
    ArrayList<ItemExtrasDetails> itemExtrasDetails = new ArrayList<>();
    ArrayList<ReviewDetails> reviewList = new ArrayList<>();
    ViewPager viewPager = null;
    MenusMVP.viewRev viewRev;

    public MenusPresenter(MenusMVP.view view, MenusModel model, Context context) {
        this.view = view;
        this.model = model;
        this.context = context;
        callback = this;
        dbCallback = this;
    }

    public MenusPresenter(MenusMVP.viewMenu viewMenu, MenusModel model, Context context) {
        this.viewMenu = viewMenu;
        this.model = model;
        this.context = context;
        callback = this;
        dbCallback = this;
    }

    public MenusPresenter(MenusModel model, Context context, MenusMVP.viewRev viewRev) {
        this.model = model;
        this.context = context;
        this.viewRev = viewRev;
        callback = this;
    }

    @Override
    public void onMenuDetailsCreated(ViewPager viewPager, int id) {
        this.viewPager = viewPager;
        Log.e("seifoo1", String.valueOf(id));
        view.showProgress();
        model.getAllCategories(callback, id);
    }

    @Override
    public void setBranchData(String logo, String name, String category, double distance) {
        view.setBranchLogo(logo);
        view.setBranchName(name);
        view.setBranchCategory(category);
        view.setBranchDistance(distance);
    }

    @Override
    public void onMenuFragmentCreated(int categoryId, int branchId, int type) {
        viewMenu.showMainProgress();
//        if (MySingleton.getmInstance(context).isLoggedIn()) {
//            model.getFavoriteMeals(this);
//        }
        model.getMenus(callback, categoryId, branchId);
    }


    ArrayList<MenuItemDetails> favMenusList = new ArrayList<>();

    @Override
    public void onDialogCreated(int menuId, Dialog dialog, MKLoader loader, FrameLayout container) {
        this.dialog = dialog;
        viewMenu.showProgress(loader, container, dialog);
        model.getSizesExtras(callback, menuId);
    }

    @Override
    public void onPlusClicked() {
        viewMenu.increaseClick();
    }

    @Override
    public void onMinusClicked() {
        viewMenu.decreaseClick();
    }

    @Override
    public void onAddToCartClicked(int menuId, int branchId, int userId, String photo, int count, int sizeId, String size, String arabicSize, String itemName, String itemArabicName, String extrasId, String extras, double price, String time) {
        Log.e("user id 1", "" + userId);
        if (!MySingleton.getmInstance(context).isCartFull()) {
            model.addToCartDB(dbCallback, new CartItemDetails(userId, branchId, menuId, photo, count, sizeId, size, arabicSize, itemName, itemArabicName, extrasId, extras, price, time));
        } else {
            if (MySingleton.getmInstance(context).getCartData().getId() != branchId) {
                viewMenu.showErrorMessage(context.getString(R.string.error_2));
            } else {
                model.addToCartDB(dbCallback, new CartItemDetails(userId, branchId, menuId, photo, count, sizeId, size, arabicSize, itemName, itemArabicName, extrasId, extras, price, time));
            }
        }
    }

    @Override
    public void onReviewCreated(int branchId) {
        viewRev.showProgress();
        model.getAllReviews(callback, branchId);
    }

    private int id;

    @Override
    public void onSubmitReviewClicked(int branchId, float rate, boolean isRated, String comment) {
        if (!MySingleton.getmInstance(context).isLoggedIn()) {
            viewRev.goLogin();
        } else {
            id = branchId;
            if (comment.equals("")) {
                viewRev.showErrorMessage(context.getString(R.string.error_3));
                return;
            }
            try {
                model.addReview(callback, branchId, rate, true, comment);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    ArrayList<MenuItemDetails> menuItemDetails = new ArrayList<>();

    @Override
    public void onSuccess(String response, int type) throws JSONException {
        if (type == 1) {
            Log.e("categMenus", response);
            if (MySingleton.getmInstance(context).getAppLang().equals(context.getString(R.string.settings_language_english_value))) {
                JSONObject jsonResponse = new JSONObject(response);
                JSONArray menuCategories = jsonResponse.getJSONArray("menuCategories");
                Log.e("ssssssss", String.valueOf(menuCategories.length()));
                for (int i = 0; i < menuCategories.length(); i++) {
                    int id = menuCategories.getJSONObject(i).getInt("id");
                    String name;
                    if (menuCategories.getJSONObject(i).getString("name").equals("")
                            || menuCategories.getJSONObject(i).getString("name").equals("null")) {
                        name = "no data available";
                    } else {
                        name = menuCategories.getJSONObject(i).getString("name");
                    }
                    Log.e("sssssss", name);
                    String photo;
                    if (menuCategories.getJSONObject(i).getString("photo").equals("")
                            || menuCategories.getJSONObject(i).getString("photo").equals("null")) {
                        photo = "null";
                    } else {
                        photo = menuCategories.getJSONObject(i).getString("photo");
                    }
                    int brandId = menuCategories.getJSONObject(i).getInt("brand_id");
                    Categories menuCategories1 = new Categories(id, name, photo, brandId);

                    categories.add(menuCategories1);
                }
            } else {
                JSONObject jsonResponse = new JSONObject(response);
                JSONArray menuCategories = jsonResponse.getJSONArray("menuCategories");
                Log.e("ssssssss", String.valueOf(menuCategories.length()));
                for (int i = 0; i < menuCategories.length(); i++) {
                    int id = menuCategories.getJSONObject(i).getInt("id");
                    String name;
                    if (menuCategories.getJSONObject(i).getString("arabic_name").equals("")
                            || menuCategories.getJSONObject(i).getString("arabic_name").equals("null")) {
                        name = context.getString(R.string.list_empty);
                    } else {
                        name = menuCategories.getJSONObject(i).getString("arabic_name");
                    }
                    Log.e("sssssss", name);
                    String photo;
                    if (menuCategories.getJSONObject(i).getString("photo").equals("")
                            || menuCategories.getJSONObject(i).getString("photo").equals("null")) {
                        photo = "null";
                    } else {
                        photo = menuCategories.getJSONObject(i).getString("photo");
                    }
                    int brandId = menuCategories.getJSONObject(i).getInt("brand_id");
                    Categories menuCategories1 = new Categories(id, name, photo, brandId);

                    categories.add(menuCategories1);
                }
            }
            view.hideProgress();
            view.setupViewPager(viewPager, categories);
        } else if (type == 2) {
            Log.e("menu", response);
            JSONObject jsonResponse = new JSONObject(response);
            JSONArray menus = jsonResponse.getJSONArray("menus");
            int branchesLength = menus.length();
            for (int i = 0; i < branchesLength; i++) {
                int id = menus.getJSONObject(i).getInt("id");
                int branchId = menus.getJSONObject(i).getInt("branch_id");
                String name = menus.getJSONObject(i).getString("name");
                String arabicName = menus.getJSONObject(i).getString("arabic_name");
                String recipe = menus.getJSONObject(i).getString("recipe");
                String photo;
                if (menus.getJSONObject(i).getString("photo").equals("") ||
                        menus.getJSONObject(i).getString("photo").equals("null")) {
                    photo = "null";
                } else {
                    photo = menus.getJSONObject(i).getString("photo");
                }
                String time = menus.getJSONObject(i).getString("time");
                int statusInt = menus.getJSONObject(i).getInt("status");
                boolean isAvailable;
                if (statusInt == 1) {
                    isAvailable = true;
                } else {
                    isAvailable = false;
                }
                int menuCategoryId = menus.getJSONObject(i).getInt("menu_category_id");
                double price = menus.getJSONObject(i).getDouble("price");
                MenuItemDetails menuItemDetails1 = new MenuItemDetails(id, branchId, name, arabicName, recipe,
                        photo, time, isAvailable, menuCategoryId, false, price);
                menuItemDetails1.setPrice(price);
                menuItemDetails.add(menuItemDetails1);
            }


            if (menuItemDetails != null && menuItemDetails.size() > 0) {
                if (MySingleton.getmInstance(context).isLoggedIn()) {
//                    viewMenu.loadData(getFinalMenus(menuItemDetails, favMenusList));
                    model.getFavoriteMeals(this);
                } else {
                    viewMenu.hideMainProgress();
                    viewMenu.loadData(menuItemDetails);
                }

            } else {
                viewMenu.hideMainProgress();
                viewMenu.showEmptyText(context.getString(R.string.list_empty));
            }
        } else if (type == 3) {
            if (itemSizeDetails != null && itemSizeDetails.size() > 0) {
                itemSizeDetails.clear();
            }
            if (itemExtrasDetails != null && itemSizeDetails.size() > 0) {
                itemExtrasDetails.clear();
            }
            Log.e("sizes , extras", response);
            JSONObject jsonResponse = new JSONObject(response);
            JSONArray sizes = jsonResponse.getJSONArray("sizes");
            JSONArray extras = jsonResponse.getJSONArray("extras");
            for (int i = 0; i < sizes.length(); i++) {
                int id = sizes.getJSONObject(i).getInt("id");
                String size = sizes.getJSONObject(i).getString("size");
                String arabicSize = sizes.getJSONObject(i).getString("arabic_size");
                double price = sizes.getJSONObject(i).getDouble("price");
                ItemSizeDetails itemSizes = new ItemSizeDetails(id, size, arabicSize, price);
                itemSizeDetails.add(itemSizes);
            }
            for (int i = 0; i < extras.length(); i++) {
                int id = extras.getJSONObject(i).getInt("id");
                String name = extras.getJSONObject(i).getString("name");
                String arabicName = extras.getJSONObject(i).getString("arabic_name");
                double price = extras.getJSONObject(i).getDouble("price");
                ItemExtrasDetails itemExtras = new ItemExtrasDetails(id, name, arabicName, price);
                itemExtrasDetails.add(itemExtras);
            }
            viewMenu.hideProgress(dialog);
            viewMenu.sizesList(dialog, itemSizeDetails);
            viewMenu.checkBoxExtra(dialog, itemExtrasDetails);

        } else if (type == 4) {
            JSONObject jsonObject = new JSONObject(response);
            JSONArray reviews = jsonObject.getJSONArray("reviews");
            for (int i = 0; i < reviews.length(); i++) {
                double rate = reviews.getJSONObject(i).getDouble("rate");
                String comment = reviews.getJSONObject(i).getString("comment");
                String branchName = reviews.getJSONObject(i).getString("name");
                String[] data = reviews.getJSONObject(i).getString("created_at").split(" ");
                String date = data[0];
                String time = data[1];
                String userName = reviews.getJSONObject(i).getString("user_name");
                String userPhoto = reviews.getJSONObject(i).getString("user_photo");
                int genderInt = reviews.getJSONObject(i).getInt("user_gender");
                String gender;
                if (genderInt == 0) {
                    gender = "Male";
                } else {
                    gender = "Female";
                }
                ReviewDetails reviewsDetails = new ReviewDetails(rate, comment, branchName, date, time, userPhoto, userName, gender);
                reviewList.add(reviewsDetails);
            }
            viewRev.hideProgress();
            if (reviewList != null && reviewList.size() > 0) {
                viewRev.loadData(reviewList);
            } else {
                viewRev.showEmptyText(context.getString(R.string.list_empty));
            }
        } else if (type == 5) {
            model.getAllReviews(this, id);
            Log.e("add review response ", response);
            viewRev.clearAdapter();
//            {"status":"success","desc":"review created successfully"}
        } else if (type == 6) {
            Log.e("favMenu", response);
            JSONObject jsonResponse = new JSONObject(response);
            JSONArray menus = jsonResponse.getJSONArray("menus");
            int branchesLength = menus.length();
            for (int i = 0; i < branchesLength; i++) {
                int id = menus.getJSONObject(i).getInt("id");
                int branchId = menus.getJSONObject(i).getInt("branch_id");
                String name = menus.getJSONObject(i).getString("name");
                String recipe = menus.getJSONObject(i).getString("recipe");
                String photo;
                if (menus.getJSONObject(i).getString("photo").equals("") ||
                        menus.getJSONObject(i).getString("photo").equals("null")) {
                    photo = "null";
                } else {
                    photo = menus.getJSONObject(i).getString("photo");
                }
                String time = menus.getJSONObject(i).getString("time");
                int statusInt = menus.getJSONObject(i).getInt("status");
                boolean isAvailable;
                if (statusInt == 1) {
                    isAvailable = true;
                } else {
                    isAvailable = false;
                }
                int menuCategoryId = menus.getJSONObject(i).getInt("menu_category_id");
                double price = menus.getJSONObject(i).getDouble("price");
                favMenusList.add(new MenuItemDetails(id, branchId, name, recipe, photo, time, isAvailable, menuCategoryId, true, price));
            }
            viewMenu.hideMainProgress();
            viewMenu.loadData(getFinalMenus(menuItemDetails, favMenusList));

        }
    }

    private ArrayList<MenuItemDetails> getFinalMenus(ArrayList<MenuItemDetails> menusList, ArrayList<MenuItemDetails> favMenusList) {
        if (favMenusList.size() > 0 && favMenusList != null) {
            for (int i = 0; i < menusList.size(); i++) {
                for (int j = 0; j < favMenusList.size(); j++) {
                    Log.e("i (" + i + ")", menusList.get(i).getId() + "");
                    Log.e("j (" + j + ")", favMenusList.get(j).getId() + "");
                    if (menusList.get(i).getId() == favMenusList.get(j).getId()) {
                        menusList.get(i).setChecked(true);
                        break;
                    }
                }
            }
        }

        return menusList;
    }

    @Override
    public void onFail(VolleyError error, int type) {
        if (type == 1) {
            view.hideProgress();
            if (error instanceof NoConnectionError || error instanceof TimeoutError) {
                Toast.makeText(context, context.getString(R.string.internet_error), Toast.LENGTH_LONG).show();
            } else if (error instanceof ServerError) {
                Toast.makeText(context, context.getString(R.string.server_error), Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(context, context.getString(R.string.wrong), Toast.LENGTH_LONG).show();
            }
        } else if (type == 2) {
            viewMenu.hideMainProgress();
            if (error instanceof NoConnectionError || error instanceof TimeoutError) {
                Toast.makeText(context, context.getString(R.string.internet_error), Toast.LENGTH_LONG).show();
            } else if (error instanceof ServerError) {
                Toast.makeText(context, context.getString(R.string.server_error), Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(context, context.getString(R.string.wrong), Toast.LENGTH_LONG).show();
            }
        } else if (type == 3) {
            viewMenu.hideProgress(dialog);
            if (error instanceof NoConnectionError || error instanceof TimeoutError) {
                Toast.makeText(context, context.getString(R.string.internet_error), Toast.LENGTH_LONG).show();
            } else if (error instanceof ServerError) {
                Toast.makeText(context, context.getString(R.string.server_error), Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(context, context.getString(R.string.wrong), Toast.LENGTH_LONG).show();
            }
        } else if (type == 4) {
            viewRev.hideProgress();
            if (error instanceof NoConnectionError || error instanceof TimeoutError) {
                Toast.makeText(context, context.getString(R.string.internet_error), Toast.LENGTH_LONG).show();
            } else if (error instanceof ServerError) {
                Toast.makeText(context, context.getString(R.string.server_error), Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(context, context.getString(R.string.wrong), Toast.LENGTH_LONG).show();
            }
        } else if (type == 5) {
            if (error instanceof NoConnectionError || error instanceof TimeoutError) {
                Toast.makeText(context, context.getString(R.string.internet_error), Toast.LENGTH_LONG).show();
            } else if (error instanceof ServerError) {
                Toast.makeText(context, context.getString(R.string.server_error), Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(context, context.getString(R.string.wrong), Toast.LENGTH_LONG).show();
            }
        } else if (type == 6) {
            viewMenu.hideMainProgress();
            viewMenu.loadData(menuItemDetails);
        }
    }


    @Override
    public void onSuccess(String done) {
        if (done.equals("done")) {
            Toast.makeText(context, context.getString(R.string.done), Toast.LENGTH_LONG).show();
            viewMenu.addToCartClick(this.dialog);
        } else {
            Toast.makeText(context, context.getString(R.string.wrong), Toast.LENGTH_LONG).show();
        }
    }


}
