package seif.example.com.jahhizly.Menus.Review;

import java.io.Serializable;

/**
 * Created by seif on 10/31/2017.
 */
public class ReviewDetails implements Serializable {
    private double rate ;
    private String comment ;
    private String branchName ;
    private String date ;
    private String time ;
    private String userPhoto ;
    private String userName ;
    private String gender ;

    public ReviewDetails() {}

    public ReviewDetails(double rate, String comment, String branchName,
                         String date, String time, String userPhoto, String userName ,String gender) {
        this.rate = rate;
        this.comment = comment;
        this.branchName = branchName;
        this.date = date;
        this.time = time;
        this.userPhoto = userPhoto;
        this.userName = userName;
        this.gender = gender;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getUserPhoto() {
        return userPhoto;
    }

    public void setUserPhoto(String userPhoto) {
        this.userPhoto = userPhoto;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
