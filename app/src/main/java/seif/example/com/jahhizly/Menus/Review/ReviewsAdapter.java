package seif.example.com.jahhizly.Menus.Review;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;

import java.util.ArrayList;

import me.anwarshahriar.calligrapher.Calligrapher;
import seif.example.com.jahhizly.Other.CircleTransform;
import seif.example.com.jahhizly.R;
import seif.example.com.jahhizly.Utils.MySingleton;
import seif.example.com.jahhizly.Utils.URLs;

/**
 * Created by seif on 10/31/2017.
 */
public class ReviewsAdapter extends RecyclerView.Adapter<ReviewsAdapter.Holder> {
    private ArrayList<ReviewDetails> reviewDetails = new ArrayList<>();

    public ReviewsAdapter() {
    }

    public void setReviewDetails(ArrayList<ReviewDetails> reviewDetails) {
        this.reviewDetails = reviewDetails;
        notifyDataSetChanged();
    }

    public void clear() {
        if (reviewDetails != null && reviewDetails.size() > 0) {
            reviewDetails.clear();
            notifyDataSetChanged();
        }
    }

    @Override
    public ReviewsAdapter.Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new Holder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.review_list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ReviewsAdapter.Holder holder, int position) {
        Calligrapher calligrapher = new Calligrapher(holder.itemView.getContext());
        if (MySingleton.getmInstance(holder.itemView.getContext()).getAppLang().equals(holder.itemView.getContext().getString(R.string.settings_language_arabic_value))) {
            calligrapher.setFont(holder.itemView, "fonts/Arabic/arabic_font.ttf");
        } else {
            calligrapher.setFont(holder.itemView, "fonts/English/Roboto_Regular.ttf");
        }
        if (reviewDetails.get(position).getGender().equals("Male")) {
            Glide.with(holder.itemView.getContext()).load(URLs.ROOT_URL + reviewDetails.get(position).getUserPhoto())
                    .error(R.mipmap.sidemenu_pic_profile_man)
                    .crossFade()
                    .thumbnail(0.5f)
                    .bitmapTransform(new CircleTransform(holder.itemView.getContext()))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.reviewImg);
        } else {
            Glide.with(holder.itemView.getContext()).load(URLs.ROOT_URL + reviewDetails.get(position).getUserPhoto())
                    .error(R.mipmap.sidemenu_pic_profile_woman)
                    .crossFade()
                    .thumbnail(0.5f)
                    .bitmapTransform(new CircleTransform(holder.itemView.getContext()))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.reviewImg);
        }

        holder.reviewUsername.setText(" " + reviewDetails.get(position).getUserName() + " ");
        holder.reviewCommment.setText(" " + reviewDetails.get(position).getComment() + " ");
        if (reviewDetails.get(position).getRate() >= 0 && reviewDetails.get(position).getRate() <= 2) {
            holder.rateBar.setFillColor(holder.itemView.getResources().getColor(R.color.red_3));
            holder.rateBar.setPressedBorderColor(holder.itemView.getResources().getColor(R.color.red_3));
            holder.rateBar.setPressedFillColor(holder.itemView.getResources().getColor(R.color.red_3));
            holder.rateBar.setBorderColor(holder.itemView.getResources().getColor(R.color.red_3));

        } else if (reviewDetails.get(position).getRate() > 2 && reviewDetails.get(position).getRate() <= 3.75) {
            holder.rateBar.setFillColor(holder.itemView.getResources().getColor(R.color.yellow));
            holder.rateBar.setPressedBorderColor(holder.itemView.getResources().getColor(R.color.yellow));
            holder.rateBar.setPressedFillColor(holder.itemView.getResources().getColor(R.color.yellow));
            holder.rateBar.setBorderColor(holder.itemView.getResources().getColor(R.color.yellow));
        } else if (reviewDetails.get(position).getRate() > 3.75 && reviewDetails.get(position).getRate() <= 5) {
            holder.rateBar.setFillColor(holder.itemView.getResources().getColor(R.color.green_1));
            holder.rateBar.setPressedBorderColor(holder.itemView.getResources().getColor(R.color.green_1));
            holder.rateBar.setPressedFillColor(holder.itemView.getResources().getColor(R.color.green_1));
            holder.rateBar.setBorderColor(holder.itemView.getResources().getColor(R.color.green_1));
        }
        holder.rateBar.setRating((float) reviewDetails.get(position).getRate());
    }

    @Override
    public int getItemCount() {
        return (null != reviewDetails ? reviewDetails.size() : 0);
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView reviewUsername, reviewCommment;
        ImageView reviewImg;
        SimpleRatingBar rateBar;

        public Holder(View itemView) {
            super(itemView);
            reviewUsername = (TextView) itemView.findViewById(R.id.review_username);
            reviewCommment = (TextView) itemView.findViewById(R.id.review_comment);
            reviewImg = (ImageView) itemView.findViewById(R.id.review_pic);
            rateBar = (SimpleRatingBar) itemView.findViewById(R.id.review_rate);
        }
    }



}
