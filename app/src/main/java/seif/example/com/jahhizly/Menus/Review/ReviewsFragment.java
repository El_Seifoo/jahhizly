package seif.example.com.jahhizly.Menus.Review;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Space;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.tuyenmonkey.mkloader.MKLoader;

import java.util.ArrayList;

import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;
import me.anwarshahriar.calligrapher.Calligrapher;
import seif.example.com.jahhizly.BranchLocationActivity;
import seif.example.com.jahhizly.Branches.MakeOrder.Branches;
import seif.example.com.jahhizly.Login.LoginActivity;
import seif.example.com.jahhizly.Menus.MenusMVP;
import seif.example.com.jahhizly.Menus.MenusModel;
import seif.example.com.jahhizly.Menus.MenusPresenter;
import seif.example.com.jahhizly.Other.CircleTransform;
import seif.example.com.jahhizly.R;
import seif.example.com.jahhizly.Utils.MySingleton;
import seif.example.com.jahhizly.Utils.URLs;

/**
 * Created by seif on 10/24/2017.
 */
public class ReviewsFragment extends Fragment implements MenusMVP.viewRev {
    private View view;

    public ReviewsFragment() {
    }

    private Branches branch;
    private ViewPager viewPager;
    private MyViewPagerAdapter myViewPagerAdapter;
    private LinearLayout dotsLayout;
    private TextView[] dots;
    private int[] layouts;
    private RecyclerView reviewRecView;
    private ReviewsAdapter adapter;
    RecyclerView.LayoutManager layout;
    MenusMVP.presenterRev presenter;
    Button submitTxt;
    MKLoader loading;
    TextView emptyText;

    public void sendBranchObj(Branches branch) {
        if (branch != null) {
            this.branch = branch;
        }
    }

    @Override
    public void onResume() {
        BranchLocationActivity.time = 0;
        super.onResume();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_reviews, container, false);
        Calligrapher calligrapher = new Calligrapher(getContext());
        if (MySingleton.getmInstance(getContext()).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            calligrapher.setFont(getActivity(), "fonts/Arabic/arabic_font.ttf", true);
        } else {
            calligrapher.setFont(getActivity(), "fonts/English/Roboto_Regular.ttf", true);
        }
        design();
        loading = (MKLoader) view.findViewById(R.id.loading);
        emptyText = (TextView) view.findViewById(R.id.empty_txt);

        presenter = new MenusPresenter(new MenusModel(getContext()), getContext(), this);
        reviewRecView = (RecyclerView) view.findViewById(R.id.reviews_rec_view);
        layout = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        int screenSize = getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK;
        switch (screenSize) {
            case Configuration.SCREENLAYOUT_SIZE_XLARGE:
                reviewRecView.setLayoutManager(new GridLayoutManager(getContext(), 2, LinearLayoutManager.VERTICAL, false));
                break;
            case Configuration.SCREENLAYOUT_SIZE_LARGE:
                reviewRecView.setLayoutManager(new GridLayoutManager(getContext(), 2, LinearLayoutManager.VERTICAL, false));
                break;
            case Configuration.SCREENLAYOUT_SIZE_NORMAL:
                reviewRecView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                break;
            default:
                reviewRecView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        }
        adapter = new ReviewsAdapter();

        presenter.onReviewCreated(branch.getId());
        viewPager = (ViewPager) view.findViewById(R.id.view_pager);
        dotsLayout = (LinearLayout) view.findViewById(R.id.layoutDots);
        fillData(view);
        layouts = new int[]{
                R.layout.add_rate_slide,
                R.layout.add_comment_slide,
        };
        // adding bottom dots
        addBottomDots(0);

        myViewPagerAdapter = new MyViewPagerAdapter();
        viewPager.setAdapter(myViewPagerAdapter);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);
        submitTxt = (Button) view.findViewById(R.id.submit_txt);
        return view;
    }

    private void fillData(View view) {
        if (MySingleton.getmInstance(getContext()).isLoggedIn()) {
            if (MySingleton.getmInstance(getContext()).userData().getGender().equals("Male")) {
                Glide.with(getContext()).load(URLs.ROOT_URL + MySingleton.getmInstance(getContext()).userData().getPhoto())
                        .error(R.mipmap.sidemenu_pic_profile_man)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .bitmapTransform(new CircleTransform(getContext()))
                        .thumbnail(0.5f)
                        .into((ImageView) view.findViewById(R.id.user_review_pic));
            } else {
                Glide.with(getContext()).load(URLs.ROOT_URL + MySingleton.getmInstance(getContext()).userData().getPhoto())
                        .error(R.mipmap.sidemenu_pic_profile_woman)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .bitmapTransform(new CircleTransform(getContext()))
                        .thumbnail(0.5f)
                        .into((ImageView) view.findViewById(R.id.user_review_pic));
            }

            ((TextView) view.findViewById(R.id.user_review_name)).setText(MySingleton.getmInstance(getContext()).userData().getName());
            ((TextView) view.findViewById(R.id.user_review_phone)).setText(MySingleton.getmInstance(getContext()).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? convertDigits(MySingleton.getmInstance(getContext()).userData().getPhone()) : MySingleton.getmInstance(getContext()).userData().getPhone());
        } else {
            ((TextView) view.findViewById(R.id.user_review_name)).setVisibility(View.GONE);
            ((TextView) view.findViewById(R.id.user_review_phone)).setVisibility(View.GONE);
        }

        Glide.with(getContext()).load(URLs.ROOT_URL + branch.getBrandPhoto())
                .error(R.mipmap.restaurant_icon_menu)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .bitmapTransform(new CircleTransform(getContext()))
                .thumbnail(0.5f)
                .into((ImageView) view.findViewById(R.id.rev_branch_img));
        ImageView makeOrderType, bookTableType;
        makeOrderType = (ImageView) view.findViewById(R.id.rev_branch_type_make_order);
        bookTableType = (ImageView) view.findViewById(R.id.rev_branch_type_book_table);
        if (branch.getType() == 0) {
            makeOrderType.setVisibility(View.VISIBLE);
            bookTableType.setVisibility(View.VISIBLE);
        } else if (branch.getType() == 1) {
            makeOrderType.setVisibility(View.GONE);
            bookTableType.setVisibility(View.VISIBLE);
        } else if (branch.getType() == 2) {
            makeOrderType.setVisibility(View.VISIBLE);
            bookTableType.setVisibility(View.GONE);
        }
        ((TextView) view.findViewById(R.id.rev_branch_name)).setText(MySingleton.getmInstance(getContext()).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? convertDigits(" " + branch.getName() + " ") : " " + branch.getName() + " ");
        TextView txtRate = (TextView) view.findViewById(R.id.rev_branch_rate);
        if (branch.getRate() >= 0 && branch.getRate() <= 2) {
            txtRate.setBackground(getResources().getDrawable(R.mipmap.con_rate_restaurant));
        } else if (branch.getRate() > 2 && branch.getRate() <= 3.75) {
            txtRate.setBackground(getResources().getDrawable(R.mipmap.con_rate_restaurant_y));
        } else if (branch.getRate() > 3.75 && branch.getRate() <= 5) {
            txtRate.setBackground(getResources().getDrawable(R.mipmap.con_rate_restaurant_g));
        }
        txtRate.setText(MySingleton.getmInstance(getContext()).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? convertDigits(" " + branch.getRate() + " ") : " " + branch.getRate() + " ");
        ((TextView) view.findViewById(R.id.rev_branch_category)).setText(MySingleton.getmInstance(getContext()).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? convertDigits(" " + branch.getBrandCategory() + " ") : " " + " " + branch.getBrandCategory() + " ");
        if (branch.getOpenHour().equals("") || branch.getCloseHour().equals("")) {
            ((TextView) view.findViewById(R.id.rev_branch_open_close_time)).setVisibility(View.GONE);
        } else {
            String[] open = branch.getOpenHour().split(":");
            String[] close = branch.getCloseHour().split(":");
            ((TextView) view.findViewById(R.id.rev_branch_open_close_time)).setText(MySingleton.getmInstance(getContext()).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? convertDigits(" " + open[0] + ":" + open[1] + " - " + close[0] + ":" + close[1]) : " " + open[0] + ":" + open[1] + " - " + close[0] + ":" + close[1]);
        }
        if (branch.getDistance() == -1) {
            ((TextView) view.findViewById(R.id.rev_branch_distance)).setVisibility(View.GONE);
            ((Space) view.findViewById(R.id.space)).setVisibility(View.VISIBLE);
        } else {
            ((TextView) view.findViewById(R.id.rev_branch_distance)).setText(MySingleton.getmInstance(getContext()).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? convertDigits(" " + branch.getDistance() + " " + getString(R.string.dist)) : " " + branch.getDistance() + " " + getString(R.string.dist));
        }
    }


    private void addBottomDots(int currentPage) {
        dots = new TextView[layouts.length];

        int[] colorsActive = getResources().getIntArray(R.array.array_dot_active);
        int[] colorsInactive = getResources().getIntArray(R.array.array_dot_inactive);

        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(getContext());
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(colorsInactive[currentPage]);
            dotsLayout.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(colorsActive[currentPage]);
    }

    private int getItem(int i) {
        return viewPager.getCurrentItem() + i;
    }

    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageSelected(int position) {
            addBottomDots(position);
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        @Override
        public void onPageScrollStateChanged(int arg0) {
        }
    };

    @Override
    public void loadData(ArrayList<ReviewDetails> reviewDetails) {
        emptyText.setText("");
        adapter.setReviewDetails(reviewDetails);
        ScaleInAnimationAdapter scaleInAnimationAdapter = new ScaleInAnimationAdapter(adapter);
        scaleInAnimationAdapter.setFirstOnly(false);
        reviewRecView.setAdapter(scaleInAnimationAdapter);
        scaleInAnimationAdapter.notifyDataSetChanged();
    }

    @Override
    public void showErrorMessage(String error) {
        Toast.makeText(getContext(), error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showProgress() {
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        loading.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        if (getContext() != null) {
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            loading.setVisibility(View.GONE);
        }
    }

    @Override
    public void goLogin() {
        Intent intent = new Intent(getContext(), LoginActivity.class);
        intent.putExtra("LoginIntent", "FromReview");
        intent.putExtra("ReviewBranchObject", branch);
        startActivity(intent);
        getActivity().finish();
        if (MySingleton.getmInstance(getContext()).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            getActivity().overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        } else {
            getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }
    }

    @Override
    public void showEmptyText(String message) {
        emptyText.setText(message);

    }

    @Override
    public void clearAdapter() {
        adapter.clear();
    }

    /**
     * View pager adapter
     */
    String comment = "";
    double userRate;

    public class MyViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter() {
        }

        EditText editText;

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(layouts[position], container, false);
            if (position == 1) {
                editText = (EditText) view.findViewById(R.id.add_comment_edit_txt);
                editText.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                    }

                    @Override
                    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    }

                    @Override
                    public void afterTextChanged(Editable editable) {
                        comment = editable.toString().trim();
                        Log.e("data", String.valueOf(comment));
                    }
                });
            } else if (position == 0) {
                final SimpleRatingBar rate = (SimpleRatingBar) view.findViewById(R.id.rating_bar);
                rate.setOnRatingBarChangeListener(new SimpleRatingBar.OnRatingBarChangeListener() {
                    @Override
                    public void onRatingChanged(SimpleRatingBar simpleRatingBar, float rating, boolean fromUser) {
                        if (rating >= 0 && rating <= 2) {
                            rate.setFillColor(getResources().getColor(R.color.red_3));
                            rate.setPressedBorderColor(getResources().getColor(R.color.red_3));
                            rate.setPressedFillColor(getResources().getColor(R.color.red_3));
                            rate.setBorderColor(getResources().getColor(R.color.red_3));

                        } else if (rating > 2 && rating <= 3.75) {
                            rate.setFillColor(getResources().getColor(R.color.yellow));
                            rate.setPressedBorderColor(getResources().getColor(R.color.yellow));
                            rate.setPressedFillColor(getResources().getColor(R.color.yellow));
                            rate.setBorderColor(getResources().getColor(R.color.yellow));
                        } else if (rating > 3.75 && rating <= 5) {
                            rate.setFillColor(getResources().getColor(R.color.green_1));
                            rate.setPressedBorderColor(getResources().getColor(R.color.green_1));
                            rate.setPressedFillColor(getResources().getColor(R.color.green_1));
                            rate.setBorderColor(getResources().getColor(R.color.green_1));
                        }
                        int current = getItem(+1);
                        if (current < layouts.length) {
                            viewPager.setCurrentItem(current);
                        }
                        userRate = rating;
                    }
                });
                Log.e("user rate", "" + userRate);
            }
            submitTxt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

//                    int current = getItem(+1);
//                    if (current < layouts.length) {
//                        viewPager.setCurrentItem(1);
//                    } else {
                    presenter.onSubmitReviewClicked(branch.getId(), (float) userRate, true, comment);
                    editText.setText("");
//                    }
                }
            });
            container.addView(view);
            return view;
        }

        @Override
        public int getCount() {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }


    private void design() {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        view.setLayoutParams(params);
    }

    private String convertDigits(String string) {
        if (string==null) return null;
        String rslt = "";
        for (int i = 0; i < string.length(); i++) {
            switch (string.charAt(i)) {
                case '0':
                    rslt += "٠";
                    break;
                case '1':
                    rslt += "١";
                    break;
                case '2':
                    rslt += "٢";
                    break;
                case '3':
                    rslt += "٣";
                    break;
                case '4':
                    rslt += "٤";
                    break;
                case '5':
                    rslt += "٥";
                    break;
                case '6':
                    rslt += "٦";
                    break;
                case '7':
                    rslt += "٧";
                    break;
                case '8':
                    rslt += "٨";
                    break;
                case '9':
                    rslt += "٩";
                    break;
                default:
                    rslt += string.charAt(i);
            }
        }
        return rslt;
    }
}
