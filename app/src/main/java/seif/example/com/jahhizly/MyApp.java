package seif.example.com.jahhizly;

import android.app.Application;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.util.Base64;
import android.util.Log;

import net.gotev.uploadservice.UploadService;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;

/**
 * Created by sief on 5/1/2018.
 */

public class MyApp extends Application {
    private static MyApp sApplication;

    @Override
    public void onCreate() {
        super.onCreate();
        printHash();
        UploadService.NAMESPACE = "seif.example.com.jahhizly";

    }

    public static MyApp getApplication() {
        return sApplication;
    }

    public void setLocale(Locale locale, String lang) {
        Configuration config = getBaseContext().getResources()
                .getConfiguration();
        Locale.setDefault(locale);
        config.setLocale(locale);
        config.setLayoutDirection(locale);
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
    }

    public void printHash() {

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "seif.example.com.jahhizly",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash: ", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }

        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

    }
}
