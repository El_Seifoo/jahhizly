package seif.example.com.jahhizly.Profile;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.LayoutDirection;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import de.hdodenhof.circleimageview.CircleImageView;
import me.anwarshahriar.calligrapher.Calligrapher;
import seif.example.com.jahhizly.Cart.MyCartActivity;
import seif.example.com.jahhizly.ChangePassword.ChangePasswordActivity;
import seif.example.com.jahhizly.EditUser.EditUserActivity;
import seif.example.com.jahhizly.Login.UserInfo;
import seif.example.com.jahhizly.Other.CircleTransform;
import seif.example.com.jahhizly.R;
import seif.example.com.jahhizly.Utils.MySingleton;
import seif.example.com.jahhizly.Utils.URLs;


public class MyAccountActivity extends AppCompatActivity implements ProfileMVP.view {

    ProfilePresenter presenter;
    TextView username, email, phone, dob, city;
    CircleImageView profilePic;
    Button edtData, chngPassword;


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_account);
        Calligrapher calligrapher = new Calligrapher(this);
        if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            calligrapher.setFont(this, "fonts/Arabic/arabic_font.ttf", true);
            ((LinearLayout) findViewById(R.id.phone_container)).setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        } else {
            calligrapher.setFont(this, "fonts/English/Roboto_Regular.ttf", true);
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        profilePic = (CircleImageView) findViewById(R.id.profile_pic);
        edtData = (Button) findViewById(R.id.edit_data_btn);
        chngPassword = (Button) findViewById(R.id.chng_pw_btn);
        username = (TextView) findViewById(R.id.profile_name);
        if (getResources().getConfiguration().getLayoutDirection() == LayoutDirection.RTL) {
            username.setTextDirection(View.TEXT_DIRECTION_RTL);
        }
        email = (TextView) findViewById(R.id.profile_email);
        phone = (TextView) findViewById(R.id.profile_phone);
        dob = (TextView) findViewById(R.id.profile_dob);
        city = (TextView) findViewById(R.id.profile_city);
        presenter = new ProfilePresenter(this, this, new ProfileModel(this));
        presenter.onProfileCreated();

        edtData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onEditDataClicked();
            }
        });

        chngPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onChngPasswordClicked();
            }
        });

    }


    @Override
    public void setUserInfo(UserInfo userInfo) {
        username.setText(" " + userInfo.getName() + " ");
        email.setText(" " + userInfo.getEmail() + " ");
        phone.setText(MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? convertDigits(userInfo.getPhone() + " ") : " " + userInfo.getPhone() + " ");
        if (userInfo.getBirthDate().contains("T")) {
            String[] dateObirth = userInfo.getBirthDate().split("T")[0].split("-");
            dob.setText(MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value)) ? convertDigits(dateObirth[2] + " - " + dateObirth[1] + " - " + dateObirth[0]) : dateObirth[2] + " - " + dateObirth[1] + " - " + dateObirth[0]);
        } else {
            dob.setText(MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))? convertDigits(" " + userInfo.getBirthDate() + " "):" " + userInfo.getBirthDate() + " ");
        }
        city.setText(" " + userInfo.getCity() + " ");
        if (userInfo.getGender().equals("Male")) {
            Glide.with(this).load(URLs.ROOT_URL + userInfo.getPhoto())
                    .error(R.mipmap.sidemenu_pic_profile_man)
                    .thumbnail(0.5f)
                    .transform(new CircleTransform(this))
                    .into(profilePic);
        } else {
            Glide.with(this).load(URLs.ROOT_URL + userInfo.getPhoto())
                    .error(R.mipmap.sidemenu_pic_profile_woman)
                    .thumbnail(0.5f)
                    .transform(new CircleTransform(this))
                    .into(profilePic);
        }


    }

    @Override
    public void showErrorMessage(String error) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void goEditData(UserInfo userInfo) {
        Intent intent = new Intent(this, EditUserActivity.class);
        intent.putExtra("UserInfo", userInfo);
        startActivity(intent);
        if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        } else {
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }
    }

    @Override
    public void goChngPassword(String token) {
        Intent intent = new Intent(this, ChangePasswordActivity.class);
        startActivity(intent);
        if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        } else {
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onProfileCreated();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            } else {
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private String convertDigits(String string) {
        String rslt = "";
        for (int i = 0; i < string.length(); i++) {
            switch (string.charAt(i)) {
                case '0':
                    rslt += "٠";
                    break;
                case '1':
                    rslt += "١";
                    break;
                case '2':
                    rslt += "٢";
                    break;
                case '3':
                    rslt += "٣";
                    break;
                case '4':
                    rslt += "٤";
                    break;
                case '5':
                    rslt += "٥";
                    break;
                case '6':
                    rslt += "٦";
                    break;
                case '7':
                    rslt += "٧";
                    break;
                case '8':
                    rslt += "٨";
                    break;
                case '9':
                    rslt += "٩";
                    break;
                default:
                    rslt += string.charAt(i);
            }
        }
        return rslt;
    }

}
