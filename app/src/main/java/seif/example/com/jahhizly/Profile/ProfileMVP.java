package seif.example.com.jahhizly.Profile;

import seif.example.com.jahhizly.Login.UserInfo;

/**
 * Created by seif on 10/30/2017.
 */

public interface ProfileMVP {
    interface view {
        void setUserInfo(UserInfo userInfo);
        void showErrorMessage(String error);
        void goEditData(UserInfo userInfo);
        void goChngPassword(String token);
    }

    interface presenter {
        void onProfileCreated();
        void onEditDataClicked();
        void onChngPasswordClicked();
    }
}
