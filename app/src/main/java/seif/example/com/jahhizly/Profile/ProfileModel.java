package seif.example.com.jahhizly.Profile;

import android.content.Context;

import seif.example.com.jahhizly.Login.UserInfo;
import seif.example.com.jahhizly.Utils.MySingleton;

/**
 * Created by seif on 10/30/2017.
 */
public class ProfileModel {
    Context context ;

    public ProfileModel(Context context) {
        this.context = context;
    }

    protected UserInfo getUserData(){
        return MySingleton.getmInstance(context).userData();
    }

    protected String getUserToke(){
        return MySingleton.getmInstance(context).UserKey();
    }
}
