package seif.example.com.jahhizly.Profile;

import android.content.Context;

import seif.example.com.jahhizly.Login.UserInfo;
import seif.example.com.jahhizly.R;

/**
 * Created by seif on 10/30/2017.
 */
public class ProfilePresenter implements ProfileMVP.presenter {

    Context context ;
    ProfileMVP.view view ;
    ProfileModel model ;

    public ProfilePresenter(Context context, ProfileMVP.view view, ProfileModel model) {
        this.context = context;
        this.view = view;
        this.model = model;
    }



    @Override
    public void onProfileCreated() {
        if (model.getUserData() != null){
            view.setUserInfo(model.getUserData());
        }else {
            view.showErrorMessage(context.getString(R.string.wrong));
        }
    }

    @Override
    public void onEditDataClicked() {
        if (model.getUserData() != null){
            view.goEditData(model.getUserData());
        }else {
            view.showErrorMessage(context.getString(R.string.wrong));
        }

    }

    @Override
    public void onChngPasswordClicked() {
        if (model.getUserToke() != null){
            view.goChngPassword(model.getUserToke());
        }else {
            view.showErrorMessage(context.getString(R.string.wrong));
        }
    }
}
