package seif.example.com.jahhizly.SignUp;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.LayoutDirection;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.tuyenmonkey.mkloader.MKLoader;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.hdodenhof.circleimageview.CircleImageView;
import me.anwarshahriar.calligrapher.Calligrapher;
import seif.example.com.jahhizly.Login.LoginActivity;
import seif.example.com.jahhizly.Login.UserInfo;
import seif.example.com.jahhizly.Other.CircleTransform;
import seif.example.com.jahhizly.Other.FixedHoloDatePickerDialog;
import seif.example.com.jahhizly.R;
import seif.example.com.jahhizly.Utils.MySingleton;
import seif.example.com.jahhizly.Verification.VerificationActivity;

public class SignUpActivity extends AppCompatActivity implements SignUpMVP.view, DatePickerDialog.OnDateSetListener {

    EditText name, email, phone, password, birthday;
    AutoCompleteTextView city;
    Button regBtn;
    RadioGroup radioGroup;
    RadioButton gRBtn;
    SignUpPresenter presenter;
    Calendar myCalendar;
    DatePickerDialog date;
    MKLoader loading;
    FrameLayout container;
    CircleImageView profilePic, selectPic;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        Calligrapher calligrapher = new Calligrapher(this);
        if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            calligrapher.setFont(this, "fonts/Arabic/arabic_font.ttf", true);
            ((LinearLayout) findViewById(R.id.phone_container)).setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        } else {
            calligrapher.setFont(this, "fonts/English/Roboto_Regular.ttf", true);
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        profilePic = (CircleImageView) findViewById(R.id.sign_up_prof_pic);
        selectPic = (CircleImageView) findViewById(R.id.sign_up_choose_prof_pic);
        presenter = new SignUpPresenter(this, new SignUpModel(this), this);
        loading = (MKLoader) findViewById(R.id.loading);
        container = (FrameLayout) findViewById(R.id.container);
        name = (EditText) findViewById(R.id.reg_name_edt_txt);
        email = (EditText) findViewById(R.id.reg_email_edt_txt);
        phone = (EditText) findViewById(R.id.reg_phone_edt_txt);
        password = (EditText) findViewById(R.id.reg_pw_edt_txt);
        password.setTextLocale(Locale.ENGLISH);
        birthday = (EditText) findViewById(R.id.reg_dob_edt_txt);
        birthday.setTextLocale(Locale.ENGLISH);
        city = (AutoCompleteTextView) findViewById(R.id.reg_city_edt_txt);
        String[] cities = getResources().getStringArray(R.array.cities);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, cities);
        city.setAdapter(adapter);
        city.setThreshold(1);
        regBtn = (Button) findViewById(R.id.reg_btn);

        if (getResources().getConfiguration().getLayoutDirection() == LayoutDirection.RTL) {
            name.setGravity(Gravity.RIGHT);
            email.setGravity(Gravity.RIGHT);
            phone.setGravity(Gravity.RIGHT);
            password.setGravity(Gravity.RIGHT);
            birthday.setGravity(Gravity.RIGHT);
            city.setGravity(Gravity.RIGHT);
            ((RadioButton) findViewById(R.id.male_rd_btn)).setGravity(Gravity.RIGHT);
            ((RadioButton) findViewById(R.id.female_rd_btn)).setGravity(Gravity.RIGHT);
        }
        radioGroup = (RadioGroup) findViewById(R.id.gender_rd_group);
        selectPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectProfilePic();
            }
        });
        regBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (bitmap != null) {
                    presenter.onRegBtnClicked(getPath(filePath), convertDigits(getName()), convertDigits(getEmail()), convertDigits(getPhone()),
                            convertDigits(getPassword()), convertDigits(getGender()), convertDigits(getBirthDay()), convertDigits(getCity()),
                            getIntent().hasExtra("LoginViaFB") ? "1" : getIntent().hasExtra("LoginViaTwt") ? "2" : "0",
                            getIntent().hasExtra("LoginViaFB") ? ((UserInfo) getIntent().getExtras().getSerializable("LoginViaFB")).getSocialId() : getIntent().hasExtra("LoginViaTwt") ? ((UserInfo) getIntent().getExtras().getSerializable("LoginViaTwt")).getSocialId() : "");
                } else {
                    presenter.onRegBtnClicked(convertDigits(getName()), convertDigits(getEmail()), convertDigits(getPhone()),
                            convertDigits(getPassword()), convertDigits(getGender()), convertDigits(getBirthDay()), convertDigits(getCity()),
                            getIntent().hasExtra("LoginViaFB") ? "1" : getIntent().hasExtra("LoginViaTwt") ? "2" : "0",
                            getIntent().hasExtra("LoginViaFB") ? ((UserInfo) getIntent().getExtras().getSerializable("LoginViaFB")).getSocialId() : getIntent().hasExtra("LoginViaTwt") ? ((UserInfo) getIntent().getExtras().getSerializable("LoginViaTwt")).getSocialId() : "");
                }
            }
        });

        myCalendar = Calendar.getInstance();

        birthday.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                showDatePicker();

            }
        });

        if (getIntent().hasExtra("LoginViaFB")) {
            Toast.makeText(this, getString(R.string.complete), Toast.LENGTH_LONG).show();
            UserInfo userInfo = (UserInfo) getIntent().getSerializableExtra("LoginViaFB");
            if (!userInfo.getName().equals("")) {
                name.setText(userInfo.getName());
                name.setFocusable(false);
                name.setClickable(false);
            }
            if (!userInfo.getEmail().equals("")) {
                email.setText(userInfo.getEmail());
                email.setFocusable(false);
                email.setClickable(false);
            }
            if (!userInfo.getBirthDate().equals("")) {
                birthday.setText(userInfo.getBirthDate());
            }
            if (!userInfo.getGender().equals("")) {
                RadioButton male = (RadioButton) radioGroup.getChildAt(0);
                RadioButton female = (RadioButton) radioGroup.getChildAt(1);
                if (userInfo.getGender().trim().toLowerCase().equals("male")) {
                    male.setChecked(true);
                } else {
                    female.setChecked(true);
                }
            }

        } else if (getIntent().hasExtra("LoginViaTwt")) {
            Toast.makeText(this, getString(R.string.complete), Toast.LENGTH_LONG).show();
            if (!((UserInfo) getIntent().getSerializableExtra("LoginViaTwt")).getName().equals("")) {
                name.setText(((UserInfo) getIntent().getSerializableExtra("LoginViaTwt")).getName());
                name.setFocusable(false);
                name.setClickable(false);
            }
            if (!((UserInfo) getIntent().getSerializableExtra("LoginViaTwt")).getEmail().equals("")) {
                email.setText(((UserInfo) getIntent().getSerializableExtra("LoginViaTwt")).getEmail());
                email.setFocusable(false);
                email.setClickable(false);
            }
        }
    }

    private final static int SELECT_IMG_REQUEST = 1010;

    private void selectProfilePic() {
        ActivityCompat.requestPermissions(
                SignUpActivity.this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                SELECT_IMG_REQUEST
        );
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == SELECT_IMG_REQUEST) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, "Select Image"), SELECT_IMG_REQUEST);
            } else {
                Toast.makeText(getApplicationContext(), "permission", Toast.LENGTH_LONG).show();
            }
            return;
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    Bitmap bitmap;
    Uri filePath;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SELECT_IMG_REQUEST && resultCode == RESULT_OK && data != null) {
            filePath = data.getData();
            Log.e("path", filePath + "");
            try {
                InputStream inputStream = getContentResolver().openInputStream(filePath);
                bitmap = BitmapFactory.decodeStream(inputStream);
                Glide.with(this)
                        .load(bitmapToByte(bitmap))
                        .asBitmap()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .error(R.mipmap.login_pic)
                        .transform(new CircleTransform(this))
                        .thumbnail(0.1f)
                        .into(profilePic);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private byte[] bitmapToByte(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

    public String getPath(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
        cursor.close();

        cursor = getContentResolver().query(
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();

        return path;
    }

    private void updateLabel() {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        String DOB = sdf.format(myCalendar.getTime());
        birthday.setText(DOB);
    }

    @Override
    public String getName() {
        return name.getText().toString().trim();
    }

    @Override
    public String getEmail() {
        Log.e("emaillllll", email.getText().toString().trim());
        return email.getText().toString().trim();
    }

    @Override
    public String getPhone() {
        Log.e("phone 1", phone.getText().toString().trim());
        return phone.getText().toString().trim();
    }

    @Override
    public String getPassword() {
        return password.getText().toString().trim();
    }


    @Override
    public String getGender() {
        int selected = radioGroup.getCheckedRadioButtonId();
        gRBtn = (RadioButton) findViewById(selected);
        if (gRBtn == null) {
            return null;
        }
        return String.valueOf(gRBtn.getText());
    }

    @Override
    public String getBirthDay() {
        return birthday.getText().toString().trim();
    }

    @Override
    public String getCity() {
        return city.getText().toString().trim();
    }

    @Override
    public boolean checkMailValidation(String email) {
        String regExpn =
                "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                        + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                        + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";
        CharSequence inputStr = email;
        Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        if (matcher.matches())
            return true;
        else
            return false;
    }

    @Override
    public void showDatePicker() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2222, Calendar.DECEMBER, 29, calendar.getMaximum(Calendar.HOUR_OF_DAY), calendar.getMaximum(Calendar.MINUTE), calendar.getMaximum(Calendar.SECOND));
        Calendar calendar1 = Calendar.getInstance();
        calendar1.set(1970, Calendar.DECEMBER, 29, calendar.getMinimum(Calendar.HOUR_OF_DAY), calendar.getMinimum(Calendar.MINUTE), calendar.getMinimum(Calendar.SECOND));
        date = new FixedHoloDatePickerDialog(
                new ContextThemeWrapper(
                        SignUpActivity.this,
                        R.style.DatePickerDialogStyle),
                this,
                myCalendar.get(Calendar.YEAR),
                myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH)
        );
        date.getDatePicker().setMaxDate(calendar.getTimeInMillis());
        date.getDatePicker().setMinDate(calendar1.getTimeInMillis());
        date.show();
    }

    @Override
    public void clearPhoneAndPw() {
        phone.setText("");
        password.setText("");
    }

    @Override
    public void showErrorMessage(String errorMessage) {
        Toast.makeText(SignUpActivity.this, errorMessage, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showProgress() {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        loading.setVisibility(View.VISIBLE);
        container.setForeground(new ColorDrawable(Color.parseColor("#66000000")));
    }

    @Override
    public void hideProgress() {
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        loading.setVisibility(View.GONE);
        container.setForeground(new ColorDrawable(Color.TRANSPARENT));
    }

    @Override
    public void goVerification(String phone) {
        Intent intent = new Intent(this, VerificationActivity.class);
        intent.putExtra("Verification", phone);
        startActivity(intent);
        if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        } else {
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            } else {
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private String convertDigits(String string) {
        if (string == null) return null;
        String rslt = "";
        for (int i = 0; i < string.length(); i++) {
            switch (string.charAt(i)) {
                case '٠':
                    rslt += "0";
                    break;
                case '١':
                    rslt += "1";
                    break;
                case '٢':
                    rslt += "2";
                    break;
                case '٣':
                    rslt += "3";
                    break;
                case '٤':
                    rslt += "4";
                    break;
                case '٥':
                    rslt += "5";
                    break;
                case '٦':
                    rslt += "6";
                    break;
                case '٧':
                    rslt += "7";
                    break;
                case '٨':
                    rslt += "8";
                    break;
                case '٩':
                    rslt += "9";
                    break;
                default:
                    rslt += string.charAt(i);
            }
        }
        return rslt;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        myCalendar.set(Calendar.YEAR, year);
        myCalendar.set(Calendar.MONTH, month);
        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        updateLabel();
    }
}
