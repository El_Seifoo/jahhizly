package seif.example.com.jahhizly.SignUp;

/**
 * Created by seif on 10/21/2017.
 */
public interface SignUpMVP {

    interface view {
        String getName();

        String getEmail();

        String getPhone();

        String getPassword();

        String getGender();

        String getBirthDay();

        String getCity();

        boolean checkMailValidation(String email);

        void showDatePicker();

        void clearPhoneAndPw();

        void showErrorMessage(String errorMessage);

        void showProgress();

        void hideProgress();

        void goVerification(String phone);
    }

    interface presenter {
        void onRegBtnClicked(String name, String email, String phone, String password, String Gender, String BirthDay, String city, String loginType, String socialId);

        void onRegBtnClicked(String photo, String name, String email, String phone, String password, String Gender, String BirthDay, String city, String loginType, String socialId);

        void onDobClicked();
    }
}
