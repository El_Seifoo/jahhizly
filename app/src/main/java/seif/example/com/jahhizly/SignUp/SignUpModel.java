package seif.example.com.jahhizly.SignUp;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.ServerResponse;
import net.gotev.uploadservice.UploadInfo;
import net.gotev.uploadservice.UploadNotificationConfig;
import net.gotev.uploadservice.UploadService;
import net.gotev.uploadservice.UploadStatusDelegate;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import seif.example.com.jahhizly.BuildConfig;
import seif.example.com.jahhizly.Login.UserInfo;
import seif.example.com.jahhizly.R;
import seif.example.com.jahhizly.Utils.MySingleton;
import seif.example.com.jahhizly.Utils.URLs;

/**
 * Created by seif on 10/21/2017.
 */
public class SignUpModel {
    private Context context;

    public SignUpModel(Context context) {
        this.context = context;
    }

    protected void signUp(final VolleyCallback callback, final UserInfo userInfo) {
        if (userInfo.getGender().equals(context.getString(R.string.male))) {
            userInfo.setGender("0");
        } else {
            userInfo.setGender("1");
        }
        Map<String, String> params = new HashMap<String, String>();
        params.put("name", userInfo.getName());
        params.put("email", userInfo.getEmail());
        params.put("password", userInfo.getPassword());
        params.put("phone", (context.getString(R.string.phone_code) + userInfo.getPhone()).trim());
        Log.e("phone 2", (context.getString(R.string.phone_code) + userInfo.getPhone()).trim());
        params.put("gender", userInfo.getGender());
        params.put("city", userInfo.getCity());
        params.put("birthday", userInfo.getBirthDate());
        params.put("login_type", userInfo.getLoginType());
        params.put("social_id", userInfo.getSocialId());
        Log.e("params", params.toString());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                URLs.REGISTER,
                new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            callback.onSuccess(response, userInfo.getPhone());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.onFail(error);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MySingleton.getmInstance(context).addToRQ(jsonObjectRequest);

    }

    protected void signUpWithPhoto(final VolleyCallback callback, final UserInfo userInfo) {
        Log.e("imgaaaaaaaaat", userInfo.getPhoto());
        if (userInfo.getGender().equals(context.getString(R.string.male))) {
            userInfo.setGender("0");
        } else {
            userInfo.setGender("1");
        }
        final String uploadID = UUID.randomUUID().toString();
        try {
            UploadService.NAMESPACE = BuildConfig.APPLICATION_ID;
            String token = "Bearer " + MySingleton.getmInstance(context).UserKey();
            new MultipartUploadRequest(context, uploadID, URLs.REGISTER_PHOTO)
                    .addFileToUpload(userInfo.getPhoto(), "file")
                    .addHeader("Authorization", token)
                    .addParameter("name", userInfo.getName())
                    .addParameter("email", userInfo.getEmail())
                    .addParameter("password", userInfo.getPassword())
                    .addParameter("phone", userInfo.getPhone())
                    .addParameter("gender", userInfo.getGender())
                    .addParameter("city", userInfo.getCity())
                    .addParameter("birthday", userInfo.getBirthDate())
                    .setNotificationConfig(new UploadNotificationConfig())
                    .setMaxRetries(1)
                    .setDelegate(new UploadStatusDelegate() {
                        @Override
                        public void onProgress(UploadInfo uploadInfo) {
                            Log.e("progress", uploadInfo.getProgressPercent() + "");
                        }

                        @Override
                        public void onError(UploadInfo uploadInfo, Exception exception) {
                            callback.onUploadFailed();
                        }

                        @Override
                        public void onCompleted(UploadInfo uploadInfo, ServerResponse serverResponse) {
                            Log.e("serverResponse", serverResponse.getBodyAsString());
                            try {
                                callback.onSuccess(new JSONObject(serverResponse.getBodyAsString()), userInfo.getPhone());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onCancelled(UploadInfo uploadInfo) {

                        }
                    })
                    .startUpload();
        } catch (Exception e) {
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }


    protected interface VolleyCallback {
        void onSuccess(JSONObject response, String phone) throws JSONException;

        void onFail(VolleyError error);

        void onUploadFailed();
    }
}
