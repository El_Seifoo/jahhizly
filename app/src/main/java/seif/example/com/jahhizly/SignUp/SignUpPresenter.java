package seif.example.com.jahhizly.SignUp;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import seif.example.com.jahhizly.Login.UserInfo;
import seif.example.com.jahhizly.R;

/**
 * Created by seif on 10/21/2017.
 */
public class SignUpPresenter implements SignUpMVP.presenter, SignUpModel.VolleyCallback {
    SignUpMVP.view view;
    SignUpModel model;
    Context context;
    SignUpModel.VolleyCallback callback;

    public SignUpPresenter(SignUpMVP.view view, SignUpModel model, Context context) {
        this.view = view;
        this.model = model;
        this.context = context;
        callback = this;
    }

    @Override
    public void onRegBtnClicked(final String name, final String email, final String phone, final String password, final String gender, final String birthDay, final String city, String loginType, String socialId) {
        Log.e("email", email);
        if (name.equals("") || email.equals("") || phone.equals("") ||
                password.equals("") || gender.equals("") || birthDay.equals("") || city.equals("")) {
            view.showErrorMessage(context.getString(R.string.error_1));
            return;
        }
        if (!view.checkMailValidation(email)) {
            view.showErrorMessage(context.getString(R.string.check_mail));
            view.clearPhoneAndPw();
            return;
        }
        ///////////////////////////
        //  Sign Up API At Model //
        ///////////////////////////
        view.showProgress();
        model.signUp(callback, new UserInfo(name, email, phone, birthDay, city, gender, password, loginType, socialId));
    }

    @Override
    public void onRegBtnClicked(String photo, String name, String email, String phone, String password, String gender, String birthDay, String city, String loginType, String socialId) {

        if (name.equals("") || email.equals("") || phone.equals("") ||
                password.equals("") || gender.equals("") || birthDay.equals("") || city.equals("")) {
            view.showErrorMessage(context.getString(R.string.error_1));
            return;
        }
        if (!view.checkMailValidation(email)) {
            view.showErrorMessage(context.getString(R.string.check_mail));
            view.clearPhoneAndPw();
            return;
        }
        ///////////////////////////
        //  Sign Up API At Model //
        ///////////////////////////
        view.showProgress();
        model.signUpWithPhoto(callback, new UserInfo(photo, name, email, phone, birthDay, city, gender, password, loginType, socialId));
    }

    @Override
    public void onDobClicked() {
        view.showDatePicker();
    }

    @Override
    public void onSuccess(JSONObject response, String phone) throws JSONException {
        Log.e("resopnse", response.toString());
        JSONObject status = response.getJSONObject("status");
        String stats = status.getString("status");
        if (stats.equals("error")) {
            Toast.makeText(context, status.getString("desc"), Toast.LENGTH_LONG).show();
            view.hideProgress();
            view.clearPhoneAndPw();
        } else {
            view.hideProgress();
            view.goVerification(phone);
        }
    }

    @Override
    public void onFail(VolleyError error) {
        view.hideProgress();
        if (error instanceof NoConnectionError || error instanceof TimeoutError) {
            Toast.makeText(context, context.getString(R.string.internet_error), Toast.LENGTH_LONG).show();
        } else if (error instanceof ServerError) {
            Toast.makeText(context, context.getString(R.string.server_error), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(context, context.getString(R.string.wrong), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onUploadFailed() {
        view.hideProgress();
        view.showErrorMessage(context.getString(R.string.error_5));
    }
}
