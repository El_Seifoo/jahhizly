package seif.example.com.jahhizly.Splash;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.VideoView;

import java.io.IOException;

import seif.example.com.jahhizly.Branches.BranchesActivity;
import seif.example.com.jahhizly.Login.LoginActivity;
import seif.example.com.jahhizly.Menus.MenuItem.CartDbHelper;
import seif.example.com.jahhizly.R;
import seif.example.com.jahhizly.Utils.MySingleton;

public class Splash_Screen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash__screen);
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(50);
//        final MediaPlayer mediaPlayer = MediaPlayer.create(this, R.raw.splash_sound);
//        mediaPlayer.start();
        new Thread() {
            public void run() {
                try {
                    sleep(3900);
                    loadMain();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    loadMain();
                }
            }
        }.start();
    }


    protected void loadMain() {
        Intent intent = new Intent(this, BranchesActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
}