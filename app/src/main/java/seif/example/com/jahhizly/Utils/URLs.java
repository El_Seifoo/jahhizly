package seif.example.com.jahhizly.Utils;

/**
 * Created by seif on 8/13/2017.
 */
public class URLs {
    public static final String ROOT_URL = "http://jahezzli.com/api/";
    public static final String REGISTER = ROOT_URL + "shopper_signup";
    public static final String REGISTER_PHOTO = ROOT_URL + "shopper_signup_photo";
    public static final String LOGIN = ROOT_URL + "shopper_login";
    public static final String EDIT_USER = ROOT_URL + "edit_shopper";
    public static final String ADD_PROF_PIC = ROOT_URL + "upload_user_photo";
    public static final String CHANGE_PASSWORD = ROOT_URL + "shopper_new_password";
    public static final String PROFILE = ROOT_URL + "profile";
    public static final String ALL_CATEGORIES = ROOT_URL + "categories";
    public static final String BRANCH_MENU = ROOT_URL + "shopper_menus/";
    public static final String MENU_ITEM_DETAILS = ROOT_URL + "shopper_menu/";
    public static final String MENU_CATEGORIES = ROOT_URL + "shopper_menu_categories/";
    public static final String MAKE_ORDER = ROOT_URL + "shopper_create_order";
    public static final String PREVIOUS_ORDER = ROOT_URL + "shopper_previous_orders";
    public static final String PREVIOUS_ORDER_ITEM = ROOT_URL + "shopper_previous_order/";
    public static final String FAVORITE_MENUS = ROOT_URL + "shopper_favorite_menus";
    public static final String FAVORITE_BRANCHES = ROOT_URL + "shopper_favorite_branchs/";
    public static final String USERS_REVIEWS = ROOT_URL + "shopper_branch_reviews/";
    public static final String ADD_USER_REVIEW = ROOT_URL + "shopper_create_review";
    public static final String ADD_FAV_BRANCH = ROOT_URL + "shopper_create_favorite_branch";
    public static final String REMOVE_FAV_BRANCH = ROOT_URL + "shopper_delete_favorite_branch";
    public static final String ADD_FAV_MENU = ROOT_URL + "shopper_create_favorite_menu";
    public static final String REMOVE_FAV_MENU = ROOT_URL + "shopper_delete_favorite_menu";
    public static final String BRANCH_TABLES_NUMS = ROOT_URL + "shopper_branch_tables/";
    public static final String RESERVATION = ROOT_URL + "shopper_table_reservation";
    public static final String UN_RATED = ROOT_URL + "shopper_unreviewed_branches";
    public static final String SET_DEV_IP = ROOT_URL + "shopper_set_device_id";
    public static final String USER_VERIFICATION = ROOT_URL + "shopper_send_code";
    public static final String USER_SEND_VERIFICATION_CODE = ROOT_URL + "shopper_sms_verefiction";
    public static final String RESET_PASSWORD = ROOT_URL + "shopper_reset_password";
    public static final String BRANCHES = ROOT_URL + "shopper_branches/";
    public static final String SHOPPER_BRANCHES = ROOT_URL + "shopper_branches";
    public static final String TABLES = ROOT_URL + "shopper_tables";
    public static final String SOCIAL_LOGIN = ROOT_URL + "social_login";
}
