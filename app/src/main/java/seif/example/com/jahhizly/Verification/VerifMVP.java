package seif.example.com.jahhizly.Verification;

/**
 * Created by seif on 11/16/2017.
 */
public interface VerifMVP {
    interface view {
        void showMessage(String message);
        void showProgress();
        void hideProgress();
        void showResend();
        void goHome();
    }

    interface presenter {
        void onSendCodeClicked(String phone , String code);
        void onResendClicked(String phone);
    }

}
