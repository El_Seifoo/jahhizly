package seif.example.com.jahhizly.Verification;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.NoConnectionError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import seif.example.com.jahhizly.Login.UserInfo;
import seif.example.com.jahhizly.R;
import seif.example.com.jahhizly.Utils.MySingleton;

/**
 * Created by seif on 11/16/2017.
 */
public class VerifPresenter implements VerifMVP.presenter, VerifModel.VolleyCallback {
    Context context;
    VerifMVP.view view;
    VerifModel model;

    public VerifPresenter(Context context, VerifMVP.view view, VerifModel model) {
        this.context = context;
        this.view = view;
        this.model = model;
    }


    @Override
    public void onSendCodeClicked(String phone, String code) {
        if (code.equals("")) {
            view.showMessage(context.getString(R.string.error_1));
            return;
        }
        model.sendCode(this, phone, code);
    }

    @Override
    public void onResendClicked(String phone) {
        model.resendCode(this, phone);
    }

    @Override
    public void onSuccess(JSONObject response, int which) throws JSONException {
        view.hideProgress();
        if (which == 1) {
            Log.e("ssss", response.toString());
//            JSONObject jsonResponse = new JSONObject(response);
            if (response.has("user")) {
                String token = response.getString("token");
                Log.e("token ", "ssssssss");
                MySingleton.getmInstance(context).saveUser(token);
                JSONObject user = response.getJSONObject("user");
                int id = user.getInt("id");
                String name;
                if (user.getString("name").equals("") || user.getString("name").equals("null")) {
                    name = "No data";
                } else {
                    name = user.getString("name");
                }
                String email;
                if (user.getString("email").equals("") || user.getString("email").equals("null")) {
                    email = "No data";
                } else {
                    email = user.getString("email");
                }

                String phone;
                if (user.getString("phone").equals("") || user.getString("phone").equals("null")) {
                    phone = "No data";
                } else {
                    phone = user.getString("phone");
                }

                String photo;
                if (user.getString("photo").equals("") || user.getString("photo").equals("null")) {
                    photo = null;
                } else {
                    photo = user.getString("photo");
                }
                String birthDate;
                if (user.getString("birthday").equals("") || user.getString("birthday").equals("null")) {
                    birthDate = "No data";
                } else {
                    birthDate = user.getString("birthday");
                }
                String city;
                if (user.getString("city").equals("") || user.getString("city").equals("null")) {
                    city = "No data";
                } else {
                    city = user.getString("city");
                }
                String gender;
                if (user.getString("gender").equals("") || user.getString("gender").equals("null")) {
                    gender = "No data";
                } else {
                    if (user.getString("gender").equals("0")) {
                        gender = "Male";
                    } else {
                        gender = "Female";
                    }
                }
                UserInfo profileData = new UserInfo(id, name, email, phone,
                        photo, birthDate, city, gender);
                Log.e("first req", profileData.getName() + "  Seifoo");
                MySingleton.getmInstance(context).saveUserData(profileData);
                MySingleton.getmInstance(context).loginUser();
                view.goHome();
            } else {
                String stats = response.getString("status");
                if (stats.equals("error")) {
                    view.showMessage(response.getString("desc"));
                }
                view.showResend();
            }
        } else if (which == 2) {
            Log.e("Resend code response", response.toString());
        }
    }

    @Override
    public void onFail(VolleyError error, int which) {
        view.hideProgress();
        if (which == 1) {
            view.showResend();
            if (error instanceof NoConnectionError || error instanceof TimeoutError) {
                Toast.makeText(context, context.getString(R.string.internet_error), Toast.LENGTH_LONG).show();
            } else if (error instanceof ServerError) {
                Toast.makeText(context, context.getString(R.string.server_error), Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(context, context.getString(R.string.wrong), Toast.LENGTH_LONG).show();
            }
        } else if (which == 2) {
            if (error instanceof NoConnectionError || error instanceof TimeoutError) {
                Toast.makeText(context, context.getString(R.string.internet_error), Toast.LENGTH_LONG).show();
            } else if (error instanceof ServerError) {
                Toast.makeText(context, context.getString(R.string.server_error), Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(context, context.getString(R.string.wrong), Toast.LENGTH_LONG).show();
            }
        }
    }


}
