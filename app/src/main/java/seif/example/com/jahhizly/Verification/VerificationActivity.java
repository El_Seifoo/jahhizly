package seif.example.com.jahhizly.Verification;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.LayoutDirection;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

import me.anwarshahriar.calligrapher.Calligrapher;
import seif.example.com.jahhizly.Branches.BranchesActivity;
import seif.example.com.jahhizly.R;
import seif.example.com.jahhizly.Utils.MySingleton;

public class VerificationActivity extends AppCompatActivity implements VerifMVP.view {

    String phone;
    VerifPresenter presenter;
    Button sendCodeBtn;
    TextView resendTxt;
    EditText codeEdtTxt;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);
        Calligrapher calligrapher = new Calligrapher(this);
        if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            calligrapher.setFont(this, "fonts/Arabic/arabic_font.ttf", true);
        } else {
            calligrapher.setFont(this, "fonts/English/Roboto_Regular.ttf", true);
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        phone = getIntent().getExtras().getString("Verification");
        Log.e("phone number", phone);
        sendCodeBtn = (Button) findViewById(R.id.verification_btn);
        resendTxt = (TextView) findViewById(R.id.resend_code);
        codeEdtTxt = (EditText) findViewById(R.id.verifi_phone_edt_txt);
        codeEdtTxt.setTextLocale(Locale.ENGLISH);
        if (getResources().getConfiguration().getLayoutDirection() == LayoutDirection.RTL) {
            codeEdtTxt.setGravity(Gravity.RIGHT);
        }

        presenter = new VerifPresenter(this, this, new VerifModel(this));
        sendCodeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                presenter.onSendCodeClicked(convertDigits(phone), convertDigits(codeEdtTxt.getText().toString().trim()));
            }
        });


        resendTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.e("phone", phone);
                presenter.onResendClicked(convertDigits(phone));
            }
        });
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void showResend() {
        resendTxt.setVisibility(View.VISIBLE);
    }

    @Override
    public void goHome() {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.signup_dialog);
        if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
            dialog.getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
        }
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                Intent intent = new Intent(VerificationActivity.this, BranchesActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                if (MySingleton.getmInstance(VerificationActivity.this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                } else {
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                }
            }
        });
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.show();
        Thread th = new Thread() {
            public void run() {
                try {
                    sleep(1500);
                    dialog.dismiss();
                } catch (Exception e) {

                }
            }
        };
        th.start();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            if (MySingleton.getmInstance(this).getAppLang().equals(getString(R.string.settings_language_arabic_value))) {
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            } else {
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private String convertDigits(String string) {
        String rslt = "";
        for (int i = 0; i < string.length(); i++) {
            switch (string.charAt(i)) {
                case '٠':
                    rslt += "0";
                    break;
                case '١':
                    rslt += "1";
                    break;
                case '٢':
                    rslt += "2";
                    break;
                case '٣':
                    rslt += "3";
                    break;
                case '٤':
                    rslt += "4";
                    break;
                case '٥':
                    rslt += "5";
                    break;
                case '٦':
                    rslt += "6";
                    break;
                case '٧':
                    rslt += "7";
                    break;
                case '٨':
                    rslt += "8";
                    break;
                case '٩':
                    rslt += "9";
                    break;
                default:
                    rslt += string.charAt(i);
            }
        }
        return rslt;
    }

}
